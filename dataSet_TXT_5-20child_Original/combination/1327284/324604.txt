Consistent foreground vs. background opening of tabs
It seems that there are many cases where tabs are opened in the foreground vs. the background differently. There's very flimsy rationale for this in a few cases but it often breaks down. There is no way to open a link in the foreground using the mouse - unless you flip a default pref. 

It seems to me that the "select new tabs opened from links" should be a global default toggle, with a modifier key to override in specific instances for deterministic selection. This would be documented in the Tabbed Browsing preferences panel. 

The default for most users would be to open tabs in the foreground, with a modifier key to override into the background for specific use cases. Switching the pref would invert this.