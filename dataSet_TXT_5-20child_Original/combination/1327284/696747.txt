'Search <Search Engine> for "<selection>"' should respond to middle-click
Do you close the book you're currently reading to look up something in a dictionary or encyclopedia? Me neither ;-)

After bug 695482, context menu 'Search <Search Engine> for "<selection>"' open search results in the current tab, which I find annoying.

Please make the option respond to middle-click so the search results are displayed in a background tab next to the current one and not in the foreground tab.