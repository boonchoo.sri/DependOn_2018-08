[meta] Inconsistencies in (shift)-middle-click handling
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160526082509
As it may seem, MiddleClick is default action to open the same link in a new background tab.
As it may seem, Shift is default shortcut to revert MiddleClick action. However,
it is hard to develop a muscle memory and get used to browser behavior, because it's inconsistent.
After 5 years of using the browser I still not sure whether I should hold Shift to get what I want.

I chose 7 most commonly used (by me) items supporting MiddleClick and described how they support
3 basic logical aspects in browser behavior. It appears, they don't (See the table below).


"Logical aspects":
1) Obeys "When I open a link in a new tab, switch to it immediately" in about:preferences#general
2) Shift+MiddleClick reverts "switch" action (changes original behavior to allow user to choose)
3) MiddleClick opens a new tab next to current

 Item                                    |  (1)    |  (2)    |  (3)    |
-----------------------------------------+---------+---------+---------|
 A link on a web page                    |   Yes   |   Yes   |   Yes   |
 "View image" menuitem                   |   No    |   Yes   |   Yes   |
 "Reload" button in urlbar               |   No    |   Yes   |   Yes   |
 Autocomplete suggestion in urlbar       |   No    |   Yes   |   No    |
 A bookmark                              |   No    |   Yes   |   No    |
 "Home page" toolbarbutton               |   No    |   No    |   No    |
 "New tab" toolbarbutton                 |   No    |   No    |   Nooo  |


Expected result:  Consistency. What means at least one of (X), (Y), (Z):
 X) MiddleClick on ALL said items should open either always background tab, or always foreground tab

 Y) Shift+MiddleClick on ALL said items should always "revert" MiddleClick action, i.e. if MiddleClick
    opens selected tab, Shift+MiddleClick should open not selected tab, and vice versa.

 Z) MiddleClick on ALL said items should either always open new tab next to current,
    or always open new tab in the end of list.