LineGraphWidget should be able to display negative values
Created attachment 8544943
Screen Shot 2015-01-06 at 6.43.53 PM.png

While it can show negative values for the min/max, the graph can not render any lines below 0.

In the image, the first point is 0.2, which makes sense if the bottom of the graph is 0 -- there should also be -1 values later (confirmed that the data is correct).