Pressing Enter to select tag from autocomplete closes bookmarks properties dialog
Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1b2pre) Gecko/20081101 Minefield/3.1b2pre ID:20081101020236

If you open the bookmarks properties dialog for a bookmark and try to add some tags, you have to hit enter to select the tag from the autocomplete list. Doing so will close the whole dialog immediately. That shouldn't happen. Instead the tag has to be completed.

Steps to reproduce:
1. Add a bookmark by clicking the star and enter a tag e.g. "bug"
2. Open the bookmarks sidebar and select properties from the newly added bookmark
3. Go to the tags field and remove the tag "bug"
4. Press "b" to show up the autocomplete list - "bug" will be selected
5. Hit Enter

After step 5 the bookmarks properties dialog should not close as what it does actually.