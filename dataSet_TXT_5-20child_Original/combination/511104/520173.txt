Reset lightweight theme previews when leaving the source page
Steps to reproduce:

1. Go to http://getpersonas.com/
2. Hover over a persona to trigger the in-browser preview
3. Without mousing out, put focus into the address bar and type an off-site URL (mozilla.com, etc.)

Expected:
The personas preview reverts to the previously applied persona (or the default)

Actual results:
The preview persona sticks and becomes the current default.

I don't know if there's a way to remove the preview when the page unloads, but if so, we should probably add this just to avoid confusion.