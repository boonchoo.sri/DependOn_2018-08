Identity state of IDENTITY_MODE_UNKNOWN is set for about:home when a new window is created
about:home should have an identity state of IDENTITY_MODE_CHROMEUI, but instead it ends up with an identity state of IDENTITY_MODE_UNKNOWN when a new window is created.

Setting a breakpoint within onSecurityChange shows that the location is set as about:blank, which leads the location.protocol to become "http:".

Gavin, do you know how we can change gBrowser.contentWindow.location to return about:home when a new window is created instead of about:blank?