DevTools discovery purges all devices every scan
The discovery mechanism added in bug 975591 is a bit too eager to purge devices...  It never records that any have replied. :|