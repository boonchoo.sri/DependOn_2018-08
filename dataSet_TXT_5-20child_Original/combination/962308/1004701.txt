Add QR decoder library
For WiFi debugging, we intend to exchange keys via a QR code that is scanned with the phone.

To support this, we need to decode a QR code.

There appears to be one main JS decoder[1].  I'll wrap it with a simple API.  The decoder is under the Apache 2.0 license, which the licensing FAQ suggests doesn't require an explicit review.

[1]: https://github.com/LazarSoft/jsqrcode