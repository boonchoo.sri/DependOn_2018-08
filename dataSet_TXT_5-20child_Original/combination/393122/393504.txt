handler service should store file extensions when storing a MIME info object
The handler service doesn't store file extensions when asked to store a MIME info object.  That's bad if any code that twiddles those extensions uses the handler service to store its MIME info objects.

I'm not sure any code does this yet.  The only code I found that twiddles extensions is helperApp.js, and it writes to the datasource directly.  But undoubtedly some code will do so eventually, and it would be good for it to work when that happens.