Shield - Fixing Known Addressable Issues
Created attachment 8700118
Shield User Stories - Known Addressable Issues

Firefox breaks in (some) predictable ways with known recovery steps. Firefox should be able to identify when a user is likely experiencing one of these problems. Then, as an advocate for our users, we should ask them if they would like us to fix the problem for them automatically.

Attached are the user stories for the Self Repair portion of the Shield project.