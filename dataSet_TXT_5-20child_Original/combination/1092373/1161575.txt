Heartbeats fade out animation should be improved
* I got sampled on Nightly for heartbeat.
* Submitted a rating
* The "fade out" animation started, making the heartbeat bar slide under the navbar
* The content seemed to follow along?
* Once the heartbeat bar was completely gone, the content suddenly jumped/resized to fill out the empty area at the bottom

While this doesn't seem critical, it's not exactly an impressive/smooth UI.