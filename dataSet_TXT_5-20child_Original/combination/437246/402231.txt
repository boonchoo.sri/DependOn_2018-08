places organizer should allow browsing and searching of downloads
Copied over the relevant requirement bits from email:

The goal would be to be able to, from the Places organizer in a "Downloads" view:

 - search and list by name of downloaded file
 - try to launch the downloaded file based on the location to which
it was saved on disk
 - re-start the download if the file can't be found

> What would the default visible columns be? Shortname and local path?

Also probably "downloaded from" and show the referrer, which IIRC we
now already store.

Also:

1. The (i) info button now opens the Places Manager with focus on the
selected download
2. The Search button, as always, goes off to search the Places Manager

The design expects that the Places Manager knows:

 - name of the file
 - source URL for download
 - target location for download
 - date/time of the download
 - size of the download