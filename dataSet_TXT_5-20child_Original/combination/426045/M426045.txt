[meta] create a comprehensive test suite for the SessionStore API
SessionStore is somewhat behind other (sub)components in how comprehensive the test suite is (see URL). In order to ensure the integrity of our code for Firefox.next, it'd be nice to have

* multiple tests per method in our public API [1], including...
* edge-case tests (passing in e.g. non-ASCII strings, empty arguments, restoring dozens of tabs, etc.)
* asynchronous tests - since several API calls can't be considered successful unless a page has been completely loaded (and e.g. text data restored)
* failure tests (passing in invalid arguments should fail the call)
* regression tests for the bugs from [2] (where reasonably feasible)

[1] http://developer.mozilla.org/en/docs/nsISessionStore
[2] https://bugzilla.mozilla.org/buglist.cgi?quicksearch=FIXED+:"session+restore"

Please file new bugs for the tests you're going to add and make them blocking this tracking bug.