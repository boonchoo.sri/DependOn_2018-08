Page load document inspector momentary flash/display
When loading a page with the Developer Tools open, regardless of tab, the inspector highlighter that shows the dashed lines and padding of a node in the document, will momentarily flash on the page then disappear. Typically, it's the body node, as loading a new page means nothing was really selected.

It's reproducible on OSX Mavericks and Win7. I haven't tested others.