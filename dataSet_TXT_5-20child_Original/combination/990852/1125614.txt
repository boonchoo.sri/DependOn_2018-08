Firefox PDF rendering is broken on some PDFs
Created attachment 8554262
Firefox vs Chromium PDF rendering.png

User Agent: Mozilla/5.0 (X11; Linux x86_64; rv:37.0) Gecko/20100101 Firefox/37.0
Build ID: 20150112123848

Steps to reproduce:

Open http://onoratainstanta.ro/pdf/Policy_Paper-ROLUL_CSM_ICCJ_%C8%98I_MP_fata_de_politicile_publice_din_justitie.pdf in Firefox 35 safe mode addons disabled


Actual results:

The PDF looked really bad, it should have latin characters but it did not. It might be an encoding issue


Expected results:

The PDF file should have had latin characters.