"Reddit Enhancement Suite" doesn't work on Nightly 22.0a1
User Agent: Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20130315 Firefox/22.0
Build ID: 20130315030943

Steps to reproduce:

Installed https://addons.mozilla.org/en-US/firefox/addon/reddit-enhancement-suite/


Actual results:

It doesn't work anymore since the last couple Nightly updates.


Expected results:

There should be an additional option on Reddit top right options to control RES settings, along with many page changes done by the extesion. It doesn't work at all.