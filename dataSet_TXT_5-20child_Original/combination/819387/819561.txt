pages opened by AdBlock Plus during installation should open in the same PB window
Using latest birch nightly, when you install an add-on like AdBlock Plus (restartless) from within a PB window it will launch a page with information but it will launch the page in the public window. It should open it in the same window from which it was installed.

Steps:
1. Open a private browsing window using command-shift-p
2. Go to addons.mozilla.org
3. Look for adblock plus and install it

Expected: Whatever pages are launched from installing an add-on are launched within the PB window.

Actual: At the end of the installation this page opens in the non-private window: chrome://adblockplus/content/ui/firstRun.xhtml