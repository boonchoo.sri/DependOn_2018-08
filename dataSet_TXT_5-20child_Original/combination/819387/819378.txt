Entries kept in cache when using Download Helper add-on with youtube
Mozilla/5.0 (X11; Linux i686; rv:20.0) Gecko/20121207 Firefox/20.0

1. Install Video Download Helper: https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/?src=hp-dl-mostpopular
2. Clear cache.
3. Start Private Browsing
4. Visit youtube and download a video using download helper
5. Exit private Browsing

Actual: private browsing data preserved (2 entries - one for the downloaded item)
Expected: no data preserved