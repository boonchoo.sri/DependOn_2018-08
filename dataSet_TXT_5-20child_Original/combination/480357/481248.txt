domain indicator flashes briefly on HTTPS pages that load HTTP content
Use the back/forward buttons to navigate to the page in the URL, and you'll see the domain indicator flash. Janky.