idl is hiding the fact QUERY_TYPE_BOOKMARKS does not support grouping atm
Created attachment 315193
Test case demonstrating the problem

The only RESULTS_AS setting that works for Bookmark queries is RESULTS_AS_URI.  For everything else, no results are returned in the result set.  

I'll attach a test that demonstrates this behavior. 

= Expected Behavior =
The results as queries should work for bookmarks too, the RESULTS_AS_DATE* should work if the bookmarks have a visit associated with them, the RESULTS_AS_SITE should always work since bookmarks have URIs associated with them.

= Actual = 
Only RESULTS_AS_URI returns any results for a bookmark query.