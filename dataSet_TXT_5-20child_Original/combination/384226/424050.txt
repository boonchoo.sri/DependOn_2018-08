Results do not listen to annotation notifications, causing annotation depending queries to not liveupdate
Created attachment 310709
Testcase

If you are querying using query.annotation = x, then changing a query set member's annotation to a new name should remove that member from the query set.

1. Add item uri http://foo to the database with annotation Name x and annotation value y
2. Create a query using query.annotation = x
3. Change uri http://foo to use annotation name "text/somethingelse" instead of x.
4. Check the query result set.

= Actual =
Uri http://foo is still in the query set

= Expected =
Uri http://foo is no longer in the query set

Attaching a XPCShell test for this.