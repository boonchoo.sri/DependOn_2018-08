ExcludeItems is not ignored for history queries
Created attachment 305931
Testcase

As the attached test case demonstrates, the excludeItems query option is not being ignored for history queries. It is claimed in the interface specification that this should be ignored for non-bookmark queries:
http://mxr.mozilla.org/mozilla/source/toolkit/components/places/public/nsINavHistoryService.idl#992

If you comment out the excludeItems line, (line 75), the test will pass as expected.

Tested on current pull of trunk codebase: 02-26-08.