Remove the lock icon from the status bar
Created attachment 438260
Weave is not secure

During the site identity work of Firefox 3, we decided to move away from using the lock icon as a symbol for SSL because users were interpreting it as meaning "this site is run by good people" as opposed to its actual meaning.

However (for reasons that I honestly can't entirely remember) the lock icon was left in the status bar on the far right side.  This might have been some form of compromise for lock icon defenders, or maybe we just actually forgot to remove it.

Either way, the current presence of the lock icon isn't consistent with how we would like to frame site identity and the concept of security.

As a side issue, the lock icon is also now being conflated with the messaging from Weave since it visually groups to the set of Weave UI currently being placed in the status bar.

The removal of the lock icon also fits into our overall plans of re-framing the status bar into the "extension bar" which is going to be utilized a lot by jetpack (http://mozillalabs.com/jetpack/2010/04/08/the-single-ui-element/ ) but will also of course be great real estate for standard Firefox extensions.