Opening link in new tab with Java applet applies that tab's URL, security icon, etc to all tabs
If a link opened in a new tab contains a Java applet, all others tabs get that
tab's URL, security icon, etc.

1. Force links that open in new windows to open in new tab. Don't set "Select
tabs opened from links".
2. Open testcase.
3. Click
4. Go back to first tab

The secure site's URL and security icon appear in the first tab. This appears in
the Javascript Console:

Error: focusedWindow has no properties
Source File: chrome://global/content/bindings/tabbrowser.xml
Line: 551