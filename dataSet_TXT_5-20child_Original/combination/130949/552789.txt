Mixed-content warning fires inappropriately when following links from some http pages to https pages
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6

When going directly to a secured page, the SSL cert is properly engaged and page loads without any warnings.

However, when going to this same secure page VIA a hyperlink from a different page, a warning is thrown: "You have requested an encrypted page that contains some unencrypted information. Information that you see or enter on this page could easily be read by a third party."

Some interesting facts:
1. This is only happening in Firefox .. not IE, Opera or Safari.
2. After receiving the aforementioned warning, merely clicking REFRESH resolves the issue and the page loads with SSL fully engaged.
3. Going directly to the page in question with HTTPS specified causes page to properly load with SSL fully engaged.



Reproducible: Always

Steps to Reproduce:
TO SEE WARNING:
1. Visit http://www.foapom.com/site/events.asp
2. Click ORDER NOW for the Pageant event.
3. Arrive at HTTPS://www.foapom.com/order/tickets1.asp?ccd=pom
4. Note that (1) mixed encryption warning received unless you have it suppressed and (2) SSL is not fully engaged.
5. Click REFRESH - warning goes away and page/SSL loads properly

TO SEE NO WARNING / PAGE LOAD DIRECTLY:
1. Open a fresh browser window
2. Visit HTTPS://www.foapom.com/order/tickets1.asp?ccd=pom
3. Note that page/SSL loads without any issues.




I have inspected the underlying code, using conventional means as well as Firebug with Codeburner add-on. All images, JavaScript variables, and similar resources are referring/defaulting to HTTPS protocol, when visiting the tickets1.asp page mentioned in above steps.