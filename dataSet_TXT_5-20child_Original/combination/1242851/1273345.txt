Styling issues connected to focus in devtools (introduced by bug 1242851)
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160511030221
All of A-E require some decision; I decided to file 1 bug instead of five.

A) filter in DOM inspector has dotted outline instead of blue highlight:
    STR: open DOM inspector -> press Tab -> click in filter field
B) red/yellow border of filter fields in inspector conflict with blue focus highlight
    STR: open inspector -> press Tab -> click in ruleview filter -> type "a" -> type "asdf"
C) double focusring on sidebar tabs, "show more nodes" buttons and probably more
    STR: open data:text/html,<body onload='S="";for(i=0;i++<999;)S+="<br>";document.body.innerHTML=S'>
         -> open inspector -> focus "show all 999 nodes" button using Tab key -> click on "rules" tab 
D) light theme: black focusring on selected node (which is SHINY BLUE!1!!)
    STR: set light devtools theme -> open inspector -> click "<body" -> press Tab -> press Shift+Tab
E)(bug 1272999!)  console: multiline text is clipped, unnecessary scrollbar, largely increased height

+ some more issues with focusring which were missed in bug 1242851, so I haven't mention them