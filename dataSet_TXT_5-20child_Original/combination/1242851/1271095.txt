Multiline code pasted in console is partially clipped
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160504043118
STR:
1. Open url   data:,for(i=0;i++<1){%0Aconsole.log("Hello, world!");%0A}
2. Copy all text on the page opened in Step 1
3. Open console and paste all text in command line

AR:  Text is partially invisible
ER:  All text should be visible

This is regression from bug 1242851. Regression range:
> https://hg.mozilla.org/integration/mozilla-inbound/pushloghtml?fromchange=5a937f17f3369f64554d5552ecfeb22ef4757fcb&tochange=e60f6cb27fdd2f16d9c4aa9f1a79274af4f71184