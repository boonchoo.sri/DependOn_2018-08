De-dupe stale mobile clients
Following up on bug 1250531 for mobile. From IRC:

> 10:24 <kitcambridge> nalexander: ack. when a user disconnects sync on Fennec, does it delete the client from the clients collection?
> 10:24 <•nalexander> kitcambridge: I think we do not, or at least we don't try hard.
> 10:24 <•nalexander> kitcambridge: the reason is that we don't want to try to get a token-server token, etc, after the user has disconnected.
> 10:25 <•nalexander> kitcambridge: yeah, I don't think we implemented that!  https://dxr.mozilla.org/mozilla-central/source/mobile/android/> services/src/main/java/org/mozilla/gecko/fxa/receivers/FxAccountDeletedService.java
> 10:27 <•nalexander> kitcambridge: it's hard, for token caching/fetching reasons.

So stale and duplicate mobile clients are likely to show up in the Synced Tabs list.

FWIW, Fennec hides devices with the same name that haven't synced in a week (bug 1180287). I don't think we need to worry about the 3-week TTL, because Desktop now fetches the full clients collection for every sync.

But we could implement the same de-duping logic. What do you think, Mark?