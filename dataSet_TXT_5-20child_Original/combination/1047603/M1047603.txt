[e10s] Non-remote tabs and chrome in e10s windows do not handle target="_blank" or window.open links properly.
Steps to reproduce:
1. Bookmark http://blogs.yahoo.co.jp/alice0775/31315844.html
   And make sure it should open in Sidebar(i.e Check "Load this bookmark in the sidebar")
2. Open the bookmark
3. Click a link "https://github.com/alice0775" in Sidebar panel

Actual Results:
New tab open, but no content.

Expected Results:
New tab open, and content should display.