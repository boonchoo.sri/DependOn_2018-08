Side by side add-on installation pop-ups
[Affected versions]:
Firefox 53.0a1 (2016-11-22)


[Affected platforms]:
Windows 10 64-bit
Ubuntu 16.04 32-bit


[Steps to reproduce]:
1.Launch Firefox with clean profile.
2.Navigate to https://addons.mozilla.org
3.Choose an add-on and click on “+ Add to Firefox” green button. 
4.Complete the installation process by clicking on “Install” button from doorhanger.
5.While the confirmation doorhanger is still displayed, choose another add-on and click on “+ Add to Firefox” green button.


[Expected Results]:
The second add-on doorhanger appears under the first one. 


[Actual Results]:
- The doorhangers are displayed one next each other.
- UI issues: http://screencast.com/t/qTqmPh6zZyOK