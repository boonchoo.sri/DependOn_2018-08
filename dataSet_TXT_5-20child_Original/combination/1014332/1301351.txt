Remove bogus "toolbarbutton" class from manage-share-providers and add-share-provider buttons
AFAIK there's no CSS or JS code doing anything with a "toolbarbutton" class. We should remove it.

The manage-share-providers and add-share-provider buttons can be found in browser/base/content/browser.xul