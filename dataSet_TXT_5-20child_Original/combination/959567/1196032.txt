Search suggestions opt-in in the urlbar should not appear in private windows
Search suggestions in the urlbar are disabled in private windows, so the opt-in notification should not be shown there.