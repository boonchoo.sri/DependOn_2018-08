Feed existence not exposed by default UI
User-Agent:       Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b7) Gecko/20100101 Firefox/4.0b7
Build Identifier: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b7) Gecko/20100101 Firefox/4.0b7

Now that Firefox 4 has eliminated the location bar icon for RSS and Atom feeds, the default UI does not inform the user that a page includes feed metadata at all.  To find out whether a page has feed metadata one has to either open the Bookmarks menu on every page to check whether or not the Subscribe entry is greyed out or add a new Subscribe button to the toolbar.  Is that a reasonable default?

Reproducible: Always