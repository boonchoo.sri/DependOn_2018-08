Show all registered service workers in about:debugging
Currently, we list *live* service workers in about:debugging. The problem is that they are transient (and tend to die frequently).

Instead, we should list service worker *registrations*, which are more permanent.

However, there can be either 0, 1 or 2 live service worker(s) associated to one registration at any given time:

0) Clicking on "Debug" next to the registration should fire up a new service worker, then open a toolbox on it.

1) Clicking on "Debug" should open a toolbox on the one live service worker.

2) A service worker is finishing up, and a new one is installing. Clicking on "Debug" should probably wait for the new service worker to be live, then open a toolbox on it.