Add a mode to highlight overdraws
In the canvas debugger, it would be awesome to have a mode where the preview becomes black and each draw just adds a white shape with low opacity (like .2) so that as multiple draws stack on top of each other, that area gets brighter and brighter. This helps developers identify points where they are doing excessive overdraws and helps them potentially speed up their code.

*Not* a good first bug, just a mentored bug for the very brave.