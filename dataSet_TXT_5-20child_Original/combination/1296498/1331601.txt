View source open an blank page when using responsive design view
Steps to reproduce:
1) Ctrl+Shift+M to open responsive design view
2) Ctrl+Shift+M to exit responsive design view
3) Ctrl+U to open page source

Actual results
A blank page (completely white) opened in a new tab

Expected results
The page source should have opened in a new tab

Versions tested
53.0a1 (2017-01-16) (64-bit) [issue present]
51.0b14 (64-bit) [issue NOT present]
50.1.0 [issue NOT present]