Change text style in filter field to look like in other fields
The font within the filter field of the Rules side panel and the Computed side panel is monospaced, while in other filter fields the system font is used and it is displayed in italics.

To have a unified layout the font in those two fields should be adjusted accordingly.

Sebastian