Create a Tabs from Other Devices sidebar
This would replace the functionality of the Tabs from Other Devices in-content page (Bug 988317).

UX: https://mozilla.invisionapp.com/share/NA47717JG#/screens/101713710