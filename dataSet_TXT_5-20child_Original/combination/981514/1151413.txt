executeSoon should set async caller
|makeInfallible| should use |callFunctionWithAsyncStack| to connect the function it runs with the outer environment.