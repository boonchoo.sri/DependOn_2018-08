[a11y] "Getting started with Nightly" dialog's close button must be accessible.
Right now it's just a span.

It must do the following:

* be implemented as BUTTON tag
* be present in the tab order (Fred and I talked about it being the last one within the dialog)
* have a clear focused state (when it has a keyboard focus)
* have an internationalized label (e.g. alt text for the image)