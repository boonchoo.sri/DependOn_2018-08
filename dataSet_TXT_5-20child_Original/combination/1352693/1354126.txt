Restyle customize mode footer
See https://mozilla.invisionapp.com/share/5ZAEYEW8M#/screens/225203398 .

We should restyle the footer to match the spec. This includes:

- extending it underneath the overflow panel (hamburger panel will be removed)
- changing the background/border colours of the footer
- updating the titlebar switch imagery / code
- adding a 'done' button' next to 'restore defaults' that closes customize mode
- ensuring the design works well on narrow window sizes (I'll file a UX bug for this).