Need persist-storage permission notification icon
Created attachment 8805051
permission_notification_icons_example.png

Need one small icon to place on the URL bar and one large icon to place on the request doorhanger for the persist-storage permission. See permission_notification_icons_example.png for other permission notifications as example.