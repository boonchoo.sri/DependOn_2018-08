bug #387996 caused Txul and Ts regressions
from irc:

<sayrer> sspitzer regressed Ts last night
<sayrer> sspitzer, yeah Txul and Ts on all platforms :/

<mfinkle> sspitzerMsgMe: not even a little bit :)
<mfinkle> 956ms -> 972ms
<mfinkle> on linux
<mfinkle> Ts

<sspitzerMsgMe> part of my patch does some work in browser.js one time (creating the "Places" folder), on delayed startup.  but if we start with a new profile, or a profile-from-before-my-fix, we'd pay that price every time.
<sspitzerMsgMe> sayrer / mfinkle:  let me take this to a bug, I'll cc you guys.

my plan would be to make the default profile have the places folder and the queries (so the hidden browser.places.createdDefaultQueries pref would be set to true) and then see the Ts / Txul numbers.

otherwise, we're measuring code that that the user runs once.