[Mortar] Remove de-scoped user features on PDF viewer UI.
The following UI components are not going to be implemented by now. We need to remove them:
    - Hand tool
    - Document info
    - Attachment
    - Thumbnail