Show a tooltip in Bookmarks and History sidebars.
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8

I don't think anyone has filed this enhancement before (and I'm not sure if
Seamonkey lacks similar support), but I searched through the existing entries
for the History sidebar and I'm surprised that nobody has suggested a similar
enhancement. 

I use the History sidebar in Firefox fairly frequently, and while it shows the
title of web pages in the sidebar I think it would be really useful if the URL
of a particular entry was shown as a tool tip text when the mouse is paused over
the entry. This could easily apply to the Bookmarks sidebar as well.

Reproducible: Always
Steps to Reproduce:
1. Open Bookmarks or History sidebar.
2. Pause mouse over entry in sidebar.
3. Notice that there's no URL displayed as a tool tip text.

Actual Results:  
No URL is shown.

Expected Results:  
The URL entry is shown in the sidebar.