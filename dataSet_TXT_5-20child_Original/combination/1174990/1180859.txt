[Control Center] Remove special icon for EV certs and use a single DV/EV icon instead
DV currently gets a grey lock, and EV a green lock with a block showing the organization name. The control center shows a green lock for DV, and a green lock with a check mark for EV.

Aislinn, Tanvi, and I think we should get rid of the DV/EV distinction with regard to the icons in the URL bar and the control center.

The control center as well as the URL bar should simply display a green lock for secure connections. The organization in the URL bar and the control center puts enough emphasis on the identity part of EV, doing anything more would signal that the connection is more secure than with a DV cert.