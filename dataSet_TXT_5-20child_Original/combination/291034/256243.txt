Unabled to remove downloads / navigate downloads dialog mouseless
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040803 Firefox/0.8
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040803 Firefox/0.8

Downloads
While you can navigate the items, you can't remove an item - you can open it
with "enter" or space though. Open/Remove links should have access keys for when
an item is selected (o/r); change "Options" access key to Alt+p perhaps?

Reproducible: Always
Steps to Reproduce:
1. Open downloads dialog
2. Try to remove an item with keyboard only
3.

Actual Results:  
Unable to focus and remove item

Expected Results:  
Gain focus on "remove" link