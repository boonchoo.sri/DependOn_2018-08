Pinstripe should follow "Full keyboard access" system preferences
User-Agent:       Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8a) Gecko/20040506 Firefox/0.8.0+
Build Identifier: Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8a) Gecko/20040506 Firefox/0.8.0+

Keyboard navigation (tabbing) doesn't work for buttons and checkboxes and pop-up
menus with the default theme (Pinstripe) dialogs.  For example, dialogs like
allow cookie, save bookmark, preferences, etc.  I have "Full keyboard access"
enabled in my System Preferences, so this should work.

Reproducible: Always
Steps to Reproduce:
1.  Try to nagigate dialogs with keyboard.

Actual Results:  
nothing

Expected Results:  
keyboard navigation should work