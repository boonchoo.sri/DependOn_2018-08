Variables View: [object undefined] for Window when inspecting raw objects
If you try to inspect the Window object with the Variables View, using the rawObject approach, you get [object undefined] for the window property, for |self| and for other objects.

It seems vview does not correctly determine the class for some objects.

After a bit of investigation I saw the problem is in getGrip(). You might want to use WebConsoleUtils.getObjectClassName() to determine the class name - this is something reusable from the Web Console property panel. If you use that for the |class| grip property, things should be fine.