Add en-* translations to Pocket
For the next Pocket release Marketing would like to have the remaining en-* locales shipped and turned on such that we can be globally working in english language variants.  This helps with english press around the globe who run similar stories but currently won't have access to the feature.

I don't believe this depends on bug 1161138 but if that were to land we might get the additional locales as part of the process.