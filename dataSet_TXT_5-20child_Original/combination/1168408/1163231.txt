Test failures on Aurora due to Pocket button causing toolbar overflow
Ran a Try build before uplifting bug 1161881, to enable pocket, and found bc1 failures on Windows 7 (although, interestingly, not Windows 8):

https://treeherder.mozilla.org/#/jobs?repo=try&revision=f646c1e8bb6f

Investigation reveals this is a result of the toolbar being in overflow. Aurora is the devtools branch, and has a number of extra buttons on the navtoolbar. One of the tests is explicitly checking for "toolbar not in overflow" at the beginning (and fails), and some of the other tests are broken because test widgets end up in an unexpected place (the overflow panel).

We could fix this by making the window a little wider, but I don't know if that's going to cause other problems. Some of the tests can be fixed by having them add their widgets farther to the left in the toolbar, so that they don't end up in overflow.

Failing tests:

  browser/components/customizableui/test/...
    browser_943683_migration_test.js
    browser_947987_removable_default.js
    browser_962069_drag_to_overflow_chevron.js
    browser_973641_button_addon.js
    browser_976792_insertNodeInWindow.js
    browser_984455_bookmarks_items_reparenting.js
    browser_985815_propagate_setToolbarVisibility.js
    browser_987177_xul_wrapper_updating.js
    browser_988072_sidebar_events.js