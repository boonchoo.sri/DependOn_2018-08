The user is able to save Pocket's "My List" to itself, twice
Created attachment 8601451
screenshot from Ubuntu 14.04 (x64)

Reproducible on:
Nightly 40.0a1 (2015-05-04)

Affected platforms:
Windows 7 (x64), Ubuntu 14.04 (x64), Mac OS X 10.9.5

Steps to reproduce:
1. Launch Firefox.
2. Make sure you're logged into Pocket.
3. Open Pocket's "My List".
4. Click the Pocket button from the toolbar and right-click the "Save" button several times.
5. Dismiss the panel and check "My List".

Expected result:
* Pocket's "My List" cannot be saved to Pocket.
* Right-clicking UI elements from the panel has no effect.
* The user cannot add the same page (with the same URL) twice to Pocket.

Actual result:
* After a few right-clicks, "My List" is displayed twice in Pocket. 
* The attempt of removing both of the entries from Pocket causes queue.js:4135:3 to throw the following error in the Browser Console: 
> TypeError: item is undefined