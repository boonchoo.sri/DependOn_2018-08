Allow user to disable/enable display of Search Suggestions in Awesome Bar
Created attachment 8459800
Mockup showing show/hide Search Suggestions in Awesome Bar

A user should be able to turn on and off Search Suggestions in their Awesome Bar. By default, Search Suggestions should be off.

A user can toggle search suggestions in the Awesome Bar by clicking a "Show Search Suggestions" link.