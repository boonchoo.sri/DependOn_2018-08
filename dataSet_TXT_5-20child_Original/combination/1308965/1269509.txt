Shortcut to open DOM Inspector (Ctrl+Shift+W) conflicts with the one for closing Firefox
The DOM Inspector currently uses Ctrl+Shift+W as shortcut, which is already used to close the application.

Instead Ctrl+Shift+D could be used, which doesn't seem to have another action assigned at the moment.

Sebastian