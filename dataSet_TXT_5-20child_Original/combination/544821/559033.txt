[Mac] New toolbar button style
Created attachment 438740
v1

Some notes:
 - This removes the backforward dropmarker because it isn't present in the
   mockups. Stephen, was its ommission intentional?
 - Extensions' icons are stretched to 20x20px, because that's the size of
   the Toolbar.png icons. I'm waiting for the outcome of bug 547419 before
   changing that.
 - This needs some platform-color-ization. I'd like to do that in a follow-up
   bug. Most importantly, the white shadow under the buttons has a different
   opacity on Leopard vs. Snow Leopard, so we'll definitely need a platform
   color for that.
 - The toolbarbutton[type="menu-button"] setup is really really nasty. I
   haven't found a better solution for them that works in icon/text mode with
   the label under the button.