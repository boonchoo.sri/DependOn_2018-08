Vertical lines between non-tab tab bar elements look out of place in the tabs on top mode
Created attachment 459370
Screenshot of the lines that don't look quite right

Steps to reproduce:
 1) Use a Mac
 2) Update to nightly from around 2010-07-21
 3) Make sure "tabs on top" is enabled.

Actual results:
In the right, the three non-tab UI elements are bounded by vertical lines that end sharply at the title bar. The lines don't look right for unified windows, since unified windows generally don't have elements that end sharply at the title bar.

Expected results:
Expected these UI elements not to have the lines around them or, alternatively, expected the lines to fade gradually upwards to make the transition to the title bar look less sharp.