Bookmarks look very big and don't always match the style of toolbar
Created attachment 8651572
Bookmarks look Very big if placed in Navigation toolbar, Menu toolbar or Tabs toolbar.png

STR:
1. Enter Customize
2. Move Bookmarks Toolbar Items to Navigation toolbar
3. Exit Customize

Result:       Bookmarks Toolbar Items (visually) take full height of toolbar and look just like
              they are still placed in Bookmarks Toolbar. And very big.
Expectations: Currently, every toolbar has it's own style for buttons, and other buttons follow it.
              So Bookmarks Toolbar Items should follow it too.