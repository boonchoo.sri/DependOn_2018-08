Lazily load ContentWebRTC.jsm
ContentWebRTC.jsm is imported for two reasons:

1. Add a bunch of observers, once per process.
2. Add a bunch of message listeners.

We shouldn't have to pay the price of a .jsm unless we're actually using WebRTC.

We can avoid the import for 1 by moving the addObserver calls into ContentObservers.jsm, and thunkifying them. Maybe avoiding the compartment for ContentObservers.jsm would be nice, but it is an easy way to have code only run once per process.

The import for 2 can be done just by thunkifying the message listeners. It is ugly but it works.