Lazily load NetUtil.jsm in PdfStreamConverter.jsm
This moves us one step closer to not loading NetUtil.jsm in the content process at startup in a clean profile.