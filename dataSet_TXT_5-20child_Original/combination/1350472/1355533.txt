Try to avoid loading AboutPocket.jsm in the content process at startup
I'm not entirely sure what this means, but Gijs seemed to think it would be possible to stop loading this .jsm in the content process, which can save memory.

(In reply to :Gijs from comment #8)
> I'm confused, actually, it doesn't look like the panel in which we load the
> pocket iframe is remote, so that would allow removing this (ie all it does
> is register about:pocket-signup and about:pocket-saved, which are never used
> in the content process AIUI...) - but it probably *should* ideally be
> remote, except that would probably require its own set of work.
> 
> Regardless, the only reason this seems to need a jsm right now is to
> facilitate loading its own content pages. It could (and should) just use
> other URLs for that, they don't need to be about: URLs at all, I think. Per
> bug 1236755 they shouldn't be chrome URLs, but resource: URLs should be
> unprivileged enough for what we want here, maybe/hopefully?