Consider removing Preferences.jsm
+++ This bug was initially created as a clone of Bug #1357517 +++

With bug 1338306 and bug 1345294 fixed, most of the reasons for using Preferences.jsm are now gone, and using this module adds some overhead, so we should consider removing it.

We are removing it from the startup path with bug 1357517 and bug 1385952.