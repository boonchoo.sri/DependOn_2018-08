Private Browsing mode (global toggle for saving/caching everything)
User-Agent:       Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7) Gecko/20040615 Firefox/0.9
Build Identifier: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7) Gecko/20040615 Firefox/0.9

In the next version of OS X, Safari will have a "Private Browsing" mode which,
basically, disables writing to cache, URL history, etc.
I think this is a good idea, and it's small and useful enough that it should go
into the browser, not an extension.

Reproducible: Always
Steps to Reproduce: