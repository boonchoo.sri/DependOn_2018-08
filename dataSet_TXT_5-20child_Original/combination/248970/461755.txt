Error console should be cleared when leaving the private browsing mode
Based on email conversation with mconnor, we need to clear the error console when leaving the private browsing mode.

Patch forthcoming.