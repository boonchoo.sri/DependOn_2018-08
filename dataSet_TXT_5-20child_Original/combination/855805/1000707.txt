The separator between in the combined bookmark button doesn't invert on dark themes
Created attachment 8411564
139_tabsOutsideTitlebar_twoPinnedWithOverflow_fullScreen_onlyNavBar_darkLWT_notCustomizing.png

It's a dark separator which is hard to see on a dark theme.