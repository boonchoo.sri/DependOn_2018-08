UX - New bookmark drop-down panel should use platform-consistent styling when in the toolbar
Created attachment 783931
Clash of panels

When the bookmark button is in the toolbar and one presses the drop-down indicator button to view bookmarks, the dropdown panel is now a customized grayish color. However when using that to enter child views, we switch to the native UI, which causes a non-appealing clash. (See screenshot)

While the bookmark button is in the toolbar, we should style its drop-down panel to match OS X as closely as possible.