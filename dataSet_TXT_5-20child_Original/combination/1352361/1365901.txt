[Photon] Replace 1px toolbar button border with padding
Currently toolbar button size matches spec (28x28) but there's a 1px border that should be replaced with padding to match the spec.