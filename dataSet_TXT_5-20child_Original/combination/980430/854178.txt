The debugger should do something smart when an app is updated in the simulator
This seemed to happen a lot yesterday at a local Firefox OS app hacking event: suppose you're working locally on an app and you're in the midst of debugging. You realize what's wrong and go to your favorite editor to change something in the source. Then you hit update in the Simulator dashboard, but the debugger doesn't update. In fact, it doesn't do anything and you're stuck with the old sources. Even worse, if you were paused on a breakpoint when hitting update, the simulator either appears to be unresponsive and you have no idea why, or it even actually freezes to death in some cases with no option other than force-closing it.

All of this results in pretty annoying "make sure debugger is not paused, close toolbox, update app in the dashboard (hope it works), reconnect, switch to debugger, find your source again, add all of your breakpoints again" cycles.

Surely we can be smarter about this.