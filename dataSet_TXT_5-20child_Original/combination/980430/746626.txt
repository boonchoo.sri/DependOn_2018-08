Make bound functions inspectable
I'm told there's currently no way to determine the function that a bound function will actually call, and this information isn't exposed in the console and whatever other places one would want to expose it.  That is, given this:

  var f = (function g() { "use strict"; m.call(arguments); }).bind(null, 1, 2, 3);
  f();

there's no way to know that the second line will invoke |g|, with |this === null|, or that it'll invoke it with the leading arguments |1, 2, 3|.  We should be able to expose this; it apparently can come in handy when debugging, sometimes (see the URL, although the use case is admittedly esoteric).

This is the UI half of the problem; bug 746622 is the JS engine half of the problem.