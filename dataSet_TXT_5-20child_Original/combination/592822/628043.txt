The last closed window is restored when a secondary window is left open and a new browser window is opened
User-Agent:       Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0b10pre) Gecko/20110122 Firefox/4.0b10pre
Build Identifier: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0b10pre) Gecko/20110122 Firefox/4.0b10pre

 

Reproducible: Always

Steps to Reproduce:
1. Open Library
2. Close other Firefox windows
3. Open some bookmark from Library
4. Close opened bookmark
5  Open it one more time from library or any other ones
Actual Results:  
Closed tabs are restored

Expected Results:  
Closed tabs shouldn't be restored, only opened ones