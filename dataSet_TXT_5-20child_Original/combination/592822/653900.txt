Firefox reopen parent page when open new window after parent closed
User-Agent:       Mozilla/5.0 (Windows NT 6.0; rv:2.0.1) Gecko/20100101 Firefox/4.0.1
Build Identifier: Mozilla/5.0 (Windows NT 6.0; rv:2.0.1) Gecko/20100101 Firefox/4.0.1

In version 4.0 and 4.0.1, the Firefox change the window control.
I have created a page1,htm, this page open page2.htm in new window by javascript function.

After open the page2.htm, if you close the page1.htm, all new links opened in page2.htm are redirect to page1.htm and not to correct page or website.

Reproducible: Always

Steps to Reproduce:
1.Access http://www.3wdesigner.com.br/firefox4/page1.htm
2.Click on "Click to open page 2 on new window"
3.Close the window for page1.htm
4.Return to window with page2.htm
5.Click on "open google in new window"

Actual Results:  
page1.htm opened again

Expected Results:  
Google opened in the new window

I have this same function working without problems in Firefox 3.6.x