shortcut ctrl-j doesn't close the download-window anymore (behaves differently recently)
User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0
Build ID: 20130409194949

Steps to reproduce:

open the downloads-window using the shortcut ctrl-j
try to close it again using the same shortcut ctrl-j

on windows 7, using Firefox:	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0


Actual results:

the downloads-window opens but doesn't close on the second ctrl-j press


Expected results:

it should close
(ctrl-j used to close the downloads-window in previous versions of firefox)