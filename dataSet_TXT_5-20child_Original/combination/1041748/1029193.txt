[UX] Define experience for tracking-blocking
The proposed experience encompasses

1. The ability to turn on a more active variant of Do Not Track that actually blocks tracking (through a pref)
2. Some indicator in the browser (analogous to our mixed content indication) that DNT is being enforced for the current site/page
3. The ability to turn tracking-blocking off for a site while on it if the blocking is causing a user to have difficulties with the site (should be unusual).