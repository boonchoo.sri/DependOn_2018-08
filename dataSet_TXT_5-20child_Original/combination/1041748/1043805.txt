Blocked content doorhanger should show a large red shield when both types of protection are disabled
The blocked content notification is becoming non-dismissible (bug 1043803). This means that we'll be showing the doorhanger when a type of protection is disabled on a page - and this should be made more obvious.

We can do that by adding a red "Protection is disabled" warning. And to make it more useful, add a "Report broken site" link to allow people to report when a page is broken when this protection is active.

Mockup: attachment 8459851