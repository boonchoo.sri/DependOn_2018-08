Responsive Design View uses incorrect scrollbar styling under Windows 10
Created attachment 8674186
scrollsbars_responsive_design_view_windows10.png

User Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0
Build ID: 20151014030223

Steps to reproduce:

* go on any website e.g. http://www.google.com
* activate Responsive Design View (ctrl+shift+m)



Actual results:

Scrollbars under Windows 10 are drawn in the standard Windows 10 style and thus covering parts of the website


Expected results:

Scrollbars should look like under Mac OS X, thinner and more transparent