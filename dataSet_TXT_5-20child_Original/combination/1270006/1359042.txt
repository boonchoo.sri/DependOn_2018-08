real download progress not in sync with fill of download button
Created attachment 8860950
screenshot download progress

Nightly 55.0a1 20170423 on Windows 8.1

See the screenshot. The download progress reported by the menu/list of the download panel says 110/462 MB (so less than a quarter), but the button is almost at half (both for height and area).