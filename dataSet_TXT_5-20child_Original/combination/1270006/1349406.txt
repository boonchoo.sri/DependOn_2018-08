Only each 2nd click on ">>" button in toolbar opens "more tools" panel if downloads button is placed inside
I noticed a problem in Nightly 55. It doesn't happen in Beta 52, Beta 53, ESR 45.
Sometimes when button ">>" in toolbar is clicked, it doesn't open "more tools" panel. More precisely, only each 2nd click works, if downloads button is placed inside "more tools" panel. It happens if downloads panel appeared even once.
Here's what I do

1. Open downloads panel. Close the panel
2. Resize window so that downloads button is inside "more tools" panel (appears when clicking >> button in toolbar)
3. Click >> button to open "more tools" panel.

Result: button >> looks pressed, but panel doesn't appear
Expected: "more tools" panel should open

*Pribably* it's a problem with "Downloads panel", because so far I only noticed this issue with downloads button.