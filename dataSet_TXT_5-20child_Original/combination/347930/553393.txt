Tabbar toolbar appears to bleed into page content
Created attachment 433421
Screenshot of 3.6 and trunk

Active tabs on the tabbar toolbar now appear to bleed into page content due to a black line no longer being present under the tabbar.