The search suggestions hint does not fit the window (if the window's width is less than 400 pixels)
[Affected versions]:
- Nightly 55.0a1
 
[Affected platforms]:
- All
 
[Steps to reproduce]:
1. Launch Nightly with a new profile
2. Re-size the window - the window's width is less than 400 pixels
3. Focus the URL Bar 
4. Open a new tab and start typing
 
[Expected result]:
- Step 3 - The search suggestions hint should be properly displayed as an animation
- Step 4 - the URL drop-down and the hint should be properly displayed
 
[Actual result]:
- Step 3 - the hint does not fit the window
- Step 4 - the drop-down and the hint are not compacted enough to fit the windows size
- For more details, please see the screenshot: https://bug1368437.bmoattachments.org/attachment.cgi?id=8874517