Abstract away the visuallyselected attribute on anonymous tab nodes
Created attachment 8783640
patch

This is an implementation detail that themes should not know about. It's particularly bad that we set /both/ the selected and visuallyselected attributes, inviting the use of the wrong attribute.

This patch makes all anonymous tab node use the selected attribute. The parent tab node still has the visuallyselected attribute, which sucks but fixing this requires more invasive changes.