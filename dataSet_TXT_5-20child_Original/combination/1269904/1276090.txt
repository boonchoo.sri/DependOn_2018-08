Don't increment the bookmarks score during a batch operation
Created attachment 8757076
0001-Don-t-increment-the-sync-score-during-a-batch-bookma.patch

The bookmarks engine treats every bookmark change as "significant" - even ones that are happening as part of a batch operation. This means our tracker is almost guaranteed to miss changes - a batch operation is almost certainly making multiple changes but we will stop listening after the first.

This probably becomes moot with Kit's patch, but I had this patch in my tree and figured I see what people thought about it in the meantime.

Richard/Kit, can you see any reason this isn't the right thing to do for our current tracker?