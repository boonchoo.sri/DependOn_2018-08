Stop using Promise.jsm in debugger
Removal of Promise.jsm usage in debugger need particular attention as it uses sync promise is various places.
We should focus on Promise.jsm usages and only that. Getting rid of sync-promises is yet another subject (bug 940537).

Promise.jsm is imported only in 3 places:
$ egrep 'require\("promise"\)' -r devtools/client/debugger/
devtools/client/debugger/content/actions/breakpoints.js:const promise = require("promise");
devtools/client/debugger/content/actions/sources.js:const promise = require("promise");
devtools/client/debugger/panel.js:const promise = require("promise");

Whereas sync-promises are from 2 others, but this ends up exposing that to many files:
$ egrep deprecated-sync -r devtools/client/debugger/
devtools/client/debugger/debugger-controller.js:var promise = require("devtools/shared/deprecated-sync-thenables");
devtools/client/debugger/test/mochitest/head.js:promise = Cu.import("resource://devtools/shared/deprecated-sync-thenables.js", {}).Promise;

So we should only modify the 3 files importing promise module and ignore the rest.