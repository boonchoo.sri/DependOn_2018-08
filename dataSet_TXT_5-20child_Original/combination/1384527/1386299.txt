Stop using devtools event-emitter module as a JSM
In order to help running tools in a tab like debugger, inspector and netmonitor,
we should stop using Cu.import to import devtools modules.
event-emitter is one such example.
Now that this module has been copied to toolkit in order, we can remove the boilerplate and always load it via require().

It would also help bug 1384527, by removing an occurence of Promise.jsm
and bug 1381542 by removing this hard to maintain compatility code.