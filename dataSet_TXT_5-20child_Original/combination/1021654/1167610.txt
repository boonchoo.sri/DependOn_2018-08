Remove newtab-intro-panel
From what I can tell, this panel in newTab.xul is no longer being used, as of bug 1138818.