Enable and test automigration on beta 50
Per discussion between Michael, Stephen, Justin and me, we're looking for a way to enable the auto-migration code on pre-release channels. This will involve some compromises in comparison with the 'ideal' state, but we'll get feedback quicker this way. In no particular order, I'll try to do patches that:

- show a notification bar on about:home offering the user to undo the import
- remove that notification and undo options once it's been shown on 3 separate days
- remove the notification and undo options if the user adds/moves/changes bookmarks or passwords
- does some telemetry on when those things happen
- does some telemetry on how much time the migration takes so that can inform when/how we switch to using about:newtab instead of about:home for these cases.
- disable the 'firstrun' pages on pre-release channels (after discussing that with stakeholders)