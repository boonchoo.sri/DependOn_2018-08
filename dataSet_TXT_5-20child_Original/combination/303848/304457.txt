Can't tab between location and search boxes
Since bug 303848, it's impossible to tab forward from the location bar to the
search bar, using the default toolbar.

To reproduce, focus the location bar and press tab.  You expect focus to go to
the search bar, but it doesn't.  Instead, the entire contents of the location
bar are selected.

Shift-tabbing back from the search bar to the location bar works properly.

I'm experiencing this on the Mac, but I'm told that it affects all platforms.