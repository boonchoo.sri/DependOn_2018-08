The Downloads Panel footer should use the same style as the application menu
Pros of doing this :
- Matches better shorlander's design
- In sync with other panel footer styling
- Makes the styling consistent across platforms

Cons :
- It's not a subview