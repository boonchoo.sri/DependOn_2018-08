Add highlight-on-hover to box model tab
When each region of the box model is hovered we should move the guides on the page to outline only that region e.g. content, padding, border or margin.