We should display the dimensions of the node in the NodeInfobar
It would be nice to have a label with the dimensions for the node (width x height).
We could have this in the infobar, or maybe in the toolbar.

PS: Here, I am not talking about the Layout View.