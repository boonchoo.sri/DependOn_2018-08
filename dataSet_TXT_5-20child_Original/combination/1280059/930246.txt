Font name should be in a more plain font
The font name is shown in a bold monospace font. It actually looks like an unusual font, and thus makes me think that that is what that font looks like. In reality, the font is applied only to the preview text above the name.

I shadowed someone using the tool for the first time and they were really confused as well.

Does that make any sense?