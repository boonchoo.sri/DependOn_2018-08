De-dupe view source logic in browser.js and nsContextMenu.js
Recent view source work (bug 1067325 and others) has duplicated some logic in:

* BrowserViewSourceOfDocument from browser.js
* viewPartialSource from nsContextMenu.js

We should extract the common code to one function, since all windows that include nsContextMenu.js also include browser.js.