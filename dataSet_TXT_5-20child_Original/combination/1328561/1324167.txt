Create telemetry probes to track which preference categories are opened
"FX_PREFERENCES_CATEGORY_OPENED": {
    "expires_in_version": "56",
    "kind": "enumerated",
    "n_values": 25,
    "releaseChannelCollection": "opt-out",
    "description": "Count how often each preference category is opened. 0=general, 1=search, 2=content, 3=applications, 4=privacy, 5=security, 6=sync, 7=advanced-general, 8=advanced-datachoices, 9=advanced-network, 10=advanced-updated, 11=advanced-certificates. Extra values are reserved for future categories."
  },