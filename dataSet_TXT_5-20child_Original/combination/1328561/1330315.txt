Add a telemetry probe to track how the Preferences are opened
This telemetry probe should track how the Preferences are opened.

This could be one of the following (comment if I missed one):
- typed about:preferences in to location bar
- Firefox menu > Preferences/Options
- (Title bar) Tools menu > Options
- Preferences/Options button on about:home