Web Console needs to respect Error Console prefs
As documented here: https://developer.mozilla.org/en/Setting_up_extension_development_environment#Development_preferences the Error Console watches certain prefs to see which kinds of things to log.

The Web Console should be able to log the same things and use the same prefs to control the behavior.