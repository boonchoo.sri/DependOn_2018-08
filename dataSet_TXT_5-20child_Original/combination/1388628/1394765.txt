Remote type set by gBrowserInit.onDOMContentLoaded is bogus when restoring a session
from bug 1388628 comment 43:

> I think bug 1351358 may also play a role here, attachment 8864117
> in particular, as it updates the initial browser's
> remoteness based on the homepage URL even if we'll restore the session
> instead of loading that URL... This is quite bogus, but without bug 1365541
> (and bug 1351677 before that), it didn't matter as far as session restore
> was concerned because it wouldn't have used that tab.