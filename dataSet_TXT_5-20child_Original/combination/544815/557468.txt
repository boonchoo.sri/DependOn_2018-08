No tab drop indicator appears when enabled Tabs over the Navigation Toolbar
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US;
                        rv:1.9.3a4pre) Gecko/20100406 Minefield/3.7a4pre ID:20100406003858
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US;
                        rv:1.9.3a4pre) Gecko/20100406 Minefield/3.7a4pre ID:20100406003858

No tab drop indicator appears when enabled Tabs over the Navigation.

Reproducible: Always

1. Start Minefield with new profile
2. View > Toolbars > "Tabs on Top" checked
3. Open several tabs
4. Dragging over a tab on tabtoolbar

Actual Results:
 No tab drop indicator appears

Expected Results:
 tab drop indicator appears