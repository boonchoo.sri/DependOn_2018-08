Places UI: The bookmarks sidebar should not display contents by default
Note: this is a meta tracking bug for UI mockups and discussion of the overall user experience.

In Firefox 4 when we remove the Library window in favor of interactive content area pages for bookmark organization (bug 524057), the Bookmarks Sidebar will need to effectively become the left window pane of the Library window.  The key difference is that the Library window only shows folders, leveraging the right window pane for the display of contents.

We should modify the title of the Bookmarks sidebar to become an interactive control, with the sub-option when selected:

[] Display folder contents

By default this option should be turned off.