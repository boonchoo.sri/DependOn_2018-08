Firefox has problems remembering page zoom on Windows 7
Because of the high resolution (I also have increased DPI to 125%) I need to use page zoom on every page; 3 times ctrl+ and then it is more comfortable to read.
But it happens very often, once in about 3 times, that page zoom settings are ignored the next time. Firefox remembers the setting but not at the right moment. So I need to click on a link and after going back Firefox remembers the correct setting.
 
Also reproduced in safe-mode.

STR:

- Start Firefox and on the homepage (http://www.nu.nl/ in my case) press 3 times ctrl+ 
- Restart Firefox
- Page reverts to reset page zoom once in about 3 times
- Click on a link or go to a different site
- Click Back
- Firefox suddenly remembers the correct zoom setting

On the Windows XP partition of this computer under the same circumstances I haven't seen this problem yet.