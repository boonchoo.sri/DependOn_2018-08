Implement optional taskbar preview-per-tab
Related to bug 474054 where we show a preview of the tabs, but this is to actually show the full size contents of the actual tab (incase another tab is actually focused in that window).

This is supposed to be just a preview, so we probably don't want to actually switch tabs for the user when the hover leaves the tab preview without selecting a tab.