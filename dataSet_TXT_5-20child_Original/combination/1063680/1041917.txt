[e10s] Bugzilla doesn't load when I file a bug from "mozilla crash reports"
Steps To Reproduce:
1. Make sure e10s, set browser.tabs.remote.autostart = true and restart
2. Tab Crashed
3. Open about:crashes and Click a crash ID to open "mozilla crash reports" 
4. Click one of link "Bugzilla - Report this bug in".

Actual Results:
New tab opened, but nothing load in the tab

Expected Results:
Bugzilla should be loaded