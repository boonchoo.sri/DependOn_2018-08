Add in-product notification when e10s is first enabled
We want to warn Nightly users that stuff will break and how to disable e10s.