TypeError: panel.hidePopup is not a function
Sometimes "TypeError: panel.hidePopup is not a function" is thrown.

It's not clear if there's an issue or if it's expected behavior. To suppress these errors, one can check for the existence of "panel.hidePopup" before calling it.

Noticed in [1] and [2].

[1] https://dxr.mozilla.org/mozilla-central/source/addon-sdk/source/lib/sdk/panel/utils.js#99
[2] https://dxr.mozilla.org/mozilla-central/source/browser/devtools/webide/content/webide.js#249