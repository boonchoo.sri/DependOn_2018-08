Make simulators configurable from WebIDE
Created attachment 8513463
simulator configuration (mockup)

Allow the configuration of simulators in WebIDE, so that developers can:
- Choose the simulator runtime, either a specific simulator addon (already possible) or custom B2G/Gaia.
- Choose different screen sizes

Once these simulator profiles are configurable, more simulation options can be added:
- Fake GPS coordinates
- MNC/MCC codes
- Language
- Time zone
- Hardware/sensors specification
- etc.

(for a rough visual idea, see attached mockup)