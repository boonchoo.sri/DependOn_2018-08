Autoconfig aka MissionControl is not privileged to default/lock CAPS
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7

Mission Control (autoconfig) is one preferred method for enterprises and institutions to centrally manage preferences for Firefox deployments.

The autoconfig system does not have the entitlements to set capabilities.policies.* settings for the end user.  Effectively, this prevents Mission Control adopters from enabling Security Zones that are on par with Internet Explorer.

For example, enabling "trusted" security zones and a governed manner will permit enterprises to enable cross-side scripting for internal, trusted domains to ease the pains of developing Rich Internet Applications for both browsers.

Asking users to manage their own user.js for CAPS settings is unacceptable for large-scale deployments of the Firefox browser. 

Reproducible: Always