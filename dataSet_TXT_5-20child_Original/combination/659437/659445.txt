location bar input should not be dependent on how long it takes the user to press enter
Copy of bug 659437 comment 2:

(In reply to bug 659437 comment #1)
> As warned against in bug 566489 comment 13, I seem to get different
> behavior depending how quickly I hit Enter after typing.
> 
> With arewefastyet.com in my history, I sometimes get a google search for
> 'arewe' (if I hit enter quickly) and sometimes get arewefastyet.com (if I'm
> not fast yet).

I strongly advocate for fixing this. It is ironic that a UX-efficiency win also costs the user in efficiency, because now the user has to wait for an async operation to get consistent results. We really should create a synchronous fast path for the inline autocompletion as described in bug 566489 comment 19.