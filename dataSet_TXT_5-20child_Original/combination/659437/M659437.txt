Tracking bug for URL autocomplete edge cases
Bug 566489 landed, and I want a place where people can log their text editing edge cases (make sure you include platform!) and other issues encountered.

Currently reported:

* OmniBar add-on breaks the inline autocomplete
* Entering text causes a lot of flickering because the background color is changed on every keypress (comment was: "Please compare behavior with Chrome or Camino 2.1a1 (with the enabled "browser.urlbar.autofill" option).
* Ctrl-Backspace doesn't do what's expected (Windows)
* It broke keyword bookmarks. Now `ma<Enter>` tries to find a URL instead of expanding to my bookmark. (definitely an edge case, as keywords generally exist to be able to do things like "w cats" to search Wikipedia for "cats")

Please add other issues below, and we'll separate out bugs for the ones that are things we want to fix. There will be certain edge cases that may not be worth fixing, but I'd like to collect them here first.