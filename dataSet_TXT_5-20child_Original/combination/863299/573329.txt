Drop support for text and icons+text toolbar modes in the main browser window
As discussed in bug 559033 starting around bug 559033 comment 10. It's not completely clear we want to do this yet, as far as I can tell.

We need this questions answered from UX:
 - Should we do this at all?
 - Should we drop support for text-only mode, too?

Dão, do you want to create the patch?