"Show more bookmarks" icon on the Bookmarks Toolbar should be mirrored for RTL builds
Created attachment 8899902
Show more bookmarks LTR icon

When the Bookmarks Toolbar is overflowing, the "Show more bookmarks" icon should be mirrored in RTL builds.

See screenshot attached for how it currently looks.