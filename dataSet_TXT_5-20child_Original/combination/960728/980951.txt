Ensure devtools/markupview/markup-view.xhtml is free of inline script and styles.
There are a number of attributes in markup-view.xhtml that will cause breakage when CSP is applied:
* An onclick attribute
* A number of style attributes