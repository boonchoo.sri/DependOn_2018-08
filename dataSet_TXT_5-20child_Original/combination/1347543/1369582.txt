Synced Tabs button has old "spinner" icon when in the toolbar
Look at the "Synced Tabs" button in either the hamburger menu or "customize" area - notice how it looks like a tab icon. Move the button to the toolbar - note how it is now the "old" sync spinner icon. It should be the tab icon in all places.

It appears that sync.svg in the tree is still the old spinner, while it is correct in menuPanel.svg. I'm not sure if simply pulling the svg from menuPanel.svg and putting it in sync.svg is enough (and TBH, I'm not sure why menuPanel.svg isn't used in all cases.

Note also that http://searchfox.org/mozilla-central/source/browser/themes/shared/browser.inc.css#120 appears to imply sync.svg is used in the new "page action" menu, but I don't see it there in practice, so I'm not sure what's going on there.