Disabling gfx.font_rendering.opentype_svg.enabled breaks image context paint
User Agent: Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.97 Safari/537.36 Vivaldi/1.10.834.9

Steps to reproduce:

disable these

gfx.font_rendering.opentype_svg.enabled

svg.disabled


Actual results:

toolbar icon breaks
setting gfx.font_rendering.opentype_svg.enabled false
for rendering hw/software/security reasons,
breaks this and turns it into black.
can it be fixed?


Expected results:

Should not break icons,
and yes won't enable those preferences as for testing/hw/software/security reasons.