9% AWSY regression on May 3rd from Autoland push 4d6cbe2ebbd1 (bug 1347543)
== Change summary for alert #6408 (as of May 03 2017 10:54 UTC) ==

Regressions:

  9%  Images summary linux64 opt      5509877.04 -> 6030491.56

For up to date results, see: https://treeherder.mozilla.org/perf.html#/alerts?id=6408

I did some retriggers and this ended up pointing out bug 1347543 as the root cause.

While we don't have an official policy for AWSY regressions, it is good to know what we have and fix what is reasonable.  I am curious why this is linux64 only.