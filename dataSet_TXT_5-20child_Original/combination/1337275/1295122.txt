Password sync fails with "Services.logins is undefined"
Created attachment 8781100
error-sync-1471265149150.txt

User Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:48.0) Gecko/20100101 Firefox/48.0
Build ID: 20160726073904

Steps to reproduce:

Clean install of Firefox 48 (64-bit) on a clean install of Windows 10. Entered credentials for Firefox sync.


Actual results:

Everything syncs except for my passwords/logins. This actually started happening on Firefox 47 on Windows 7, same machine. I wiped everything and started fresh, but the same problem occurred.

At the same time my sync issues started, I started having TLS issues on my corporate network. The certificates are never recognized as valid. I can still get to the page if I click through the exception dialogs, but it also will not let me add a permanent exception. If that box is checked, I cannot click the "Confirm Security Exception" button.


Expected results:

Sync should have finished successfully.