Style editor should not add additional closing bracket when this would result in two closing brackets
Situation:
When I write CSS from scratch, I got into the habit of simply writing two brackets automatically for a given block:
body {}

However, when I do that, it results in two closing brackets because the Style Editor automatically adds a closing bracket, and I add another one.

Could we make the whole thing smarter to simply jump after the closing bracket once I added the second one?