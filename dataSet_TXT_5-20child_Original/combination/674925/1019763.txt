Tab Detaching: Handle when a tab drag is cancelled
A drag could be cancelled in a number of different situations:

1. Dragging a tab but it it currently hovering over a tabstrip
2. Dragging a tab in a new window position
3. Dragging a tab when the last tab