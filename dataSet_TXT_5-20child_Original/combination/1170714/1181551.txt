[tablet mode] The about dialog doesn't honor the limitations of side-by-side usage while Firefox is in focus
Created attachment 8630989
screenshot from Windows 10 x64 (10162)

Reproducible on:
* Nightly 42.0a1 (2015-07-07)
* Aurora 41.0a2 (2015-07-07)
* Beta 40.0b2 (20150706172413)

Affected platforms:
* Microsoft Surface Pro 2 - Windows 10 Pro x64 (Insider Preview Build 10162)

Steps to reproduce:
1. Enable "Table Mode" in Windows 10.
2. Launch Firefox.
3. Open the about dialog from menu → help → about firefox.
4. Drag & drop the browser window from the top to the left-hand side of the screen, to trigger side-by-side usage.

Expected result:
Any dialog box, window or tab originating from Firefox will respect the limitations set by the side-by-side view in Windows 10.

Actual result:
The dialog box is displayed on top of the side-by-side separator while Firefox is in focus.