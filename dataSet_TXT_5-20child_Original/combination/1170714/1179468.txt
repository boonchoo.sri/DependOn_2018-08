[tablet mode] Links opened from the Win 10 mail app in tablet mode should snap Firefox on the side
STR :
- Open a link from the win 10 mail app

When edge is set as default browser :
- Edge snaps to the right side and keeps the mail app on the left side

When ffx is set as default browser :
- Firefox opens full screen and the mail app disappears from your screen

Expected result :
Firefox should snap to the right