If Menu toolbar is shown, then it causes additional indent on top of the window in Fullscreen mode
STR:   (Nightly 42.0a1 (2015-08-06))
1. Make sure you have hidden menu bar, bookmarks bar and title bar
2. Switch to Fullscreen mode
3. Press Alt several times

RESULT:   [watch video]
Part of toolbar (some pixels in height) is visible on the top of the screen

EXPECTATIONS: one of 2 will happen:
A) Nothing
B) I will see all toolbars in their current states (as of non-fullscreen mode), including menu bar, that will be visible. When I press Alt again in this scenario, I expect all toolbars to hide again.