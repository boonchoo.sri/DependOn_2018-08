In fullscreen mode, toolbars always hide when popup hides, even if I click in urlbar
>>>   My Info:   Win7_64, Nightly 45, 32bit, ID 20151119030404
STR:
1. Click in content area (to make sure that urlbar/searchbar aren't focused
2. Press F11 to switch to fullscreen mode, wait until transition is finished
3. Move mouse pointer to the top side of the screen to show toolbars
4. Right-click any tab/button placed on one of toolbars
5. Click in urlbar/searchbar   [OR]
   Right-click any tab/button/urlbar/searchbar

Result:       
 Toolbars hide after Step 5.

Expectations:   
 Toolbars should stay, because urlbar is focused.

Regressed between 2015-10-26 and 2015-10-27 by bug 1192683:
> pushlog_url:   https://hg.mozilla.org/integration/mozilla-inbound/pushloghtml?fromchange=a6f01cc9c740cc536a8d4e73565e1ee0db55427b&tochange=d70472571f1d0313456c99d8f34e3ecbc9831cfe