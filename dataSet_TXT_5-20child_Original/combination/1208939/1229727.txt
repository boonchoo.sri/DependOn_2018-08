[OS X] When going into video full screen while already being in OS X full screen mode, the title bar overlaps our messaging
Created attachment 8694672
Screen_Shot_2015-11-18_at_09.54.02.png

STR:
- Go to a video on youtube.com
- Put the browser window into full screen mode (green circle at the top left of the window)
- Play the video and put it into full screen using the controls on the youtube page
- Move the mouse to the top edge of the screen

Expected:
The button to exit full screen mode appears.

Actual:
The button appears, but it is overlapped by the title bar that also slides down when moving the mouse to the top edge. (see attachment)