Noticeable delay when opening of new tabs if activity stream is enabled
The first time you open a new tab in a new window there is a noticeable delay. This is a recent regression.

Could it be related to the fact we disabled the preallocated content process?