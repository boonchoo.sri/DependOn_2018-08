Activity Stream tile labels are too small
[Tracking Requested - why for this release]:

The tile labels significantly undercut OS default font sizes. This is problematic especially for visually impaired users.

I have zoomed this page to 133% to see stuff comfortably even though I see my screen unimpaired (I'm only slightly shortsighted).