Turn on Activity Stream by default for Nightly
We'll need to match the building of activity-stream as we don't want to be enabled in Firefox but not actually have the content to load:
https://searchfox.org/mozilla-central/source/browser/extensions/moz.build#39-42