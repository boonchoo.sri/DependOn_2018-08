Allow explicitly triggering update for OpenH264
We need an interface to trigger the GMP update search from bug 1009816 explicitly.
That will allow us to let the user trigger it from the AddonManager.