Reattaching a lone tab into another window causes an empty window to be added to Recently Closed Windows
STR:
0. Look at Recently Closed Windows menu
1. Open 2 windows (A & B). Make sure window B only has one tab.
2. Drag the lone tab from window B to window A
3. Recently Closed Windows now has a menu entry for (Untitled).

Expected Behavior:
No new entry is added to the menu.

Proposed Solution:
One possibility is to just ignore empty windows in session store, though this could possibly cause issues with the win/linux need for a non-popup window.