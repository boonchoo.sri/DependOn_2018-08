frequent timeout in browser_394759_privatebrowsing.js, caused by browser_394759.js and browser_354894.js
Just seen on Firefox-Unittest tinderbox page:

WINNT 5.2 mozilla-central test opt everythingelse on 2009/09/25 14:46:12
http://tinderbox.mozilla.org/showlog.cgi?log=Firefox-Unittest/1253915172.1253918100.15234.gz#err0
{
Running chrome://mochikit/content/browser/browser/components/sessionstore/test/browser/browser_394759_privatebrowsing.js...
TEST-PASS | chrome://mochikit/content/browser/browser/components/sessionstore/test/browser/browser_394759_privatebrowsing.js | sessionstore.js was removed

command timed out: 1200 seconds without output
program finished with exit code 1
}

There were only two possibly-guilty changesets -- one was mac-only, and one was a fix in a different test:
http://hg.mozilla.org/mozilla-central/pushloghtml?fromchange=213da42e5f65&tochange=8b4d2fc47ecf

So, I'm calling this sporadic.