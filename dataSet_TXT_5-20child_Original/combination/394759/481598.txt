Starting Private browsing from window-less state shows last closed tab from PB mode in list of recently closed tabs after stop
Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1b3pre) Gecko/20090304 Shiretoko/3.1b3pre ID:20090304022008

If the Private Browsing mode is started from a window-less state and the user stops it right away the "about:privatebrowsing" item is shown in the list of recently closed tabs.

Shouldn't the list be empty? It is visible since bug 481090 has been fixed.