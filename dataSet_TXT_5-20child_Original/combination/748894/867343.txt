Back out star button UI changes
Bug 748894 extracted the star button out from the URL bar and put it into a split button.

Today, the UX team told me that after a work week, they've got another direction they'd like to take with the star button (being tracked in bug 855805).

Jared and I were discussing this with fang, limi and shorlander, and I think the idea is to back out bug 748894 just after it hits the Aurora channel, before the first nightly build.

Marco - sorry for doing this, especially after I put on the pressure to have bug 748894 landed so I could get bug 855805 going. But it sounds like the intermediate step that we currently have on Nightly is not something we want to ship.