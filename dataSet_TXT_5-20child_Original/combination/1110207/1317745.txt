Rename "round trip time" to "latency" in network throttling
The network throttling code uses the name "round trip time" in several places, but I think "latency" would be more accurate, since the value is used as a single delay before data starts to stream in.

(To me, RTT is the sum of propagation delay in both directions, which is not what the value is used for here.)