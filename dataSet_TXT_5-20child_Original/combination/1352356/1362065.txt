Allow touch-based drag/drop of tabs by "touchstart, touchmove, touchend" gesture (in addition to/as opposed to the current double-tap-drag gesture)
Hardware: Surface Pro 2
Windows version: Windows 10 — 1703 "Creators Update"


On Nightly drag and drop appears to be broken.

This affects dragging tabs, Customization Mode, dragging tiles on New Tab and content HTML based drag and drop.

The release version of Firefox (53) works fine for me.