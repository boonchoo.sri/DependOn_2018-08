JSON Viewer: empty arrays should hide the zero count
Helen, what do you think about hiding the size count when the size is zero?  

via bug 1223143 comment 7

> ## JSON tab - misleading representation of empty arrays
> 
> Empty arrays are represented as:
> 
> key_name [0]
> 
> (or "key_name: [0]" in object summaries)
> 
> This, and the syntax coloring used (the zero is green, like a number literal
> in the Console) suggests that the value for key_name is an array with one
> value, the number 0. Yet the value is an *empty* array.
> 
> It should probably be represented as "key_name: []".
> 
> For the record, empty objects are: "key_name { }".

I wouldn't characterize the 0 as misleading, however it does seem superfluous given the colouring and that an empty array would give just as much meaning without confusing the eye.