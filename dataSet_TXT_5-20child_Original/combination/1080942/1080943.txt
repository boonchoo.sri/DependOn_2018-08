UITour: allow opening the Hello panel
The tour page will need to open the Hello panel in two cases:

1) If we have a standalone whatsnew page, the first step would be to open the panel (to point out the New Room button)

2) If the user switches tabs, upon switching back to the tour tab the Hello panel should reopen.

In the normal case this wouldn't be needed, since the tour is launched from the panel and it can keep itself open.

API tbd. Maybe the page should just indicate when it wants the panel open/closed (state based), and UITour handles (re)opening it as needed?