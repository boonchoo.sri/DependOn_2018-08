UiTour: Hello FTE incoming conversation / tab issues
There are some remaining issues that seem to need attention after Bug 1080953 landed.

The main issue seems to be when the incoming call is received. The hello panel does not open when the blue icon is clicked, and when it does it seems to fire repeated "Loop:IncomingConversation" events which interfere with the page.

For example:

- I leave the tab open after sharing the link, but close the conversation view:

When the other person connects, the hello icon turns blue as expected. I go to click it, but the hello panel doesn't open. At this point I get sent *lots* of "Loop:IncomingConversation" events to the point where it starts to slow down the browser.

Please note this also seems to effect all scenarios where the conversation connects. The next time the hello icon is clicked, I get hundreds of events one after the other?

Not quite sure what's going on...