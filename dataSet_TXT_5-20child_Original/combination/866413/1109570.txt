[e10s] No "Feeds" section in the Page Info window
Reproduced using latest Nightly 37.0a1 2014-12-09 under Ubuntu 14.04 32-bit, Win 8.1 64-bit and Mac OSX 10.9.5

Steps to reproduce:
1. Go to websites that provides an RSS feed, such as https://blog.mozilla.org or https://blog.mozilla.org/addons/
2. Bring up the identity panel for the websites by clicking on its favicon and pressing the "More Information..." button.
3. Select the "Feeds" section from the website identity panel

Actual results:
No "Feeds" section in e10s. Sometimes "Media" tab is missing too. All sections are displayed with delay.

Expected results:
"Feeds" section is available for websites that offer feeds.