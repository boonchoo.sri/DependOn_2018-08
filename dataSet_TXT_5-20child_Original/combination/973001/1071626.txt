make the global WebRTC sharing indicators actually list the streams with e10s enabled
Bug 973001 fixed enough of the WebRTC UI with e10s enabled to have the permission prompts and the indicators showing, but clicking on the global or menubar indicators doesn't work: with e10s enabled, these indicators will show empty lists of streams.

This was split off from bug 973001 because it doesn't prevent WebRTC from being used.