Make sure there is no reflow after swapping in a preloaded newtab page
Currently there is a reflow after swapping a new tab's docShell with the one from a preloaded browser. This is caused because a resize is detected:

http://mxr.mozilla.org/mozilla-central/source/layout/base/nsPresShell.cpp#1878

The first step to make this work is to keep the preload browser's size in sync with the content area's size so that there really is no need to reflow.

As a second step we need to find out why the subdocument size returns 0x0 here:

http://mxr.mozilla.org/mozilla-central/source/content/base/src/nsFrameLoader.cpp#1849

If I hard-code it to the right content area size there is no reflow after swapping docShells.