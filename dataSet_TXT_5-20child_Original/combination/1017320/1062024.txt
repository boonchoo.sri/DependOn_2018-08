callbacks passed to a cpow (like a DOM listener) throw "Permission denied to pass object to chrome" (e10s)
Created attachment 8483174
e10s-cpow.zip

none of the shims listed in bug 1031609 comment 0 work in Jetpack addons when tabs.remote.autostart pref is set.

SDK creates Sandbox-es to load both the addon code and SDK modules, so it's possible somehow those compartments are not marked as "addon code", and thus interposition is not enabled.

attached is a STR addon that shows:
1) browser.contentWindow doesn't work, while contentWindowAsCPOW does
2) attaching a DOM event listener to a window CPOW throws when that listener should be invoked:
  "Permission denied to pass object to chrome" 


to run, execute this line from the addon dir:

  \addon-sdk\bin\cfx run -v -b \nightly\firefox.exe --e10s


(or install the xpi manually, and check the browser console)