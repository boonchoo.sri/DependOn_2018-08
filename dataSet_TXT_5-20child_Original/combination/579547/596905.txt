Remove release date in the new about window (about box)
Steps to reproduce:
 1) Install a nightly for 2010-09-15
 2) Run it
 3) Choose About Minefield

Actual results:
It says "(released 09-15-2010)"

Expected results:
Expected an en-US version to use one of these date formats:
2010-09-15
September 15th 2010
15 Sep 2010
09/15/2010

The last one is the worst of these, since it may confuse non-U.S. users of en-US builds when the day <= 12.