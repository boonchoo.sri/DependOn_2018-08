Move certain style rules for the new About Window for Firefox 4 to the skin provider
I'm a little concerned that a couple style rules for the About Window will break third-party themes.

Since a large number of theme developers use to track only skin provider changes, those rules will not be on the developers radar, so developers will maybe only take notice of issues through user complains about usability.

Just some examples of potential risks of breaking themes:

#trademark {
  font-size: xx-small;
  text-align: center;
  *color: #999999;*
  margin-top: 10px;
}

It is always a problem to define font colors for an element without knowing the background color from the element. In this case the background color will be that from the window element, defined on the global stylesheet. This will be an issue for all third-party dark themes.

Another example:

.update-throbber {
  width: 16px;
  min-height: 16px;
  -moz-margin-end: 3px;
  *list-style-image: url("chrome://global/skin/icons/loading_16.png");*
}

Actually, nothing guarantees that this image really exists on the skin package. Some developers may be using a .gif for it, or have placed this image inside another folder, or have named it differently.
This is also a bad example for extension developers. A lot of compatibility issues between extensions and themes are caused by this "blind linking" to images that are supposed to be there...

On my theme I had problems because of the padding: 0 for the update button since I'm using the three parts approach to skin buttons on my theme (due to backwards compatibility).

So, this bug is a proposal to create a aboutDialog.css on the skin provider with the appearance rules, letting the behavior rules and those branding related rules on the content.