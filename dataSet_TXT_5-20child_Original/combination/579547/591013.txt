Add "Firefox Help" to the About window
As part of the update to the About window, the Support team suggested that we add "Firefox Help" to the window, since it's now moving towards being a useful starting point for troubleshooting — with the inclusion of Check for Updates in addition to the version number.

My initial inclination would be to de-emphasize the "Credits" button (turn it into a link in the body text after the version number or similar), and put the troubleshooting stuff as the main actions.

(I also heard it mentioned that they may want to add a link to the privacy policy in this window, so accommodating that in the glorous ASCII mock-up below)

Something like this: [this is a link] (this is a button)


__________________________________________

                Logo 
               Firefox
__________________________________________
             version 4.0
              [Credits]

[Licensing information] — [Privacy Policy]

Mozilla/5.0 (Macintosh; Intel Mac… 
__________________________________________
(Check for updates)                 (Help)