Tab shape appears on an inactive tab after moving one tab from the right to the left
Created attachment 8867454
tabs.mov

As you can see in the screencast there is a visible tab shape on an inactive tab after moving on tab from the right to the left. It seems to be a regression in of the last Nightly builds, maybe from bug 1355507.