Control+Shift+click on bookmark should open a background tab
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8

Control + Shift + Clicking on a bookmark from the drop down menu or the sidebar
loads the bookmark in a new window. This is inconsistent with the key modifiers
for clicking on hyperlinks within documents.

Ctrl + Shift + Click on hyperlink = New foreground/active tab
Ctrl + Shift + Click on bookmark = New window

Shift + Click on hyperlink = New window
Shift + Click on bookmark = New window 

Ctrl + Click on hyperlink = New background tab
Ctrl + Click on bookmark = New background tab

This behaviour should be changed for consistency, and because there appears to
be no other way to launch a bookmark in an active/foreground tab.

Reproducible: Always
Steps to Reproduce:
1. Select "Bookmarks" from the Firefox menu bar.
2. Hold down Ctrl and Shift, click on a bookmark.
Actual Results:  
The bookmark is launched in a new window.

Expected Results:  
Launched the bookmark in an active tab, as per the document hyperlink behaviour.