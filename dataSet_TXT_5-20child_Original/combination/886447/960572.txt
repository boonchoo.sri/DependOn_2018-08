[meta][Session Restore] Provide a clean API for accessing data object
Created attachment 8361116
Rough sketch of a possible API

At the moment, saving proceeds as follows:
1. collect data;
2. build big object;
3. serialize object;
4. send to worker for writing.

I would like to replace step 2 by something a little more abstract that will later let us rework the back-end without having to rewrite everything.