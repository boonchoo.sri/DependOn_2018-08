Backout the patch for bug 887515 and its dependencies from Firefox
+++ This bug was initially created as a clone of Bug #914258 +++

The patch for bug 896291 didn't get uplifted to Aurora in time for the mozilla-aurora -> mozilla-beta merge, and there is also an add-on compat issue that is being tracked in bug 895436.

As such, I'd like to back out the patches for bug 887515 and its dependencies from Fx26 and Fx27 (removing it from non-mc branches). This will give me the full Fx28 Nightly cycle to make the necessary API changes.