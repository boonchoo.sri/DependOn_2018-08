Update the text of the Private Browsing prompt
Spin off from bug 411929 comment 68.

New text wanted for the private browsing prompt:

Title: "Start Private Browsing" (Windows / Linux only)

Message Text: "Would you like to start Private Browsing?"

Informative Text: "Minefield will save your current tabs for when you are done
with your Private Browsing session."

Action Button: "Start Private Browsing"