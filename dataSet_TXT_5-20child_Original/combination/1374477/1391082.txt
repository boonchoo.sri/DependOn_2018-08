Page action panel ordering can get messed up
I noticed that when I'm restoring all my windows on startup, in some of the windows, the web compat item is first in the panel, followed by a separator and then all the built-in items.  Also, in new windows, Pocket is now getting placed above the bookmark item, and I'm not sure when that started happening but I don't think it was always broken.

At least part of the problem is that BrowserPageActions.init incorrectly calls placeAction with only two arguments instead of three, and the second argument is wrong: https://dxr.mozilla.org/mozilla-central/source/browser/base/content/browser-pageActions.js#51

I'll have a patch soon.