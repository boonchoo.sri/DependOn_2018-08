Decide on initial Enhanced Tiles placement/replacement of history
For the initial landing, we'll want to place enhanced tiles at the right spot relative to the user's own browsing history.

dcrobot, Boriss provided some initial thoughts in bug 988447.

Some possible options of aggregating/deduping:

- find the score of the highest ranked history tile and insert enhanced tile before that history tile (if it's an exact match, the history tile gets replaced; if it's not an exact match, there'll be both an enhanced tile and a history tile)
- replace the first history tile with the enhanced tile while keeping other history
- only show one tile (history or enhanced) for a given site (eTLD+1)
\- either take the normal scoring and only show the highest ranked
\- or add up scores of multiple tiles to potentially bump it up to position 1

clarkbw, is this something you should decide while dcrobot thinks of a way to integrate longer term?