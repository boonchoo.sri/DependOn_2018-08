Last used submenu/item is visible/highlighted in "Hamburger" (and page action, and other photon panel) menu(s) after landing patch from bug #1374749
Created attachment 8908619
screencast.mp4

STR:
1. Open "Hamburger" menu
2. Press "Help" item
3. Press "Esc" keyboard button
4. Open "Hamburger" menu
and see that last used "Help" menu is visible, instead of "Hamburger" menu.
When "Add-ons"/"Options"/"Customize..." items are used, they're just highlighted.
It shows proper menu after moving mouse on some menu items.

Regression caused by:
Bug #1374749

Regression pushlog:
https://hg.mozilla.org/integration/autoland/rev/5b700335fc30f2faf8aa12c9e38bc05e6c1e5e22