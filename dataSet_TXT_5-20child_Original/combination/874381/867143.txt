[SessionStore] Cache state aggressively
During data collection, it looks like much of the time is spent recollecting already collected data. We should cache it aggressively.