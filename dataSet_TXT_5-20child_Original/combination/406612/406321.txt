Identity portion of urlbar doesn't turn green with EV Certificates under win32 and Linux
Like it or not, I think we are going to be stuck with the green bar for EV Certificates.

I think the CAs have publicized the green bar enough that it is kind of too late to do something different.

I think something like the way it displays if you add the following to userChrome.css is what is probably needed.

#identity-box.verifiedIdentity {
  -moz-appearance: none !important;
  background-color: #AFA !important;
}