Sync UI: Changing password on other client/via web leads to unknown error
Corresponds to fx-sync bug 571897:

If you change your sync password while another client is still connected, the next sync on that client will produce "unknown error" rather than a helpful login failure notice.