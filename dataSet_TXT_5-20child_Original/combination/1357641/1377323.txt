New tab suggestion bar cannot be closed if the zoom is >= 150%
I tried zooming in as the tiles are too small for me, but the "X" button to close the suggestion bar disappeared.