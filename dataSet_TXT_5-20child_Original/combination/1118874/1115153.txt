Loop: Create API to allow web to retrieve the loop.gettingStarted.seen pref
Create new API to allow the web to get the value set in the loop.gettingStarted.seen value in about:config.

This will be used to show users different content in the /whatsnew and /firstrun tours.

Targeting Fx 36.