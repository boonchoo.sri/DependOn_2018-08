UITour: Info panel positioning breaks when the target is in the overflow panel when opened
+++ This bug was initially created as a clone of Bug #1085330 +++

When opening an infor panel anchored at an item in the toolbar, info panel positioning breaks when the browser is resized down to a width where the icon moves into the "More tools..." overflow panel.

Bug 1085330 missed the info panel case so this is a follow-up for that.