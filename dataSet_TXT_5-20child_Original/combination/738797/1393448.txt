Accelerator keys on Options page don't work
User Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0
Build ID: 20170814072924

Steps to reproduce:

1. Menu > Options.
2. Press the accelerator key associated with any option.

Example #1: on the General page, press Alt-K to toggle 'Show tab previews in the Windows taskbar'.

Example #2: on the General page, press Alt-H to toggle 'When you open a link in a new tab, switch to it immediately'.



Actual results:

In most cases, nothing happens (e.g. example #1). In other cases, pressing the accelerator triggers an item on the main menu (e.g. example #2).



Expected results:

The appropriate action should be triggered.

Example #1: on the General page, pressing Alt-K should toggle 'Show tab previews in the Windows taskbar'.

Example #2: on the General page, pressing Alt-H should toggle 'When you open a link in a new tab, switch to it immediately'.