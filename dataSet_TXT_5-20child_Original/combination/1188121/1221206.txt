Turn on Insecure Password Warning for Firefox Dev Edition
The pref for this is nightly only right now (https://bugzilla.mozilla.org/show_bug.cgi?id=1217156).

This bug is to enable in on dev edition.  The "depends on" bugs below are blocking this change.

So far they are:
https://bugzilla.mozilla.org/show_bug.cgi?id=1217766 - don't warn for pdf.js
https://bugzilla.mozilla.org/show_bug.cgi?id=1217133 - don't warn for localhost

I don't think the other bugs (dependencies of the meta bug 1217142) are needed to turn this feature on for developer edition.  If others disagree, please provide your thoughts here.