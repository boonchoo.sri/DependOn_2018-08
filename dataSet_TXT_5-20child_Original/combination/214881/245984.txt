Bookmarks context menu (Personal Bar & Bookmarks Manager) lacks mnemonics (access-keys/underlined keyboard shortcuts)
This is the Firefox equivalent of Bug 214881 (for Seamonkey).

Fixing this bug would not only make Firefox more accessible, but it would also
minimize the effect of bugs like Bug 172675 (by making it almost as quick to
open a folder in new tabs).

Prog.