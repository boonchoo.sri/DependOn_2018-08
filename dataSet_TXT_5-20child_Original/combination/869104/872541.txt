(Australis) Menu bar should not be movable
In the current UX Build - 23.0a1 (2013-05-14) it is possible to move the 'Menu Bar' in customization mode.
I assume that this is not desired.

At least it breaks the new customization mode when it is moved somewhere.