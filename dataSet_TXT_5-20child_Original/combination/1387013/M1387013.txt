Stuff we can remove now that XUL extensions are no longer supported
+++ This bug was initially created as a clone of Bug #1347507 +++

This is a tracking bug for stuff we can remove now that XUL extensions are no longer supported. Bugs for removals of things that are obsoleted by the removal of XUL extensions can depend on this bug.