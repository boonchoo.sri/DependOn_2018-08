Translation infobar option to report language detection problem
When Firefox detects the language of a webpage, in the "Options" dropdown in the infobar we want to have an option that says:

- Not in {Detected Language}? Report this to Firefox