Highlights cards are no longer displayed after refreshing the page on a new profile
Created attachment 8906646
highlight cards not displayed after refresh.gif

[Affected versions]:
- Firefox 57.0a1 Build ID 20170910220126

[Affected Platforms]:
- All Windows
- All Mac
- All Linux

[Prerequisites]:
- browser.newtabpage.activity-stream.enabled is set to true in about:config on a clean new profile.

[Steps to reproduce]:
1. Start the browser with the profile from prerequisites.
2. Navigate to 12 different websites.
3. Open a New Tab.
4. Restart the browser with the console command.
5. Observe the Highlights section.
6. Refresh and observe the Highlights section.

[Expected results]:
- Step 5 & 6: The same Highlights cards are displayed.

[Actual results]:
- Step 5: A few Highlights cards are displayed.
- Step 6: Some Highlights cards disappear.

[Notes]:
- The issue is also reproducible if you close and reopen the browser as long as you have the "When Nightly starts: Show my windows and tabs from last time" option selected in about:config.
- The issue becomes more apparent if Activity Stream is the default home page.
- Attached a screen recording of the issue.