[All-Aboard] [Utility Track] Find your stuff
User Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:46.0) Gecko/20100101 Firefox/46.0
Build ID: 20160502172042

Steps to reproduce:

Third content item for the utility track will introduce users to the awesome bar in Firefox

Flow:

User clicks on add-on icon
Side bar is opened containing content
User clicks "try it now"
Highlight is produced over awesome-bar
User receives the relevant badge.