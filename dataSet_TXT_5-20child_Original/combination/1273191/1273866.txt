[All-Aboard] Content schedule
The on-boarding add-on consists of a total of 10 pieces of content, excluding /firstrun, snippets and the import data content. These 10 pieces are divided equally between the two cohorts i.e utility track and values track

These individual items will be shown to the user on a 24 hour schedule until all content has been shown to the user. If a user does not use their browser as often, the content will be shown the next time they open Firefox.