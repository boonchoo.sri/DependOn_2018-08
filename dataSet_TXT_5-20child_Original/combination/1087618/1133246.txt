Buttons aren't aligned properly with content on about pages using info-pages.css
The following code fixes the issue :

.button-container > button:first-child {
  -moz-margin-start: 0;
}