Tabcrash icon resize when changing Firefox width
Created attachment 8558484
Screencast showing the issue

Reproducible in builds:
- Latest Nightly

Reproducible on OSs:
- Windows 7 64-bit
- Ubuntu 14.04 32-bit
- Mac OS X 10.9.5

STR:
1. Open Firefox.
2. Load about:tabcrashed.
3. Resize Firefox width.

Expected results: The icon does not change size and it disappears after a while.

Actual results: The icon increases size after a while then it disappears.

Notes:
1. This is a regression 

m-c:
Last good nightly: 2015-01-31
First bad Nightly: 2015-02-01

Last good revision: d7e156a7a0a6
First bad revision: c2359a6a6958

https://hg.mozilla.org/mozilla-central/pushloghtml?fromchange=d7e156a7a0a6&tochange=c2359a6a6958

m-i:
Last good revision: d0c5e3607389
First bad revision: c2359a6a6958

Pushlog:

https://hg.mozilla.org/integration/mozilla-inbound/pushloghtml?fromchange=d0c5e3607389&tochange=c2359a6a6958

Probably caused by: bug 1112304

2. Reproduces on all platforms.