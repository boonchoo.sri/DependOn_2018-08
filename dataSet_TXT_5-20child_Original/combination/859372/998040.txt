Move the webapps actor to a SDK module
In order to help loading actors lazily (bug 988237),
we should move the webapps actor to a SDK module.
There is no rush in converting it to protocol.js,
we can start by just loading it as a module, like tracer.js.