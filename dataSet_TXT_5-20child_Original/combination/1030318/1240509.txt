Browser mochitest using focus() fails after previous test used EventUtils on Linux opt x64 e10s
Created attachment 8709018
focus-issue-testcase.diff

Follow up to Bug 1240368. Highlights intermittent test that depends on a previous test execution.

STRs

- pre : e10s only
- pre : Linux 64 opt only
- intermittent test : try on a slow VM, or slow it down manually to increase the chance of failures
- import focus-issue-testcase.diff
- run ./mach mochitest --e10s devtools/client/focusbug/test/browser_focusbug*
- (repeat a few times)

Actual : 

From time to time the second test will fail on with the following error message.

> The following tests failed: 17 INFO TEST-UNEXPECTED-FAIL | devtools/client
> /focusbug/test/browser_focusbug_02.js | The container is now focused. - Got 
> null, expected [object XULElement]

Expected : 

I assume nothing is wrong with the tests so : tests should always pass.
Unless anything _needs_ to be done to avoid the first test causing a crash of the second one.

===========================
Investigation : 

Running focusbug_02 independently never fails.

Some more details in Bug 1240368, but :

Waiting in the beginning of focusbug_02 fixes the issue. 

Triggering a "mousedown" event using EventUtils and waiting for the focus also fixes the issue (however I'm guessing that EventUtils is triggering events asynchronously so this might just work for the same reason as a simple "wait").

I tried checking if focus() might be asynchronous here, but trying to wait for a focus event to be raised after calling focus() never resolves.

Also while isolating the issue, I managed to have the same kind of failures in focusbug_02 when I simply had a js error in focusbug_01.

Finally, simply using a focus() in focusbug_01 has no impact on focusbug_02. Only had the issue when relying on EventUtils.