Some favicons are more pixelated in Firefox than in Chrome and Edge on Retina Macs and HiDPI Windows
Created attachment 8739183
faviconffchrome.png

User Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0
Build ID: 20160315153207

Steps to reproduce:

Used Firefox on a Surface Pro 4 (I assume this issue is not specific to the SP4 though, it's just the only computer I can test on right now)


Actual results:

I've noticed on the Surface Pro 4 that a good number of favicons in the tab look worse than they do in Chrome. It looks like they aren't scaling as nicely as they do in Chrome. I've attached a screenshot of some examples


Expected results:

Favicons should look normal