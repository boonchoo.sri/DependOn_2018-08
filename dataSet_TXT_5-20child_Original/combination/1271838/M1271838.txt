[QX cluster] Consider improvements for hidpi support
We've made significant progress on support for hidpi display devices, most primary UI has been updated but it's still easy to notice things that should be improved.

* A major area is support for storing hidpi favicons -- bug 492172. Icons cached in the favicon service are generally only available in 16x16 size.

* Supporting @sizes in <link rel=icon> will let us use hidpi icons in more cases (bug 751712).

* The tab curve is still not quite right on hidpi -- bug 995733

* Finishing up hidpi theme work. We need to do some triage in this area, as there are a number of outdated and cross-purpose tracking bugs. The most relevant starting point bug I know of (since I did it ;) is bug 1016543.

* Convert Toolbar.png to SVG, so it scales properly on more display sizes, is easier to update, and simplifies the UX team's workflow. (bug 1054016)