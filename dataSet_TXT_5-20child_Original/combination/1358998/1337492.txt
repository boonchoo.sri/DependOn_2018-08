Late loading of identity icon makes Firefox feel less responsive
Some hover effects on buttons in Firefox seem to load the hover graphic only once the mouse moves over the button. That leads to a white flash when hovering a button for the first time.

The most prominent example is the i-button in the URL bar, but I can swear that I've seen this in a few other places as well.

STR:
- Quit Firefox
- Start Firefox
- Move the mouse over the i-icon in the URL bar

Expected result:
- The hover state gets shown immediately

Actual result:
- There is a short flash where the icon disappears before the hover graphic is loaded. This flash lasts even longer if the machine is busy (e.g. while loading Activity Stream)