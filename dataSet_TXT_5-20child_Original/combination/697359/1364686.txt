Use about:downloads to show downloads also in a not-private window
Use about:downloads in the Downloads Panel and Menu Bar to show downloads also in a not-private window.