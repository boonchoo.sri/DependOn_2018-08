Bookmark properties dialog needs tagging UI
Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9b3pre) Gecko/2008010705 Minefield/3.0b3pre ID:2008010705

When you want to modify the bookmark properties within the bookmarks sidebar you are not able to add/remove tags because no UI exists yet. Fields (like the details deck inside the Library) should be added to be able to modify the tags for that bookmark.