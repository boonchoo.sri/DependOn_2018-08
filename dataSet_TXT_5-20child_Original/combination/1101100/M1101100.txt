[e10s] Make “Save As… Complete Document” work in e10s tabs
Splitting bug 1058251; that will cover basic ”Save As” functionality, and this will cover getting the “Complete Document” mode (and by extentsion nsIWebBrowserPersist) working for e10s.

+++ This bug was initially created as a clone of Bug #1058251 +++