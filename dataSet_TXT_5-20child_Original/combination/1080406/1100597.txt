Green star icon that shows when new version is available after browser restart is only shown in first browser window
When a new version is avialable, the hamburger icon shows a green icon with a star in it, to emphasize that I can get a new version by restarting the browser.


This green update/restart icon only shows in the main window.


STR:
1) Open Firefox, create a new window (e.g. CTRL+N)
2) Wait for an automated update to be downloaded and prepared in the background or trigger through Help/About
3) The icon shows only in the first window