Support per-window private browsing in the Downloads Panel
We need to support per-window private browsing mode in the new downloads panel.  The use case that we want to support is rather simple:

* Never add downloads initiated in a private window anywhere else (not in the Library window, nor in the downloads panel for other private windows
* Always keep the downloads initiated in a private window visible in the download panel belonging to that window.  That is, we should never set a maximum number of downloads to show in the private window's download panel, because there is nowhere else for that download to go.

Hint: to get a per-window PB build (still in the experimental stages), add "export MOZ_PER_WINDOW_PRIVATE_BROWSING=1" in your .mozconfig (or add that variable with a value of 1 to browser/confvars.sh).

Mike, do you think you're going to have the bandwidth to work on this?