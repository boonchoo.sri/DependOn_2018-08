Inspector doesn't understand pseudoclasses
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160526082509
STR_1:  (:first-child, :nth-child())
1. Open   data:text/html,<!DOCTYPE html><input type="checkbox" checked>
2. Open inspector, focus search field in inspector
3. Type "input:first-child" or ":nth-child(1)", press Enter

AR:  No node becomes highlighted. Search field in inspector says "No matches"
ER:  Inspector should cycle through found elements


STR_2:  (:checked)
1. Open   data:text/html,<!DOCTYPE html><input type="checkbox" checked>
2. Open inspector, focus search field in inspector
3. Type ":not(:checked)", press Enter 3 times

STR_3:  (:hover)
1. Open   data:text/html,<!DOCTYPE html><input type="checkbox" checked>
2. Open inspector, right-click <input> element in markup, enable :hover in context menu
3. Focus search field in inspector, type ":hover", press Enter 3 times


This is regression from bug 835896. Regression range:
> https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=43f23a377c8510f29aa9546453b3b3bcb7810b07&tochange=437545b472e74a71ca112f4b4a300ae0a646288b@ (Unavailable until Jan 4) Brian Grinstead [:bgrins]:
It seems that this is a regresion caused by your change. Please have a look.