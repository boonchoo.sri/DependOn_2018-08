Inspector "Search HTML" finds ::before pseudoelements as text, but not by selector
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160526082509
STR_1:
1. Open url   data:text/html,<style>body:before{content:"z"}
2. Open devtools -> inspector
3. Type "::before" in search field "Search HTML", press Enter

AR:  Inspector finds ::before pseudoelement. "There's no "::before" in HTML, so it must be selector"


STR_2:
1. Open url   data:text/html,<style>body:before{content:"z"}
2. Open devtools -> inspector
3. Type "body::before" in search field "Search HTML", press Enter

AR:  Inspector doesn't find anything


ER:  Either X or Y.  X is better
 X) Inspector should search for "body::before" as selector
 Y) Inspector shouldn't find "::before" to avoid confusion, just like GoogleChrome does