Source Editor: add a method to convert mouse coordinates to character offsets
Once we expose the Orion mouse events we also need a way to convert the mouse coordinates to character offsets.

As a bonus we also need a method to find the line index from a given character offset.

(both methods are available in Orion, we just need to expose this API in the Source Editor)