[a11y] Improve accessibility of animation inspector.
There are several accessibility issues that we must address:

* Missing labels for:
  * pick element icon
  * pause button
  * seek backward and play buttons

* Timer is not accessible
* Animation track is only exposed as least without any meaningful name value description etc.