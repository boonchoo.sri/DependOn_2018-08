[Regression] Click the site permissions icon, then click the plugin indicator, doesn't open the plugins notification
I have a problem with Firefox Beta 52. It doesn't happen in Firefox ESR 45.
Sometimes clicking on plugin indicator, and permissions like push/geolocation doesn't work.
It happens unpredictably, however, I noticed one specific scenario when it happens

1. Open http://dagobah.net/flashswf/CatGotLost.swf
2. Click on doorhanger in location bar
3. Click on plugin indicator in location bar

Result: Panel with site info closes
Expected: Panel with site info should close and panel with plugin info should open

Same happens with permissions like push/geolocation

https://gauntface.github.io/simple-push-demo/
http://dagobah.net/flashswf/CatGotLost.swf
http://html5demos.com/geo