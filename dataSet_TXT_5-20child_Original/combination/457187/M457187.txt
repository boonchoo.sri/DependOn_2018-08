Make the tabs toolbar customizable
Steps to reproduce:

1. Go View > Toolbars > Customize 
2. Attempt to drag and drop the new 'new tab' button
3. Nothing happens

Expected Results:

Should at least be able to place each side of the tab bar or in the nav bar. Having static items all around Firefox make it awkward for the user.