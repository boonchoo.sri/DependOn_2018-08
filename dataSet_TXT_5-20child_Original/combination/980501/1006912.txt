Web Audio Editor needs to be destroyed and recreated after used
From the ContentObserver refactoring I think, the 'start-context' event doesn't get emitted from the web audio actor when refreshing.

Currently, the first node created fires the start-context event, as well as a node for the AudioDestination. Since we can't hook into constructors with ContentObserver/CallWatcher, this was done instead of listening for the actual AudioContext creation. The front and back end needs to be rewritten to handle persisting across reloads.