Allow progressive load of lines in rule and computed views
In the Rule and Computed views we should display properties one by one, maybe with a fast animation (top to bottom) rather than waiting for all the rules before we show them.

Filter on EdgarMallory