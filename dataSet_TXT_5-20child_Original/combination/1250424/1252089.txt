Properly report in the crash metadata which sanitization promise we are currently blocked on
At the moment we report which sanitizations completed, but we don't report which sanitization we are waiting.
Would be useful to know what is blocking when we abort, to direct investigations.