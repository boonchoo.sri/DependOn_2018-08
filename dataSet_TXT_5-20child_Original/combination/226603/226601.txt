Need Application Icons for Firebird
We need a consistent application icon set for Windows and MacOS X. (Linux can
ride along with Windows, probably). 

This bug basically just requires one icon, but in various sizes for Windows:

48x48, 32x32, 16x16 - true color, 24-bit color, 16-bit color, 256 color, 16 color. 

and one icon resource for MacOS X: 

128x128 true color (the OS does the scaling)

The constraints are as follows...

- the icons must feature the same motif across platforms
- the MacOS X icon may adopt an "Aqua" look... this should be done in a way
that's slicker than just sticking the windows icon in a bauble. (sorry kerz ;-)