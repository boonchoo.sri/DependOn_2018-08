[UX] Should doorhangers be less transient under some circumstances?
Under certain circumstances:

Bug 1004055 - Users don't know where the doorhanger went
Bug 1004061 - doorhanger should persist if the user switches app and back to the browser again

disappearing doorhangers seem to be causing some interaction problems. In these cases it is possible that:

(a) the interactions should be using something other than doorhangers
(b) doorhanger behaviour needs to become more nuanced to better support these interactions

I think this all relates to Bug 1064257 - [UX] Unify and improve behavior of doorhanger dialogs, incidentally.