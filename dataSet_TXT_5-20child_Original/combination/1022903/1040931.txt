API addition: add extensionID parameter to addEngineWithDetails
Because packed extensions aren't supported for bundled extensions, it's become common to use addEngineWithDetails as a workaround.  However, this effectively means those engines won't be removed when the add-on is, which isn't the right user experience (and is a potential form of abuse).

To manage this with minimal pain, we should add (and require by policy for now) that add-ons calling this method must also pass in their correct add-on ID.  The search service should then watch for add-on notifications and remove added plugins if/when the plugin is removed or blocked.  In addition, we should validate that the add-on ID is still present whenever we're regenerating search.json.

Nominating for backlog based on discussion with Gavin, and cc-ing Jorge/Kris so they know this is coming.  Probably won't happen in 33, but should ideally be in an early 34 iteration.