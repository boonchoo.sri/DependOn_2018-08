Polish the history view
(In reply to Thomas Stache from bug 888572 comment #15)
> Would it be possible to add some (sub)section headers to the view? It now
> looks funny with some menu items, page titles, more menu items, page titles
> - only structured with separators. It's hard to make sense of it all.

Additionally, it seems we should be setting the favicon size for these and aren't, because on my Windows machine I see double-size icons for some of the items in the recently closed tabs/windows list.