Add test to ensure there are no uninterruptible reflows when loading about:newtab
With bug 1071635 fixed we should add a test to ensure we don't regress this.