Investigate lazily loading tiles that aren't immediately visible on low resolutions
The way newtab page current displays tiles is that all 3x5 potential tiles are immediately created even if they don't all fit the current window. But if the user resizes the window to be larger, the tiles are immediately available as they were hidden by css overflow hidden.

Potentially we could lazily create the tile and related nodes only when it would be visible. This could help in both animation performance and memory usage as there's less content to try to render then have hidden.