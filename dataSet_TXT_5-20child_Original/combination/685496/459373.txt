Warning for page redirection should show target URL
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3

Firefox implements a warning message when a page uses META REFRESH to redirect or refresh the page. See http://kb.mozillazine.org/Accessibility.blockautorefresh

Problem: One does not know where the page is going to be redirect to.

Reproducible: Always

Steps to Reproduce:
1. Enable checkbox labeled “Warn me when web sites try to redirect or reload the page”, located under “Tools → Options → Advanced → General → Accesibility”. 
2. Go to above URL
3. 



Solution: Warning message should contain target URI to which one gets redirected after clicking "allow".