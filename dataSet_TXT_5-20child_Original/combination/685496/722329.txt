Redirect doesn't allow to open popup window (Redirect via JavaScript doesn't allow to set site permissions)
Hello,

I encounter a problem in Firefox 3.0.1 and in Firefox 9.0.1. On Windows.

My pref “Warn me when web sites try to redirect or reload the page” is ticked. Firefox' help says : “When this preference is enabled, Firefox will prevent websites from redirecting you to another page, or automatically reloading.” Unfortunately, it does not work in all cases.

I want my bus timetable. I have a Firefox window. I go to http://www.ratp.fr/informer/pdf/horaires/pivot_horaire.php?reseau=busratp&service=THL&ligne=290 .

Expected behaviour :

The popup yellow bar appears. I click on the “Options” “buttenu” — see request bug 688130. I choose the blocked address. I get my bus timetable.

Actual behaviour :

The popup yellow bar appears, and disappears very quickly. I don't have the time to understand what has appeared, let alone to click on “Options”. I land on a page where I don't have any access to my bus timetable. I have been redirected, although I did not allow it. I try again, several times, but with a variant : after I request the address, I press Esc Esc Esc Esc Esc Esc Esc Esc Esc Esc Esc Esc very quickly. At last, I get the popup yellow bar to stay, I click on “Options”, I choose the blocked address, and I get my bus timetable. This is not very accessible — to say the least.

From the W3C, I read the techniques for the guideline “3.5 Toggle automatic content retrieval (P1)” : www.w3.org/TR/UAAG10-TECHS/guidelines.html#tech-configure-content-retrieval :

“Allow configuration so that the user agent only retrieves content on explicit user request.”

See the W3C's User Agent Accessibility Guidelines 1.0 : http://www.w3.org/TR/UAAG10/guidelines.html#tech-time-independent : “2.4 Allow time-independent interaction (P1)”.

Thanks for correcting that.

Nicolas