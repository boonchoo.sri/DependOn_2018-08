Option "Warn me when web sites try to redirect or reload the page" does not work correctly. Does not handle more frames.
User-Agent:       Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.12) Gecko/20080201 Firefox/2.0.0.12
Build Identifier: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9b4) Gecko/2008030318 Firefox/3.0b4

It seems that the option "Warn me when web sites try to redirect or reload the page" can only handle one meta refresh element per document. If a document is a frameset or is using iframes and the referenced frames are containing meta refresh elements, then only one refresh gets executed.

Tested with Firefox 3.0b4 from [0] and the nightly build 2008-03-20-04-trunk from [1].

[0] http://www.mozilla.com/en-US/firefox/all-beta.html
[1] http://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/2008/03/2008-03-20-04-trunk/


Reproducible: Always

Steps to Reproduce:
1. Extract testcase.zip.
2. Open testcase/testcase.html.
3. All meta refreshes are executed after the defined time-outs. See testcase-1.png.
4. Enable the option "Warn me when web sites try to redirect or reload the page" under Edit -> Preferences -> Advanced -> General -> Accessibility.
5. Open testcase/testcase.html again.
6. A toolbar is now displayed at the top of the document. See testcase-2.png.
7. Press the allow button.
8. Only one meta refresh is executed immediately. See testcase-3.png.