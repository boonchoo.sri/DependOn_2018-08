Get Firefox to launch with the new .app bundle structure
Get Firefox to launch with the new bundle structure, mostly by changing the GRE and XRE directory to point to the Resources directory instead of MacOS.

Note that this also requires the patch in bug 1048687.