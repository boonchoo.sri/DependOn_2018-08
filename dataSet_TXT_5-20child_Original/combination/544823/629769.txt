Need new default theme preview images for the add-ons manager
User-Agent:       Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b11pre) Gecko/20110128 Firefox/4.0b11pre
Build Identifier: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b11pre) Gecko/20110128 Firefox/4.0b11pre

Current screenshot of default theme is using old GUI. This needs to be updated to reflect new GUI in Firefox 4.

Reproducible: Always