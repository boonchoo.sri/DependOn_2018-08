[e10s] Searching from a new tab's address bar does not work the first time when e10s is enabled
Story
Enter a search text into the urlbar
Google is selected in seach bar

Result
Errorpage, Message URL is not valid

Expected Result
Search & Display of seach results with searchengine currently selected in searchbar

Reproducible: Always on Build Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:36.0) Gecko/20100101 Firefox/36.0 ID:20141103030205 CSet: 0b81c10a9074