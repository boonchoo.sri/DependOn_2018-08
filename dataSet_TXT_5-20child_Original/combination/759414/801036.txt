Keyboard shortcut to focus the Social API sidebar
It is currently possible to Tab to the Social API sidebar, tabbing through the whole browser, but that is probably too tedious of a solution for keyboard users.

It would be nice if we had a keyboard shortcut that could move focus into the Social API sidebar.