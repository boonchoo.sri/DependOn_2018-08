Split up the "Older than 6 days" history folder
User-Agent:       Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a7pre) Gecko/2007073104 Minefield/3.0a7pre
Build Identifier: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a7pre) Gecko/2007073104 Minefield/3.0a7pre

Bug 332748 made me think of filing this bug. There may be several usability issues with the "Older than 6 days" folder:
1. it looks weird to have one folder per day, then one folder for 174 days.
2. expanding it takes a long time (bug 385245)
3. it contains thousands of entries so it's nearly useless to scroll down in the hope to find /the/ page you visited x months ago.

It'd be cool if history folders were dynamically created on the basis of browser.history_expire_days, or if a, say, 3-month timeframe was used:
+ 1 week ago
+ ...
+ 1 month ago
+ ...
+ 3 months ago

Reproducible: Always

Steps to Reproduce:
1.
2.
3.