improve history sidebar view configuration
* search is broken out into it's own bar
* add a "view" bar
* add a dropdown for selecting which items to view
** "tagged items", "history"
* the old "view" dropdown is adjacent to it
* allow saved searches, as in the bookmarks sidebar?

alex: what did you have in mind for the "plus" button?