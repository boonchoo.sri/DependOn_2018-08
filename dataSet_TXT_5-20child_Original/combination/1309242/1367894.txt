Refactor the JSON Viewer stream converter
Created attachment 8871441
json-stream.patch

The stream converter is completely buggy. This patch fixes

 - Bug 1364794, bug 1224071, bug 1327752, bug 1327753, and probably bug 1365804
   (big files became corrupted)
 - Bug 1364953 (NUL truncated the source)
 - Bug 1367881 (UTF-8 was not enforced)

(tests should be added in the respective bugs)

The problems are
 - With the approach I used, I couldn't find a simple way to trigger standards mode instead of quirks one. I thought about document.write("<!DOCTYPE html>") but that breaks various other things and gets so messy.
 - Adding the stylesheets at the end causes relayout. And the font-size changes, which is ugly. Maybe inserting a Link header could fix this.

I think it's worth it anyways, the problems can be addressed in other bugs.