[Win] Tabs and menubar in the titlebar for restored windows
Created attachment 683793
WIP 1 - styling changes

On XP the titlebar needs to be behind the tabs when tabs are on top. This will require changing TabsInTitlebar (in browser.js) to work when sizemode != "restored" along with some theme changes.

XP Designs:
* Luna: http://people.mozilla.com/~shorlander/files/australis-designSpecs/australis-designSpecs-windowsXP-lunaBlue-mainWindow.html
* Silver: http://people.mozilla.com/~shorlander/files/australis-designSpecs/australis-designSpecs-windowsXP-silver-mainWindow.html
* Olive: http://people.mozilla.com/~shorlander/files/australis-designSpecs/australis-designSpecs-windowsXP-olive-mainWindow.html