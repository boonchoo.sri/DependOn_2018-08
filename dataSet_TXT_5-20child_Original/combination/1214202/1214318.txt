Create a TreeList Widget
Lists can be trees that are all top level items. I think this widget should handle both scenarios. We should have a consistent way of rendering our trees and lists with default rendering for consistency, as well as able to be extensible for our current uses. We have a many implementations (abstract tree widget, treewidget.jsm, variablesview, JSONResponseViewer, and Heap Tree View from memory tool prototype), that can be implemented with the same core and extended as needed.

VariablesView may be it's own thing and should not be migrated to this.

Current table usage:
* Storage Tool (TreeWidget.jsm)
* Resources/new StyleEditor view (Alex working on?)
* JIT Optimizations (TreeWidget.jsm)
* Webconsole (custom)
* VariablesView in webconsole, web audio, storage, scratchpad, tooltips?, netmonitor, debugger, webide
* Debugger (global search, watch expressions, results panel) using SimpleListWidget
* Performance tool and a few others use SideMenuWidget.
* JSON Response Viewer (custom)
* Performance tool waterfall view, Abstract Tree Widget
* Performance tool call tree, allocations, memory tool heap view all use Abstract Tree Widget, but are table-tree views and should be handled differently (?)

New TreeList widget requirements
* Expandable/collapsible rows
* Filter data via predicate
* Extensible to support complex scenarios like waterfall view