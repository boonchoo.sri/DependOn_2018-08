[meta] Devtools Widget Unity
Copied from the thread on the mailing list[3]:

We have a range of widgets with overlapping/duplicated functionality, some
rotting with no updates, and many fixes/polish for tools are in these
widgets themselves. To have a cohesive widget story that’s shared across
our tools would be a solid win, ideally sharing features across tools for
free (imagine if all of our table views were instantly sortable/resizable!).

Some potential goals this endeavor would achieve are:
* Remove/reduce neglected current widgets
* Share widget features across tools
* Any refactoring/new widgets have an opportunity to kill XUL in a component
* New components we do not have and need (resizable/sortable table, only
render in view list)
* Take advantage of React/redux combo on tools that use it for differential
rendering

There are a few ways we could go about this — based on an audit of our
widgets, most of the duplicated code revolve around tree, lists and tables.
Lists can be thought of as a subset of trees, whereas adding table
functionality can extend either. Whether or not these all should extend
from the same core widget, not sure. To go about this process, we should
make/choose a widget that solves the issues, and begin wiring it up to
current tools, dropping depreciation messages in current widgets, and
ultimately remove them. We have a lot of cruft here.

At the bottom is an audit of implementations and consumers for each of the
types (tree, list, table). This was spawned from a discussion[0] about
adding sortable/resizable columns in the performance call tree, which isn’t
possible because it’s not really a table. In terms of tree-table
requirements based on our current bugs across several tools, the following
would achieve what we need:

* Only render visible items
* Custom line items for each consumer of widget
* Tree capabilities (expand/collapsible)
* Table capabilities (sortable, resizable, reorderable?)

For the tree implementation, the one that seems the most complete in all
the functionality above, except table capabilities, fitzgen’s Tree in the
initial memory tool prototype from years ago[1]. If we can identify a
robust/flexible way to implement a table-tree widget, it would handle the
table widget problem (all top level tree items), as well as the list widget
(all top level tree items, single “column”), and a normal tree widget
(single “column”).

If this sounds good and like something we want to tackle in the future,
let’s talk about how we’re going to do just that, and I’ll make some bugs.
I love making bugs.

*Widget Audit - List Widgets (4 implementations, 5 current consumers)*
* webconsole (custom)
* Netmonitor (custom)
* FastListWidget
  * Vestigial includes in Debugger (can be removed)
* SimpleListWidget
  * Debugger (global search, watch expressions, results panel)

*Widget Audit - Tree Widgets (5 implementations, 14 current consumers)*
* Abstract Tree Widget
  * Performance (call tree/allocations, waterfall)
* TreeWidget
  * storage, jit optimizations
* VariablesView
  * Webconsole, web audio, storage, scratchpad, tooltips?, netmonitor,
debugger, webide (manifest)
* JSONResponseViewer Tree?[2]
* Heap Tree View (fitzgen’s patch on memory tool prototype)[1]

*Widget Audit - Table Widgets (1 implementation, 2 consumers)*
* Table Widget
  * storage, webconsole (console.table?)

[0] https://bugzilla.mozilla.org/show_bug.cgi?id=1201742
[1]
https://bugzilla.mozilla.org/attachment.cgi?id=8487436&action=diff#a/browser/
[2] https://bugzilla.mozilla.org/show_bug.cgi?id=1132203
[3] https://groups.google.com/forum/#!topic/mozilla.dev.developer-tools/hH_VBUx_rZA