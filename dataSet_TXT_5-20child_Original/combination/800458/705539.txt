Firefox hangs and has to be forced to close after closing out program
User Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:8.0.1) Gecko/20100101 Firefox/8.0.1
Build ID: 20111120135848

Steps to reproduce:

On  my new Mac Book Pro, I went to Firefox, Quit Firefox. 


Actual results:

Once I do this, I get the little multicolored beachball, and it just hangs.  This happens each time I close it out, although it only started about 4 days ago.  I once left it hanging just to see how long, or if, it would resolve eventually, after 30 min I gave up and forced it to quit.  I get no Firefox error report window, but I do get a report to Apple window,  which usually gives  me an error report.  


Expected results:

Firefox should have closed out and I should have been able to close down my machine.