Broken Shift+F2 key shorcut for gcli
You get this exception:
  TypeError: window.DeveloperToolbar is undefined

Looks like a rebase issue of bug 1359855 over bug 1382661.