missing page and image "Properties" menu item
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.3a1pre) Gecko/20090908 Minefield/3.7a1pre
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.3a1pre) Gecko/20090908 Minefield/3.7a1pre

Right click context menus for images and web pages have no Properties item

Reproducible: Always

Steps to Reproduce:
1. Right click page or image
2. Look at bottom of menu
3. No Properties! :(
Actual Results:  
No properties menu item

Expected Results:  
Properties menu itme

Bring it back please? I miss it for images especially as I want to see location and dimensions of an image