permissions UI for social content
When working on the WebRTC in Social API demo (https://github.com/anantn/socialapi-demo) we had to completely turn off the permission UI with the media.navigator.permission.disabled preference because the UI didn't appear when requesting access to the camera/microphone from a SocialAPI iframe. 

For reference, the permission UI for regular web pages has been added in bug 729522.