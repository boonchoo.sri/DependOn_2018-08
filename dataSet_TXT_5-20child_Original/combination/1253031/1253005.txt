Clearing "Offline Web Content and User Data" does not clear Service Workers or their caches.
STR:

1. Visit https://www.pokedex.org/
2. Preferences -> Advanced -> Network -> Offline Web Content and User Data -> Clear Now

What should happen:

- The service worker is unregistered
- All cached data is cleared

What actually happens:

- The service worker is still visible in about:serviceworkers
- All data cached by the service worker is still present

To verify that cached data persisted, visit https://www.pokedex.org/manifest.json and run the following in the console:

    caches.match('/img/icon-48.png').then(x => console.log(`Resource ${(x && x.ok) ? "WAS" : "WAS NOT"} found in cache:`, x))