Setting Firefox to clear all history on exit does not clear Service Worker caches.
STR:

1. Enable "Use custom settings for history" in Preferences -> Privacy.
2. Enable "Clear history when Firefox closes"
3. Settings... -> Set Firefox to automatically clear everything
4. Visit https://www.pokedex.org/
5. Quit Firefox

What should happen:

- The service worker is unregistered
- All cached data is cleared

What actually happens:

- The service worker is unregistered
- All data cached by the service worker is still present

To verify that cached data persisted, visit https://www.pokedex.org/manifest.json and run the following in the console:

    caches.match('/img/icon-48.png').then(x => console.log(`Resource ${(x && x.ok) ? "WAS" : "WAS NOT"} found in cache:`, x))