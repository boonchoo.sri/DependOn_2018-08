Move gDevTools.jsm to a commonjs module
As said in bug 1172010 comment 2, there is way to shuffle gDevTools.jsm into one or multiple commonjs modules. It may relates to bug 1188401 works also.

We should move gDevTools to a module so that its content it automatically re-evaluated when we reload devtools codebase via `tools reload` gclit command!