Pocket e2e test
The simple test should:

Prereqs:
- Create a new pocket account via fxa

- Get the api key for that account from getpocket.com

e2e test
- Login to pocket via the test pocket/fxa account

- Navigate to a website (any news site or similar) and add an article using the "Add to Pocket" toolbar button

- Verify the item (url) was added to pocket via an api query

Pocket API docs:
https://getpocket.com/developer/docs/authentication
http://getpocket.com/developer/docs/v3/retrieve

Looking through the docs, it may be easier for us to just log in to https://getpocket.com/login using selenium or marionette...