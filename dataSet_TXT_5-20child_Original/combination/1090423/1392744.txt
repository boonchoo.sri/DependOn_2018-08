Support opening a Browser Toolbox for more than one running Firefox instance
STR:

./mach run --profile /tmp/profile1
Open Browser Toolbox
./mach run --profile /tmp/profile2 --setpref devtools.debugger.chrome-debugging-port=6081
Open Browser Toolbox

Expected:
The second Browser Toolbox targets the second process

Actual:
The second Browser Toolbox targets the first process