Browser Toolbox Element Picker doesn't pick nodes in menus
STR:

1. Latest Nightly (only version I've tried at least)
2. Open Browser Toolbox
3. Click element picker button to activate pick mode
4. Switch to main browser window (alt+tab)
5. Open any menu using the keyboard (context menu, alt+something...)
6. Try to hover and click on the menu

The menus are completely unresponsive to the mouse movement, and the clicks go through to whatever node is beneath them.

I would expect the clicks to pick the menu item node clicked itself.

Brian, CC'ing you because you asked in the message to firefox-dev what keeps us using DOMi. Well, this keeps me using DOMi. :)