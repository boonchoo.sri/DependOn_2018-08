'Bookmark all tabs' dialog is broken
User Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0
Build ID: 20170721030204

Steps to reproduce:

1. Download latest Nightly (20170721030204) with clean profile
2. Open it, should be 3 first-run tabs
3. Right-click and select "Bookmark all tabs"
4. Dialog has only "Add bookmark" button"
5. Click button


Actual results:

Folder named '[Folder name]' was created in Bookmarks Menu. No bookmarks present in folder.


Expected results:

Dialog should have folder selector and text box for folder name. Created folder should have the 3 bookmarked tabs.