[OSX] Buttons in doorhanger notification should be themed as bookmark star panel's buttons in location bar
User-Agent:       Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.4; fr; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10
Build Identifier: 

For consistency in the OSX UI the theming of buttons in the panel of the bookmark star button and buttons in doorhanger notification should be themed in the same way.

I've never seen a white button on OSX, or at least (I don't rmember well) an as white as the current one in doorhanger's buttons, so it seems to me strange.

Reproducible: Always