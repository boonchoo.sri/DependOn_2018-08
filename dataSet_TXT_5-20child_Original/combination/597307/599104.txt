URL progress moves LTR in RTL mode
Created attachment 478048
Conflicting Progress Screenshot

The new URL progress bar moves LTR when in RTL mode while background tab progress moves RTL.

STR:
- Put browser in RTL mode
- Navigate or refresh a page
- Watch progress go LTR
- Refresh a background tab
- Watch progress go RTL

Expected:
- All progress bars should move RTL