Exceptions dialog updates live when a site is added to the list, but doesn't update when a site is removed
46.0a1 (2016-01-11) Win 7
STR:
1. Set privacy.trackingprotection.ui.enabled=TRUE
2. Open FF, go to about:preferences#privacy
3. Open Exceptions and leave it open
4. In a new tab, open cnn.com
5. Open the control center and disable protection for cnn.com
6. Switch back to tab 1 -> cnn.com added to the list
7. Switch to cnn.com and enable protection in control center
8. Switch back to tab 1

Actual results:
cnn.com still displayed in the list

Expected results:
cnn.com should be removed from the list