Dim the main view when the subview is shown in the Control Center
[Note]:
- Screenshot: http://i.imgur.com/v4NHRxc.png

[Affected versions]:
- latest 52.0a1 Nightly
- latest 51.0a2 Aurora

[Affected platforms]:
- Windows 10 x64
- Ubuntu 14.04 x86
- Mac OS X 10.11

[Steps to reproduce]:
1. Open Firefox
2. Go to https://people.mozilla.org/~fqueze2/webrtc/
3. Select "Audio & Video" and share the devices.
4. Click on the site identity panel
5. Click on the ">" icon to view more information about the connection.

[Expected result]:
- The clear permission buttons are not displayed.

[Actual result]:
- The clear permission buttons are shown.

[Regression range]:
- This is not a regression.