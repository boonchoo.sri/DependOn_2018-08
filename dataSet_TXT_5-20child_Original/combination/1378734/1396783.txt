devtools/client/inspector/inspector.js loads too many dependencies
It takes 55ms/15% of JS computing in the parent process.

With the upcoming patch it drops down to 4.4ms/1.6%.

Profile without the patch:
  https://perfht.ml/2iYWNDJ

Profile with the patch:
  https://perfht.ml/2eB0Z7w

Again, as for bug 1396619, various deps are loaded later, so the win isn't necessary the pure difference that I highlight here.

http://searchfox.org/mozilla-central/source/devtools/client/inspector/inspector.js#15-41
Most of this can be lazy loaded and especially the one that aren't used in Inspector constructor, nor init method. There is even value in lazy loading deps only used in deferredOpen.

It allows calling Inspector.init sooner and so run necessary action like initCssProperties in the content process sooner/in parallel.