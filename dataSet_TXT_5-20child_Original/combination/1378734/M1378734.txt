[meta] Use the profiler to optimize DevTools startup
Use the profiler to get more granular information about hotspots and create workarounds… we should pay special attention to loops, especially when they call code that loops.

Filter on EdgarMallory