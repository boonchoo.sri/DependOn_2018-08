Tab-resize occurs immediately after leaving tab-bar when a persona or theme used.
User-Agent:       Mozilla/5.0 (Windows NT 5.1; rv:6.0a1) Gecko/20110423 Firefox/6.0a1
Build Identifier: 

In bug 465086, it is implemented that tab resize should not occur until cursor leaves tab toolbar.

When using default theme, tab resize doesn't happen until pointer leaves tab-bar and url-bar both, like Chrome.

But when a theme or a persona is used. tab resize occurs immediately adter pointer leaves tab-bar. Tab resize occurs when pointer leaves tab-bar and goes address bar (url-bar). 

Reproduced in Aurora and Nightly in Windows 7.

Reproducible: Always

Steps to Reproduce:
1. Wear a persona or install a theme on Firefox.

2. Open a couple of tabs in browser. 

3. Close 2-3 tabs from the middle and make cursor go to the address bar.


Actual Results:  
Tabs widths are resized immediately.

Expected Results:  
Tabs should not be resized since pointer didnt leave the whole toolbar.