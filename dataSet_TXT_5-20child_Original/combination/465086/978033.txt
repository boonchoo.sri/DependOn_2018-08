Using custom tab widths breaks bug #465086
Using this code 

.tabbrowser-tab:not([pinned])[fadein]{ min-width: 54px !important; }

or

.tabbrowser-tab:not([pinned]) {
  max-width: 250px !important;
  min-width: 54px !important;
}
.tabbrowser-tab:not([pinned]):not([fadein]) {
  max-width: 0.1px !important;
  min-width: 0.1px !important;
}

in custom userChrome.css breaks bug #465086 as tabs resize when I closing a tab with the mouse instead of maintaining the same size until the mouse exits the tab bar area.
What's more the same thing happens when you add this code in add-on.