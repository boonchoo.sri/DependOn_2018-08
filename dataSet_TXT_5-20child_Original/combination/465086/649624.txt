tab.style.maxWidth set by add-ons should be respected
User-Agent:       Mozilla/5.0 (Windows NT 5.1; rv:6.0a1) Gecko/20110413 Firefox/6.0a1
Build Identifier: Mozilla/5.0 (Windows NT 5.1; rv:6.0a1) Gecko/20110413 Firefox/6.0a1

http://mxr.mozilla.org/mozilla-central/source/browser/base/content/tabbrowser.xml#2919

2919       <method name="_unlockTabSizing">
2920         <body><![CDATA[
2921           this.tabbrowser.removeEventListener("mousemove", this, false);
2922           window.removeEventListener("mouseout", this, false);
2923           if (this._hasTabTempMaxWidth) {
2924             this._hasTabTempMaxWidth = false;
2925             let tabs = this.tabbrowser.visibleTabs;
2926             for (let i = 0; i < tabs.length; i++)
2927               tabs[i].style.maxWidth = "";
2928           }
2929           if (this._usingClosingTabsSpacer) {
2930             this._usingClosingTabsSpacer = false;
2931             this._closingTabsSpacer.style.width = 0;
2932           }
2933         ]]></body>
2934       </method>

Reproducible: Always