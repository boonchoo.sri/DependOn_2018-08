Tiles freeze after first drag and drop of unpinned tiles on fresh profile
User Agent: Mozilla/5.0 (Windows NT 6.1; rv:41.0) Gecko/20100101 Firefox/41.0
Build ID: 20150614030204

Steps to reproduce:

1. Open a new tab.
2. If you have pinned tiles, unpin them all.
3. Restart browser.
4. Try to drag and drop the unpinned tiles on new tab.
(It may take you upto 10 drag and drop, or it may not happen at all. in that case try again)


Actual results:

Tried to drag and drop several unpinned tiles and they freeze. The number of successful drag and drop is not specific, but it happens within 10 drag and drop, sometimes it does not happen. But I faced this issue on almost 4/5 PCs of my friends and also on different builds.
Thought it was just a glitch, but got this today on another PC and found it worth filing.


Expected results:

There should be no trouble to drag and drop all the tiles every time I try.