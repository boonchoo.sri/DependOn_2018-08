devtools.html localization: remove dependencies on Services.strings in l10n.js
For devtools.html localization, we need to be able to synchronously load properties files.