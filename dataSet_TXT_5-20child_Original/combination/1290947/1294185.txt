localization: migrate toolbox-window.xul to use properties file instead of dtd
For devtools.html, XHTML files will no longer be able to load entities from external DTD files. All XHTML/XUL files relying on DTDs for localization should move all the DTD entities to properties files.

This bug is about migrating all localization strings defined in toolbox-window.xul.