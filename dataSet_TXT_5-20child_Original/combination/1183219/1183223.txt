Create a markers directory temporarily inside docshell/base where all marker logic should go into
First step towards taking all the markers logic outside docshells.

Once everything is moved out, we may consider moving that directory somewhere else, but in the meantime, it's a good way to start things going.