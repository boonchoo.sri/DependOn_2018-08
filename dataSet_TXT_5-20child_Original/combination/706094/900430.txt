Should be able to Tab complete and cycle through the completion suggestions in the markup view.
When editing the style attribute in markup view, you get CSS properties and values suggestions. The suggestions in the popup can be cycles throug husing up/down arrows, but you have to press right arrow to complete that property and keep typing. In rule view, it makes direct sense to press TAB key to complete the selected suggestino and move on to the next editor, but in markup view, you have to remain in the same editor . Currently, take the following example:

style="di|

where the cursor is '|' . The user will now have a popup giving the following suggestions : "direction" and "display". Now the user shold be able to press tab once to complete to "direction" and one more time to cycle to "display" without leaving the editor as he has to now write the value of the property "display".