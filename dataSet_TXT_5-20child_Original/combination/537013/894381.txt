FindBar buttons should be enabled when moving tab between windows
Build Identifier:
http://hg.mozilla.org/mozilla-central/rev/5976b9c673f8
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20130715 Firefox/25.0 ID:20130715211256

Steps To Reproduce:
1. Open any page
   ex. https://developer.mozilla.org/en-US/docs/Code_snippets
2. Open FindBar and type "mozilla"
3. Detach the tab
4. Click Up/Down arrow in the FindBar

Actual Results:
Nothing happens

Expected results:
Find prev/next should be performed