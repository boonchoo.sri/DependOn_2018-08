Per-tab find bar remembers last-used search string inconsistently
User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0 (Beta/Release)
Build ID: 20130923194050

Steps to reproduce:

The per-tab find bar from bug 537013 attempts to remember the last-used search string to default to when searching in new tabs, but does so inconsistently. With the Firefox 25 beta I've found it default to a search string I used half an hour ago instead of one of maybe ten strings used since then etc.

Here are some different scenarios:

#1
open the browser
press ctrl-F or F3
search for "test" in tab A
open a new tab
press ctrl-F or F3
OK (defaults to "test")

#2
open the browser
press ctrl-F or F3
search for "test" in tab A
press escape (closing the find bar)
open a new tab
press ctrl-F or F3
NOT OK (blank - did not remember "test")

#3
open the browser
press ctrl-F or F3
search for "test" in tab A
open a new tab
switch back to tab A
press escape (closing the find bar)
switch back to tab B
press ctrl-F or F3
OK (here it does remember the search text even though the find bar in tab A was closed before the tab B find bar was opened)

#4
open the browser
press ctrl-F or F3
search for "test1" in tab A
open a new tab
press ctrl-F or F3
search for "test2" in tab B
press escape (closing the find bar)
close tab A (without switching to it)
open a new tab
press ctrl-F or F3
NOT OK - searching for "test1" instead of test2" (ie. it remembers the old search string from the closed tab instead of the most recent)

#5
open the browser
open 5 tabs
press ctrl-F or F3 in tab5, then press escape
switch to tab1
press ctrl-F or F3
search for "test"
switch to any other tab
NOT OK - searching for "test" now works in tab 1-4 only, not tab5



Actual results:

Looks like the search string gets saved only if a find bar is open when switching from one tab to another.
And restored only when search is opened for the first time in a tab.



Expected results:

It should be saved whenever search is used (including find-next/F3), not just if the find bar is open when switching away from a tab.
And it should be restored whenever search is opened and the search term would be blank.

Bug 913536 discusses something similar, but not quite the same.