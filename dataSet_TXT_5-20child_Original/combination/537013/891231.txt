Find bar is displayed with a white background color
Created attachment 772490
Screenshot of bug

When the findbar got moved from the browser-bottombox to a child of browserContainer, the findbar lost its inherited background-color. This background-color was previously applied via:

>  #navigator-toolbox > toolbar:not(:-moz-lwtheme),
>  #browser-bottombox:not(:-moz-lwtheme) {
>    background-color: hsl(210,75%,92%);
>  }

See attached screenshot.