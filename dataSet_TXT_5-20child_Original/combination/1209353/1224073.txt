Errors generated from console evaluation in worker toolbox come through as [Object object]
STR:
devtools.debugger.workers = true
Open http://bgrins.github.io/devtools-demos/worker/webworker.html
Open debugger
Click on worker to open toolbox
Switch to console
Run something that causes an error, like `asdf;`

Expected: See 'ReferenceError: asdflkj is not defined'
Actual: See '[Object object]'