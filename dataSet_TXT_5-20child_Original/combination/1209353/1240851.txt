Make ConsoleAPIStorage thread safe
We need to make the console storage API thread safe, so we can expose it to the worker debugging API