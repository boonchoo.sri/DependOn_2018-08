HTTPS address is preferred from address bar even when it is an error (e.g. 403 Forbidden)
User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1
Build ID: 20120905151427

Steps to reproduce:

1. Visit https://www.example.com/ , which returns a 403 Forbidden status code. 
2. In the address bar, begin typing 'www.exam', 'www.example.com/' is shown in the address bar with 'ple.com/' highlighted
3. Press enter to load the page



Actual results:

https://www.example.com/ is loaded


Expected results:

http://www.example.com/ should be loaded because https://www.example.com/ is an error page. 

Should only prefer previously-visited HTTPS addresses over their HTTP counterpart if the HTTPS address had returned a successful status code (e.g. not 403, 404, etc)