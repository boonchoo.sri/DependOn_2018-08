URLs auto-completed in address bar might unexpectedly use ftp:// instead of http://
I have visited pages on "dd-wrt.com" on both the http and ftp protocols.

Whenever I type in "www.dd" into the location bar, it suggests "www.dd-wrt.com", and autofills that into the location bar.  When I press Enter to confirm the suggestion, it takes me to "ftp://dd-wrt.com/" instead of "http://www.dd-wrt.com/" the http site.