Address/Awesome bar: URL completion overrides selection from drop-down list
Steps to reproduce:
* With the 2 June Nightly, create a new tab.
* Start typing in awesome bar. Choose characters such that URL completion occurs and a selection list appears.
* Click on a selection from the drop down list.
* Verify the URL from the selected item appears in the awesome bar.
* Press Enter.

Expected: Visit the selected URL, as shown in the awesome bar.

Actual: The URL from the URL completion step reappears in the awesome bar and that page is visited.

I see this issue on Ubuntu, also seen on Windows (<http://forums.mozillazine.org/viewtopic.php?p=12028013#p12028013>).