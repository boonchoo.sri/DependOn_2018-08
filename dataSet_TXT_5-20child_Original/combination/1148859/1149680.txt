Include Firefox channel in tiles fetch
There's been requests in bug 1127600 to target aurora/developer channel with different directory tiles. We'll also want this for suggested tile bug 1120311.

In particular, with bug 1148859 uplifting to 38, an ESR version, we'll want to be able to have the option to separately manage tiles for ESR vs regular release firefox.