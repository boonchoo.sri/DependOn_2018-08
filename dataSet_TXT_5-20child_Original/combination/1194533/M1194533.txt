Run e10s tests on OSX
As we're getting closer to having e10s ride the trains, having good automated test coverage is going to get more and more important. We currently only run mochitests with e10s on Linux and I'd like to extend that to OSX and Windows.

I understand that there's a cost in machine time to this, so we might want to figure out a throttling strategy to not cause too much drag on treeherder. What needs to be done to make this happen?

I have a try run at https://treeherder.mozilla.org/#/jobs?repo=try&revision=1cad3dcad6ff