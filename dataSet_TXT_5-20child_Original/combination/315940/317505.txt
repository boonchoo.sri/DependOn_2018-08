Add relative dates to places queries
Places queries have begin and end dates, but it is common to want "everything since last week" or "today" which are not currently representable. Add these.