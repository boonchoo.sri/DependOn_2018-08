Improve accessibility and keyboard interaction
This bug tracks work to be done on top of the Downloads Panel in bug 726444.

* Allow cycling through the "show download history" entry using the arrows.
* Focus usually returns to where one was before download manager when pressing
  Escape. However, if nothing was focused when DM was invoked, focus should be
  set explicitly to the document, so that it doesn't get into a limbo state.
* The option to open the file in focus, available using the ENTER key, is not
  announced. Understand how to address this when using screen readers.