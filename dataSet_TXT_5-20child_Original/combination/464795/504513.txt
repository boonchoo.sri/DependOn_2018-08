DownloadLastDir.jsm is imported during startup
http://hg.mozilla.org/mozilla-central/annotate/547693481fd4/toolkit/content/contentAreaUtils.js#l526

There's no reason for this to be imported unconditionally into the global scope at startup with getTargetFile being the only consumer.