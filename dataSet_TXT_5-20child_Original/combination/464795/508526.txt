Make gDownloadLastDir smarter
Followup from bug 499733 comment 11.

The gDownloadLastDir object in toolkit needs to be smarter, so that it saves the nsIFile as either a pref value or an in-memory value based on the current private browsing mode.