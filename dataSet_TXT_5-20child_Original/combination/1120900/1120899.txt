Refresh animation player widgets if duration, delay or iterationCount change.
The animation inspector panel assumes these values don't change during the lifetime of an animation. This is an incorrect assumption.

Whenever duration, delay or iteration count change, the UI of the player widget should be refreshed.