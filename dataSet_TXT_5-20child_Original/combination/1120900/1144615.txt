Add playback rate selection to the animation inspector panel
When platform bug 1120339 gets resolved, we'll be able to control animations' playback rate via AnimationPlayer objects.
Which means we'll be able to expose this to devtools users through the animation inspector panel.