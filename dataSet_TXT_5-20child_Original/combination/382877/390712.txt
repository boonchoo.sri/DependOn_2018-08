reloading a live bookmark generates warnings
Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9a7pre) Gecko/2007080213 Minefield/3.0a7pre ID:2007080213

repro:
Go to bookmarks toolbar, a live bookmark, rightclick and choose reload.

result:
Warning: reference to undefined property Ci.nsINavHistoryResultNode.RESULT_TYPE_REMOTE_CONTAINER
Source file: chrome://browser/content/places/controller.js
Line: 486

Warning: reference to undefined property asContainer(node).remoteContainerType
Source file: chrome://browser/content/places/controller.js
Line: 494