Remove hardcoded fonts in about:privatebrowsing
The `font: message-box` rule inside common.css handles system fonts fine.
See https://bugzilla.mozilla.org/show_bug.cgi?id=1190427#c46