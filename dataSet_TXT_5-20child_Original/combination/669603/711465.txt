Reconsider DOM session storage behavior
DOM Session storage is retained across crashes, which is specifically allowed by the spec.  We also appear to restore session storage data across a quit and restore-last-session, which is mildly contrary to the intent of the spec, though not disallowed that I know of.

IE8 doesn't restore the data across crashes, and I assume it doesn't restore it across a quit-and-restart.  I assume IE9 does save it across crashes, but that's a shaky assumption.  Chrome apparently doesn't restore it under any circumstance.

I don't know if session storage data is cleared on a history clear (it probably should be).

Various patches are under consideration that may affect these behaviors, such as in bug 669603 (to store the data in the cache, which would cause it to be lost on a crash).

Our options are:
a) Current behavior - restore session storage data on restore-last-session after a quit or a crash
b) Restore session storage data on restore-last-session after a crash only (more in keeping with the spec)
c) Never restore session storage data (like Chrome and maybe IE8)
d) Restore session storage data on restore-last-session after a quit, but NOT after a crash (current patch in bug 669603).

Note: in crash cases, there's always a chance the data won't be recoverable.

Explicitly choosing a behavior will set what our options for improving the performance are.