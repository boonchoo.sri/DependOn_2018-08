Come up with an alternate API registering tool keyboard shortcuts instead of XUL <key> elements
We currently use XUL <keyset> and <key> elements, which handle `modifiers`, localized `key` or `keycode` and integration with <command>s
https://dxr.mozilla.org/mozilla-central/search?q=path%3Adevtools%2Fclient+%3Ckey&redirect=true&case=false 

They also handle ‘accel’ nicely as cmd on OSX and ctrl on windows.