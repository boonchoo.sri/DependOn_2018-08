Help context menu needs menu icons
User-Agent:       Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9b4pre) Gecko/2008021803 Minefield/3.0b4pre
Build Identifier: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9b4pre) Gecko/2008021803 Minefield/3.0b4pre

Help context menu needs menu icons

Firefox main window menus has icons that can (and should) be used in Help
context menu:

Back
Forward
Select All
Zoom In
Zoom Out

Reproducible: Always