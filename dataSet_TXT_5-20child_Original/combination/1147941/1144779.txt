Do not use mouse coordinates from onMouseUp to set selection end when dragging in Graph
Right now the coordinates from the mouseup event are used to finalize the selection in the Graph.  This can cause weirdness when a mouseup happens outside of the toolbox and then it is triggered from the next mousemove.

STR:

Start a new selection on graph
Move the mouse up to the browser content
Release mouse
Move the mouse back into the timeline

Notice that the selection ends at the location of the mouse currently, when it should stay where it was when the mouse went up.

Dragging and resizing an existing selection doesn't have this behavior, because the coordinates are used from mousemove instead.