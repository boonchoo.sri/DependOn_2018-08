Selection is broken if toolbox is zoomed
STR:
- record
- stop recording
- zoom toolbox
- try selecting a region of the overview
- see that the selection is shifted

I guess the mousemove handler doesn't take into account the zoom level and assume the canvas still has a 1:1 aspect ratio. window.devicePixelRatio can change any time.

I think we should invalidate the whole UI on resize, redraw any canvas-based widgets.

(see other zooming-related bugs)