Panels should have a blur and semi-transparency on OS X
Created attachment 8335000
safari-fx-bg.png

User Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.71 (KHTML, like Gecko) Version/7.0 Safari/537.71

Steps to reproduce:

When trying to show any Firefox popup in new UI, they feel unnatural to OS X.

For example, “downloads” or “customise” popups.

Take a look at the attached screenshot.


Actual results:

Popups don't have blur and semitransparent background


Expected results:

Popups must have blur and semitransparent background