Australis - Legacy button not positioned correctly in Australis panel
Created attachment 8336789
Screenshot of offset icon

I have a legacy toolbar. I've changed by button orientation to vertical to match Australis.

When I put the button in the Australis panel, the button is offset by a few pixels (image attached).

The button is using standard toolbar CSS - class="chromeclass-toolbar-additional toolbarbutton-1"

The button should be a perfect aligned square just like the other buttons.