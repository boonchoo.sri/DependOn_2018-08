[Australis] Menu panel layout is broken
Created attachment 8364789
Australis menu regression, Win7 vs. Ubuntu

Bug 944947 hadn't fixed the Linux layout bug, and introduced padding regression on widgets.

Could you go back to the previous right and left paddings for Widgets, or is it already the best we can have? Because of that, we have more widgets going to 3 lines, smaller paddings could help avoiding that.
And some labels like "Mar-que-pages" are *really* ugly and hard to read, it could totally fit on one line, I think. And this particular widget is still overflowing (as you can notice on the Linux screenshot).

Last thing, some labels are not behaving the same way on Windows and Linux. Look at "Nouvelle fenêtre privée" (New private tab) for instance, 3 lines on Linux, 2 lines on Windows. It becomes tricky to adjust localization with an inconsistent behavior.