Alert which warns about unassociated protocol (scheme) is not tab-modal
User-Agent:       -
Build Identifier: Mozilla/5.0 (X11; Linux x86_64; rv:2.0b9pre) Gecko/20100101 Firefox/4.0b9pre

The warning about URLs containing a protocol (schema) which is not associated with a program is not tab-modal but windows-modal.

Reproducible: Always

Steps to Reproduce:
1. open testcase
2.
3.
Actual Results:  
Windows-modal alert.

Expected Results:  
Tab-modal alert.