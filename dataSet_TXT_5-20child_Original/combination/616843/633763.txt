Cookie confirmation dialogs should be tab-modal
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)

Accepting cookies is a content-specific action that does not require an application-modal dialog.

With the recent implementation of tab-modal dialogs, cookie confirmation should be done with one of those.

Bug 515521 should be fixed along with this.

Reproducible: Always