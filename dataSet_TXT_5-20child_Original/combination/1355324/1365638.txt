Change label text for "Close sidebar" to "Close Sidebar" on header menu
The option to "Close sidebar" should have a capital S for Sidebar since it is kind of like a button action.

Reference: https://mozilla.invisionapp.com/share/PJBJCIBMQ#/screens/231191185_Explainer_-_Sidebar