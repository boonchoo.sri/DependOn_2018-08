Very long lag while stopping profiler on a WebIDE connected device
There are a variety of errors that happen when you take profiles on a device.  The STR are a little fuzzy and there seem to be a variety of errors that happen:

Open WebIDE
Connect to an android device
Start a profile and stop it
Start another profile and stop it.  Notice that it takes a very long time (sometimes > 10 second) to finalize.
If that all went well, try another one and while it's still trying to finish, press the button a couple more times.

At some point errors surface like "Protocol error (unknownError): Error: Wrong state while attaching to the debugger:Expected 'detached',but current state is 'attached'."1 protocol.js:20".

It's hard to tell exactly what is going on.  I doubt it's specific to the WebIDE connection but may be due to less resources on the server or a greater lag time for RDP