Add a search field to the new tab page
We know that many users don't know/use the search field in the UI or the awesomebar. Providing a search field on the new tab page would make their search experience more fluid.

I know that there are several projects around new tab happening right now, but this looks like such a quick and easy win to me, that we should do it right away, even when the layout of the page will be changed again in the future.

This is a good starting point:
https://wiki.mozilla.org/File:Centered_search_actually_default_32423.png