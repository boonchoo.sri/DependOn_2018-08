Add Wikipedia logo to search plugin so about:newtab can use it
Created attachment 8441068
patch

(In reply to Joanne Nagel from bug 1006203 comment #15)
> As we discussed previously, I don't have agreements with Twitter or
> Wikipedia, so we should be able to use whatever assets they have published
> online.

This patch uses the images Boriss posted to bug 962490 comment 34.

Matt, this patch is similar to the one you reviewed in bug 1009299.