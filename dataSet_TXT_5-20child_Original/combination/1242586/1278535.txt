Reduce the number of tab stops in the Console's toolbar
STR:
1. With NVDA and Firefox running, open the web console with Ctrl+Shift+K.
2. Shift-tab through the output and into the toolbar.

Expected: The tool bar should have one, at most two tops, inside the button group and maybe additionally on the search field.
Actual: Tab and shift tab stop at every control.