Support %o and %O format strings
https://github.com/DeveloperToolsWG/console-object/blob/master/api.md#format-specifiers

>%o 	Formats the value as an expandable DOM Element (or JavaScript Object if it is not)
> %O 	Formats the value as an expandable JavaScript Object