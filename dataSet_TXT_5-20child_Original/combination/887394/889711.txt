recently closed tab not in history, not restored with Ctrl+Shift+T
User Agent: Mozilla/5.0 (X11; Linux i686; rv:25.0) Gecko/20130702 Firefox/25.0 (Nightly/Aurora)
Build ID: 20130702110300

Steps to reproduce:

close tab via 'x' on tab


Actual results:

using Ctrl+Shift+T does not restore recent tab.

also:
'Nightly button'  > History or History menu> Recently Closed Tabs > * Recent or Last Closed Tab is Not listed 

*note: Older tabs, before today's update (02.07.13, 2013.07.02).



Expected results:

Ctrl+Shift+T should restore last closed tab and subsequent C+Sh+T's presses should restore progressively older 'Closed Tabs'.

'Recently Closed Tabs' should be listed at or near top of History > 'Recently Closed Tabs' list.