Fix 18 tests failures on devtools/client/inspector/boxmodel due the EventEmitter refactoring
Failing tests:

devtools/client/inspector/boxmodel/test/browser_boxmodel.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_computed-accordion-state.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_editablemodel.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_editablemodel_bluronclick.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_editablemodel_border.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_editablemodel_pseudo.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_editablemodel_stylerules.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_guides.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_layout-accordion-state.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_navigation.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_offsetparent.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_positions.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_properties.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_pseudo-element.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_rotate-labels-on-sides.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_sync.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_update-after-navigation.js
devtools/client/inspector/boxmodel/test/browser_boxmodel_update-after-reload.js


The refactoring is currently only on:

https://github.com/zer0/gecko/tree/event-emitter-1381542

We need to address the test failures before land this patch in m-c.