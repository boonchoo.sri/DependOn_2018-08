[meta] Remove the legacy EventEmitter from DevTools
In bug 1381542 we introduced a new Event Emitter, while we're keeping the previous one for a smoother transition.

This bug is intended for replacing the old EventEmitter with the new one.