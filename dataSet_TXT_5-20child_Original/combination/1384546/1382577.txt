Fix 8 tests failures on devtools/client/canvasdebugger due the EventEmitter refactoring
Failing tests:

devtools/client/canvasdebugger/test/browser_canvas-frontend-call-stack-01.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-call-stack-02.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-img-thumbnails-02.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-record-01.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-record-02.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-reload-01.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-reload-02.js
devtools/client/canvasdebugger/test/browser_canvas-frontend-snapshot-select-01.js


The refactoring is currently only on:

https://github.com/zer0/gecko/tree/event-emitter-1381542

We need to address the test failures before land this patch in m-c.