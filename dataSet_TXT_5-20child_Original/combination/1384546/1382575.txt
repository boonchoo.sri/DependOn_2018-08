Fix 7 tests failures on devtools/client/aboutdebugging due the EventEmitter refactoring
Failing tests:

devtools/client/aboutdebugging/test/browser_addons_debug_info.js
devtools/client/aboutdebugging/test/browser_addons_debug_webextension_popup.js
devtools/client/aboutdebugging/test/browser_addons_debugging_initial_state.js
devtools/client/aboutdebugging/test/browser_addons_install.js
devtools/client/aboutdebugging/test/browser_addons_reload.js
devtools/client/aboutdebugging/test/browser_addons_remove.js
devtools/client/aboutdebugging/test/browser_addons_toggle_debug.js

The refactoring is currently only on:

https://github.com/zer0/gecko/tree/event-emitter-1381542

We need to address the test failures before land this patch in m-c.