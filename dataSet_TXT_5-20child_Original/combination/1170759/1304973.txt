Identity popup expander button shouldn't have a white gradient in the background unless hovered
Created attachment 8794090
patch

The gradient is invisible with default themes (because the panel background is white) and it looks wrong in dark themes such as high contrast themes.