Control center subview black and white arrow icons don't have the same size
The white icon (the one that is shown when a subview is entered) seems smaller than the black icon (the default one).