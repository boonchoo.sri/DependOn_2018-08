Implement a SideMenuWidget (add a tree view to the remote debugger's script selector)
I have some ancient code for organizing a directory structure in a XUL tree, and for organizing them according to the chrome:// protocol's format.  I think it would make an interesting XUL panel where the existing menulist of files is.

I'll need some UX mocks of how this should appear.  I'm thinking the panel begins with a checkbox.  When that checkbox is not checked, show the menulist as it is now, inside the panel and below the checkbox.  When the checkbox is checked, show a tree structure based on the protocols first.  We can switch between the menulist and the tree by a XUL deck.

http://sourceforge.net/p/verbosio/legacy/ci/bcb7ee71cc81ef6844fa34e52fccc157ac1b9ed5/tree/src/pack-viewers/ is where the code for the chrome registry viewer currently lives.

I'm certainly willing to write patches for this.