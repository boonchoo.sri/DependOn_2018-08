No padding in the sidebar on pages without stylesheets.
Created attachment 702052
Style Editor on a page without stylesheets.

On pages without any stylesheets the Style Editor sidebar gets the width of its longest string ("Perhaps you'd like to append a new style sheet?") without any padding from its sides. It looks odd.