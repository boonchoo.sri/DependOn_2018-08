Hide the menu text while the Firefox menu is opening to improve the smoothness of the animation
From https://bugzilla.mozilla.org/show_bug.cgi?id=1296070#c9,

> PanelUI's _adjustLabelsForAutoHyphens also takes some time.
> 4) We might want to investigate hiding the text while the menu is opening

If we do this we will need to make sure that it doesn't look odd with the text appearing at some later time.