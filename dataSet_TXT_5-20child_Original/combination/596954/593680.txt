Tab animation perf issues
User-Agent:       Mozilla/5.0 (X11; Linux i686; rv:2.0b6pre) Gecko/20100903 Firefox/4.0b6pre
Build Identifier: Mozilla/5.0 (X11; Linux i686; rv:2.0b6pre) Gecko/20100903 Firefox/4.0b6pre

I've hit a performance regression for the opening/closing tab animation.

This seems to be because of the new tab style.

In this build, everything is really smooth:

http://hourly-archive.localgho.st/hourly-archive2/mozilla-central-linux/1283579728-20100903225528-2396696f1631-firefox-4.0b6pre.en-US.linux-i686.tar.bz2

In the following one, it's not (I only get ~3 frames drawn):

http://hourly-archive.localgho.st/hourly-archive2/mozilla-central-linux/1283590742-20100904015902-bd474ff6f86c-firefox-4.0b6pre.en-US.linux-i686.tar.bz2


Reproducible: Always

Steps to Reproduce:
1.Get the second build.
2.
3.
Actual Results:  
The new tab style looks great but its animation are sluggish (well, certainly on older hardware).

Expected Results:  
Smooth opening/closing a tab as it was on the first build mentioned.

I've got a Celeron 1.6Ghz computer with the nouveau driver.

Is there any profiling tool I could use to provide more data?