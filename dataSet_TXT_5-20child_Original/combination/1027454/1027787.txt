Report unique device name from discovery
Device discovery currently uses the host name / Android device name as the device name it sends out, which then appears in the runtime list.

If you have multiple devices with the same name, then there's no way to tell them apart at the moment.  Should more info to the name or something to help discriminate.  Or perhaps expose b2g version, etc.?