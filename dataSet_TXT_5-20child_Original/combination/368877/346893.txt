Scale combo box does not have MSAA Name
Test environment:
   Windows XP, Firefox 2 (Bon Echo) build 20060801, Microsoft Inspect Objects
   Optional: JAWS screen reader

Steps to recreate:
1. Start Inspect Objects  (optinally, start JAWS also)
2. Start Firefox
3, Open print preview window (Press Alt+F, then "V")
4. Press Tab until you highlight the "Scale" combo box. 
5. Note the Inspect Objects output.  There is no NAME property set.  It should be set to the same text as the combo box label: "Scale:"
6. If you run JAWS, you will hear the word "combo box" or something similar, but you will not hear the label, since the MSAA Name property was not set.

Expected results:
   Inspect Objects should show Name = "Scale", and the screen reader should read the label when announcing the combo box.