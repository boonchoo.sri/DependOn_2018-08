Can't Click on Form Autofill Entry If Search Bar is Removed
User Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:37.0) Gecko/20100101 Firefox/37.0
Build ID: 20150111030201

Steps to reproduce:

1. Open the Customize Firefox UI
2. Drag the Search Bar to the "Additional Tools and Features" area
3. Exit Customize
4. Open https://bugzilla.mozilla.org
5. Double click the search field at the top
6. Click on any entry


Actual results:

Nothing


Expected results:

Search field should be filled with the selected entry

Video: http://screencast-o-matic.com/watch/coVXqvewzc

Started with: https://hg.mozilla.org/mozilla-central/pushloghtml?changeset=206205dd8bd1