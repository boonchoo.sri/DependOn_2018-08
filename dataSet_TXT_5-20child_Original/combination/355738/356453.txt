add back UI hook for "Clear Private Data" to trunk-with-places
add back UI hook for "Clear Private Data" to trunk-with-places, as well as (I'm guessing) the code to clear history

from http://lxr.mozilla.org/seamonkey/source/browser/base/content/browser-menubar.inc#524

#ifndef MOZ_PLACES
              <menuseparator id="sanitizeSeparator"/>
              <menuitem id="sanitizeItem"
                        accesskey="&clearPrivateDataCmd.accesskey;"
                        label="&clearPrivateDataCmd.label;"
                        key="key_sanitize" command="Tools:Sanitize"/>
#endif