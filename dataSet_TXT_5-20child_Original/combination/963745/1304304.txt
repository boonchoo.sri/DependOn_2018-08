[polish] After switching to subview for malware warning, the position for return button is not aligned
Created attachment 8793220
screenshot

+++ This bug was initially created as a clone of Bug #950058 +++

Please refer to the screenshot.
The button for returning to main view at left of the panel is not aligned well. It's too close to the left of the panel. User may somewhat not aware that it's a button designed to be clicked. We need to polish its position in some way. For example maybe let it align to center at the remaining space in main view.

We made some try on bug 950058 comment #19 but not going very well. Let's discuss it here.