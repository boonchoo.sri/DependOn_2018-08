Disabled back button is too translucent
[Tracking Requested - why for this release]: unintended visual regression in the primary UI

The back button's background's opacity changes when the button becomes disabled / enabled. This looks broken. Every new tab starts out with a disabled back button, so this happens all the time.

On Mac, native buttons keep their full opacity when they become disabled and only the glyph or text on top of them becomes less opaque. We should match that, and we did, until the recent changes.