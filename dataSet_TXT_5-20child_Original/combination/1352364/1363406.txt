Regression: "Show your bookmarks" button shrinks in size if moved to the Bookmarks Toolbar
Created attachment 8865886
Shrinking button

Tested environment:
Windows 10 build 1607

STR:
1. Install latest Nightly
2. Enable the Bookmarks Toolbar
3. Move the "Show your bookmarks" button to the Bookmarks Toolbar

AR:
The "Show your bookmarks" button shrinks in size (the "Bookmark this page" button remains unchanged). See attached.

ER:
It shouldn't.

This bug doesn't seem to affect any other built-in Firefox buttons.

Regression range:
Good: 2.5.17
Bad: 3.5.17

Mozregression narrows it down to:
2017-05-09T17:48:09: DEBUG : Using url: https://hg.mozilla.org/mozilla-central/json-pushes?changeset=a748acbebbde373a88868dc02910fb2bc5e6a023&full=1
2017-05-09T17:48:10: DEBUG : Found commit message:
Backed out changeset d3197ffef609 (bug 1338217) for failures in test_webassembly_compile.html on at least Windows VM debug a=backout a=merge