Consolidate styling for toolbarbutons in the tab bar
Bug 1352364 moved the styling for toolbarbutons in the tab bar from Windows to shared but left behind some styles in the Linux and Mac stylesheets. Not sure how this works on Mac, but it's causing problems with the all-tabs button on Linux.

Generally, the goal is to share the toolbarbutton styling completely and get rid of these platform differences. Also, I think we should start using the --toolbarbutton* CSS variables here and replace the legacy styling that I hacked together ages ago as a band-aid for Firefox 4...