The new autoscroll image looks fuzzy
I guess this is invalid, but I'm going to try it anyway.
The new autoscroll png images look fuzzy compared to what we had, making it more difficult to actually see where the autoscroll image is.

Now, I know that IE has a similar fuzzy image, but there are a few key differences here:
- The inside of the autoscroll image is not partly transparent like it is in Firefox (so it's more visible by default)
- The autoscroll image of IE has larger arrows, which also make it more visible
- The mouse cursor changes, depending whether you're under or above the autoscroll image, which makes it more likely to detect where the autoscroll image is, anyway.