close.png should use close box with red background for active state
See attachment 310986 [details]
close.png - the first state is wrong, I think

I believe that the states should be:

active | hovered | pressed | inactive

the newly checked in close.png has a grey background in the "normal" state,
which makes it look inactive on luna (see
http://img521.imageshack.us/my.php?image=zzvx8.jpg).

Alex: can you update close.png and re-attach? It would be great if we could get this fixed for beta, as I've seen a bunch of people asking about it.