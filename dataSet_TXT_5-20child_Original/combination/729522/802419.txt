Cannot select which mic you want to use when audio and video is requested on gUM with multiple mic input devices
Steps:

1. Call gUM with video and audio requested with an integrated and USB mic on your device (i.e. 2 input sources for audio)
2. When the permission prompt appears, select the drop down

Expected:

Each mic should be listed as choices in the doorhanger prompt.

Actual:

Neither mic is listed in the doorhanger prompt.