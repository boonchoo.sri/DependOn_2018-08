Showing a camera icon when only the microphone has been requested is confusing
When getUserMedia is called to request only an audio device, the permission prompt that is shown displays a camera icon. The notification icon in the location bar is also a camera. This is confusing when only the microphone is shared.

I think this was implemented in bug 729522, where the initial mockup (https://bug729522.bugzilla.mozilla.org/attachment.cgi?id=666072) used a different icon for microphone-only.