"Open All in Tabs" change a currently selected App Tab with the first URL of the folder
User-Agent:       Mozilla/5.0 (X11; Linux i686; rv:2.0b10) Gecko/20100101 Firefox/4.0b10
Build Identifier: Mozilla/5.0 (X11; Linux i686; rv:2.0b10) Gecko/20100101 Firefox/4.0b10

When you have an App Tab focused (in my case, Google Calendar) and choose "Open All in Tabs" the first bookmark of the folder replaces the current App Tab content instead of opening a new tab like the other bookmarks.


Reproducible: Always

Steps to Reproduce:
1. Create a folder with two bookmarks (ex: https://www.xgold.ca/ and http://www.xsoli.com/)
2. Focus a App Tab (ex: Google Calendar)
3. Click at "Open All in Tabs" of the folder with the two bookmarks.

Actual Results:  
The App Tab get replaced with https://www.xgold.ca.

Expected Results:  
I think the first bookmark should be opened in a new tab, like the others.