"Report Web Forgery" should unpin an app tab
This action should open in a new tab instead of overwriting the current one, at least for app tabs (since we don't want those to be overwritten)