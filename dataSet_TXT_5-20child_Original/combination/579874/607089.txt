Home button should open in a new tab when current is an app tab
User-Agent:       Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b8pre) Gecko/20101024 Firefox/4.0b8pre
Build Identifier: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b8pre) Gecko/20101024 Firefox/4.0b8pre

While all other links open in new tabs when clicked in an app tab, including history and bookmarks, pressing the "Home" button will open the home page within the app tab.

This should be fixed by making it open in a new tab if clicked while in an app tab.

Reproducible: Always