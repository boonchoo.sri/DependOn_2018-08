Navigation from Customize page fails in some edge cases
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160526082509
There're 3 scenarios: A, B and C

>>>
STR_1:
1. Set about:blank as homepage
2. Right-click Australis menu, click "Customize..."
3.A) Press Alt+Home
3.B) Press Ctrl+K
3.C) Open new tab, type text "ya.ru" in urlbar, select it, drop it onto customize tab in tabs toolbar

AR:  (A) - the tab loses its favicon.   (B),(C) - infinite gray spinner is displayed in <tab>
ER:  Either X or Y
 X) If navigation supposed to work,
   (A) - about:blank should open,   (B) - about:home should open,   (C) - https://ya.ru should open
 Y) If navigation isn't supposed to work, such actions should not affect favicon in <tab>
Either way, behavior is wrong.

(A) is regression from bug 1249608. Regression range:
> https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=62cc4b503870cf7be4ae0c530a3d7ab99c8585b9&tochange=ca83a163091aff314159011e639520f25daa3a99

(B),(C) are regression from bug 1249608. Regression range:
> https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=ca83a163091aff314159011e639520f25daa3a99&tochange=eff6731606da854ee031a06a64191484c09e72e4