Object inspector should use bigger slices for huge arrays
Inspect new Array(1e3).fill() with the Object Inspector, it uses slices of 100 elements:

   ▶ [0...99]
   ▶ [100...199]
   ▶ [200...299]
   ▶ [300...399]
   ...

Now inspect new Array(1e5).fill(). It still uses slices of 100, which is bad because there are too many slices, so the browser freezes for a bit.

Instead, the size of the slices should depend on the array length, e.g. something like

   10 ** Math.max(2, Math.ceil(Math.log10(array.length))-2)

This should ensure a maximum number of 100 slices, using a power of 10 as the slice size.

If necessary, you could then use subslices like in VariablesView.