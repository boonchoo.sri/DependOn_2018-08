Object inspector freezes with huge typed arrays
1. Open web console
2. Enter this code:
   new Uint8Array(1e5)
3. Click the resulting object.

Since it's a huge object, with variablesView you see slices:

   ▶ [0...24999]
   ▶ [25000...44999]
   ▶ [50000...74999]
   ▶ [75000...99999]

However, it seems Object Inspector attempts to display the whole list or properties or something like that, because it freezes my browser.