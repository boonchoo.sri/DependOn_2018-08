Adjust Australis's browser theme for Windows 8 and up
+++ This bug was initially created as a clone of Bug #859751 +++

While Australis looks terrific on Windows 7 and Vista, it looks totally out of place on Windows 8. It just doesn't fit in almost every way:
1. Color scheme - gray and white instead of blue.
2. Buttons and text fields - flat instead of bulky.
3. Hover effects - they're mixed because some are hard coded and some are not.

IMPORTANT: this bug is about the browser theme, NOT the toolkit theme work.