US: websocket connections discovery across a tab
As a developer I'd like to view a list of all websocket connections currently open by an application/web page in a tab.