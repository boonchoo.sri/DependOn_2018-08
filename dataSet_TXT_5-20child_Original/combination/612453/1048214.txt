Search suggestion box is moved down when "Choose What I Share" infobar opens
Created attachment 8467014
ChooseWhatIShare.png

Reproducible on:
Mozilla/5.0 (X11; Linux i686; rv:34.0) Gecko/20100101 Firefox/34.0
Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:34.0) Gecko/20100101 Firefox/34.0
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0

Steps to reproduce:
1. Open Firefox with a new profile
2. On about:home page enter some string in the search bar - search suggestions are displayed
3. Wait until the Infobar notificaton "Nightly automatically sends some data to Mozilla so that we can improve your experience - Choose What I Share" is displayed on the bottom of the page
4. Repeat these steps on the about:newtab page

Actual results:
When the Infobar is displayed on the screen, the search suggestions pop-up is moved down with some pixels

Expected results:
The Infobar shouldn't interact with search suggestions

Note: 1. For Linux and Mac, the Infobar appears after setting the pref datareporting.policy.firstRunTime into the past (eg 1404475616000) and restarting Firefox