New error pages are unable to be styled by 3rd party complete themes
Since the new look of the landed in Firefox 32, complete themes are unable to style the new error pages (like about:neterror), because the files aren't in a themable directory (only styles from content are loaded).

about:neterror:

chrome://browser/content/aboutneterror/netError.css
(chrome://global/skin/netError.css exists but isn't loaded anymore)

about:certerror:

chrome://browser/skin/aboutCertError.css
chrome://browser/content/certerror/aboutCertError.css