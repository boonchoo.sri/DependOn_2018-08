scroll position for restored inactive tabs is not remembered
User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0
Build ID: 20150826023504

Steps to reproduce:

1. Enable "Show my windows and tabs from last time" and "Don't load tabs until selected".
2. In one tab, load a site that's scrollable and has a form for text input; scroll whatever amount and input whatever text in the form.
3. In another tab, load any site. Make this tab active before the next step.
4. Close the browser, then open it.
5. Repeat step 4, then select the tab from step 2.



Actual results:

Scroll position and form data for tab in step 2 are not restored.


Expected results:

Scroll position and form data for tab in step 2 are restored.

The mozregression tool produced this:
https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=3e737d30f842a9cb961f962d8b5675624211463c&tochange=2434e8134f3f7d571dd199e0e6a95b88dd8b6daf