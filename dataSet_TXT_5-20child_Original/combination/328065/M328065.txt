Firefox 2 update to default themes for XP, Vista, OS X and Linux (winstripe, pinstripe, gnomestripe)
This is a tracking bug for the Firefox 2 Visual Update feature. The requirements and details of this feature are located on the wiki at http://wiki.mozilla.org/FX2_Visual_Update

Individual items for this feature will be logged as bugs that block this one which will block the release.