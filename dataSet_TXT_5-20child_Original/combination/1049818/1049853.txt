Replace system library calls (e.g. 0xXXXXXX) with a human-readable name
We receive this data in the profiler data, but we don't map back to the provided library names. We should.