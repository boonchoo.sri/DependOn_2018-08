Warn about TLS 1.0 usage
Given how much of a mess it is to remove SSLv3, we should start early with TLSv10.