Firefox post update corrupts Windows 10 user profiles when using registry protection software
User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0
Build ID: 20160823121617

Steps to reproduce:

OS: Windows 10 (german localization)
Firefox version 48.0.1
As a windows user with standard user privileges (no admin privileges), open the Firefox "About Firefox" menu and update Firefox to version 48.0.2. The update finished with restarting Firefox as usual.


Actual results:

Afterwards all Windows 10 profiles, except the one the Firefox update was done with, are corrupted and will start as temporarily loaded profiles. Windows restarts or logging off and on will not repair the Windows userprofiles. As a result of the temporarily profiles, no access to the user files were given.

This happened at 3 out of 3 updated PCs.


Expected results:

Windows profiles should not be corrupted.