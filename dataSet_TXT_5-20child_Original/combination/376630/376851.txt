API for sorting a result by an annotation
We need an API for setting a manual sorting method for a result, e.g. for sorting a bookmarks result based on the description annotation.