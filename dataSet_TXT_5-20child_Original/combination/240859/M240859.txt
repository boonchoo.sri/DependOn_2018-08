Full Screen Navigation Bar Should Have Auto Hide
User-Agent:       Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Q312461; .NET CLR 1.0.3705; .NET CLR 1.1.4322)
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6)

This is an enhancement request: The full screen navigation bar should have an 
auto hide feature, as IE's does.

Reproducible: Always
Steps to Reproduce: