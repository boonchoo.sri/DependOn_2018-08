Reduced CSS coverage styles don't include @keyframe rules for used animations
Created attachment 8450177
testcase

STR:
 1. Load the testcase.
 2. Open the developer toolbar (Shift+F2)
 3. Execute "csscoverage start"
 4. Refresh the page.
 5. Execute "csscoverage stop"
 6. Execute "csscoverage report"
 7. Look at the textfield in the right column of the report.


Expected results:

The textfield contents should include the @keyframes rule:
<style>
@keyframes animation {
  from {
    transform: translateY(0);
  }
  to {
    transform: translateY(10px);
  }
}
#usedRule {
	 width: 100px;
	 height: 100px;
	 border: 1px solid black;
	 animation: 0.5s ease-in-out 0s alternate none infinite animation;
 }
</style>


Actual results:

The @keyframes rule is left out:
<style>
#usedRule {
	 width: 100px;
	 height: 100px;
	 border: 1px solid black;
	 animation: 0.5s ease-in-out 0s alternate none infinite animation;
 }
</style>