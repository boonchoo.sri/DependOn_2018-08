When an attribute is focused, Delete should only delete the attribute, not the entire element
1. Open Inspector (DOM tree view).
1. Click an attribute on an HTML element.
2. Press the Backspace (Delete) button.

Result: element is deleted (!!)
Expected: attribute is deleted

In contrast, pressing Enter does what I expect (edits only the attribute). CF bug 994555.