Add permanent/'sticky' area to overflow panel (behind a pref)
It should be possible to move items in/out of the overflow panel permanently. To do this, we need to (conditionally, based on the pref) add an area to CUI similar to the current panel area.

We'll need to add a separator between this area and the items that appear in the menu dynamically.

We will need to be careful to ensure the move to toolbar and/or remove context menus continue to work (though updating them to go to/from this area is bug 1354078).


This bug is blocked on making the panel use a panelmultiview in order for subviews to work correctly.