"More tools..." button stays enabled/visible without any items in menu if the only items in the menu come from extensions disabled since opening that window
Created attachment 8894815
screenshot.png

STR:
1. Add to "More tools..." menu some extension button, like for example uBlock Orgin
2. Disable this extension in Add-ons Manager
and see that "More tools..." button stays enabled and visible without any items in menu

Workaround:
- restart Firefox
- open new window and close old one