Update overflow panel styling
Created attachment 8855283
Menus spec

We should update the styling of items in the overflow panel. Styling should match that of other vertically-laid out panels in terms of size, font size, and margins/paddings. We should try as much as possible to share this styling across these panels and to reduce duplication that may have crept in as a result of Australis.