Display hyperlink URLs at bottom of window (instead of right side of location bar)
Created attachment 423136
patch

We need an alternative way to display URLs in order to make browsing without a status bar feasible. We could display them in the location bar (the Fission extension does this), but I found that slightly strange, and it's not an option for full-screen mode. A tooltip near the mouse (like Opera does it, I think) seems too distracting, as you don't want to look at the URL in all cases. So I propose we follow Chrome.

Try server build:
https://build.mozilla.org/tryserver-builds/dgottwald@mozilla.com-try-69b4af1af901/