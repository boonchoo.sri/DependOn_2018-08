Remove favicon from Location Bar and add indicator for sites without identity information
This bug covers the final visual styling of the personal/site identity block for all platforms.  Changes include:

-removing the favicon (visually redundant with the favicon in the tab).  Drag operations can be completed with the tab.
-adding a down arrow to indicate that it contains additional information
-inactive and active area for particular notifications (will be used by geolocation and account manager)