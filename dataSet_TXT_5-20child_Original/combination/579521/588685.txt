Update Site Identity Panel
This is a tracking bug for various changes to the site identity panel.  This single piece of secondary UI touches several different projects:

-Location bar and Site identity
-Notification
-Firefox button (some menu items were moved over here)
-Site prefs

So all of the bugs under here are going to have dependencies over to those other projects as well.