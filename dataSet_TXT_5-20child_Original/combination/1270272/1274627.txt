Remove the "Almost Done" first run page
Created attachment 8754902
almostdone.png

The "Almost Done" first run page was never meant to be our first run page. It was interim step in a process to get us to offering existing users a way to sign into their sync accounts when installing Firefox on a new device (see bug 670989). 

In addition there are a number of problems with this page:
1. We are trying to simplify the new user flow so what we can get people browsing the web quickly. This page is a roadblock in that flow.
2. This page is misleading. It is sometimes seen as a requirement for using the browser.
3. This page has no context. It does not give much information to the user about the value and purpose of accounts. And it's presented at a time when a user hasn't used Firefox yet so they have no practical context for what an account does.
4. The result of a lack of context is that the page is good at getting people into single-device accounts. These have very little user benefit. And one of the perceived benefits of a single-device account is backup which our sync system is not designed for. Here are some support threads:
https://support.mozilla.org/en-US/questions/1105144
https://support.mozilla.org/en-US/questions/1109116
https://support.mozilla.org/en-US/questions/1106441
https://support.mozilla.org/en-US/questions/1097317
5. Another problem with lack of context is that people create poor passwords because they are unaware of what that password secures. So people who create accounts on this page are 33% more likely to use a common (insecure) password than those signing up in other places. 
6. This page is not effective. About 9% of people register on this page. That compares to about 55% of people who register in places with the proper context (both informational and experiential).

So, since this page was not designed to be in this part of the flow, is not effective, and most importantly, it's actively harming people, we should remove it now.