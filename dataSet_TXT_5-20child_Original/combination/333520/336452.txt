Improve search-engine confirmation dialog
Improve dialog that asks for confirmation when adding a search engine with the JS function.

+------------------------------------------------------+
|                                                      |
|  /-\     Add %s to drop-down list of search engines? |
|  | |                                                 |
|  \-/ V   o Start using this one right now.           |
|     \                                                |
|                                         [No]  [Yes]  |
+------------------------------------------------------+