rephrase "Select new tabs opened from links"
The preference "Select new tabs opened from links" from the Tabs Options panel should be rephrased.

From http://wiki.mozilla.org/Link_Targeting#Options:
"Rewording this preference should be done, the current text: "Select new tabs opened from links" is awful, has confused pretty much everyone I've seen read it and does nothing to advertise its association with a very specific mode of tabbed browsing usage - the background queue."

One suggestion is "Always bring new tabs to the front".