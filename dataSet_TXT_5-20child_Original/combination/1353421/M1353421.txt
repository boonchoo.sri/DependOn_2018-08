[meta] Implement sidebar updates for Photon
Created attachment 8854486
Mock of the sidebar switcher

For Photon, we're changing a few things regarding how sidebars work. Specifically:

- we'll update the header styling
- we'll make it possible to switch between different sidebars from the header
- the sidebar toolbar button should appear in the toolbar by default
- the sidebar button reflects when the sidebar is open