Closing toolbox when toolbox tabs are not initialized leaves broken toolbox
Created attachment 8823061
Screenshot

STR:
- Open a tab with toolbox closed
- Press Alt+Cmd+I (Mac) or Ctrl+Shift+I twice quickly.
- The toolbox stays in a broken state