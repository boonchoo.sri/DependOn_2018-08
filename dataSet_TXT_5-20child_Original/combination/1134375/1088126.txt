[e10s] Context Menu displays all possible Menu Entries after dragging tab out of window
Created attachment 8510398
Firefox Contextmenu Bug before & after  dragging tab to new window.png

Story:
Clean Profile
Start Session, open multiple tabs (I used www.heise.de and its children)
Drag one Tab from the primary window.
New Window gets created
Context Menu of new tab in new window shows all possible menu items.

Expected behavior:
Show applicable context menus

Reproducible: Always on this build ID

BuildID
Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:36.0) Gecko/20100101 Firefox/36.0 ID:20141022030202 CSet: 88adcf8fef83

Follow Up to bug 1012784 
May be related bug 1060070

Attachments:
Firefox Contextmenu Bug before dragging to new window (correct).png