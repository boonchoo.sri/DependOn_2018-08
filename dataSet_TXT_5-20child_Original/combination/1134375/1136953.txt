[e10s] about:* Tab drag to window fails to paint content. The dropped tab is completely blank. flipping between tabs/reloading does not help
Build Identifier
https://hg.mozilla.org/mozilla-central/rev/07c04d7a483a
Mozilla/5.0 (X11; Linux i686; rv:39.0) Gecko/20100101 Firefox/39.0 ID:20150225030226

This is similar to Bug 1087969, but flipping between tabs/reloading does not help

Steps to reproduce:
1. Open 2 e10s window
2. Open about:*  ex. about:config
2. Drag & drop the about: tab between them

Actual results:
The dropped tab is completely blank. flipping between tabs/reloading does not help

Actual results:
The dropped tab should display