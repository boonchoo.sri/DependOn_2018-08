[e10s] browser.permanentKey is broken when detaching a tab
While working on a test for bug 1136910 I discovered that when you detach a tab to a new window its permanentKey doesn't transfer correctly. The swapDocShells code currently copies _permanentKey which should mean permanentKey copies too because it is just a getter for _permanentKey. For some reason though for the tab in the new window browser.permanentKey != browser._permanentKey. This may be breaking session store in some way.

STR:

1. Open a tab to some webpage that loads remotely.
2. Detach the tab.
3. Open the browser console
4. Try "gBrowser.selectedBrowser.permanentKey == gBrowser.selectedBrowser._permanentKey"

In a sane world this would return true, but in the real world it returns false.

I'm probably going to make swapDocShells copy permanentKey to fix this in bug 1136910 but seems like something is going wrong here that might warrant further investigation, maybe other things are broken?