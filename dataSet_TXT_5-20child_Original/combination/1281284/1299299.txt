Validate/update Telemetry metrics collected for arewee10syet for Shims and CPOWs
arewee10syet:  Evaluate reason codes for python notebook - anything interesting there on how we count shims or is there something that should be adapted.

python scripts are linked from column headings in http://arewee10syet.com/

reason: add-ons that have no shims or CPOWs but are not marked MPC:true are great candidates for testing that they will just work.  So can you spot check the methodology to check that it looks OK?