Change JIT Optimizations to be a view option rather than a recording feature
With bug 1175705, this has optimizations as a per-recording basis, like the other settings which optionally record memory, allocations, framerate, etc, rather than a visual toggle. This lets us not do extra work on the client to assemble optimization data, and an opportunity to not send opt data over the wire.

Is this an option we can add to the profiler? This does mean previous recordings with opt data will not be shown unless the recording was marked with having opt data.