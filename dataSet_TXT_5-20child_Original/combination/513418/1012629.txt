Infer from each toolbar's text color whether we should use inverted icons
Created attachment 8424774
patch

* makes us use white icons on dark high-contrast themes (no bug on file, I think)

* fixes bug 947356 / bug 994623

* should work automatically for a dark private browsing theme when that's implemented

* it's a net code simplification (removes a bunch of rules on Windows in particular)