Fade out and cut off third-to-nth line of toolbarbutton labels in menupanel
As a followup from bug 872544 we should fade out the third and following lines of the labels in the menupanel to ensure a single long button label doesn't break the complete flow.

I think this is lower-prio, but feel free to up it if anyone thinks I've underestimated how important this is.