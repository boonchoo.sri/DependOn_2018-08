tpaint regression on windows platforms found on fx-team Jan 22nd
I saw on graph server a large regression in tpaint:
http://graphs.mozilla.org/graph.html#tests=[[82,131,25]]&sel=none&displayrange=30&datatype=running

I followed it down to the fx-team branch:
http://graphs.mozilla.org/graph.html#tests=[[82,132,25]]&sel=none&displayrange=30&datatype=running

This is also seen on datazilla:
https://datazilla.mozilla.org/?start=1390065227&stop=1390623295&product=Firefox&repository=Fx-Team-Non-PGO&os=win&os_version=6.1.7601&test=tpaint&graph_search=228214210aa5&x86_64=false&project=talos

the source for tpaint is in talos:
http://hg.mozilla.org/build/talos/file/79702830f03d/talos/startup_test/tpaint.html

Here is the documentation we have for tpaint:
https://wiki.mozilla.org/Buildbot/Talos/Tests#tpaint

I tracked it down via retriggers on tbpl and this is the revision where the regression was introduced:
https://tbpl.mozilla.org/?tree=Fx-Team&rev=228214210aa5

Is this an expected regression?  It doesn't seem to affect mac and linux as much, quite possibly this has specific windows code?