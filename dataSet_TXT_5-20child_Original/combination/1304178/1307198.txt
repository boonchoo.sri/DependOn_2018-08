Fix text focus on new output area
When selecting text in the new output area, it is greyed out and the jsterm area remains focused.  It should show up as normal selected text and blur the jsterm area