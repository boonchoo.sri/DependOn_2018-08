Firefox is not pinned to the taskbar when using the stub installer and not making it the default browser
On Windows 7 and 8.1, if you install Firefox with the Stub Installer, leaving the option for pinning Firefox to the taskbar checked AND:
 * Uncheck make Firefox my default browser on Windows 7
 * Dismiss modal default browser prompt or fail to set Firefox as default in Windows 8.1
THEN, Firefox will not be pinned to the taskbar.

More info in Bug 1190351