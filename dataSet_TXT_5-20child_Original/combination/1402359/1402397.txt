CamelCase all React component files in \devtools\client\webconsole\new-console-output\components\
We recently agreed to name our React component file names using the CamelCase convention.

However the files in \devtools\client\webconsole\new-console-output\components\ do not follow this convention, so let's rename them.

This is an easy fix, but make sure you read the contribution docs before starting: http://docs.firefox-dev.tools/ so that you can build Firefox locally and verify that things still work after your change.

After building, to test that things still work, run Firefox and open the console panel, making sure that it loads correctly.