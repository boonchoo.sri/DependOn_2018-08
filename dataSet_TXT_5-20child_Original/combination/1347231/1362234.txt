Update Firefox Screenshots to version 6.6.1
This bug updates the version of Firefox Screenshots in the tree from 6.6.0 to 6.6.1.

The changes address Talos regressions tracked by bug 1361792.