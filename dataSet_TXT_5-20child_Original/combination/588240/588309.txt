Convert change password to a doorhanger panel
We should convert the change password notification from using a notification bar to using a doorhanger panel anchored to the site identity block.

More details about the general design direction for doorhanger panels can be found in the parent bug 588240, please use that bug for any discussion or concerns.