Page Info / Permissions tab doesn't cover iframes from different domains.
If there was a way to do this that would be great. Even if I right-click on the frame and choose "View Page Info", I still only get controls for the main site.

(From Bug 933917 comment 4)
> (In reply to Ian Nartowicz from comment #2)
> > Note that a single-domain version of about:permissions appears in a tab of
> > the page info dialog.
> 
> Hidden gem, thanks! Unfortunately, it doesn't cover iframes, which means
> using it to e.g. control camera access for https://jsfiddle.net/srn9db4h/
> doesn't work.

Note that jsfiddle.net's iframe is from fiddle.jshell.net.