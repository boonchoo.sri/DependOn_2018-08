Params aren't shown as expected in the HTTP inspector
Created attachment 8799764
firefoxdeveloper.png

User Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36

Steps to reproduce:

I submitted a form, containing this:

<input type="checkbox" name="box[]" value="123" checked>
<input type="checkbox" name="box[]" value="456" checked>




Actual results:

In the web console, under POST tab, the Params section only show this:

box[]  123


Expected results:

box[]  123
box[]  456

In the Raw Data section both of them shows:

box%5B%5D=123&box%5B%5D=456