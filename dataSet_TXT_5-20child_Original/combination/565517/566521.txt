History pulldown (on back button) should contain more than 10 items when possible
(Note: this is filed as part of the “Paper Cut” bugs — we assume that there may be multiple existing bugs on this. Please make them block this bug, and we will de-dupe if they are indeed exactly the same. Thanks!)

To reproduce:
1. browse through more than 20 links
2. Use the pull down menu of the back button (press and hold and wait a sec)
3. Notice that your last 10 items are the only ones showing, even though you have plenty of screen real estate

Recommendation:
We should show as many histories items as possible (per screen size) in this drop down from the back button.