Opening tabs at the end when new and next to when context-triggered is confusing — consolidate
(Note: this is filed as part of the “Paper Cut” bugs — we assume that there may be multiple existing bugs on this. Please make them block this bug, and we will de-dupe if they are indeed exactly the same. Thanks!)

To reproduce:
1. Start Firefox, open multiple tabs
2. Open a new blank tab
3. Open a context-triggered tab

Recommendation:
Always open tabs next to current tab or at the end of the tab bar. Simple is better.