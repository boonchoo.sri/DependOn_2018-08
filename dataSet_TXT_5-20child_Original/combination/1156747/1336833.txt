Printing a lot of content hangs Firefox unusable and unnavigable
Consider the following code:

> <html><script>
> function print() {
> 	for(var i = 0; i < 1000; ++i) console.error('I print, therefore I am! ' + i);
> 	requestAnimationFrame(print);
> }
> requestAnimationFrame(print);
> </script></html>

This page prints so much that the browser becomes unresponsive. The slow script dialog/banner does not come up on this, and it is not possible to navigate away from the page or close the browser from X in the corner. User has to kill the browser from task manager to resume.