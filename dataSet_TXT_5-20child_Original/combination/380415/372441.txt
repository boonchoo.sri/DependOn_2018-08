Implement registerProtocolHandler for arbitrary protocols
It should be possible for web applications to act as handlers for content types and protocols, as specified in the WHATWG HTML5 draft.