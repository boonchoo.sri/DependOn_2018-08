AudioNode Inspector should evaluate floats without a leading 0
When altering a property on an AudioNode, like a GainNode's "gain", I should be able to enter in `.1` and have that set the gain to `0.1`.

Currently we `JSON.parse` the string entered into the VariablesView, so this fix would be in the `_onEval` method in `browser/devtools/webaudioeditor/webaudioeditor-view.js`