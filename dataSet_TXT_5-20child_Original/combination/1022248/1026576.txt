Translation / scale of the graph is not reset when the page is reloaded
So if the last graph was huge and I had scrolled down but then I reload the page, and the graph is smaller now I might just get an apparently empty graph, but the fact is that the graph is there, only scrolled away.

Should the graph be "centered" or scaled to show the graph when drawn for the first time or something similar?