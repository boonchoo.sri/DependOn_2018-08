Render connections only once
Created attachment 8448301
multiconnect-wae.png

While creating a beautiful image, the web audio editor should not render connections to another audio node multiple times, even if node.connect(otherNode) is called several times.