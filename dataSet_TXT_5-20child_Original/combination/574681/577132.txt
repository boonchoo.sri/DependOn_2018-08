Give items in Aero Glass area ability to change their styling to adapt to Aero's color scheme
User-Agent:       Mozilla/5.0 (Windows; Windows NT 6.1; WOW64; sk; rv:2.0b2pre) Gecko/20100706 Minefield/4.0b2pre
Build Identifier: Mozilla/5.0 (Windows; Windows NT 6.1; WOW64; sk; rv:2.0b2pre) Gecko/20100706 Minefield/4.0b2pre

Some days ago, I saw screenshots of new Trillian 5 and I must say, they are very attractive. They adapt single button idea, but they take it even further. It looks native. I mean, 100% native. And ikt can change it's color according system. So I have two requests:
1. Made Firefox Button look like 100% native title bar button.
2. Give Firefox Button ability to adapt to Aero's color scheme

Reproducible: Always