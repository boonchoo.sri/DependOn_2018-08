identity block lacks active and open state styling and shouldn't show the focus outline when clicked
Created attachment 8882924
dotted-borders.png

Nightly 56 x64 20170702100334 @ Debian Testing (Linux 4.9.0-3-amd64, Radeon RX480)

Please compare the attached image with:
https://people-mozilla.org/~shorlander/projects/photon/Mockups/linux.html

The (still) dotted borders ever were very annoying.