Reader View promo panel is automatically dismissed
Reproducible on latest 40.0a1 (2015-05-10)
Affected platforms: Mac OS X 10.9.5 and Ubuntu 14.04 x64

Steps to reproduce:
1. Launch Firefox with a clean profile.
2. Navigate to a Reader View compatible page, e.g. http://bits.blogs.nytimes.com/2015/03/30/the-path-toward-tomorrows-internet/

Expected result:
The Reader View promo panel is displayed.

Actual result:
The Reader View promo panel is displayed and automatically dismissed.

Notes:
1. Not reproducible on Windows 7 x64 nor Windows 8.1 x86
2. Screencast: https://goo.gl/NkewDG