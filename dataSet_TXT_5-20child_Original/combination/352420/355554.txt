File downloads should honor Vista's parent control setting
Part of the work to integrate Firefox with Vista's builtin parental controls:

http://www.microsoft.com/windowsvista/community/parentalcontrols.mspx

Vista has a system wide parental control setting for disabling file downloads that should be honored by all desktop applications. Firefox needs to check this global setting and disable file downloads if it is set.