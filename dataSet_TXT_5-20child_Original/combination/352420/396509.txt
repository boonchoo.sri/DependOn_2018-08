Standard Vista account with Parental Controls turned on cannot launch Firefox
If a "Standard" account on Windows Vista has Parental Controls turned on for it at all, when that account tries to launch Firefox, the "busy" mouse pointer will appear for a second and then disappear. Nothing else happens and Firefox does not launch. 

If Parental Controls are turned off for the same user, Firefox will launch. If they are turned back on, it will fail to launch again.

This was found in the M8 RC1 on Windows Vista Ultimate edition: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9a8) Gecko/2007091216 GranParadiso/3.0a8