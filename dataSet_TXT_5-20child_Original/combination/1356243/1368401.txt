screenshots has gone haywire notification on startup
55.0a1 (2017-05-28) (64-bit)

Screenshot: https://i.imgur.com/fL5Iwmf.png

STR:

1. shut down firefox
2. start up firefox

Expected: no notification

Actual: i get this notification

Caspy7 also pointed out someone on Reddit seeing same thing: https://www.reddit.com/r/firefox/comments/6dobc2/firefox_screenshots_is_now_shipped_with_lastest/di45gml/