Dev Edition theme: Fix the menubar foreground color on Windows
As mentioned by Gijs in https://bugzilla.mozilla.org/show_bug.cgi?id=1158872#c3:

Right now in a non-maximized window on win8 the dark/light theme causes the windows in-titlebar text (for the menubar) to be white/black, but it doesn't replace the background and so the white/black is sometimes unreadable depending on the window color.  I expect xp + vista (classic and luna/aero) will have similar issues, esp. if the fog is applied differently because of the lwt-ness...