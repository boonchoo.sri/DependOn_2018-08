Update the default browser prefs UI and functionality for Windows 8
Created attachment 629346
current prefs panel button and text

On Windows 8 we no longer have the ability to set the browser as the default, users must do this manually. The user has the option of changing this either through an initial prompt put up by the OS on first install, or through the default programs control panel.

What we think we should do here is change the prefs button so that it opens the default programs control panel, and update the button label and text to reflect what this feature does.