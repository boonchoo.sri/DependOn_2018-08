Closing sidebar flickers the page when the sidebar is on the left
Mozilla/5.0 (X11; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0 ID:20170601100220 CSet: bdb2387396b4a74dfefb7c983733eed3625e906a

When the sidebar is on the left, closing it (e.g. Ctrl+B) flickers the site. Looking closely I noticed the sidebar actually moves to the right side for a supershort moment and than closes.

Also rerendering the side, when the sidebar is opened looks much worse than when it's on the right. Just give it a try in this bug.

When the sidebar is on the right, there is no visible issue.

STR:
1) Have sidebar set to open on the right.
2) Ctrl+B to open it.
3) Ctrl+B to close it.
4) The page is narrowed/widen instantly without any flickering or blinks.
5) Set the sidebar on the left.
6) Repeat 2) and 3).
7) Notice, that when opening the the sidebar the page jump somehow. When closing the sidebar, white area appears on the right side of the page.