Move webConsoleClient to utils/client.js
Move webConsoleClient from controller to utils/client.js and export some useful public APIs.

* Let utils/client.js takes over webConsoleClient getString, sendHTTPRequest
* Move controller public APIs to client.js
  * triggerActivity
  * inspectRequest (also use in webconsole panel)
* Export viewSourceInDebugger as a public API
* Export supportsCustomRequest as a public API
* Use these exported APIs in utils/client.js such as getString, supportCustomRequest in our codebase.