[Out Of Date Notification]  VoiceOver doesn't work with out of date notification bar
[Affected OS]:
-Mac

[Affected versions]:
44.*

[Prerequisites]:

1. Install Firefox version 44.*
2. Outofdate notifications system add-on required configurations:
2.1 about:config set extensions.systemAddon.update.url to value 	"https://aus5.mozilla.org/update/3/SystemAddons/%VERSION%/%BUILD_ID%/%BUILD_TARGET%/%LOCALE%/release-sysaddon/%OS_VERSION%/%DISTRIBUTION%/%DISTRIBUTION_VERSION%/update.xml"
2.2 about:config set extensions.logging.enabled	to value "True"
2.3 about:config Add string app.update.url.override with value "www.softvision.ro" 
3. Set update preferences ->  about:preferences#advanced and set Firefox updates to "Automatically install updates (recommended: improved security)"

4. Force system addons check : Open Tool/WebDeveloper/Web Console and run the snippet:   " Components.utils.import("resource://gre/modules/AddonManager.jsm"); AddonManagerPrivate.backgroundUpdateCheck(); "

In the Tools/ WebDeveloper/BrowserConsole a similar line should be listed: "addons.productaddons INFO Downloading from https://ftp.mozilla.org/pub/system-addons/outofdate-notifications/outofdate-notifications-1.2@mozilla.org.xpi to /tmp/tmpaddon"

5. Restart FF to install the OutOfDate System Addon - !!!! Do not open About:Support to check if updates are being downloaded --> use about:support

6.Enable VoiceOver:  System Preferences -> Accessibility -> VoiceOver -> Enable VoiceOver

[Steps]:
1. Open FF44.* 
2. Start the VoiceOver tool
3. Focus the out of date notification bar

[Expected Result]:
The VoiceOver tool informs you that "Your Firefox is out of date. Please download a fresh copy."

[Actual Result]:
The VoiceOver tool doesn't informs you that "Your Firefox is out of date. Please download a fresh copy."