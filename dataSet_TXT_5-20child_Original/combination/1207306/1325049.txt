Global sharing indicator doesn't work correctly with e10s multi
Steps to reproduce:
1. Enable e10s multi (set dom.ipc.processCount to 2 in about:config)
2. Load https://people-mozilla.org/~fqueze2/webrtc/ and share the camera.
3. Load http://mozilla.github.io/webrtc-landing/gum_test.html in a second tab, and share the camera.
4. Click the "Stop" button.
5. (Mac) Click the Camera icon in the menubar.

Expected result:
The menu should offer to control sharing for the first tab.

Actual result:
The menu has only one disable item, saying "Sharing Camera with 0 tabs".


This is because when receiving the "webrtc:UpdatingIndicators" message from any content process, webrtcUI.jsm clears the webrtcUI._streams array that contained info about active streams from all processes: http://searchfox.org/mozilla-central/rev/cc2a84852bd4e6f6d8d4d5b17b8382bb5d005749/browser/modules/webrtcUI.jsm#217