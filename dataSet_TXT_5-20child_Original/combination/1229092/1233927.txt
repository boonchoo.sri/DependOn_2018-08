Switching between large JS files in debugger is slow
CodeMirror has this API: http://codemirror.net/doc/manual.html#api_doc

It looks like it allows you to dump a large file into it (like 50MB) and then get back an object that has all the internal state already set up, and you can simply swap it back in later. Doing so should make switching to large files almost instant, even if they originally took a long time to load.