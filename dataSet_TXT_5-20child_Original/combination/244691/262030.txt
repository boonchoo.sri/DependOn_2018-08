unnamed bookmarks makes extra margins on the right
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.8a4) Gecko/20040927
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.8a4) Gecko/20040927

I have some unnamed bookmarks with favicon to acts like a button when clicking
in the Bookmarks Toolbar. However, it has extra margins when hovering which
makes it non-square and makes it ugly. I appreciate everyone who can solve this
issue as I don't have any tools to work on CVS.

Reproducible: Always
Steps to Reproduce:
1. Create a bookmark without Name.
2. Move your mouse on that bookmark inside the Bookmarks Toolbar.
3. It has extra right margins when hovering which makes it quite ugly.