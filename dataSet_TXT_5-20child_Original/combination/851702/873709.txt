Firefox v23 - "Disable JavaScript " Check Box Removed from Options/Preferences Applet
Created attachment 751273
Screen Shot 2013-05-17 at 8.45.29 PM.png

User Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:23.0) Gecko/20130517 Firefox/23.0 (Nightly/Aurora)
Build ID: 20130517004016

Steps to reproduce:

Firefox Automatically Updated to v23 in the Aurora Channel, and even though I had deliberately disabled JavaScript, the update re-enabled JavaScript, and removed the "Disable JavaScript" option from the settings applet.


Actual results:

The ability to disable JavaScript is now obfuscated, and users are deliberately discouraged against manipulating their JavaScript preferences. This is wrong, and inhibits a users understanding of what happens when they load a web page.

The following is not an acceptable work around:
about:config > javascript.enabled 

This destroys a non-technical user's grasp of the differences between static HTML and programatically manipulated HTML. It hides the setting amidst hundreds of other obscure settings, and does not emphasize the extremely powerful tool that JavaScript is, and the fact that it is optional.


Expected results:

When updating Firefox, my personal settings should not be disturbed. If I have disabled JavaScript, it may be for a very good reason. The JavaScript setting should be nearly at my fingertips, as a native setting, with out the assistance of an add-on. 

In the preferences applet, it's about 5 clicks deep.

In the "about:config" registry, I must first KNOW that the registry exists, and know what to type to find it. I must bypass a warning, assuring that I understand the nature of the settings I will be "tampering" with. I must know to search for the registry key. I must figure out what registry key to search for. I must type in the search string correctly and locate the proper setting, and I must change the setting properly. 

Take note that there is cognitive dissonance in the idea that changing settings in "about:config" may be dangerous, even though this is the only way to disable JavaScript now, and that leaving JavaScript enabled at all times is not "universally safe." Please don't pretend that it is.