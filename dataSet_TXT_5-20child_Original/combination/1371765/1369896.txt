[UX] Download button should only appear in the toolbar when there are current or recent downloads
In the Photon design specs for Downloads (bug 1353434) there is a proposed change of behavior: the download button will be hidden/absent from the toolbar until a download begins (or resumes.) 

This bug is splintered out from bug 1352065 to address the details of this behavior change - and its implications - without getting tangled up in the animation changes.