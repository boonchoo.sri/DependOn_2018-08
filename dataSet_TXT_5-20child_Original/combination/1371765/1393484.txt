Download button doesn't appear when using an add-on to download a youtube video
STR:
1. Install https://addons.mozilla.org/en-US/firefox/addon/easy-youtube-video-download/
2. Go to https://www.youtube.com/watch?v=bjmgzLVuWLU (or any other youtube video page)
3. Use the add-ons' download widget to download the mp4 version of the video.

Expected result: the Firefox download button should appear and show me the progress of my download.

Actual result: the Firefox button does not appear. The file does download though.

I also got the same result with this add-on https://addons.mozilla.org/en-US/firefox/addon/video-downloader-pure/