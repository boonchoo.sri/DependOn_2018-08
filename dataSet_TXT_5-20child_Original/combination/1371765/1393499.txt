Download button is not removed in background windows after download list is cleared (if indicator was never loaded in the background window)
User Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0
Build ID: 20170824100243

Steps to reproduce:

Nightly 24-08-17, create new profile, open new window, go to http://imgur.com/gallery/OyQKJaD
Right click on image - save as - choose any directory
Tools - Downloads - Clear Downloads


Actual results:

Download notification arrow is removed in active window but not in background window


Expected results:

Download notification arrow is removed in all windows