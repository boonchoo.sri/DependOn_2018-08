Trace client should always emit "enteredFrame" and "exitedFrame" events in sequence order
enteredFrame and exitedFrame packets contain a sequence number, since building up a call tree depends on the ordering of these packets, and we don't want to rely on the fact that packets are currently always received from the server in the order they were sent.

Currently, enteredFrame and exitedFrame events are emitted by the trace client whenever a packet is received. If we can't rely on receiving the packets in order, the client should queue unexpected packets and wait for the next in the sequence.