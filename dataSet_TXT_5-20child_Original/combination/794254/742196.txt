"Copy Property" and "Copy Property Value" in the style pane doesn't work in some cases
Build identifier: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:14.0) Gecko/20120403 Firefox/14.0a1

Also reproducible on Windows 7.

STR:
1) Right-click on page, Inspect Element.
2) Show the style pane if it's not already showing.
3) Right-click a property name on one of the applied rules.
4) Click on "Copy Property Value" in the context menu.

Expected Result:
The property's value is copied to the clipboard.

Actual Result:
The clipboard contents are not modified.

A similar problem happens for "Copy Property" if you right-click on the property value.

Jared discovered that the menu options don't work on Windows 7 if some text in the property or its value were selected.  On OS X, the STR are always reproducible regardless of text selection.