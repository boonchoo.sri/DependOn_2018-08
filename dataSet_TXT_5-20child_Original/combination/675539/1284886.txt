Add discardBrowser method to tabbrowser.xml
WebExtensions can make use of it too.

e.g. browser.tabs.unload()

Like it does with browser.tabs.remove() also calling gBrowser.removeTab
https://dxr.mozilla.org/mozilla-central/source/browser/components/extensions/ext-tabs.js#546