Always show hand cursor when new download panel is open
Created attachment 629392
Bug_cursor video

User Agent: Mozilla/5.0 (X11; Linux x86_64; rv:14.0) Gecko/20120601 Firefox/14.0a2
Build ID: 20120601042008

Steps to reproduce:

1.- Open the new download panel
2.- Move the cursor over download panel
3.- Move out of the panel through bottom


Actual results:

Always show the hand cursor


Expected results:

It should show normal cursor