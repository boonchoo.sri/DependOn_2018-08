Tab-focus behaviour different when closing a tab spawned via 'Undo Close Tab'
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1b1) Gecko/20060719 BonEcho/2.0b1
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1b1) Gecko/20060719 BonEcho/2.0b1

1. Ensure that you have at least two tabs open (it doesn't matter whether or not they're blank).
2. Focus the very first tab you have open.
3. Open a new tab with focus (for example, by pressing Ctrl+T or equivalent).
4. Navigate to any web site.
5. Close the current tab.

Focus now switches to the first tab you have open, as expected.

However, now do the following:

1. Right-click the tab bar and select 'Undo Close Tab'.
2. Close the current tab.

Focus now switches to the rightmost tab -- *not* the first tab you have open, as before.

Is this intended?  It seems a little inconsistent.

Reproducible: Always