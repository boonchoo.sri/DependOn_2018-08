Middle click on open tab focuses randomly on another tab.
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1

When I middle click on a tab, the focus loops through all of the other tabs and gives focus to a random tab instead of the tab to the right.  The tab that is selected to be closed is closed.

Reproducible: Always

Steps to Reproduce:
1. Open 3 or more tabs.
2. Middle click on a link in the second or third tab.
3. Goto the new tab.
4. Middle click on that tab (to close).
Actual Results:  
Selected tab is closed.  The focus loops through all of the other tabs and gives focus to a random tab.

Expected Results:  
Focus goes to the tab to the right (or immediate left if there is none to the right).

Theme: Noia (will confirm with default theme - if no additional notes, assume it happens with the default theme).