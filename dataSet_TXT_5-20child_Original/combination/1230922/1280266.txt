Netmonitor JS stack traces shouldn't include chrome frames
Requests JS stack traces in Netmonitor sometimes show chrome:// stack frames. This is confusing for users - they should only see the JS code from the page content.

The chrome:// frames should be shown only in browser toolbox.

Two examples of this behavior:

1) When the page is reloaded, the main document request shows stack trace from the browser calling into webNav.reload at chrome://browser/content/tab-content.js:73

2) When issuing a request from the console (i.e., type "fetch('x')"), the request stack trace contains frames like:

WCA_evalWithDebugger resource://devtools/server/actors/webconsole.js:1299