Improve initiator info to network requests
The Network panel should provide information about the initiator of each network request.

This information differs between the different origins of the requests:

1. When parsing HTML, SVG, etc.
For requests caused by parsing HTML (e.g. <script> or <img> tags) the related HTML file (and maybe line and column numbers) and/or HTML node may be displayed.
Clicking it would switch to the Inspector panel and select the element there.

2. When parsing CSS
For requests caused by parsing CSS (e.g. url() or image() notations) the related CSS file (and maybe line and column numbers) and/or CSS rule may be displayed.
Clicking it would switch to the Style Editor panel and highlight the related property (or even the related value) there.
Hovering it could display the related CSS rule within a tooltip.

3. When executing JavaScript
For requests caused by JavaScript functions (e.g. XMLHttpRequest.send() or appending a <script> element to the DOM structure) the related script URL (and maybe line and column numbers) and/or function name may be displayed.
Clicking it would switch to the Debugger panel and highlight the related function there.
Hovering it could display the stack trace for the function call within a tooltip.

4. When page is called
For requests caused by the user there would be a note displayed that the request was initiated by him/her (i.e. by entering a URL into the awesome bar).

5. Unknown origin
For requests that are caused by something that is not handled explicitly a note is displayed telling so.

This bug requires bug 563623 to be implemented.
Note that this feature is already available in Chrome via the 'Initiator' column in its Network panel.

Sebastian