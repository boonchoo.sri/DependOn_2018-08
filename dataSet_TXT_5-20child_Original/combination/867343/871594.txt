Replace references to BookmarksMenuButton to BookmarkingUI in browser-places.js
There are two places where we have error messages referring to BookmarksMenuButton, when BookmarksMenuButton has been renamed as BookmarkingUI.

We should update those error messages.