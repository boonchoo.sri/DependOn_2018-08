Expose CodeMirror instance and allow direct embedding of editor
The editor.js API does not expose the internal CodeMirror instance, probably to try to keep it as internal as possible, but it's a hassle to try to abstract away the entire API. We are still inherently tied to CodeMirror: our API is very CodeMirror-inspired.

Our new debugger works directly with CodeMirror and uses properties that editor.js does not expose. Instead of continuing this cat-and-mouse, let's just expose CodeMirror. That will make it easy for tools like the new debugger to integrate with CodeMirror and use editor.js when it's in the panel.

Long-term I think everyone is on board for refactoring editor.js to take a different approach, where we still bundle in modes/themes/etc but do not try to abstract away the API. We will work on this over time.