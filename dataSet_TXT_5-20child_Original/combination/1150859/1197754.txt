Remove preprocessing from browser/components/shell XUL file
+++ This bug was initially created as a clone of Bug #1197745 +++

setDesktopBackground.xul uses a bunch of preprocessing. In turn:

#ifdef XP_MACOSX
<?xul-overlay href="chrome://browser/content/macBrowserOverlay.xul"?>
#endif

can be converted to an OSX-only jar.mn manifest overlay instruction.

#ifndef XP_MACOSX
        buttons="accept,cancel"
#else
        buttons="accept"
#endif


Can be done at runtime from the corresponding JS file.

The first ifdefed thing, ie the non-mac hbox can just have its ifdefs removed.

The second thing, ie the mac-only separator + hbox... the separator could be removed, and the hbox set to be hidden by default. After that hbox, we could insert a simple in-the-file <script> that inserts the hbox on mac, hides the first hbox (see above) and unhides the second.

We'd want to be sure to have done all this before first paint, so that the dialog is correctly sized. We'd need to test on at least mac and one non-mac platform.


#ifdef XP_MACOSX
#include ../../../base/content/browserMountPoints.inc
#endif

This could become an overlay, AIUI. There are many other files that use it, and I'm so I'll file a separate bug about it, and we can leave it in for this bug...