modules loaded in child processes don't get reloaded via detools addon
Any module loaded in the child doesn't get reloaded for two reasons:
* devtools/server/child.js frame script doesn't get reloaded
  loadFrameScript caches scripts and unfortunately there is no way to force reload nor purge the cache. I opened bug 1258496 to address that.

* Loader.jsm doesn't get reloaded
  So that even if the frame script is reloaded the JSM keeps the same modules instances. We have to communicate with child processes to call its reload method.