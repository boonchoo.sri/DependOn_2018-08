We can have multiple toolboxes stacking when using the reload addon
There is various ways to have more than one toolbox open for a single tab.
You need to ask the addon to reload via CTRL+ALT+R,
then do any command that opens a toolbox, like CTRL+MAJ+I.

This is again related to some state mixed in gDevTools.jsm between the multiple loader instances.
I may be a good time to finally do bug 1188405.