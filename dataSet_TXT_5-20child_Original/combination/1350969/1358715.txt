[Performance] Queue NetworkEventUpdate in netmonitor controller
Once network request arrives will follow a series of event packets:

1. First, a "NetworkEvent" event message and invoke addRequest() in netmonitor-controller.
2. Afterward, the network request will follow up to 8 times "NetworkEventUpdate" event messages and then invoke at most 8 times updateRequest() in netmonitor-controller. 

These following possible packet updateTypes are requestHeaders, requestCookies, requestPostData, securityInfo, responseHeaders, responseCookies, responseStart, responseContent and eventTimings. The order of above NetworkEventUpdate is predictable, and the eventTimings is always the last one packet from "NetworkEventUpdate".

As a result, we can queue these "NetworkEventUpdate" into an array and then just dispatch one updateRequest() to tell redux to refresh react component if the entire "NetworkEventUpdate" packets are ready.

Bottom line is that after adopting this approach we are able to reduce updateRequest() calls from 8 times to 1 time and to gain performance win.