[meta] [Performance] New netmonitor can be slow when a lot of requests are happening in the same time
See profile here: https://perfht.ml/2oqj0Z0

Especially look at what happens in the main thread.
I could very easily make my Firefox hangs for several seconds.

I'll try to do a testcase.