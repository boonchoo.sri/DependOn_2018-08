Replace promiseErrorPageLoaded with BrowserTestUtils.waitForErrorPage in browser_ssl_error_reports.js
The promiseErrorPageLoaded[0] function is used in browser_ssl_error_reports.js[1] to wait until cert errors are loaded. We should replace it with the more reliable BrowserTestUtils.waitForErrorPage[2] function and make sure the affected tests still run correctly.

[0] http://searchfox.org/mozilla-central/rev/8fa84ca6444e2e01fb405e078f6d2c8da0e55723/browser/base/content/test/general/head.js#986
[1] http://searchfox.org/mozilla-central/source/browser/base/content/test/general/browser_ssl_error_reports.js
[2] http://searchfox.org/mozilla-central/rev/8fa84ca6444e2e01fb405e078f6d2c8da0e55723/testing/mochitest/BrowserTestUtils/BrowserTestUtils.jsm#689

You can run the test with

./mach mochitest browser/base/content/test/general/browser_ssl_error_reports.js

and learn more about Mochitest here: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/Mochitest