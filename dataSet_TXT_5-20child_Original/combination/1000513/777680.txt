"Bookmark This Page" in context menu should be highlighted
Created attachment 646092
Mockup

User Agent: Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/17.0 Firefox/17.0
Build ID: 20120724040203

Steps to reproduce:

In the context menu item "Bookmark This Page" should be highlighted when the page is added to your bookmarks. Currently, this element are the same. I created a simple mockup.