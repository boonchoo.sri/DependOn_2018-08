Allow users to revert to classic context menu
User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0 (Beta/Release)
Build ID: 20140529113510

Steps to reproduce:

New context menu as of the May 27th nightly is another example of terrible UI designs being forced on users. There are a couple reasons I find this new menu unusable including:

Those of us who have had the old context menu ingrained into our muscle memory now have to spend far more time looking for the right option

The reload, forward and bookmark buttons are no longer accessible using a simple downward mouse movement, instead require a separate rightward mouse movement


Actual results:

See above


Expected results:

There should be an option to revert to the classic menu