Line above navigation items on context menu not full width
Created attachment 8429980
contextmenu.png

User Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0 (Beta/Release)
Build ID: 20140527030202

Steps to reproduce:

Context <menu> entries appear above navigation items on context menu. The line above the navigation items does not extend the full width of the context menu.

Can be seen here:
https://developer.mozilla.org/en-US/docs/DOM_Inspector/Introduction_to_DOM_Inspector