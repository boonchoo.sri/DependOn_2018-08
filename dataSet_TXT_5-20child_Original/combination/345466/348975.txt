Tab strip color does not match OS theme
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9a1) Gecko/20060816 Minefield/3.0a1
Build Identifier: 

Looks like the new Winstripe tab strip goes together with Luna only. I suggest to use semi-transparent pngs to overlay the native color.

Reproducible: Always