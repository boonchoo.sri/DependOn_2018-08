On Linux, space between right tab scroll button and All Tabs menu looks odd
After the landing of bug 345466, the tab strip right scroll button has a strange, XOR-lloking space between it and the All Tabs button.

Screenshot to follow.