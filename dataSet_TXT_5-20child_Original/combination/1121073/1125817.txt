Disconnecting a node immediately after creating it fails
Created attachment 8554531
quickdisc.html

Due to creation of the node on the client is async, and disconnecting is sync (once on the client), disconnecting a node fails as the node does not yet exist since the creation method is not yet finished.