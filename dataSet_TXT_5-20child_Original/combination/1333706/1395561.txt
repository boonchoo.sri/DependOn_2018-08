Overlay Grid remains active in Rules after it was activated two or more times in Layout
Created attachment 8903146
Gif showing the issue

[Affected versions]:
- Nightly 56.0a1
- Latest Nightly 57.0a1
- Beta 56.0b7

[Affected platforms]:
- Ubuntu 16.04 x 64
- Windows 7 x 32
- Mac OS x 10.11

[Steps to reproduce]:
1. Go to http://labs.jensimmons.com/2016/examples/grid-content-1.html
2. Open Inspector Grid (Ctrl+Shift+C or Cmd+Alt+C)
3. From Inspector - Layout select 'ul' Overlay Grid and its node.
4. Select 'main' Overlay Grid and that should activate the main grid.
5. Go to Rules and observe grid icon (▦) 
6. Repeat step 3, 4 and 5.

[Expected result]:
- 'ul' grid icon (▦) should be deactivated, indicated by the color black.

[Actual result]:
- 'ul' grid icon (▦) is blue, which means it's activated.

[Regression range]:
- This is not a regression because it's reproducing even on Nighlty from 2017-06-20 when the feature was first implemented.