Colorpicker remains behind when an overlay grid is active and the color is changed multiple times thus any action on overlays is ignored
[Affected versions]:
- latest Nightly 56.0a1
- even though builds from 54 and up are affected, Layout view is only enabled by default in 56 so I considered 54 and 55 as unaffected.

[Affected platforms]:
- macOS 10.12.5
- Ubuntu 16.04 32bit
- Windows 10 64bit

[Steps to reproduce]:
1. Start Firefox
2. Visit a webpage that contains css grid
(eg: https://www.mozilla.org/en-US/developer/css-grid/)
3. Open Inspector(cmd+alt+C / ctrl+shift+C)
4. Enter Layout tab
5. Enable an Overlay Grid
6. Open the colorpicker on the right side of node icon.
7. Hold the mouse inside the colorpicker and move it around.
8. Disable the Overlay Grid by clicking the checkbox	

[Expected result]:
- At step 7, the color changes in real time.
- At step 8 the Overlay is disabled with success.

[Actual result]:
- At step 7 the color remains behind.
- At step 8 the Overlay can't be disabled because the color is still changing.

[Regression range]:
- This is not a regression, it was introduced when colorpicker was first introduced in bug 1338300.

[Additional notes]:
- Here is a screencast showing the issue: https://www.dropbox.com/s/cy3gwnt778x7dmo/Color%20picker%20slowness%20affects%20grid.mov?dl=0
- This issue does not reproduce if no overlay grid is enabled and step 6 is followed.