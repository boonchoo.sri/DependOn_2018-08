Area information is not displayed for boxes that don't have the space to show it above it or on left/right extremities
Created attachment 8904920
Screenshot showing the issue

[Affected versions]:
- Latest Nightly 57.0a1
- Beta 56.0b9

[Affected platforms]:
- Ubuntu 16.04 64bit
- Windows 10 64bit
- macOS 10.12.6

[Steps to reproduce]:
1. Go to http://labs.jensimmons.com/2017/03-008.html
2. Open Inspector Grid (Ctrl+Shift+C or Cmd+Alt+C)
3. From Inspector - Layout select 'main' or 'ul' Overlay Grid
4. Scroll down the page in Layout and select any box from row 1 from grid outline

Other steps to reproduce when selecting boxes from extremities:
1. Go to https://stripe.com/connect
2. Open Inspector Grid (Ctrl+Shift+C or Cmd+Alt+C)
3. From Inspector - Layout select the first Overlay Grid ('div.stripes')
4. Scroll down the page in Layout and select any box from extremities from grid outline

[Expected result]:
- Are information is displayed inside the webpage.

[Actual result]:
- Area information is not displayed for the boxes from the first row.

[Regression range]:
- This is not a regression because it's reproducing even on Nighlty from 2017-06-20 when grid outline was first available in Layout.

[Additional notes]:
- Not sure if there is a bug logged for this already, I checked a little bit but could not find one.
- ALso the numbers are also hidden for the box extremities