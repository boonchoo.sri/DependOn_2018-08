lightweight themes selection panel doesn't contract to fit smaller list
screenshot: http://cl.ly/image/2X162I0S020j

Steps:
- install a few recommended themes by clicking on them
- install a another theme or two from the web
- go into the addons manager and remove some of them
- open this panel again