Should skip windows that are closed but not yet destroyed when using nsIWindowMediator
nsIWindowMediator tracks windows until they are destroyed, but there's a brief period when windows are closed without being destroyed yet. Session restore should only care for windows that are open and should thus skip windows that aren't.

I don't think this is relevant for real users, but it likely is relevant for tests and could in fact be the cause for some intermittent failures we're seeing.