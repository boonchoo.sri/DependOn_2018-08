Show the URL bar dropmarker on toolbar hover instead of location bar hover
Bug 1203494 made the URL bar dropmarker only appear when the location bar is hovered, but that requires high precision and some specific directional movement of the mouse to trigger it with high possibility of stopping the mouse where the dropmarker will appear.

Also, as a user who frequently uses the dropmarker, I find that it disappears too quickly if I accidentally overshoot the location bar and the mouse stops on the tabstrip above the navigation toolbar.