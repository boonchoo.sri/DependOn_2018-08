about:tabcrashed form checkbox should trigger a transition
With bug 1110511 landed, we now ship with a form in about:tabcrashed for the user to give details about what they were experiencing when the crash occurred.

We show or hide that form based on whether or not the user toggles the "send report" checkbox.

That toggle is quite jarring - there's no transition between showing the form and not. We could probably smooth that out a little.