The crash reporter content is not displayed if was automatically checked on tab crash
Created attachment 8710316
crash reporter not displayed.png

STR:
1. Install http://people.mozilla.org/~tmielczarek/crashme/
2. Go to Customize and move the "crash content process" button to the menu panel
3. Crash the current tab

Actual results:
"Submit a crash report" checked, but the crash reporter content is not displayed

Note: unchecking/checking again reveals the crash reporter