Tabstrip inconsistencies
User-Agent:       Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0b10pre) Gecko/20110114 Firefox/4.0b10pre
Build Identifier: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0b10pre) Gecko/20110114 Firefox/4.0b10pre

There are inconsistencies in the Tabstrip on the left/right side.

Minefied 4.0b10pre (2011-01-14) on Mac OS 10.6.6

Thanks and regards
Mehmet

Reproducible: Always

Steps to Reproduce:
1. open a lot of Tabs, so that the arrows appear on the left/right
2. click the second tab from the left
3. Take a look between visible first tab and the arrow
4. click on the penultimate tab on the right side
5. Take a look between visible last tab and the arrow
Actual Results:  
You see inconsistencies.

Expected Results:  
No inconsistencies.