[userstory] Network Throttling Control
There is some discussion about this in Bug 1028905.  See Comments 11-15 on that bug.

> The use I can see for simulating a "slow network" is to improve perceived performance,
> like Brian said (e.g. a page shouldn't load social widgets before their main content!).
> This could be achieved by adding controllable random latency (not sure if throttling
> throughput is useful here).
>
> Then the network can be made "unreliable" to test a website's resilience in the face
> of bad conitions, e.g. by randomly dropping requests (with controllable probability).