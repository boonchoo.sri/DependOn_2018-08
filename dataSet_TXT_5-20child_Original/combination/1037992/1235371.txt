Change EnvironmentActor to protocol.js
Created attachment 8702274
EnvironmentActor.patch

Splitting this out from Bug #1037992

This is the latest review from Bug 1037992, comment 13, by jryans:


Review of attachment 8664980 [details] [diff] [review]:
-----------------------------------------------------------------

::: devtools/server/actors/script.js
@@ +3416,5 @@
>   *        The lexical environment that will be used to create the actor.
>   * @param ThreadActor aThreadActor
>   *        The parent thread actor that contains this environment.
>   */
> +let EnvironmentActor = ActorClass({

I guess these actor will manage their lifetimes outside of p.js's usual flow?  I see this one is added a to a pool, for example.