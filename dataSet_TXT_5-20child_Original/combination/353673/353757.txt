New theme landing caused a Ts regression on trunk
This was probably less-noticeable on branch since the theme went in in pieces, but when it went in in one big chunk on trunk, it's hit on Ts was fairly obvious.

The numbers are roughly a 3% increase on Linux, and a 5% increase on Windows. 

Some numbers...
argo: 897ms -> 933ms
argo test: 2138ms -> 2203ms
gais test: 1672ms -> 1750ms