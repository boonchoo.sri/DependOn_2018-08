Remove code associated with comments linking to bug 1265759
There are some comments linking to bug 1265759. Now that bug 1265759 is fixed, we can remove the comments and the code related to it.
https://dxr.mozilla.org/mozilla-central/source/devtools/client/themes/inspector.css#117
https://dxr.mozilla.org/mozilla-central/source/devtools/client/themes/inspector.css#108