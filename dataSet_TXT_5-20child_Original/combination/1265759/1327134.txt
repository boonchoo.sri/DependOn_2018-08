Close button in inspector search box briefly appears (blinks) when I open inspector
>>>   My Info:   Win7_64, Nightly 51, 32bit, ID 20160902030222 (2016-09-02)
STR_1:
1. Open http://www.rp-online.de/
2. Open inspector (Ctrl+Shift+C)

AR:  Close button in inspector search box briefly appears (blinks)
ER:  Close button in inspector search box shouldn't appear on startup

This is regression from bug 1265759. Regression range:
> https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=c9bbdb627b7804fee47aa6a6708647e6e589d09c&tochange=3269dd1a824d1b42cb021d1fb6858885179940b0