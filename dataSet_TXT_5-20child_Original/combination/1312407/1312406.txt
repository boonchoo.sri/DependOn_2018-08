Remove tab groups migration code
I discovered via eslint that one of the tests (test_TabGroupsMigrator.js) isn't fully testing everything it is meant to.

However, in talking with Gijs, this code can be removed now as it was temporary code to provide users a way forward if they had previously been using tab groups.