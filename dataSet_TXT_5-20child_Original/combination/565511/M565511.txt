Paper cuts: Startup experience
(This is a meta bug used to group the focus issues from the Paper Cuts project)

Issues that affect startup that are not related to the traditional performance metrics.