When an invalid form in an IFRAME is submitted, error messages are not displayed
User-Agent:       Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b6pre) Gecko/20100911 Firefox/4.0b6pre
Build Identifier: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b6pre) Gecko/20100911 Firefox/4.0b6pre

When the page contains an IFRAME with the form containing required fields the error message "This field is required" is not displayed

Reproducible: Always

Steps to Reproduce:
1. Open the page http://files.karczmarczyk.pl/x/
2. Don't fill in the text input, just click "Submit"

Actual Results:  
Nothing happens

Expected Results:  
Error message "This field is required" should be displayed

When you open the iframe page http://files.karczmarczyk.pl/x/iframe.html the error message is displayed