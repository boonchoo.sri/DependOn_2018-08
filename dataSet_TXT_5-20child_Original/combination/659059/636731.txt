Add GCLI commands for Workspaces
We should be able to control Workspaces via Cockpit. Initial set of commands I can think of (feel free to add/remove/adjust as makes sense):

* open workspace
* load file into workspace
* select range of lines
* object view for that range of lines ("inspect?" overloaded word?)
* evaluate that range of lines
* close the workspace (what if there are multiple?)

Note also that I think in addition to a selected DOM node notion, I think we also want a selected object. it would be good to evaluate code in the workspace and then be able to do another operation on that same object.