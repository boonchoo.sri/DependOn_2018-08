Implement Mixed Content Blocker New Icon - Frontend Changes
This bug is for the frontend changes needed for the Mixed Active Content Icon.  The patch has already been reviewed and r+'ed (https://bugzilla.mozilla.org/attachment.cgi?id=689834&action=edit), so I'll carry that over to here.


+++ This bug was initially created as a clone of Bug #782654 +++