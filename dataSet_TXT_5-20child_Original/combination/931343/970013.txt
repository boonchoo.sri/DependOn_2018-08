The bookmarks button moves to the overflow after some clicks
So I saw this the other day, then today I read of another user with the same issue and he was suggesting the issue was with double click.
Actually I can reproduce with these steps:
1. move the bookmarks button at the end of the customizable area in the navbar
2. double click on the star
3. click on remove this bookmark
4. repeat steps 2 and 3 until the button moves to the overflow

I didn't try to debug this yet