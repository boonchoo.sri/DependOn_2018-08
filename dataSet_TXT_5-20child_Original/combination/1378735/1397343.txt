Lazy load inspector front dependencies
A bunch of dependencies could be lazy loaded from inspector fronts,
especially dependencies that are only used in some request that aren't necessarely used a lot:
http://searchfox.org/mozilla-central/source/devtools/shared/fronts/