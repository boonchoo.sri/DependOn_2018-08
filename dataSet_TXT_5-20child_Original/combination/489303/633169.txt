After resizing the window using the grippy in the lower right corner the next mouse click is ignored
After resizing the window using the grippy in the lower right corner the next mouse click is ignored.

STEPS TO REPRODUCE
1. start Firefox, open two tabs
2. grab the grippy thing and resize the window then release the mouse button
3. click on the other tab

ACTUAL RESULTS
nothing happens

EXPECTED RESULTS
switch tab

This bug occurs in the first nightly with bug 489303 fixed (2011-01-18-03)
and still occurs in the latest nightly on Windows XP.