Resizer has incorrent background color when only one scrollbar is shown.
User-Agent:       Mozilla/5.0 (Windows NT 5.0; rv:2.0b10) Gecko/20100101 Firefox/4.0b10
Build Identifier: Mozilla/5.0 (Windows NT 5.0; rv:2.0b10) Gecko/20100101 Firefox/4.0b10

This bug is per Neil's comment 48 in bug 489303:

"(In reply to comment #43)

> > 2. Shouldn't the resizer be hidden when the addons bar is shown if it's going
> > to have it's own resizer?
> 
> I will fix this one. It only occurs when both scrollbars are visible (Image 5)

>I have only partially fixed this one. There scrollcorner background doesn't
appear when only one scrollbar is visible. I think I'd rather fix this in a
followup bug or patch."

This is easily testable against about:mozilla page.

- When no scrollbars are shown, resizer has transparent background.
- When both scrollbars are shown, resizer uses -moz-Dialog background color.
- When one scrollbar is shown, resizer uses white (field?) background color.  

Reproducible: Always