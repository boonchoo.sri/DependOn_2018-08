[meta] Integrate HTTPi into the Net panel
HTTPi should be used in the Net panel and replace the current sidebar. HTTPi components should be moved into netmonitor/shared directory. These are going to be shared by the Console panel as well as the Net panel.

This task can be further split into:

- Copy HTTPi code into netmonitor/shared panel directory
- Make RequestItem expandable (hidden behind a pref)
- Implement Timings Panel
- Implement Security Panel
- Remove NetworkDetailsView tabs + Make RequestItem expandable by default (removing the pref)
- Introduce an option for switching between expandability and sidebar HTTP preview