Customization listeners should be notified asynchronously
When doing batch updates, we currently notify the listeners upon each move. This can slow down the updates, as well as potentially introduce cycles if the listener undoes the change.

If we notify the listeners asynchronously we can have a better chance at removing the cycles as well as potentially improve the perf of the updates.