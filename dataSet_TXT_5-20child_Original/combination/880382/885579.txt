Narrow widgets dropped on a wide widget should place the narrow widget above the wide widget
STR:

[X  X  X]
[X  X  X]
[X][X][X]
[X][X][X]
[X][X][X]

Dragging Y on top of the second [X  X  X], should produce the following:

[X  X  X]
[Y][X][X]
[X  X  X]
[X][X][X]
[X][X][X]
[X]

Actual:
[X  X  X]
[X  X  X]
[Y][X][X]
[X][X][X]
[X][X][X]
[X]