Facebook Chat causes crashes and steals window focus
(Note: this is filed as part of the “Paper Cut” bugs — we assume that there may be multiple existing bugs on this. Please make them block this bug, and we will de-dupe if they are indeed exactly the same. Thanks!)

To reproduce:
1. Open Facebook and the chat window
2. Open other websites in tabs
3. Facebook chat causes crashes, and steals focus from your other window when someone messages you.

Recommendation:
Work with Facebook chat to find the crash issues.