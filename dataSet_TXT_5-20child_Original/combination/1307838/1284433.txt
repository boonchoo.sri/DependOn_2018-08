Don't overlap checkbox and marker in performance-filter-menupopup
Created attachment 8767896
performance-filter-menupopup.png

User Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0
Build ID: 20160704030211

Steps to reproduce:

1. Start Nightly in Windows
2. Open DevTools > Performance
3. Open the performance filter


Actual results:

checkbox and marker are overlapped.

Regression range:
https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=975d97ff77ae82b113b66d2a46dc30fc654f411a&tochange=c0edf384327531b1e302c61fa3c77cf966152182


Expected results:

Don't overlap checkbox and marker.