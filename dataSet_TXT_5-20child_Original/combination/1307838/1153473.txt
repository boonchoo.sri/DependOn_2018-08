Idle blocks should be uniquely colored and consistent in flame graph
Created attachment 8591132
idlenodecolors

The flame graph renders idle blocks as "(idle)" when displaying only content (when platform data is shown, determining what is "idle" is handled in bug 1152839) - other top level frames can get the same color, making it hard to distinguish from afar what is idle and what is activity.

At the very least, idle nodes should be unique to the entire graph. Better yet, a good color, as colors are generated randomly on first use per toolbox, so users can get used to seeing "idle" nodes as an identifying color.