Editor doe not reopen if you create a new style and then change the orientation.
STR:

 1. Open the style editor on any tab.
 2. Create a new blank style (or may be 2)
 3. Change the orientation of Style Editor from Horizontal to Vertical (by decreasing its width)
 4. Select any other style sheet (that was not created by you)
 5. Close the Style Editor.
 6. Try to re open it for that tab.

Result

Style Editor does not reopen for that tab.

Expected:

It should reopen.



I get all these errors :

Timestamp: 10/15/2012 2:49:04 PM
Error: An error occurred updating the cmd_undo command: [Exception... "Component returned failure code: 0x80004003 (NS_ERROR_INVALID_POINTER) [nsIController.isCommandEnabled]"  nsresult: "0x80004003 (NS_ERROR_INVALID_POINTER)"  location: "JS frame :: chrome://global/content/globalOverlay.js :: goUpdateCommand :: line 75"  data: no]
Source File: chrome://global/content/globalOverlay.js
Line: 81

for cmd_(cut|paste|undo|redo|switchTextDirection)