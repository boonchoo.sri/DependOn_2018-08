Remove wrapper element from trace logs
Surrounding each <hbox class="trace-item">, there is an <hbox class="side-menu-widget-item"> wrapper. FastListWrapper adds it (see |_protoElement|). We should be able to merge these two hbox's into a single hbox, and that should give us some perf benefit as well since our dom tree will be significantly smaller than it is now.

I took an initial crack at this, but then some widget methods stuff started throwing and I put this task on the back burner.