Sample locations are parsed incorrectly if they have quotes.
JS Source:

Native["java/lang/System.arraycopy.(Ljava/lang/Object;ILjava/lang/Object;II)V"] = function () {
  ...
}

Profiler Sample Location String:

Native["java/lang/System.arraycopy.(Ljava/lang/Object;ILjava/lang/Object;II)V"] (http://localhost:8080/bld/main-all.js:262)

functionName is parsed incorrectly as:

Native["java/lang/System.arraycopy