React warning when opening console with cached messages: 'validateDOMNesting(...): <a> cannot appear as a descendant of <a>'
STR:

Open data:text/html,<body></body>
Open web console
Look in browser console

Expected: no warnings

Actual:
Warning: validateDOMNesting(...): <a> cannot appear as a descendant of <a>. See a > ... > Frame > a.react-dev.js:20749:9

[173]</warning()react-dev.js:20749
[144]</validateDOMNesting()react-dev.js:19353
[42]</ReactDOMComponent.Mixin.mountComponent()react-dev.js:7077
[84]</ReactReconciler.mountComponent()react-dev.js:13611
[73]</ReactMultiChild.Mixin.mountChildren()react-dev.js:12216
[42]</ReactDOMComponent.Mixin._createContentMarkup()react-dev.js:7211
[42]</ReactDOMComponent.Mixin.mountComponent()react-dev.js:7099
[84]</ReactReconciler.mountComponent()react-dev.js:13611
[38]</ReactCompositeComponentMixin.mountComponent()react-dev.js:5975
[78]</ReactPerf.measure/wrapper()react-dev.js:12877
[84]</ReactReconciler.mountComponent()react-dev.js:13611
[38]</ReactCompositeComponentMixin.mountComponent()react-dev.js:5975
[78]</ReactPerf.measure/wrapper()react-dev.js:12877
[84]</ReactReconciler.mountComponent()react-dev.js:13611
mountComponentIntoNode()react-dev.js:11390
[113]</Mixin.perform()react-dev.js:17257
batchedMountComponentIntoNode()react-dev.js:11406
[113]</Mixin.perform()react-dev.js:17257
[53]</ReactDefaultBatchingStrategy.batchedUpdates()react-dev.js:8849
batchedUpdates()react-dev.js:15333
[72]</ReactMount._renderNewRootComponent()react-dev.js:11600
[78]</ReactPerf.measure/wrapper()react-dev.js:12877
[72]</ReactMount._renderSubtreeIntoContainer()react-dev.js:11674
[72]</ReactMount.render()react-dev.js:11694
[78]</ReactPerf.measure/wrapper()react-dev.js:12877
WebConsoleFrame.prototype.createLocationNode()webconsole.js:2563
Messages.Simple.prototype<._renderLocation()console-output.js:1031
Messages.Simple.prototype<.render()console-output.js:906
WebConsoleFrame.prototype.reportPageError()webconsole.js:1474
WebConsoleFrame.prototype._outputMessageFromQueue()webconsole.js:2151
WebConsoleFrame.prototype._flushMessageQueue()webconsole.js:2040