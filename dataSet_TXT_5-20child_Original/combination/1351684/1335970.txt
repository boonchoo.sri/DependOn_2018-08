Consider additional URL bar text to describe not secure status
This bug is to consider the use of "Not Secure" or similar to the changes that Chrome are considering to highlight to users clearly when a website is secure.

This is backed by research and explanation here: https://security.googleblog.com/2016/09/moving-towards-more-secure-web.html

This depends on the bug: https://bugzilla.mozilla.org/show_bug.cgi?id=1310447 to display a warning by default, this is to just increase the visibility of the negative indicator.

We should consider if the wording should be "Insecure" or "Not Secure" or another alternative too.