Photon toolbar button icon drop
Bug 1347543 switches us from Toolbar.png to SVGs, so later we should be able to drop in new icons without much manual CSS work.