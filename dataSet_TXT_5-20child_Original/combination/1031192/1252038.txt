Refactor storage inspector tests
The tests are currently very easy to break by making tiny changes to the DOM.

e.g. browser_tableWidget_mouse_interaction.js contains the following line:
```
node = table.tbody.firstChild.firstChild.firstChild;
```

Other tests open the storage inspector in a XUL popup, which is not good.

All of the storage inspector tests need refactoring.