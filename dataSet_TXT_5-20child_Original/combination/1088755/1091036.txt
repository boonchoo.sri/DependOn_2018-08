Allow toolbox full screen for remote runtime projects
Created attachment 8513563
Wasted Space

For "remote runtime projects" (like Fennec tabs, Valence tabs, or even the pre-installed apps on Firefox OS) there is nothing local to edit, so the current WebIDE UI has quite a bit of wasted space between the main toolbar and the toolbox (see screenshot).

The "details" pane adds very little info for these projects, for example only the full URL of a tab.

We should offer at least the option through some toggle button (if not just make it always the case) that the toolbox takes up almost all of WebIDE, leaving only the top toolbar around.