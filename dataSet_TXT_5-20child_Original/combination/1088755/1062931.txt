WebIDE->Project->Preferences are not, they interact with global devtools.editor.* prefs instead
Tools->Web Developer->WebIDE->Project->Preferences

Preferences

Editor

    Keybindings
    Tab size
    Soft tabs
    Autoindent
    Autocomplete
    Autoclose brackets

actually get and set devtools.editor.* prefs.

This means the last project changing settings wins.

Scratchpad editors also are affected by side effect (when a new one is opened or when [Pretty Print] is performed.

Is the intent to make them project specific (e.g. under devtools.webide.APP-ID.editor.*)?

I think this is needed when contributing to apps on local disk which are cloned from git or similar.

If not, the global Toolbox Options should probably be shown instead.

This would also resolve the regressions in terminology (soft tabs vs. indent using spaces, etc.).

See also Bug 1058183.