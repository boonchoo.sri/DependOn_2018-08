With tabs in the title bar, Firefox dialogs and target=_blank links open a new maximized window with tabs cut off at the top
User-Agent:       Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b10pre) Gecko/20110113 Firefox/4.0b10pre
Build Identifier: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b10pre) Gecko/20110113 Firefox/4.0b10pre

In last nightly when i click the "Learn more" link in geolocation dialog, toolbars and tab bar move upwards to align to Firefox menu button. This happens obviously only with hidden menu bar and tabs on top.

Reproducible: Always

Steps to Reproduce:
1.Enter a page that use geolocation (ex. http://www.mozilla.com/en-US/firefox/geolocation/)
2.When is required to share the location, click "Learn more" link in the dialog
3.A new windows opens
Actual Results:  
Tabs on top bar, navigation toolbar and bookmarks toolbar move upwards.

Expected Results:  
Behaviour should not change.

Tried in Windows 7 x64 with Windows Aero turned on.