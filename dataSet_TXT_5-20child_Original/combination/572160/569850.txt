For tabs in the title bar, background tabs shouldn't be transparent under Win XP and Win 7/classic
Created attachment 449023
Theme Specific Background Tabs Examples

To mesh with the system theme for Windows XP we need to switch the background tabs to match the system theme's colors.

This would require four styles of background tabs:

- Luna Blue/Olive (Brown)
- Silver (Silver/Purple)
- Royale (Grey/Blue)
- Zune (Grey)