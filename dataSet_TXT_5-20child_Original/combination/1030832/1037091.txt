Add gear button with doorhanger configuration of newtab page
From the prototype: http://bg61zt.axshare.com/new_tab.html

There'll need to be a gear icon to customize the new tab page with presets:
- enhanced
- classic
- blank

Where these presets toggle various show/hide customizations for the page:
- enhanced images
- search box
- tiles
- messaging/explanation area

enhanced = all
classic = search + tiles
blank = none