[User Story] Ability to retrieve additional information as a separate page by catagory
As a user, if I want information in addition to what is presented in-page, I will be able to retrieve additional information as a separate page by category to further my research.

Full Theme https://docs.google.com/document/d/1uFuRxuf2yb8OMZncgon_0KMWvn5DVBq4z_2X5zqXWHQ/edit