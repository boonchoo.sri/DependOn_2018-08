Fix remote url resolution test breaking aurora nightlies
A test in test_RemoteNewTabLocation.jsm will stop working on Aurora nightlies because it expects a 'nightly' update channel.

https://dxr.mozilla.org/mozilla-central/source/browser/components/newtab/tests/xpcshell/test_RemoteNewTabLocation.js#57