The new eyedropper does not work in XUL documents
User Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0
Build ID: 20160623154057

Steps to reproduce:

1. Start Nightly
2. Open DevTools > Browser Toolbox (Ctrl+Alt+Shift+I)
3. Enable new eyedropper


Actual results:

Cannot use new eyedropper in the Browser Toolbox


Expected results:

Can we use new eyedropper in the Browser Toolbox? If it is not, should disable(display: none) it. If it can be used, works well.