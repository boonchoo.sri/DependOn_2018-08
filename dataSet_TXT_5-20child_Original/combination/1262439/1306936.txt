The eyedropper doesn't work in a SVG file opened
User Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0
Build ID: 20161001030430

Steps to reproduce:

1. Start Firefox 50 beta (or later version)
2. Open file any SVG files
3. Run the eyedropper


Actual results:

The eyedropper doesn't work with the following errors.

Object { isTypedData: true, data: Object, type: "error" }cli.js:2056
Promise rejection value is a non-unwrappable cross-compartment wrapper.(unknown)
TypeError: this.magnifiedArea is undefined[Learn More]eye-dropper.js:315:9

Regression range:
https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=e0bc88708ffed39aaab1fbc0ac461d93561195de&tochange=a7a882d122e36defe6c2a102a28ae7dfc16311c5



Expected results:

The eyedropper should work in a SVG file.