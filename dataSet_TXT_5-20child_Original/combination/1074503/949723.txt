Should show code coverage in the editor while executing
Not necessarily dependent on bug 929349, but related in that we would use the tracer actor here as well.

Because of the sheer number of traces we get firehosed with from the server, I think it makes sense to just pass them off to a worker immediately. That worker can then run a reduce on the trace data and reply with the set of functions that have been called so that the debugger controller can use |DebuggerController.Parser| and |DebuggerView.editor.markText| to highlight functions that have been executed.

This could be a good project for a contributor who knows their way around the debugger and wants something in depth / a little bigger.

Here is a slightly simplified diagram:

============================================================================

+--------------------------+
| Tracer Actor (on Server) |
+--------------------------+
        |
        |
  { trace logs }
        |
        |
        V
+---------------------------+              +------------------------------+
| DebuggerController.Traces |------------->| DebuggerView.editor.markText |
+---------------------------+              +------------------------------+
       |          ^
       |          |
 { trace logs }   |
       |          |
       |    [Set of executed functions]
       V          |
+----------------------+
| Code Coverage Worker |
+----------------------+

============================================================================

Finally, we also need to be sure to handle the toggling of pretty printing and make sure that our highlighted text is updated as well.