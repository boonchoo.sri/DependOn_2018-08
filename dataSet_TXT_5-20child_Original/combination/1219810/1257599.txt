Rename »Unsorted Bookmarks« into »Other Bookmarks«
»Unsorted Bookmarks« is a problematic name because
- It implies that the user needs to do work (to sort them)
- It uses negative tone

Chrome uses »Other Bookmarks« as a name for a similar folder, which doesn't have these problems.

Since that's clearly better and consistency with other products doesn't hurt in that area, we should be using that name again.

I think this is the file that needs changing in order to accomplish that (but there might be others):
https://dxr.mozilla.org/mozilla-central/source/browser/locales/en-US/chrome/browser/browser.dtd