Permanent orange on UX branch: TEST-UNEXPECTED-FAIL | TypeError: AddonsMgrListener is undefined, Test Timed Out
+++ This bug was initially created as a clone of Bug #885012 +++

I thought there were bugs filed for these, but a quick search hasn't revealed any.

TEST-UNEXPECTED-FAIL | tests/test-widget.testNavigationBarWidgets | "Camera / Microphone Access" != "2nd widget"
TEST-UNEXPECTED-FAIL | tests/test-widget.testNavigationBarWidgets | "Bookmarks" != "3rd widget"

See:

https://tbpl.mozilla.org/php/getParsedLog.php?id=24341388&tree=UX