Dev Tools Themes: Debugger uses low-res gear image
Created attachment 8411857
gears.png

In retina mode on a Mac, the debugger shows a low-res gear icon on the right, but the toolbox options icon is high-res.  We should use the high-res one everywhere.

Screenshot attached.