6.15 - 13.69% cart  / tart  (linux64, osx-10-10) regression on push 9aea49fa17a1 (Thu Aug 3 2017)
Talos has detected a Firefox performance regression from push:

https://hg.mozilla.org/integration/autoland/pushloghtml?changeset=9aea49fa17a1

As author of one of the patches included in that push, we need your help to address this regression.

Regressions:

 14%  cart summary linux64 pgo e10s     7.71 -> 8.77
 13%  cart summary linux64 opt e10s     8.72 -> 9.84
 13%  tart summary linux64 opt e10s     4.95 -> 5.59
 11%  tart summary linux64 pgo e10s     4.30 -> 4.77
  8%  cart summary osx-10-10 opt e10s   10.53 -> 11.35
  6%  tart summary osx-10-10 opt e10s   9.81 -> 10.41


You can find links to graphs and comparison views for each of the above tests at: https://treeherder.mozilla.org/perf.html#/alerts?id=8527

On the page above you can see an alert for each affected platform as well as a link to a graph showing the history of scores for this test. There is also a link to a treeherder page showing the Talos jobs in a pushlog format.

To learn more about the regressing test(s), please see: https://wiki.mozilla.org/Buildbot/Talos/Tests

For information on reproducing and debugging the regression, either on try or locally, see: https://wiki.mozilla.org/Buildbot/Talos/Running

*** Please let us know your plans within 3 business days, or the offending patch(es) will be backed out! ***

Our wiki page outlines the common responses and expectations: https://wiki.mozilla.org/Buildbot/Talos/RegressionBugsHandling