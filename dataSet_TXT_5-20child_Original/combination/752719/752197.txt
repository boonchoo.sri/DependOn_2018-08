Make all prefs dialogs windows in-content
All dialogs that currently appear as windows should be in-content (Google Chrome handles this very good). As dialogs that I can think of right now there are :
-Exceptions, Fonts and languages in "content" preferences 
-"Cookies" and "clear recent history" in "privacy"
-"Passwords" and "Exceptions" in "Security"
-"Pair a device" and "Set up sync" in "Sync"
-Various dialogs in "Advanced"
-"Page info", "Page source", "Error console" and other devtools (in-content tab or sidebar)
-"Print"

Final result should be having no more separated windows in the browser.