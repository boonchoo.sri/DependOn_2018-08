Firefox startup rebuilds toolbars
I was profiling startup, and noticed that a few percent (hard to quantify exactly with C++-only profilers, but not more than 10%, and probably more than 1%) was spent in rebuilding the toolbars.  See http://bonsai.mozilla.org/cvsblame.cgi?file=mozilla/toolkit/content/widgets/toolbar.xml&rev=1.33#166

Anything we can do about making this code do less DOM munging?