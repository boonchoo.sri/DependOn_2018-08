Migrate items out of the add-on bar to the nav-bar
Because the addon-bar is disappearing, items will need to be moved to the nav-bar so that they do not disappear from the UI entirely.