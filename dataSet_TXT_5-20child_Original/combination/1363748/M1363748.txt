[meta] Photon - Opening any of the primary menus should be fast
Examples:

* Bookmarks menu (with many bookmarks)
* History menu
* App menu / Hamburger menu
* Downloads panel

This is a meta bug to track performance work in this area.