Showing the bookmarks panel is too slow the first time
See this profile: https://perfht.ml/2qKzDn3

This is captured on the quantum reference hardware.

It's only slow the first time (it took ~300ms to appear), the second time is fine.

The panel I'm talking about is the panel when clicking the "Show your bookmarks" toolbar icon in the main toolbar.