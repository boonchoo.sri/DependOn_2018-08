Provide finer control over zooming
Currently the zoom UI provides for only 2 steps zooming out and 3 zooming in. I think the maximum zoom out is probably about right, but we need more steps in between 1 and 0.5. The maximum zoom in is perhaps too far for my taste but I am less interested in that, but that also could do with more steps. I would suggest maybe 5 steps in each direction. I am using 0.5,0.6,0.7,0.8,0.9, 1 as the zoom out settings and that seems to give me all the zooms I need for my pages.

I'm classing this as a regression since text zoom gave better control than page zoom does currently.

Apparently this requires bug 381661 for the zooming in case.