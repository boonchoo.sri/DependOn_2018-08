Can't search for b/c because the awesomebar doesn't provide a search action
Remove Search Bar by default in Nightly.
It takes long time to get a search results from Address Bar.
It is no problem from Search Bar.


Steps To Reproduce
1. type b/c in Address bar and hit [Enter] key

Actual Results:
It takes long time to get a search results.


Expected results:
It should not take so long