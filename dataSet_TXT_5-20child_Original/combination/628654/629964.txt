dom.disable_window_status_change unexpectedly set to false by default (DOM status text persists when switching tabs)
Build Identifier:
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b11pre) Gecko/20110129 Firefox/4.0b11pre ID:20110129030338

Tooltips of status does not disappear after the loading completion of the page.


Reproducible: Always

Steps to Reproduce:
1. Start Minefield with new profile
2. Open ( http://tweakers.net/ )
3.

Actual Results:
 Tooltips of status does not disappear

Expected Results:
 Should disappear