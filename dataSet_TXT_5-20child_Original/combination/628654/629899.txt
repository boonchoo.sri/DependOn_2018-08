Improve floating status side to side switching
From bug 628654 comment 59:

  saneyuki_s 2011-01-29 07:28:40 PST

  In the current implement of status messages, if mouse hovers on
  the messages on left side, the messages is switched to right side.
  However, I seem that this implement is hard to use for wide-screen...?
  It is too big to move line of sight?


Maybe an easy fix is a CSS transition where you can see the status move from one end to the other?

On a related note, certain pages will end up setting the status text to the empty string during page load (I see this on e.g. YouTube) so we remove the mirror attribute. If your mouse is in the default corner this causes some flickering as the floating status pops up and right after switches sides. Could we instead remove the mirror attribute when the page is completely done loading?