Fix click and hold code on Mac
Bug 404109 removed the back/forward buttons, so the code to enable "click and hold" behavior no longer works. We need to fix it.