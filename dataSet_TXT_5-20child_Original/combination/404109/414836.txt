Need to sync toolbar with icons/text preferences and icon size when new localstore.rdf created
In cases when the contents of an existing localstore.rdf file is not being used  (with the exception of safe-mode) the toolbar icons/text preferences and icon size preferences need to be applied.

These cases include:

1.  localstore.rdf is absent.
2.  localstore.rdf is being ignored because it has been determined to be corrupt.
3.  The code in bug 404109 is ignoring the localstore.rdf to reset the toolbars to default so that the home button displays on the bookmarks toolbar.

Not doing so results in an inconsistency between the displayed UI and what is selected in the preferences panel.

This problem is exacerbated by the code for bug 404109 which is slated for inclusion in Firefox beta3.

That is why i requested blocker and set target-milestone to beta3.  Fell free to re-target as appropriate, I just wanted to make sure the appropriate people would be made aware of the issue before the beta3 release.