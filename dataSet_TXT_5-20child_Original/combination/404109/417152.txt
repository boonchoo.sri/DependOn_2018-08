move the Home button only if the bookmarks toolbar is visible
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9b3) Gecko/2008020514 Firefox/3.0b3
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9b3) Gecko/2008020514 Firefox/3.0b3

Not everyone uses the bookmarks toolbar. Sure, it's nice to have the option of having the home button there, but it doesn't hurt to let people have it in the main toolbar if they want instead.

Reproducible: Always

Steps to Reproduce:
1.
2.
3.