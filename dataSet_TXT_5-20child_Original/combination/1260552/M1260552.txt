Come up with something to replace a xul:splitter
We need some kind of element/component to allow resizing between two other elements.

Documentation for the available options of xul splitter: https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XUL/splitter

An example usage of it is to split between the split console and the selected tool: https://dxr.mozilla.org/mozilla-central/source/devtools/client/framework/toolbox.xul#146.

One requirement is that we can use this to split two elements that are already added in the DOM so we can use it on existing code without needing to re-write them to construct their DOM in JS: https://dxr.mozilla.org/mozilla-central/source/devtools/client/inspector/inspector.xul#182