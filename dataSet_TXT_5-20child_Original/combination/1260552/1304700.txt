No animation when collapsing HTML splitter
(In reply to Fred Lin [:gasolin] from Bug 1260552 comment #61)
> Quick test the patch and overall works great! I'll try turn .xul to .html
> based on it and find whats missing in bug 1292592 (de-xul sourceeditor).
> 
> During test I found the `hide sidebar animation` is missing when tap the
> sidebar toggle. But `show sidebar animation` still works. We can fix it in
> another bug anyway.

Still true in the version that landed, there is no animation when collapsing the panel but there is one when expanding it.

STRs:
- open inspector
- click on "collapse pane"

ER: the sidebar collapse should be animated
AR: the sidebar collapses immediately