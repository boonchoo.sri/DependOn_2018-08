Switch the Default Browser prompt to a global notification bar
We want to revisit the global notification bar approach for default browser prompts.

The first step will be getting it back in to the Firefox tree as well as enabling it, either through funnelcake builds or just for everyone while we evaluate what changes should be made to increase the conversion rates.