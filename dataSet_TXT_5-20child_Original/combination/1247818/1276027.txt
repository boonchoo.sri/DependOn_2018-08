Add a Telemetry probe to track how often Firefox is used as the default handler
Right now the only real benefit of setting Firefox as the default browser is having Firefox opened as the default handler for website links from external applications and HTML files.

We should track how often Firefox is used in this manner. If the usage is very low, we should consider removing prompts for setting Firefox as the default browser.