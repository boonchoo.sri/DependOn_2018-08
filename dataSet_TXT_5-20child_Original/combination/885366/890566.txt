Permissions should reset back to default
Not sure if this is a flaw in 885366, or existing permissions bug.

1) Visit some site (eg dolske.net)
2) Check doorhanger, no permissions listed
3) Page Info --> Media --> check Block Images from site
...
4) Open new tab for site
5) Check door hanger, permissions show "Load Images: Block"
6) Set to "Allow"
...
7) Open another new tab for site
8) Check door hanger, permissions show "Load Images: Allow"

The results in step 8 persists across a restart. Since allowing image loading is the default, I'd expect restoring "Allow" to make it stop showing up.