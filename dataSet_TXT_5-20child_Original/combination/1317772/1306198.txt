[UX] No way to know what user agent will be applied for a device
Created attachment 8795981
devices-list

After activating the responsive design mode. The list make the assumption of one device == one browser (which is something we are fighting against).

Two issues with the current setup:

* The default list doesn't contain any Firefox on Android choice (Ooops).
* The list doesn't say which browsers and its version we are talking about (aka Chrome 42 for example)