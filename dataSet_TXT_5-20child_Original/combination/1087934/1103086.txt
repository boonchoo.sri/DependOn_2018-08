add a transition page when switching share providers
There is a lag when switching providers, try using a loading page in between.  See design in bug 1087934.