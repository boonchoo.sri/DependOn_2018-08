Add a key shortcut or always accessible UI to reload devtools
We shouldn't have to open gcli to reload devtools (via `tools reload` command). We may just suggest setting devtools.loader.srcdir pref and then have a key shortcut and/or some UI within Firefox (not devtools) to allow reloading it at anytime.
It is really important to not depend on any devtools code (or very few of it)
so that we can recover if we end up breaking it!!