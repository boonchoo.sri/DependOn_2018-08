Drag-n-drop of tabs stopped working in e10s window on Linux/GTK 3.18
STR:
- Open 2 tabs
- Drag the left-most tab and drop it on the right of the right-most tab

Expected result:
- While dragging, the tab visually moves
- After dropping, the tab position is definitely set to the new position

Actual result:
- Nothing happens. In fact, when starting to drag, there's even a sort of tooltip that starts showing up and disappear immediately.

I haven't tested on non-Linux, so I'm currently assuming this is Linux only. This is not Gtk+3-related, because it used to work on Gtk+3 builds. This didn't happen on aurora 42 and started happening on aurora 43, apparently. I'm going to run it through mozregression to have a better regression window.