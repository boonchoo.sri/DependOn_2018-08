Australis - Use Windows 8 style icons for Toolbar-inverted.png (on Windows 8)
Windows 8 should have Win 8 style Toolbar-inverted.png.
If the Win 8 style Toolbar-inverted.png is too faint, we could use the Mac Toolbar-inverted.png as it shares a similar style, but with full opacity :
https://hg.mozilla.org/integration/fx-team/raw-file/02be46ac2b55/browser/themes/osx/Toolbar-inverted.png