Windows 8.x theme issues in Firefox
With the new Windows 8 stuff landing, I see these issues :
- Weird borders in bookmarks menu button in some cases
- Weird active effect on back button
- Non Windows 8 style new tab, list all tabs, switch to metro, and tab scrollbox icons.
- There shouldn't be a border-radius on the customization mode buttons
- Darken the icons on hover (to match mockup, this can be done with SVG filters)
- Sidebars still have the aero blue
- The toolbars should have a lighter color (#F8F8F8), not just because of a mockup, but because it looks better
- .panel-multiview-anchor looks bad on Windows 8.

There are 2 other issues, but they were intentional, awaiting for a future decision :
- Toolbar button open state should be blue
- No Windows 8 style icons when they are inverted.