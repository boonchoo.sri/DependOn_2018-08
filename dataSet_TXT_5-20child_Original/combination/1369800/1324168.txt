[QX cluster] Preferences refresh 2017
Attached are rough drafts of new designs and outlines for changes that should be made to the Preferences.

The visual update should wait until we can update the add-ons manager at the same time, since they use a similar visual design.