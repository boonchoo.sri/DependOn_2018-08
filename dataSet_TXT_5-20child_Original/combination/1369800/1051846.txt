Add page title to about: pages that do not have a title
Created attachment 8470861
Example of About: Page Title

The following about: pages do not have page titles. Suggested titles are included:

about:				» About Firefox* 
about:buildconfig 		» Build Config
about:config 			» Configuration
about:license 			» Licenses
about:memory 			» Memory
about:plugins 			» Plugins
about:privatebrowsing 		» Start Private Browsing
about:rights 			» About Your Rights
about:sync-log			» Sync Log
about:webrtc			» WebRTC

* or Aurora, or Nightly.