Lightweight themes leave a sliver of the "back" button uncolored, with Australis and keyhole-style button on Linux
STR:
 1. Install a lightweight theme, e.g.
  https://addons.mozilla.org/en-US/firefox/addon/furfox-tail-twister/

 2. Inspect your "back" button at the left of URLbar

ACTUAL RESULTS: There's a sliver at the bottom-right of the back button that's unpainted, which looks quite odd.


I believe this is a new issue with Australis.

Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0
30.0a1 (2014-03-15)