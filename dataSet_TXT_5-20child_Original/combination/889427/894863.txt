Allow frameworkers to spawn sub workers
Created attachment 777057
Patch

Real workers are allowed to create subworkers, so it makes sense for workers of SocialAPI providers to be allowed to do that too.
Exposing the 'Worker' constructor from the hidden frame used for the frameworker is easy, but doesn't let subworkers use WebSocket, which is very unfortunate for Talkilla service providers.
Getting bug 504553 fixed seems the best long term solution to address this, but it looks like it will take a while.
A short term solution that is being considered is to allow frameworkers to spawn subframeworkers. I started experimenting with this to check feasibility, so I'm filing a bug to attach my work in progress. We may end up wontfixing if we decide we want to follow a different path.

Feedback welcome :).