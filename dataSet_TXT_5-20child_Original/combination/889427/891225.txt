implement status panel button
This button allows providers to have a status button with notifications that can be placed in the toolbar, or australis menu, that is not dependent on being the "selected" provider.  It supports a badged icon and contains a panel with iframe.  The iframe url is defined via the manifest.  The icon may be updated via the link rel=icon tag, similar to how the chat window, app tabs, etc. work.  

TBD:

We want to reduce reliance on having a worker for functionality, but need to flesh out how badge updates happen.

- The badge would be updated via a DOM event the iframe content sets.
- The badge would be updated via the worker.