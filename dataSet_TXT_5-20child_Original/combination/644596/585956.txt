Implement console.trace() in web console
We need the ability to get access to JS stack traces from the console. It's currently a bit of a pain to get these out of the browser.

see, http://www.mozilla.org/scriptable/javascript-stack-dumper.html

Not sure how we should do this or expose this to a developer. We could create a new console function (e.g., console.dumpStack()) that we could use inside content (or chrome) code to drop a stack trace into the console at a specific point in code.