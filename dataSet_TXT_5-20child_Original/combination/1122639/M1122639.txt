Migrate Profiler/Timeline preferences to Performance
We should consolidate all current profiler and timeline prefs into the new one. Timeline pref migration won't be a deal since no one uses it now, and we would lose the migration for "devtools.profiler.show-platform-data".

"flatten-tree-recursion" and "show-idle-blocks" just added in this release (Fx38), so unlikely we'll have users upset about migration changes on those prefs.