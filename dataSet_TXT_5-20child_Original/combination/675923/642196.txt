GCLI should allow JS to be entered using {}
Previous developer tool command lines have been primarily JS based. While this is somewhat limiting, it does allow easy interaction with the DOM and JS data. GCLI should allow this access using something like the following:

> echo {window.title}
"Enter Bug"

> {console.log('hi')}
"hi"

It is expected that the command line will have a toggle switch to allow JS input to be the default.

The system will allow JS to be the default, allowing use of JS without either "{}" or the toggle switch:

> eval 2+2
4

The system may need integration with a JS parser like narcissus or uglify to allow us to recognize matching {}