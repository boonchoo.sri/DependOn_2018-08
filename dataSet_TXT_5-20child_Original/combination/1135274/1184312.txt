Remove old tracking protection/MCB shield
STR:
enable tracking protection (set privacy.trackingprotection.enabled to true)
visit a site with tracking elements (e.g. http://www.nytimes.com/ )
two shield icons appear (both the popup notification one with the chevron on the right side and the site identity one to the left of the globe icon)