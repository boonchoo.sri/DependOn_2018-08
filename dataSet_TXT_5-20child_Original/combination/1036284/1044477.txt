Page title on new tab page shown in bold
I have cnn.com on my new tab page, and for some reason the title "CNN.com - Breaking News, U.S., World, Wea..." is shown in bold.

The title of the page is: <title>CNN.com - Breaking News, U.S., World, Weather, Entertainment &amp; Video News</title>

Maybe the amp isn't being escaped properly somehow?  I tried on a new profile on OSX and it didn't seem to happen there, so I'm not sure what the issue is.