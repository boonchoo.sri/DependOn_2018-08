[e10s] View More on Share this page doesn't work
User Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0
Build ID: 20150115030228

Steps to reproduce:

Enable e10s.
Clich on Share this Page > View More


Actual results:

Open blank tab.


Expected results:

The same as in non-e10s. Open https://activations.cdn.mozilla.net/en-US/.