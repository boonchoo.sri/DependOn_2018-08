Refactor about:tabcrashed to use RemotePageManager
about:tabcrashed uses events bubbled up to browser.js in order to message things to the chrome-level.

We should also move the tracking of how many crashed browsers there are from SessionStore to something like TabCrashReporter.

We should probably use RemotePageManager instead. Because about:tabcrashed is actually loaded internally by the docShell, this will also require some slight modification to RemotePageManager.