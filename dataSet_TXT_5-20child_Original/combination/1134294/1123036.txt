Emacs key bindings stop working sometimes
User Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
Build ID: 20150114125146

Steps to reproduce:

1. Open Web IDE(Already Emacs key binding)
2. Create new project
3. open index.html, then Ctrl-n(or create new file)
4. delete created new html file


Actual results:

Emacs key binding(C-a, C-e, etc...) is not working on .html file only, but default key binding is working on .html file and Emacs key binding is working on other files.
When multiple .html files and delete one file, on all .html files, Emacs key binding does not work.

When restart Web IDE, Emacs key binding does work.


Expected results:

Even if delete .html file, Emacs key binding does work.
And if possible, I want the Ctrl-n is "move next line" instead of "create new file" on Emacs key bind.