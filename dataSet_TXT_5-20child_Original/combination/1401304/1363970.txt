Color picker doesn't work/move in RDM, when touch is enabled.
Hello,

FF 53.0.2
e10s: enabled
Windows: 10 x64

In new RDM, when touch simulation is enabled, the Inspector's color picker doesn't move (lock in somewhere in the screen), but after toggling touch simulation (turn that off) it works again.

Mehdi