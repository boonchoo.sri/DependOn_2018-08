Cannot select text in touch simulation mode
[Affected versions]:
- 52.0a2 2016-11-17
- 53.0a1 2016-11-17

[Affected platforms]:
- Win 10 64-bit
- Mac OS X 10.11.5
- Ubuntu 16.04 64-bit

[Steps to reproduce]:
1. Open a page
2. Enable RDM tool
3. Enable touch simulation
4. Hold the mouse left button over text in order to select it

[Expected result]:
- Text can be selected when touch simulation is on

[Actual result]:
- Nothing happens, the text is not selected

[Regression range]:
- This doesn't reproduce with Nightly 2016-08-15 so it doesn't seem to be a regression