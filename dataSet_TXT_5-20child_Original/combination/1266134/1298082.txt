Cleanup various things around Toolbox
In order to help 1266134, there is various cleanups we can do around toolbox.js and the Toolbox object:

 - Stop using various toolbox internals like toolbox._host, toolbox._target
 - Use helpers instead of computing them again and again:
     for ex, use toobox.win instead of toolbox._host.frame.contentWindow
 - We can get rid of toolbox.frame as all the usages of it can be replaced with equivalent using toolbox.win (this will help loading toolbox in a tab as it won't have access to the frame element when running in content)
 - Cleanup the toolbox on unload instead of relying on window-closed event of the host, which is just the unload event of the parent document. (this will reduce the amount of code to refactor in bug 1266134)