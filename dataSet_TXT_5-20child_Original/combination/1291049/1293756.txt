fix "dynamic" requires in autocomplete.js
There's a bit of code in autocomplete.js that webpack thinks is dynamically
calling require.  Easy to fix and makes webpack happy.