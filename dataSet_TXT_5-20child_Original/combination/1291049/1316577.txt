Extract the logic to load the inspector in a chrome tab out of inspector.js
For devtools html we need to keep the dependencies on files using chrome only APIs to a minimum. 

Moving the code necessary to load the inspector in a standalone chrome tab will help reduce the number of this kind of dependencies in inspector.js.

(FYI, in Bug 1291049, we will probably reuse a similar approach in order to create a fake toolbox when loading the inspector in a content tab.)