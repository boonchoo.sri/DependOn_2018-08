After move a background unloaded tab to New Window, content does not load
Build Identifier:
http://hg.mozilla.org/mozilla-central/rev/253009438c5b
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20121203 Firefox/20.0 ID:20121203030801

This is spun off from bug Bug 817947

Steps to reproduce:

1. Keep "Don't load tabs until selected" option on
2. Open several tabs
3. Close Firefox and restart
4. Move a background unloaded tab to New Window via Tab Context Menu


Actual results:
 Content does not load 

Expected results:
 Content should load after detached

Regression window(m-c)
Good:
http://hg.mozilla.org/mozilla-central/rev/cd4853b0b94a
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20120226 Firefox/13.0a1 ID:20120226031015
Bad:
http://hg.mozilla.org/mozilla-central/rev/b98fc24ac54b
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20120226 Firefox/13.0a1 ID:20120226151249
Pushlog:
http://hg.mozilla.org/mozilla-central/pushloghtml?fromchange=cd4853b0b94a&tochange=b98fc24ac54b

Regression window(m-i)
Good:
http://hg.mozilla.org/integration/mozilla-inbound/rev/a65890126750
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20120224 Firefox/13.0a1 ID:20120224213649
Bad:
http://hg.mozilla.org/integration/mozilla-inbound/rev/c928dc7c9dff
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20120225 Firefox/13.0a1 ID:20120225011949
Pushlog:
http://hg.mozilla.org/integration/mozilla-inbound/pushloghtml?fromchange=a65890126750&tochange=c928dc7c9dff

Regressed by : Bug 711193