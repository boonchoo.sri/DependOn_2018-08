5.02% tsvg_static  (windows7-32) regression on push f0d5588153e4 (Mon Aug 28 2017)
Talos has detected a Firefox performance regression from push:

https://hg.mozilla.org/integration/autoland/pushloghtml?fromchange=156c3f4e0b16462e1d86f2e9b6587e6ed861be59&tochange=f0d5588153e4981f61601ada9d61e7ebab7cb58f

As author of one of the patches included in that push, we need your help to address this regression.

Regressions:

  5%  tsvg_static summary windows7-32 pgo e10s     51.73 -> 54.32


You can find links to graphs and comparison views for each of the above tests at: https://treeherder.mozilla.org/perf.html#/alerts?id=9049

On the page above you can see an alert for each affected platform as well as a link to a graph showing the history of scores for this test. There is also a link to a treeherder page showing the Talos jobs in a pushlog format.

To learn more about the regressing test(s), please see: https://wiki.mozilla.org/Buildbot/Talos/Tests

For information on reproducing and debugging the regression, either on try or locally, see: https://wiki.mozilla.org/Buildbot/Talos/Running

*** Please let us know your plans within 3 business days, or the offending patch(es) will be backed out! ***

Our wiki page outlines the common responses and expectations: https://wiki.mozilla.org/Buildbot/Talos/RegressionBugsHandling