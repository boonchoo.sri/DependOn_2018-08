PermissionsUtils.jsm needs to stop using createCodebasePrincipalFromOrigin
In the file toolkit/modules/PermissionUtils.jsm there's this code:

> 29     for (let origin of origins) {
> 30       let principals = [];
> 31       try {
> 32         principals = [ Services.scriptSecurityManager.createCodebasePrincipalFromOrigin(origin) ];

Since is related to preferences, which we're not going to isolate by user context id (yet), the solution is to populate an origin attribute from the origin, then create a GlobalContextOriginAttribute to force the user context id to 0, then call createCodebasePrincipal with the origin and the origin attributes instead.