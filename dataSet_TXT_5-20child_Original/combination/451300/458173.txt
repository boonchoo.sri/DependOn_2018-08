Focus completely busted with Ctrl+Tab.
As of a few days ago (from what I can tell, w/o having had the time to download builds and test etc), when you hit Ctrl+Tab in Firefox, the window that opens up and shows the tabs does not have focus, and the you either need to Alt+Tab to the firefox window, or use the mouse to focus the window to be able to switch tabs.

This IMO must block beta1 (why is there no blocking beta1 flag in bugzilla?), makes using tabs in Firefox extremely hard.