In Bookmarks Library, while sorted by date, the window scrolls to follow the clicked bookmark.
User-Agent:       Mozilla/5.0 (X11; Linux x86_64; rv:2.0b8pre) Gecko/20101029 Firefox/4.0b8pre
Build Identifier: Mozilla/5.0 (X11; Linux x86_64; rv:2.0b8pre) Gecko/20101029 Firefox/4.0b8pre

Using Firefox 4, in Places, a list of bookmarks may be sorted by visit date. Selecting and clicking a bookmark will move the bookmark to the top of this list sorted by date, but also (if necessary) scroll the places window to follow that bookmark.

In Firefox 3, a clicked bookmark would move to the top of the list, but the window would not scroll to follow that bookmark. This is the preferred behaviour, I believe.

Reproducible: Always

Steps to Reproduce:
1.Sort bookmarks by visit date in the Library.
2.Scroll the window down.
3.Select and click a bookmark.
Actual Results:  
The bookmark moves to the top of the list, and the places window scrolls to follow it.

Expected Results:  
The bookmark moves to the top of the list, but the places window does not scroll to follow it.

This could be a regression, since Firefox 3 exhibits the expected behaviour.