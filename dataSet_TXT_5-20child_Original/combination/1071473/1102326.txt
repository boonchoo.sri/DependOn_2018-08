Support XPCShell as remote target
Bug 809561 adds debugging for XPCShell tests.

It avoids using tabList and works in the Connect screen, but could require some changes in WebIDE.