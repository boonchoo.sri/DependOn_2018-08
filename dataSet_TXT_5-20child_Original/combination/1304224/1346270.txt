html 5 required input tooltip is hidden by the connection is not secure tooltip
Created attachment 8845964
Html file to upload to a non secure server to test

User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0
Build ID: 20170302120751

Steps to reproduce:

Have a html5 form with a required input with the type email or password on a not secure page.


Actual results:

The not secure tooltip appears on top of the html5 required input tooltip and cannot be read.


Expected results:

Have the tooltips not appears on top of each other.
Possible solutions :
1 - Have one appear on top and the other on the bottom of the input.
2 - have them appear next to each other.
3 - Have them both appear on the bottom but one below the other.
4 - Only show one of them