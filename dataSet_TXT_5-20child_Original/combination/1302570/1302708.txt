images/filter.svg is missing when accessed from the JSON Viewer's common.css file
In the output from the test I'm writing in bug 1302570, I have this line:

missing file:///.../Nightly.app/Contents/Resources/browser/chrome/devtools/modules/devtools/client/themes/images/filter.svg referenced from file:///.../Nightly.app/Contents/Resources/browser/chrome/devtools/modules/devtools/client/themes/common.css


The reference is at http://searchfox.org/mozilla-central/rev/6b94deded39a868f248a492e74e05733d6c4ed48/devtools/client/themes/common.css#547

.devtools-filterinput {
  background-image: url(images/filter.svg#filterinput);
}


This is valid when the common.css is accessed from chrome://devtools/skin/common.css, but not when access from the second copy of common.css packaged at http://searchfox.org/mozilla-central/source/devtools/client/themes/moz.build#12, added by bug 1256757.

Possible fixes:
- package images/filter.svg for resources too.
- Make the url absolute in common.css:  url("chrome://devtools/skin/images/filter.svg#filterinput"); (like it's done at http://searchfox.org/mozilla-central/source/devtools/client/jsonview/css/search-box.css#13)