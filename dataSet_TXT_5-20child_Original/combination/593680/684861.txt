tabstrip animation is jumpy when new tab and scroll buttons appear jointly
User Agent:  

Steps to reproduce:

0. Let n be the least number for which the tab strip's scroll buttons appear 
   at given window size.
1. Open (n-1) tabs. Assure that one of those tabs displays this bug.
2. Reorder the tabs such that the tab with this bug is the last (rightmost) tab.
3. Open http://heise.de/-1337311 in new tab.


Actual results:

3. New Tab and scroll buttons appear jumpily.


Expected results:

3. Animate smoothly.