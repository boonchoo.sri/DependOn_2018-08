browser/modules/test/browser_SignInToWebsite.js uses gBrowser.contentWindow, which doesn't exist in e10s browsers
Bug 1144149 will add contentDocumentAsCPOW and contentWindowAsCPOW shortcuts to gBrowser.

When that happens, browser/modules/test/browser_SignInToWebsite.js should be updated to use that instead.

Note that since this is a test, Mochitest will take advantage of the add-on shims, and will get gBrowser.contentWindow and gBrowser.contentDocument as expected. We should just migrate them over eventually before we start dialing back the shims.