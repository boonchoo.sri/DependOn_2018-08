Get telemetry about whether users are using a custom homepage or session restore
We know that not all users see about:home, but we don't currently have data (that I can see) about whether users do/don't, or how many users do/don't.

Stating the obvious: we explicitly shouldn't gather *what* the user's homepage is set to, but only the quad-state between "about:home", "about:blank", "custom homepage" or "restore tabs from last time".