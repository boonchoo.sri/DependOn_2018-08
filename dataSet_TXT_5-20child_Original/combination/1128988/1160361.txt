GCLI actor fails to getRequisition for new B2G servers
GCLI now seems to assume files from /browser are available on servers, but that is not true for non-Firefox.

For example, a recent B2G nightly simulator build gives:

"Protocol error (unknownError): Module `devtools/commandline/commands-index` is not found at resource:///modules/devtools/commandline/commands-index.js" protocol.js:20
"Message: Module `devtools/commandline/commands-index` is not found at resource:///modules/devtools/commandline/commands-index.js
  Stack:
    GcliActor<._getRequisition@resource://gre/modules/devtools/server/actors/gcli.js:12:209
GcliActor<.specs<@resource://gre/modules/devtools/server/actors/gcli.js:2:734
actorProto/</handler@resource://gre/modules/devtools/server/protocol.js:74:9
DSC_onPacket@resource://gre/modules/devtools/server/main.js:97:215
ChildDebuggerTransport.prototype.receiveMessage@resource://gre/modules/devtools/transport/transport.js:38:337