browser hangs upon accessing this URL
firefox on BeOS (both netserver and BONE) hang when accessing this URL and other
SSL sites.  Site also hangs with mozilla 1.85a netserver build running under
BONE.  Does not hang with 1.85a netserver build running under BeOS/netserver. 
Note: bug is not consistent across all SSL sites - some work, many do not.