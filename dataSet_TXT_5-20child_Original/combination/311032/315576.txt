[BEOS] Menubar/Toolbar/bookmarks toolbar repaint incorrectly after popup windows close.
User-Agent:       Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051105 Firefox/1.6a1
Build Identifier: Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051105 Firefox/1.6a1

Recently introduced method in nsViewBeOS to reduce unneeded Draws results in improper repaint.  See attached jpg for picture.  Tested by changing

	if (paintregion.CountRects() == 0 || !paintregion.Frame().IsValid() || !fJustValidated)

to 
	if (paintregion.CountRects() == 0 || !paintregion.Frame().IsValid() /*|| !fJustValidated*/)

in nsViewBeOS::Draw.


Reproducible: Always

Steps to Reproduce:
1.start browser
2.go to url above
3.run benchJS
4.wait for step 2 (pop up windows) to complete


Actual Results:  
if any part of menubar, toolbar or bookmark toolbar is obscured by the popups, they'll be corrupted after the popups close.

Expected Results:  
windows should close and bars should return to normal.