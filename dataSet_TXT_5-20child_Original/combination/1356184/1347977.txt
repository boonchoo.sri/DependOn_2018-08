Move some DevTools technical docs to the tree
Currently we have many docs in many places. It's hard to find things, and organize them.

We discussed at the Paris work week that we should clean this up.
One concrete thing that came out of it was that docs inside the tree were good (things in /devtools/docs) and that we should keep that for very technical things like architecture, APIs, etc.

So I went over the wiki and listed the pages we have:
https://docs.google.com/document/d/1XrrvgSZka1uEeYynZRf99FI0ejiCIz_rGyuKHrFx8rI/edit#

Some of them, I think, should go in /devtools/docs/

I'll attach a patch soon to move 1 of them, and then will continue with other pages.