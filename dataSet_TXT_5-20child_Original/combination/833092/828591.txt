No completion on click on the devtool command help list entry.
Created attachment 699964
devTools.png

User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0
Build ID: 20130104151925

Steps to reproduce:

I activate the developer toolbar and activate the command help list with F1.
After that I typed the character "a".


Actual results:

When the command help list shows the results for "a" and I click on the word "addon" the other words disappears and I only see "addon", but it will not complete my entry.


Expected results:

It should complete the entry "a" with "addon".