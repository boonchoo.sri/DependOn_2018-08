theming changes to the Windows/Linux WebRTC global sharing indicator
See image 1 in attachment 8485640 for a mockup of the new expected appearance.

See bug 1052011 for the final visual design.