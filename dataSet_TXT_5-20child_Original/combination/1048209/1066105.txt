give thumbnail previews of windows/applications that could be shared
See image 6 of the redesigned WebRTC UI in attachment 8485640.

See also bug 1066102 (microphone preview) that contains some information about how we want to pass the information to the UI.