Provide a UI for migrating users' add-ons to webextensions
We're still not exactly sure how this would work, but I think the basic idea is as follows. On startup, you would be shown a tab with a list of your add-ons. For each add-on, we would show its status:

- e10s compatible (doesn't use CPOWs at all)
- e10s functional (works, uses CPOWs, probably will make Firefox slower)
- broken with e10s

For each add-on not in the first category, we'd like to provide alternatives:
- disable the add-on
- upgrade to a beta version that works better
- upgrade to a slightly different add-on (Firebug 2 -> Firebug 3 for example)

We would probably also show this UI whenever the status of an add-on changes (for example a disabled add-on being made e10s-compatible).