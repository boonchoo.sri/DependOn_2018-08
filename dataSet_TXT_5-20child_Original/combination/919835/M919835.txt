Make session data collection work with multiple processes
We have bug 516755 for this, but it seems easier to me to start fresh. Tim did a lot of work in bug 894595 to move collection work into content scripts. However, we still need to move a few more pieces.

I will file a separate bug for session restoration.