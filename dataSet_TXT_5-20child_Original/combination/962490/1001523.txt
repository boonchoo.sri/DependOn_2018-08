New Tab grid does not provide enough padding below the grid
Created attachment 8412731
screenshot of problem

If I reduce the size of the window just before any rows/cols are reduced, some of the bottom is getting cut off with scrollbars appearing for the page.

For a unit test, I'm not sure if we just resize the window height and make sure there's never a scrollbar?