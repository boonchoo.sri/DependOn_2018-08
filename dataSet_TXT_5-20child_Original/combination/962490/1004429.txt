5% tart regression for linux 64 on fx-team (v32) April 24
when http://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=bd55f7f8b48c&tochange=532886a149ab landed on fx-team April 24th we ended up with a 5% regression for tart on linux64.

here is a graph to prove it:
http://graphs.mozilla.org/graph.html#tests=[[293,64,35]]&sel=none&displayrange=30&datatype=running

I had done a few retriggers in case there was some noise:
https://tbpl.mozilla.org/?tree=Fx-Team&fromchange=96eefe207020&tochange=1f5f1fe135b9&jobname=Ubuntu%20HW%2012.04%20x64%20fx-team%20talos%20svgr

here is more information about tart:
https://wiki.mozilla.org/Buildbot/Talos/Tests#TART