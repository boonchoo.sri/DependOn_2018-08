When editing a node as HTML in the markup-view, changes should be previewed live, like in Firebug
Steps to reproduce:

1/ right-click on text in the page -> inspect element
2/ in devtools pabel, right-right -> 'Edit as HTML'
3/ Change some text in the form open in devtools

Expected behaviour:
Text is updated on the page (like in Firebug)

Actual behaviour:
Nothing happens. The only way to see that reflected is to click somewhere else in the dom tree which means that:
- The html node I was working on is no longer selected
- I have to use the mouse/touchpad while I was in typing on keybpard because there is no key that will get me our of the form fill.

That makes the devtools cumbersome to test text changes on a page.