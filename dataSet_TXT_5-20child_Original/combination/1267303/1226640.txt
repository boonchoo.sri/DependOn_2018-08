Add Events side panel
Created attachment 8690146
Firebug "Events" panel

Firebug 2.0 has an "Events" side panel which displays not only the events of the current element, but also the events of its parents.

I personally don't think it will be redundant but rather complementary to the badge displayed beside the element in the inspector panel.

Originally reported in this Firebug 3.0 issue:
https://github.com/firebug/firebug.next/issues/394

Florent