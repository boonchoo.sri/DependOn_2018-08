[markup view] Add "Copy XPath" context menu item for nodes
Similar to "Copy Unique Selector" we could have an entry that copies the XPath for a node.