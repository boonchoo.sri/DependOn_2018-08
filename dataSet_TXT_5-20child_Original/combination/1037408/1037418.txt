[UX] provide new icons for screensharing UI
Bug 1031424 designed the UX for screen sharing. The version we are planning to implement for Firefox 33 is attachment 8453837.

We will need several new icons:
For bug 1037405, we need:
- large screensharing icon to display in the doorhanger. (visible in the third column of the mockup)
- screensharing URL bar icon (see bottom part of the first column in the mockup). We need a colored and a gray version of the icon.
These icons need to be provided for all the OSes we support.

For bug 1037408, we need:
The three icons that should be used in the global indicator (top part of the first column of the mockup). I don't think these icons are OS specific, but we'll need to have a normal and an @2x version to support hidpi screens.

The icons requested here are blocking the 2 bugs mentioned above, which we are hoping to resolve before Firefox 33 reaches aurora. If you can't decide on the final icons in that timeframe, giving us quickly placeholder icons so that we can land something would be appreciated.