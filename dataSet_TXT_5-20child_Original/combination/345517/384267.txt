Enabling libxul increased binary size across all platforms
Enabling libxul caused an increase in binary size on all platforms. 

Mac: 17M -> 19M

Linux: 8.6M -> 10MB

Windows: 6.1 MB -> 6.2 MB (.exe)

I know this was to be expected, but is this tolerable?