The Reset Notification is not displayed if Firefox is opened from the terminal
Mozilla/5.0 (X11; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0
Build ID: 20130904030204

Steps to reproduce:
1. Set your computer clock to 61 days in the past
2. Install the latest Nightly 
3. Open the Profile manager from the terminal using the command "firefox -no-remote -p" 
4. Create a new profile and launch Firefox 26.0a1.
5. Use the profile for a bit and close Firefox 26.0a1.
6. Set the computer clock back to the original time.
7. From the terminal, open the profile manager again and launch Firefox 26.0a1 on the same profile that was created in step 4.

Expected results:
After launching Firefox in step 7 - the Reset notification is displayed. 

Actual results:
The reset notification is not displayed.

Notes:
The issue is reproducible also on the latest Aurora 25.0a2.
The issue is reproducible on Windows, Ubuntu 13.04 and Mac OS X 10.8.4.