"It looks like you haven't started Nightly in a while ..." message appears when it shouldn't
Created attachment 778976
cfi.png

User Agent: Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20130721 Firefox/25.0 (Nightly/Aurora)
Build ID: 20130721030203

Steps to reproduce:

Start Fx25
Takes a few tries.


Actual results:

Receive the attached notification message at the bottom of the Fx window:




Expected results:

No notification.