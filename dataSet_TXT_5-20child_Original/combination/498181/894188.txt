"resetSupported is not defined" in safe mode dialog
1. mkdir px/a/
2. While holding alt,
     firefox -profile ~/px/a/

Result:

JavaScript error: chrome://browser/content/safeMode.js, line 80: resetSupported is not defined

Probably a regression from https://hg.mozilla.org/mozilla-central/rev/37015ff213ae

safeMode.js might just need a
 -   resetSupported()
 +   ResetProfile.resetSupported()