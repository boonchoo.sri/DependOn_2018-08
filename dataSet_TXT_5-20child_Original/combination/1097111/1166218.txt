about:preferences and about:config should use native control theme
Firefox 38 replaces the Preferences dialog with the about:preferences tab, and changes the about:config style. The new styles of both configuration interfaces ignore the native look of controls. E.g. flat rectangular buttons, big radio buttons and checkboxes, colored backgrounds.

Firefox Preferences is not a web site and should not look and feel like a web site. Buttons need to look like native buttons, tabs like native tabs, etc. Selected item should have the desktop theme’s selected item background and text color.