TM should not be used in title bar
User-Agent:       Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.4a) Gecko/20030422 Firebird™ Browser/0.6
Build Identifier: Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.4a) Gecko/20030422 Firebird™ Browser/0.6

On Win98 (haven't tried on other versions) the latest nightly has "Firebird[]
Browser" in the application title bar, where the [] is one of those ugly squares
that crop up in Windows when a character can't be rendered. I'm guessing that
this is meant to be a ™.

Reproducible: Always

Steps to Reproduce:
1. Install the April 22nd nightly build of Firebird/Phoenix on a Win98 PC
2. Run it.
3. Look at the title bar.

Actual Results:  
TM sign comes up as a square.

Expected Results:  
TM sign should come up as a TM sign.