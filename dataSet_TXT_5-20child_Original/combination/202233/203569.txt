Change name in WinXP "Internet" start menu AND group name appearing in task bar to Mozilla Firebird
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.4b) Gecko/20030426 Mozilla Firebird/0.6
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.4b) Gecko/20030426 Mozilla Firebird/0.6

WindowsXP can group similar application windows to one tab in
task bar.
It should be "Firebird" instead of "Phoenix" that is currently
displayed.

Reproducible: Always

Steps to Reproduce:
1. Turn on task grouping in Control Panel of Windows XP
2. Open multiple Firebird Windows and other applications until the taskbar
overflows and grouping occurs
Actual Results:  
"Phoenix" is displayed in the taskbar

Expected Results:  
"Firebird" is displayed in the taskbar