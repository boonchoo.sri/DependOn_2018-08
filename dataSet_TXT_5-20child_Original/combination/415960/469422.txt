tag autocomplete disappears on every other character that is typed
1. Install my bookmarks into a build of 3.1 or 3.2pre
2. Try to tag something with the "printing" tag
3. Note that the autocomplete window will display on the "p", disappear on the "r", display on the "i", disappear on the "n" etc.

There are many of my tags that have this behavior.  Some of them do not.  I don't know why, and I don't know what started this.  This is a profile that has been in existence since early 3.0 days and has been upgraded by being on the nightly trunk builds forever.

I'll attach an export of my bookmarks and a screen cast of the problem (quicktime movie, I apologize).