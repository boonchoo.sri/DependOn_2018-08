Pocket icon arrow shifts to the left side
When saving the Pocket, the icon arrow shifts slightly to the right in the active pink state. This seems to only occur on Windows 10 Nightly Compact Dark theme. 

See video attachment https://cl.ly/0Q0n070H2r1n