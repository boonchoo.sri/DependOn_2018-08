Allow global notification bars to be displayed at the top of the window
Currently, all global notification bars (i.e. the ones that are not tab-specific) are being shown at the bottom of the window.
This is not visible enough for some cases like asking to become the default browser (as seen in bug 977045), so there should be a way to display those bars at the top of the viewport as well.

This is *not* about moving all bars to the top, but rather about creating the plumbing necessary to have some of them on top in the future.