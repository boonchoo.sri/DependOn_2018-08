Un closable tabs are generated if i Close tabs by using middle button or click close button
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US;
                  rv:1.9.3a4pre) Gecko/20100329 Minefield/3.7a4pre ID:20100329040204
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US;
                  rv:1.9.3a4pre) Gecko/20100329 Minefield/3.7a4pre ID:20100329040204

Un closable tabs are generated if i Close tabs by using middle button or click close button while pages are loading.
When I carried out a command by mistake, this situation is a common thing.

Reproducible: Always

Steps to Reproduce:
1. Start Minefield with new profile
2. The following urls Bookmarked in a new folder named "Forder A".
      http://mozilla.jp/firefox/3.0.5/whatsnew/
      http://spreadsheets.google.com/ccc?key=pF_tMaWf14QxNYvZaPTvsGA&hl=en
      https://wiki.mozilla.org/Extension_Manager:Install_Hooks
      https://wiki.mozilla.org/Gecko:Compositor
3. The following urls Bookmarked in a new folder named "Forder B".
      http://www.chromeextensions.org/
      http://src.chromium.org/viewvc/chrome/trunk/src/chrome/common/extensions/docs/index.html
      http://lfie.net/google-chrome-301831-dev/
4. Right click on "Forder A" and Execute "Open All in Tabs". Wait complete loading.
5. Right click on "Forder B" and Execute "Open All in Tabs".
6. Close tabs by middle button quickly WHILE PAGES ARE LOADING.

Actual Results:
 Un closable tabs are generated.
And there are many arrors in error connsole as folloes.

Error: browser.webProgress is undefined
Source file: chrome://browser/content/tabbrowser.xml
Line: 1425

Error: this.preview is undefined
Source file: file:///D:/firefox-3.7a1pre.en-US.win32/modules/WindowsPreviewPerTab.jsm
Line: 263

Error: preview is undefined
Source file: file:///D:/firefox-3.7a1pre.en-US.win32/modules/WindowsPreviewPerTab.jsm
Line: 346

Expected Results:
 Un closable tabs(except last one by default) shouldn ot be generated.

This issue happens Namoroka too.
Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.3pre) Gecko/20100327 Namoroka/3.6.3pre ID:20100327042606