[Perf][Gsheet] 41.46%(4508 ms) slower than Chrome when deleting all cells on GSheet
# Test Case
STR
1. Open the browser
2. Open the specified Gsheet document (https://goo.gl/pdR3pQ)
3. Delete all cells

# Hardware
OS: Windows 7
CPU: i7-3770 3.4GMhz
Memory: 16GB Ram
Hard Drive: 1TB SATA HDD
Graphics: GK107 [GeForce GT 640]/ GF108 [GeForce GT 440/630]

# Browsers
Firefox version: 49.01
Chrome version: 53.0.2785.116m

# Result
Browser | Run time (median value) 
Firefox | 10875 ms
Chrome  | 6367 ms