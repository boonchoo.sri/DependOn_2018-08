Commands and drag & drop stop working when bookmarks menu button moves across toolbars
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.3a6pre) Gecko/20100623 Minefield/3.7a6pre Firefox/3.6.4
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.3a6pre) Gecko/20100623 Minefield/3.7a6pre Firefox/3.6.4

Bookmark editing is not working properly with the new widget, drag & drop, deleting, etc.  Drag & drop, it moves the bookmark somewhere else above where you dropped it. Deleting doesn't work at all.  They're maybe more issues, but didn't test everything.

Reproducible: Always



Expected Results:  
Bookmaarks menu should function the same as the bookmark menu