[Mac] Bookmarks Widget changes to text if you turn on Bookmarks toolbar
Created attachment 454202
Screenshot of issue

Seen while running Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.3a6pre) Gecko/20100625 Minefield/3.7a6pre

STR:
1. Load a fresh profile.
2. Observe the bookmark widget on the right hand side of the toolbar.
3. View | Toolbars and turn on the bookmarks toolbar.
4. Observe the attached screenshot - the widget turns into text.