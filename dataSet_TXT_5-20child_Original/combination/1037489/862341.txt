Always record the request, even if the Network Monitor is not open
Would it be possible to always record the network requests, even if you don't currently have the network tab open?

My workflow usually starts in the Web Console and then when I see something happening there I want to switch to the Network view to inspect requests that just happened.

But, the network view is empty. It only records there if you have that tab open. This is not super useful for debugging in my experience because you usually don't want to reload to just get that data again.