Allow seeing the actual request path rather than just the file name in the Network Monitor
Created attachment 788185
Screen Shot 2013-08-09 at 11.21.03 AM.png

See the attached screenshot.

I am trying to find out what the last XHR request actually looks like. Unfortunately it is impossible to see the full path of the request:

1) The list of network requests shows '/?...' in the 'File' column, which is incorrect and incomplete.

2) The pane on the right truncates the 'Request URL' and doesn't allow you to select or copy it.