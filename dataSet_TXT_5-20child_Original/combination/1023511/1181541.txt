Branding identity icons for internal pages should be SVG
With bug 1175678 fixed all the URL bar icons will be SVG. We should do the same for the branding icons, there currently are three different ones: Aurora, Nightly/Unofficial, and Official.

Steven, how difficult would it be to get those in 16px and 24px versions?