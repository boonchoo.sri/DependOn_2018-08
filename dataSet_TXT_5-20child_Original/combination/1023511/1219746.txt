[HiDPI] Icons from "Let's talk about" section are blurry
Reproduced on a Microsoft Surface Pro 2 device running Windows 10 64bit using:
*42.0RC build 1
*Latest 43.0a2 Aurora
*Latest 44.0a1 Nightly

STR:
1. Open Firefox using a clean profile.
2. Navigate to google.com (or any other website).
3. Click on the Hello icon.
4. Observe the icon from the "Let's talk about" section

Expected Result:
The icon is properly displayed.

Actual Result:
The icon is blurry.


Notes:
This issue is not a regression.