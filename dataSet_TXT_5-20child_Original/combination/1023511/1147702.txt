Use a @2x Toolbar.png on Windows for basic hidpi support
A general fix for using hidpi icons on Windows (bug 1023511 & deps) has still not been prioritized, but as a short-term / quick improvement we would like to land a 200% version of Toolbar.png to incrementally improve the main toolbar icons. This is similar to what we did in bug 946987 for the initial Australis landing (and, uhh, still have not prioritized fixing bug 995733 to do that better, but I digress).

We'll need 2x versions of Toolbar.png, Toolbar-aero.png, Toolbar-inverted.png, and Toolbar-XP.png from /browser/themes/windows/, and the related CSS fixes to use the right image and region when appropriate.

[Looking at the DISPLAY_SCALING_MSWIN telemetry, we could arguably not bother with Toolbar-XP.png, as the hidpi usage on XP is extremely tiny, but I'm assuming it would be more of a hassle to _not_ fix it.]

NI shorlander for the image assets, but we could start working on the patch by just using an upscaled version.