When navigating to some pages, the tab title turns to the URL of the page and then quickly to the title of the page
For example, "facebook.com/" then "Facebook".

It's easily reproducible by opening a Mercurial changeset such as https://hg.mozilla.org/mozilla-central/rev/32af7843b1cb.

The regression was caused by bug 1367630.

If you are on a page whose title is "Title A", before bug 1367630 the transition was "Title A" -> "Facebook", after bug 1367630 the transition is "Title A" -> "facebook.com/" -> "Facebook".