Set initial tab title as new URL's hostname (without protocol or "www.")
Bug 1366870 changed new page's initial tab title to the new URL's hostname (minus protocol and "www." prefix), but bug 1367630 changed the initial tab title to include the new URL's full path.

Was that change intentional? In bug 1366870 comment 17, Philipp Sackl recommended showing the domain name instead of the full URL, but we didn't discuss showing the new URL minus the protocol and "www." prefix.

Other browsers' initial tab titles for comparison:

* Firefox 54 uses "Connecting...".
* IE11 uses the same "hostname without www" placeholder (and as implemented in Firefox 55 by bug 1366870).
* Edge uses "Blank page".
* Chrome uses "Loading..." on Windows and "Untitled" on Mac. * Safari uses "Untitled".