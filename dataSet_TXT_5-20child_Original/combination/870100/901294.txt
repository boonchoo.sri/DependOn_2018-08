Unexpected downloading will be started by after every restart of the browser
Build Identifier:
http://hg.mozilla.org/integration/mozilla-inbound/rev/96d374c8f833
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20130723 Firefox/25.0 ID:20130723004349


Steps To Reproduce:
1. Create New Profile and Start Nightly with the profile
2. Open http://rml.lri.fr/papers/Mandel-These.pdf and wait until completion of the drawing of the page

3. Exit Nightly(Alt >File > Exit) and Restart Nightly 
4. Wait 5-10 sec
   --- observe download toolbar button
5. Repeat step 3-4 if you want

Actual Results:
Unanticipated downloading will be started 

Expected Results:
Unexpected downloading should not be started

Regression window(m-i)
Good:
http://hg.mozilla.org/integration/mozilla-inbound/rev/599fe516bed5
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20130723 Firefox/25.0 ID:20130723002346
Bad:
http://hg.mozilla.org/integration/mozilla-inbound/rev/96d374c8f833
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20130723 Firefox/25.0 ID:20130723004349
Pushlog:
http://hg.mozilla.org/integration/mozilla-inbound/pushloghtml?fromchange=599fe516bed5&tochange=96d374c8f833

Regressed by: Bug 870100