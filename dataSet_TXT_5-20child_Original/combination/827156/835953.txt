Releasing webcam access in gUM still leaves the green icon UI showing the camera is active shown
Build: 1/29 Nightly

Steps:

1. Go to http://mozilla.github.com/webrtc-landing/gum_test.html
2. Select video
3. Accept permissions for a webcam
4. Select stop

Expected:

The camera should be released - resulting in no UI showing that the camera is actively being used.

Actual:

The green icon UI is still shown showing the camera is still being used.