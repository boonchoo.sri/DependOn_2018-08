[meta] UI punchlist for getUserMedia blockers
Items to resolve that relate to the UI (not necessarily bugs in the UI code) either before FF20 uplift to Aurora or soon thereafter (and uplift to Aurora).

Bug 827146 isn't a blocker for this, and needs backend first.