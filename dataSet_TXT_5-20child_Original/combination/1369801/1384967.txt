Developer Tools disappeared on Nightly for many users on July 27 [devtools]
Mozilla/5.0 (X11; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0 ID:20170727100240 CSet: e5693cea1ec944ca077c7a46c5f127c828a90f1b

In today's  build, the Web Developer menu is empty, the only devtool available is View Source, the shortcut key to activate the devtools is not doing anything.

This was seen and reported by multiple people, mostly on Linux but also Windows.