Remove try…catch inside wrapWidgetEventHandler once bug 503244 is fixed
(Quoting Dão Gottwald [:dao] from comment bug 865916 comment 88)
> It seems like bug 862627 will happen eventually, so the workaround may not
> be worth it. But if you want to keep it, you should add a comment pointing
> to a bug so we'll eventually get rid of the workaround.

https://hg.mozilla.org/projects/ux/rev/5a7f22213ce9#l3.145