Create a Character Encoding widget and subview
Character Encoding will not be in the menu panel by default, but user can add it to the panel via the customization page. The goal here to figure out the best UX approach with minimum redesign of the current model, and make sure the UI is consistent with the rest of Australis customization UI (one level subview, no scrolling in panel, etc)

- Assumptions:
1. Not a lot of users in North America use this feature but people in other countries use it quite often. Therefore we don't ship it by default but provide the option to add it back.
2. People who use this feature switch between two or several encoding methods, not choosing a different one every time. So we don't have to show the entire encoding list at all time.

- Design Proposal:
http://people.mozilla.com/~zfang/Customization/Character%20Encoding.pdf
See "Design 1"
1. In Character Encoding submenu, show "customize list", "Auto-Detect" and a list of encoding methods (based on popularity, frequency or location)
2. User can check/uncheck auto-detect or switch between encoding methods in the list.
3. User can open "customize list" panel and add a encoding in the list if it's not in submenu.