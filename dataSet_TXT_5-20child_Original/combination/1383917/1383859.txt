Tons of spew to stdout, broken page, when opening about:newtab with a local file:// top site
Created attachment 8889555
Log of the output

In my normal testing profile, whenever I open about:newtab I get a ton of spew to stdout (attached) and a broken page with no actual tiles.

Setting needinfo flags per Gijs' suggestion.