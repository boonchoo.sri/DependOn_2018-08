Investigate removal of globalOverlay.js / goUpdateCommand from toolbox
toolbox.xul loads globalOverlay.js, and then toolbox.js calls goUpdateCommand from _updateTextboxMenuItems.  The toolbox's textboxContextMenuPopup variable is used from various tools in the inspector to open a context menu programmatically on elements.

We should investigate if this whole machinery can be removed and just allow a normal context menu popup to open on the text boxes instead: https://dxr.mozilla.org/mozilla-central/search?q=textboxContextMenuPopup&redirect=true.