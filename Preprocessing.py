inputPath = 'dataSet_TXT_5-20child_Original/'
subPath = ['combination/','Summary/']
outputPath = 'dataSet_TXT_5-20child_Cleaned/'

import glob
import os
import sys
sys.path.append('../BSLib/')
import createFolders as cFolder
import createFile as cFile
import cleanText as cText

for sFolder in subPath:
    tmpInputPath = inputPath+sFolder
    #print (tmpInputPath)
    folderList = os.listdir(tmpInputPath)
    for ssFolder in folderList:
        tmpOutputPath = outputPath+sFolder
        cFolder.createFolder2(tmpOutputPath+ssFolder)
        for file in glob.glob(tmpInputPath+ssFolder+'/*.txt'):
            inputFile = open(file,'r')
            contents = inputFile.read()
            inputFile.close()

            tmpContent = cText.cleanByContent_4PMAN(contents) 

            outputPathTmp = file.replace(inputPath,outputPath)
            #print('-----',outputPath)
            outputFile = open(outputPathTmp,'w')
            outputFile.write(tmpContent)
            outputFile.close()

print ('>>> Complete!! <<<')