Left-clicking then right-clicking the selected tab should not produce a focus ring on the tab.
