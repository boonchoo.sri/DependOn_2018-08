Refactor protocol.js to allow fronts to be decoupled from actors.
For devtools.html, we have to be able to create protocol.js fronts in environments where the server is not available.

This is currently not possible, because the factory function used to create a front constructor takes the constructor for the corresponding actor as argument. This factory function uses the prototype of the actor's constructor to automatically generate the protocol specification for the actor.

Instead of generating the protocol specification for an actor from its constructor, we should define it as a separate entity. This will allow us to create a front constructor by passing the protocol specification to the factory function, instead of the actor constructor.

Similarly, instead of generating the protocol specification from the prototype of the actor's constructor, we will pass it as an argument to the factory function for the actor's constructor.
