Improve Tab Preview interpolation to reduce poor dithering
Saw this on Scott Hanselman's blog 

"One subtle but irritating thing I did notice, was that the Aero Previews for Firefox  (build  are really unclear and poorly dithered. Here's an IE8 preview showing cnn.com next to FireFox showing the same page.

I'm sure they'll fix it, but it's irritating to my eye. [...]"
