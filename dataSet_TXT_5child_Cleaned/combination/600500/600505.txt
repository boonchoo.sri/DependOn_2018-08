App update preference ui favors disabling app update
The preference ui displays

Automatically check for updates to:
[X] Firefox
[X] Add-ons
[X] Search Engines

When updates to Firefox are found:
O Ask me what I want to do
Automatically download and install update
[X] Warn me if this update will disable any of my add-ons


If someone doesn't want updates to be automatically downloaded they might just read the first couple of items on the page and not notice there are other options since they are several lines further down in the ui and instead choose to disable update checks completely.

I don't necessarily like the ui Windows has for these settings but I do think the order of choices is better than what we have.

Firefox has:
enable (with individual settings further down the page) or disable entirely
for app update
for app update
individual settings mentioned previously

Windows has:
Install updates automatically (recommended)
Download update but let me choose whether to install them
Check for updates but let me choose whether to download and install them
Never check for updates (not recommended)
