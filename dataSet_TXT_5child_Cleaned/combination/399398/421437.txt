Need a list of icons that actually have a disabled state
I need a list of all of the icons in Firefox that actually use their disabled state. Here is what I believe the list is:

-Back
-Forward
-Refresh
-Stop
-Cut
-Copy
-Paste
-Report Site
-Tab Close (used for inactive tabs)
-Pause Download (new in Firefox 

Here is what I believe is a complete list of all the icons in Firefox  that had inactive states that were either never used, or are not used in Firefox 

-Web 
-History
-Secure
-Tab Arrow End
-Tab Arrow Start
-Bookmarks
-Downloads
-Livemark Folder
-New Tab
-New Window
-Print
-Secure Broken
-Livemark Item
-Search

Am I missing any icons that should have a disabled state, or is anything here wrong? I'll resolve the bug once I'm confident we have figured this out.
