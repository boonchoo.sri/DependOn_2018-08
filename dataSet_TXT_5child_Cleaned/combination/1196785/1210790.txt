Consider removing the devtools.chrome.enabled pref.
(From J. Ryan Stinnett [:jryans] (use ni?) in bug  comment 
> I think overall the pref's existence is a bit strange... It's not a
> security protection really. It hides browser debugging so as to not
> overwhelm people (I guess?).
>
> Even then, I'm not sure it's worth it. Let's file a follow up bug and ni?
> :canuckistani so we can re-debate this somewhere that I can find again.
>
> My current opinion is that it would be nice not to need this pref, at least
> for the about:debugging page (but maybe also in other places too), since
> it's pretty clear from the categories what you are looking at, and you just
> want to use the thing. As it is today, the check box is basically saying
> "should this page function correctly?" which is odd question to ask, I think.

There are checkboxes on both pages, because they both need a pref enabled to work (worker debugging or chrome debugging for addons). In my WIP devices category, there a remote debugging checkbox. But I'm all for removing useless flags.

Jeff, what do you think. Should we nuke devtools.chrome.enabled ?
