Arrow of image preview in devtool network panel does not change position according to orientation of preview box
Created attachment 
Themes _ Free and Premium Wordpress Themes - Firefox Developer Edition.png

User Agent:  (Windows NT  WOW64; rv:44.0)  
Build ID: 

Steps to reproduce:

Open devtools -> network panel
Click "Dock to side of browser window" so that the space to top and right becomes narrow
Hover over an image in the networ panel




Actual results:

The image preview opens with a different orientation (good!) but the arrow of the preview box does not point to the right image any more (bad). See screenshots.


Expected results:

The image should not stick to the "bottom left" all the time. Depending on the orientation of the preview box the arraw should be placed at "top left", "top right" or "bottom right" sometimes.
