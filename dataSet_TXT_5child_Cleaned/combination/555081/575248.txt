Double-click on the Tab Toolbar still acts on the window size when tabs are on bottom
With today's nightly double-clicking on the Tab-Toolbar no longer opens a new tab, but instead causes the browser to become un-maximized.

More fall-out from getting Drawing in Titlebar working.
