Tweak hitzone for resize, which is too big with Tabs on Top+app Menu, leaves little room to click and drag or double click above tabs.



Windows in normal states with tabs on top with custom drawing in the title bar make it hard to use mouse events other than resize,  and double click are also hard to hit.




even out the hit zones.


reduce resize zone and all easier events to occur closer to the top of the window edge.
