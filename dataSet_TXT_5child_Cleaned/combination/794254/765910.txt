Right-clicking on a text-selected property value and choosing "Copy property value" doesn't update the clipboard
STR:
Left click on a property value in the rule view.
Right click on the selection
Choose "Copy property value"

Expected:
Selected value will now be in the clipboard. If the selection was adjusted, only the selected part should be in the clipboard.

Actual:
The clipboard is never updated, so whatever was previously in the clipboard is still there.
