Cmd-F does not focus the debugger search input
STR:

Open the debugger
Click in the source editor (where script source is shown) so it is focused
Press Cmd-F (on Mac, or the appropriate shortcut for your OS) to search the current file

ER:

A "#" character is added to the search input and the input is focused so you can type there immediately.

AR:

A "#" character is still added to the input, but it is not focused, so you have to click into the input manually.

I've seen some CodeMirror bugs fly by lately, possibly related. I think it's a recent regression.
