Define additional jump lists items for Firefox
A number of folks have been making suggestions on tasks for the task list as well as secondary lists to favorites and frequent for win7's jump list for firefox. Filing this meta bug for discussion of possible additions and behavior that the ui guys can  Items that are approved will have bugs filed off to try and get them added.

Current list of jump lists tasks when browser is open:
Open new tab
Open new window

Current list of jump lists tasks when browser is open:
(empty)

Possible additions:

launch in private mode (must have for parity with ie8, bug 
launch directly to preferred search engine
open last closed tab
linksbar links list
restore last session  open without restore
