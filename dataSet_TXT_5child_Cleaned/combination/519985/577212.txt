If browser is closed while in private browsing mode, jumplist entry is not correctly updated



The tasks "Enter private browsing" and "Quit private browsing" in the Windows  jumplist are sometimes incorrect.




Open Firefox
Start private browsing mode
Close Firefox
-> See results below

Additionally:
Click "Quit private browsing"
-> See results below

Task in jumplist says: "Quit private browsing"
Additionally:
When clicking it, Firefox will start in private browsing mode, restoring the tabs you had opened before switching to private browsing last time. Switching back to normal browsing then will open only an empty tab, so your original session gets lost!


Task in jumplist should say: "Enter private browsing"
