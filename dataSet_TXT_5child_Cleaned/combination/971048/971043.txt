Implement document walker to grab webpage's text content and format it for translation service
When a webpage content has finished loading, we need grab all of the page's text content and parse it in a way that is suitable for the translation service.

It must generate a tree similar to a DOM tree, but it must skip non-textual nodes that may exist inside the page's body (such as <script>, <style>, others?).
It also does not need to preserve the whole tree structure, as it can discard any nodes which do not directly have text content.
And it should assign unique ids to each node, which do not need to reflect the ids from the webpage.

For example, the following page:

<html>
<body>
<h1 id="header" class="h1class">Welcome to  
<div>My Sidebar
<ul>
<li>Item 
<li>Item 



<div class="main-page">
<p>This is the main page's 



<style>div { color: blue 


Should get a tree (in a JS struct) like:

<body>
<h1 id="1">Welcome to <b 
<div id="3">
<li id="4">Item 
<li 
<div>
<p id="6">This is the main page's 



In addition, this module needs to be able to parse a string serialization of this structure (as above) back into an equal JS struct.
