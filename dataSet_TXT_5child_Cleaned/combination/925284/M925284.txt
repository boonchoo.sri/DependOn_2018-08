Arrow panels are visually inconsistent across the UI and look awkward
Created attachment 
Mac OS X  identity and menu panels

I have the following (visual) issues with doorhangers in Australis (as tested on Mac and XP), in descending order of severity:

Inconsistent styling:
The identity (and similar) and menu doorhanger (and similar) differ visually. Most prominent is the lighter border and maybe shadow around the menu panel which makes the up arrow look off (on Mac); on XP, the menu panel has no shadow while the identity panel has.

Arrow too "fat":
Especially for the menu panel on Mac (at least without addressing issue #1), the arrow button looks too much in-your-face. Additionally, due to the size and location (relative to the panel) of the arrow, when using the full width of the device display (which is probably the most used size on <14" screens), the panel has to move into the window, which looks awkward, too. Having a smaller (and maybe repositioned) arrow will fix this at least partially.

Differing alignment:
As seen in the screenshots, the panels have a different offset. That stems from the different types of buttons they originate from, however it leaves a weird feeling while using Australis (at least for me on Mac).
If using arrows of different size (see issue #2) for different panels (trade-off with consistency here!), the alignment issue can be fixed (at least for some platforms). Otherwise the panel position (relative to the button it originates from) has to be changed.
