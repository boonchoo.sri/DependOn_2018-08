In the tour overlay "Open Default Browser Settings" is not clickable again if it is clicked once

Screen capture is here: 

Start the latest nightly (57.0a1) from clean profile and do not set it as default
Create new tab and click on the fox
Click on "Default Browser"
Click on "Open Default Browser Settings"
Cancel the popup default setting window.
Click on "Search" from the overlay
Click on "Default Browser" again
Verify "Open Default Browser Settings" is clickable

Actual Result:
"Open Default Browser Settings" is NOT clickable

Expected Result
"Open Default Browser Settings" should be clickable
