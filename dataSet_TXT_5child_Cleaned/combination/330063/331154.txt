Folder title doesn't update in UI when set using nsINavBookmarkService::setFolderTitle()
While adding the ability to edit folder titles in  I ran into the issue that after the title is changed, any UI that's already onscreen isn't updated. (This happens in both the Places window and the toolbar.) However, the value is sticking; windows that are opened after that show the correct values.
