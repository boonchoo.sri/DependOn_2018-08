[User Story] [Overlay] [Tour Experiment] As a user, I want to learn the benefit of "Firefox Sync"
[User Story]
As a user, I want to learn the benefit of "Firefox Sync"

[Acceptance Criteria]
- I can see the details of Firefox sync by moving my mouse over (need to confirm with @mverdi on mouseover vs click)
- I can create an account or sign-in to my existing account directly in the overlay
- When I already sign in to my account, it will show this tour is completed
- When I click "Firefox Sync" notification, it will bring me to this page
- When I already sign in to my account, "Firefox Sync" notification will not appear
