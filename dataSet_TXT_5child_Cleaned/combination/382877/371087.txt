Move some APIs from nsILivemarkService to nsIDynamicContainer
"Reload" and  Source URI" should be common features
for any dynamic directories. As a first step, I think they should
be moved to the parent interface, nsIRemoteContainer.
