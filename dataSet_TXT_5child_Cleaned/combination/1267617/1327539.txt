Clicking on the right side of identity block does nothing when notifications are visible
>>> My Info: Win7_64, Nightly   ID  (2016-07-15)
STR_1:
Open attachment 
Click any button on that page, wait  seconds
Click between the last notification in identity block and the right border of identity block

AR: No visible action
ER: Either X or Y or Z
X) The last notification should open
Y) Identity block should open
Z) Url should be selected

Note:
Option X is the best IMO in these circumstances. However, I can barely thing about what is "better"
given that current UI is broken far and wide for too long.

This was broken in bug  Regression range:
> 
