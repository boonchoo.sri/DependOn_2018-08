Urlbar loses focus when typing after clicking on the icon of a permission doorhanger
No idea how I discovered this edge case:

STR:

Open 
Click on a button to get a permission notification
Click on the doorhanger anchor icon
Type something in the urlbar

I can only type a single character before it loses focus.
