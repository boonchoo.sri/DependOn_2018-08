[New Tab Page] shows thumbnails from pages with "Cache-Control: no-store", and HTTPS pages when HTTPS disk caching is disabled
User Agent:  (Windows NT  WOW64; rv:13.0)  
Build ID: 

Steps to reproduce:

When you open a new tab, clicking the +, a new tab will open with a blank page. Click on the grid in the upper right hand corner and a  page preview appears.


Actual results:

I have found that there is too much information available to people when using this option. For instance, I filled out a form on the computer, and in the preview screen it showed my completed form, including my full name, address, phone number, etc.



Expected results:

This feature is too risky as is. Even if there is an option to shut this off, someone with a little know-how could just turn it back on and wait to retrieve someones information on a public computer.
