potential privacy risk with preview tab and password protected websites through thumbnail previews
User Agent:  (Windows NT  WOW64; rv:20.0)  
Build ID: 

Steps to reproduce:

I visited a (any) private (.htaccess) password protected website base-url 


Actual results:

I saw a preview of my private page-content when i opened the new-tab-preview (through pressing the plus-symbol in the tab-bar). I closed firefox (all windows) and started it again. There were still the thumbnail preview of my private website. A few headlines and images were clearly readable and images of persons were recognizable.


Expected results:

After asking for an login to access the page, firefox should immidiately stop making screenshots or other recordings of that content, without extra acknowledgement. If that website is to be shown, in the tab-preview, there should be a lock-symbol and "authorization required" instead of real content.
If you want to allow this for more experienced or less-paranoid persons, it should be optional to switch on, and not be on by default.
