Need Window icons for 
This is the  version of Bug  However for  there
needs to be the  version of each file, as well as the  version, such as:

JSConsoleWindow.xpm
JSConsoleWindow16.xpm

Also, that is the correct case of the JS Console for Firefox. It does not match
the case of the JS Console on Seamonkey (jsconsoleWindow.xpm)

---------- In Bug  Ben Goodger Said ----------
Mozilla and Netscape  have window icons for things like bookmarks, etc. We need
them for Firebird for the following windows:

Browser
Bookmarks
Downloads
"Security UI" (generic icon for all Security windows)
Info windows (something with an "i" in it probably)
JavaScript console
Help
DOM Inspector
Venkman

Some of these (DOMI, Venkman) can simply be pillaged from seamonkey. The others
will have to be done specifically for Firebird.
