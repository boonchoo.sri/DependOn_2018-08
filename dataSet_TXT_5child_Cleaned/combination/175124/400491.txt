shift->middle clicking on a folder of bookmarks should not replace the currently existing tab (unless it is blank), if browser.tabs.loadFolderAndReplace is set to false



I guess Mike wanted me to create a whole new bug, because he didn't open it back up when I changed Bug  description.

Basically, if browser.tabs.loadFolderAndReplace is set to false,
shift->middle clicking on a folder of bookmarks should not replace the currently existing tab unless it is blank.




Set broswer.tabs.loadFolderAndReplace to false
Create a folder with multiple bookmarks.
Go to your homepage
Hold Shift and middle click on the folder of bookmarks you created.

The bookmarks open in new tabs, but the first bookmark replaces your homepage tab.


The bookmarks open in new tabs but don't replace any open tabs.
