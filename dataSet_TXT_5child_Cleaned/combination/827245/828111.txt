Workaround multiple reflows issue and slow xbl attachment in the downloads view
Created attachment 
patch

I don't think we're going to figure out on time why adding the richlistitems using a DOM fragment doesn't hint gecko that it could avoid all these layout flushes during the giant appendChild call, not to mention fix that. However, I found out that the multiple reflows can be easily avoided by temporarily removing the richlixtbox from the document. This isn't noticeable at at all, and at least on my machine it almost completely resolves the slowness we see on Alice's profile. So, ugly as it is, I think we should go ahead and do it.

I also found out that "un-attaching" the richlistitem binding from the inactive elements has a notable positive effect. This hack I like less, because it could have some side-effects (for example, it'd be a little tricky to have searchLabel working for this items). However, the gain is notable enough that it's at least worth trying. We can take it back later if it turns out to be problematic.

Patch attached. I've also removed the _wasDone setting for history downloads as it's not necessary for them (it's only used in onStateChange).
