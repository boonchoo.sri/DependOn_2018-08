Console API messages are not consistent
The window.console API implementation does not send consistent notification messages for every kind of method. The message.arguments property does not always hold the arguments the page scripts give to the method at call time - see for example console.trace(), time(), etc.

We should cleanup the confusion.

(see bug  comment 
