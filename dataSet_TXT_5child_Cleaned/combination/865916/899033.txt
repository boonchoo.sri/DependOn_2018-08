Web Developer icon on Australis toolbar shows whole sprite on Linux x64
Created attachment 
The process of dragging the Web Developer tool onto the toolbar on   (x64)

User Agent:  (X11; Linux x86_64; rv:25.0)   
Build ID: 

Steps to reproduce:

Dragged the Web Developer tool onto the toolbar on latest Linux x64 UX Nightly (Australis) on Ubuntu  x64.


Actual results:

See attached image - instead of showing the Web Developer icon, the button shows up as the whole image sprite of multiple icons. The button remains usable, as it shows the dropdown menu of Web Developer tools when clicked.


Expected results:

The button should have shown up as the same icon visible on the tools tray, before it is dragged on the toolbar. Dragging the icon to the  menu works as expected and the correct icon is shown.
