WiFi discovery confused when primary interface changes
Could be OS-specific, I was on a Mac at the time.

STR:
Connect with multiple interfaces (ex. WiFi and Ethernet) simultaneously
Set up a non-WiFi interface as the primary interface
WiFi discovery works
Unplug non-WiFi interface (WiFi becomes primary)
WiFi discovery is now broken

In more detail, WebIDE pings the devices, and the devices respond, but WebIDE can't hear their replies anymore.
