Need a method for resetting bookmarks in Places
In Firefox  an option was added to Safe Mode to reset default bookmarks. We're looking at expanding this behaviour to help users selectively recover from broken profiles, but we need a way to make this work again in places.

We basically need a fast way to drop the existing bookmarks, and pick up the defaults on next startup.
