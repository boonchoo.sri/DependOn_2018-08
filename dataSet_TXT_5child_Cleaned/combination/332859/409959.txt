Safe mode option to disable all add-ons doesn't disable plugins
When you start safe mode you get a checkbox that you can tick to disable all your add-ons and they are then disabled when starting normally. Plugins aren't affected by this.
