"WARNING: content window passed to PrivateBrowsingUtils.isWindowPrivate. Use isContentWindowPrivate instead" from browser.js
There are even more instances of usage of isWindowPrivate around in browser.js. I wonder why we haven't converted all of them at once when turning the flip in bug 

From one of the latest Nightly builds:

WARNING: content window passed to PrivateBrowsingUtils.isWindowPrivate. Use isContentWindowPrivate instead (but only for frame scripts).



Associated source:

let isPrivate = PrivateBrowsingUtils.isWindowPrivate(aOpener || window);
switch (aWhere) {

I also see lots of other calls like that, but not sure if all of them have to be converted too:


