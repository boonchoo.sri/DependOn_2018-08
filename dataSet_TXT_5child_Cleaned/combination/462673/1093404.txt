e10s - fix browser_bug462673.js
The test seems to depend on the content window opening another tab, which happens asynchronously, and which makes the test fail:

INFO TEST-UNEXPECTED-FAIL |  | test_bug462673.html has opened a second tab - Got  expected 
Stack trace:




null:null:0
INFO TEST-UNEXPECTED-FAIL |  | dependent tab is selected - Got [object XULElement], expected null
Stack trace:




null:null:0
