Expose tabs on-demand preference
I'd like us to expose the on-demand tab loading (currently done via the browser.sessionstore.max_concurrent_tabs=0 preference) in the prefs under session restore.

It would look like this when selected:


When Firefox starts: [Show my windows and tabs from last time ▼]
[✓] restore tabs on-demand


If we can't resize the window to accommodate this extra line that only shows when restoring tabs is selected, it's OK to leave a blank line here for the other options. It should be unchecked by default, so the current behavior doesn't change unless you opt in to it.

Some additional notes:
I'm sure the wording can be improved, suggestions welcome.
Based on discussions, we probably want to add a "restore tabs on demand" preference that short-circuits the session restore concurrency setting instead of overwriting the number with 
There might be a better place or way to expose this setting, suggestions welcome.

Wanted to get a bug in, so Paul can take a look if we want to try landing this for the upcoming release — since the code is already in Firefox 
