Use back.svg for the back button and remove back-large.svg
The tail part of the back button icon is a bit longer than it previously was. See



This makes the back and forward buttons look different, which is definitely not good for the Photon compact mode but I guess it's also a general visual regression we should fix.
