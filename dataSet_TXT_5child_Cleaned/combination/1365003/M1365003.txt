[meta] There is a lot of space between the forward button glyph and the reload button glyph
Created attachment 
screenshot

The comparatively large space between the two buttons makes the layout look a little unbalanced because there's not as much space after the refresh button.
