The title of the tabs change to "Connecting" when they finish loading
When I open a new tab and load a page in it, the title seems to get stuck on "Connecting". I'm running a build with the patches in bug  and I am going to make an educated guess blaming that bug for this problem.
