Firefox migrator for new profiles
I have a rough patch already. It works on linux, not sure about other platforms.
While this might not seem necessary at first glance, this is for a browser that
will be built off of Firefox. Normal Firefox builds will never see an option to
import a Firefox profile because if they already have one they won't see the
import dialog.
