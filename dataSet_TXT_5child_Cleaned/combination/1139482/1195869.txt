Refresh tabs in sidebar mode
The sidebar mode has a known issue[1] with refreshing device tabs:

There are currently no available events to listen for when an unselected
tab navigates. Since we show every tab's location in the project menu,
we re-list all the tabs each time the menu is displayed.
TODO: An event-based solution will be needed for the sidebar UI.

Since we're no longer clicking a button to show the project list in sidebar mode, there's no click event we can use as a signal to re-request updated tab info.

Let's try either a small refresh button next to the tabs header, or something similar to this, to resolve the issue in sidebar mode.

[1]: 
