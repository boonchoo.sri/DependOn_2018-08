One-click search buttons should have a right-click menu
There are a couple of advanced actions that users should be able to perform from a one-click button. We can expose them in a right click menu.

Than menu should initially include the following two items:
- Search in a new tab
- Set %enginname as default search engine
