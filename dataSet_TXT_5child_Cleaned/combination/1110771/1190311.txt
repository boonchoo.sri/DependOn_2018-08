Context menu and suggestions panel both appear when right clicking an unfocused searchbar above or below the text field
(Jeffrey Alexander from bug  comment #43)

> Although right-clicking on the text area or icons within the unfocused
> search box does now function correctly, doing so above or below the area
> where the pointer switches to a text cursor still incorrectly reveals two
> menus.

I can reproduce this, it seems we missed an edge case while fixing bug 

Dave, would you be any chance remember enough of this code to be able to guess quickly what a fix could be?

> Likewise if left clicking the text and then right-clicking in the brief
> moment before the "suggestions" menu appears.

I haven't been able to reproduce this. I see somewhat erratic behavior when following these steps, but I haven't managed to get both the context menu and the suggestions panel to be visible at the same time.
