Firefox saves wrong window restore size while closing a maximized window.
Firefox saves wrong window restore size and window position while closing a maximized window.

Current logic is making restore window button useless. Which is too irritating.

This was tested on windows.

Steps:-
Exit Firefox, to close all Firefox instance.
Re-Start Firefox, to get a single Firefox window.
Re-Size window to  size of screen display.
Reposition window  away from top and left.
Exit Firefox, then Re-Start Firefox.
You can see Firefox remember window state,
size and position correctly.
(ie, restore state, size   from 
Now Maximize window, by clicking window maximize button.
(remember not to change 
when it is still in restore state)
Exit Firefox, then Re-Start Firefox.
You can see Firefox in Maximized window state,
ie, Firefox remember window state correctly.
Now click Restore Button.

Result:-
--------
You can see
* size is almost  of screen size
* position almost left, top of screen size

Expected:-
---------
* size  of screen size
* position at  from  of screen.



What to be done:-
---------------
We should use following common logic.

function onExitApplication(){
switch(window.state){

'maxmized' :
save_window_state('maxmized');
break;

'normal' :  restored 
save_window_state('normal');
save_window_size(window.width, window.height);
save_window_position(window.left, window.top);
break;

'minimized' :
'systray' :
default :

}
}


(Windows; U; Windows NT  en-US; rv:1.9a9pre)

