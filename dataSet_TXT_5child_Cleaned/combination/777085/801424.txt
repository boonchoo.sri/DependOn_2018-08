Markup Pane and Style Rule View would not disappear


(Windows NT  WOW64; rv:17.0)   ID:20121010150351


Steps to reproduce:
Open 
Scroll to "treeitem > treerow {…}  A tag-based rule 
Double click "treeitem"

Right click on highlight and choose "Inspect Element(Q)
Open Markup Panel
Open Style Rule View

Click "x" button at the right side of inspector-toolbar
--- observe Markup Pane and Style Rule View

Click NewTab button on the Tab bar
--- observe Markup Pane and Style Rule View

Actual results:
Markup Pane and Style Rule View would not disappear

Expected reults:
Markup Pane and Style Rule View should disappear
