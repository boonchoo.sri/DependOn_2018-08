revert the workarounds for bug #373719, bug #373721, bug #374150 and bug #374166 caused by bug #267833
revert the workarounds for bug #373719 and bug #373721 caused by bug #267833

bz writes:

We may want to do something similar to what scripts do and only fire
constructors from EndUpdate if they got posted during the update. Not sure;
need to look over those stacks a little more (in particular, the reentry of
SetView on the tree there).

Unfortunately, I won't be able to do that until the  Were we going to
enable places before then, or is this blocking places development? If so, it
might be worth doing the workaround for now.... :(

this bug tracks backing out the hacks i checked in for bug #373719 and bug #373721 (I'll attach a patch)
