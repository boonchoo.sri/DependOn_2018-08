Stop searching for autocomplete results that won't be displayed
No need to spend cycles on something we won't use. See bug  comment  for more details.
