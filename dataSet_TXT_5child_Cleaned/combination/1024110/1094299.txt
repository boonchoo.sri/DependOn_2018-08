The installer doesn't work properly while trying to launch the installed app (on a machine with  profile)
Reproducible on Aurora  (Aurora DevEdition - latest build from the gum twig: 
(Windows NT  WOW64; rv:35.0)  

Steps to reproduce:
Make sure that you are on a machine with  profile ("default").
Launch  with the "default" profile.
Use  to download the Aurora DevEdition latest gum build (go to  Run it and after it's done, click finish, with the "launch Firefox now" checkbox ticked. -> the "dev-edition-default" profile is created and the Aurora DevEdition is launched simultaneously with 
Go to Options and unchecked the "Allow Firefox Developer Edition and Firefox to run at the same time" option. Restart Aurora DevEdition.
Close Aurora DevEdition, delete the "dev-edition default" profile and then uninstall Aurora DevEdition from your computer.
Open again  with the "default" profile and use it to download again the Aurora DevEdition latest gum build. Run it and after it's done, click finish, with the "launch Firefox now" checkbox ticked.

Expected results: After step  the "dev-edition-default" profile is created and the Aurora DevEdition is launched simultaneously with 

Actual results: After step  the following message is received: "Firefox is already running, but is not responding. The old Firefox process must be closed to open a new window" (the "dev-edition-default" profile isn't created).
I think this happens because the "Allow Firefox Developer Edition and Firefox to run at the same time" option was unchecked at step 
