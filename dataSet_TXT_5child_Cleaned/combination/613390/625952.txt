Drop hostname from URL preview if it’s on the same domain
Similar to bug  we aim to make the URL preview on hover as readable and easy to parse as possible. Making this change would also reinforce that you're staying on the domain you're currently visiting.

Proposed behavior:

* If you're on  and hover over a link to  nothing changes.

* If you're on  and hover over a link to  the URL preview on hover shows  — stripping away the redundant  information.

* If you're on  and hover over a link to  we don't strip anything (since you're changing from http to https, even if it's on the same domain)

* This bug depends on bug  to be resolved, since we can't strip only the hostname but not the protocol.
