Copy full CSS Selector tool is missing its histogram
When using this tool I get

Warning: An attempt was made to write to the DEVTOOLS_COPY_FULL_CSS_SELECTOR_OPENED_COUNT histogram, which is not defined in Histograms.json

because it is missing an histogram definition in Histograms.json. So our usage reports will be incorrect.

Instructions to fully add the histogram are in 
