Better titles for Object Inspectors (PropertyPanel)
Currently, Object Inspectors (PropertyPanels) have the toString() representation of the inspected object as the panel's title. It would be nice to have the immediate name of the object instead (e.g., "document" if a user types "document" in the scratchpad and inspects it) or allow users to set the title manually.

I think the former behavior, taking the last line of a selected block and applying that to the panel titlebar would be a good first step. Manually setting the title might be a bit fiddly for most people's tastes.
