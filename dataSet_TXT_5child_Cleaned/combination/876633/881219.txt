When filtering sources, hiding items in the sources list is redundant
Created attachment 
screenshot

While filtering files, as you type, a completion popup displaying all the matched sources is shown. At the same time, unmatched entries in the sources list on the left are hidden. This creates a redundant scenario in which matched sources are displayed in two places at the same time.

Example (STR):
Open debugger on 
Type "to" in the filter box (without quotes)

Result:  matched files are shown both in the popup and the sources list.
