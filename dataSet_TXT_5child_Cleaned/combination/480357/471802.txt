Location bar identity display ignores certified CN, always showing hostname or eTLD+1 depending on browser.identity.ssl_domain_display



When the location bar is set to show the SSL Identity Name by setting browser.identity.ssl_domain_display to  in about:config the Location Bar always shows the hostname part of the domain visited, instead of the Common Name the certificate is signed for.

In case of the URL  the associated SSL cert is signed for Common Name CN=*.mozilla.org, but the additional Identity Field shows bugzilla.mozilla.org.

This can be abused by attackers by generating domain names quite simular to the original name of the domain you browsed to and presenting a certificate that certifies a shorter version of the Domain name (e.g. get a cert for *.org instead of *.mozilla.org). An enduser could not easily distinguish the legitimate cert from the fake one since in both cases he is presented the full bugzilla.mozilla.org domain name as being signed.




Set browser.identity.ssl_domain_display to  in about:config
Open up a SSL encrypted domain with a wildcard cert (e.g. bugzilla.mozilla.org


The Location Bar shows "bugzilla.mozilla.org"


The Location Bar shows the signed Common Name of the certificate (in this case *.mozilla.org)

Happy reading of the latest Certificate talks at:


