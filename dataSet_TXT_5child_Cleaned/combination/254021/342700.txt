Make undo close tab more discoverable
We added this great feature, but I don't think many people are going to discover it where it currently is (in the tab context menu).

Possible solutions:
* put it in the page's context menu
* put it in the history menu
