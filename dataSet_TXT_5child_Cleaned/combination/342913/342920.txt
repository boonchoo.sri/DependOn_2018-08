Allow users to disable full-text indexing
Users should be able to entirely disable all text-indexing of any sort of web page. (Other options should also be provided, but this is the most fundamental one.) Likely this will be accomplished with an about:config option.
