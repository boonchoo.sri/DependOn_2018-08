Reindex full text of bookmarked pages as they are revisited
When bookmarked pages are added to a full-text index for later searching, that index should be updated to hold each page's current text as the bookmarks are revisited.
