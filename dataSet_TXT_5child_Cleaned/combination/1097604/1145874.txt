[win10] Browser UI is cropped a few pixels when windows are maximized in x86 Firefox
Created attachment 
screenshot.png

User Agent:  (Windows NT  WOW64; rv:37.0)  
Build ID: 

Steps to reproduce:

Open x86 version of Nightly or Firefox in Windows  TP build 



Actual results:

Browser UI is cropped a few pixels from every side in maximized mode.
Issue is fixed after setting application compatibility to Windows  or lower.
Issue is not present in x64 version of browser.
