Caption buttons in Windows  don't include themes very well
Created attachment 
Firefox caption buttons.jpg

User Agent:  (Windows NT  WOW64; rv:37.0)  
Build ID: 

Steps to reproduce:

Downloaded a clean version of Firefox, deleted appdata firefox folder, ensured no addons were active


Actual results:

See picture. Windows caption buttons (close, minimize, restore) do not blend at all with any given firefox theme.


Expected results:

Previous versions of windows had caption buttons that were more clearly meant to be separated from individual program themes, thus allowing the addon community to explore new avenues to alter the appearance of Firefox. With the new caption buttons however, Windows has caused that themes appear incomplete. Windows may not budge on their new program, and so Firefox may have to compensate.
