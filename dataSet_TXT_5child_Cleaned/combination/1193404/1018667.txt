Create a plan for   passwords (in OS X Keychain)
Bug  to import passwords from Keychain was WONTFIXED a long time ago stating that the proper solution is to instead integrate password manager with Keychain (bug  to avoid duplicating the passwords.

There are some things that have changed since that decision:
* Chrome now exists and also uses Keychain (plus its own table for extra metadata[1]).
* We now have Sync integrated in the browser which includes Password Sync.
* Bug  has had no progress.

I think we should come up with a plan for easy migration for users with passwords saved in  as they may currently feel locked into them because of the tedious task to manually migrate the passwords one-by-one.

[1] 
