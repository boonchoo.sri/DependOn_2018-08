Thumbnail cache should be created in the Local profile folder, not the Roaming ones
User Agent:  (Windows NT  WOW64; rv:15.0)  
Build ID: 

Steps to reproduce:

Opened a recent nightly with the new thumbnail  folder


Actual results:

\Thumbnails\ was created in AppData\Roaming\Mozilla\Firefox\Profiles\4-Nightly\ AppData\Local\Mozilla\Firefox\Profiles\4-Nightly\


Expected results:

As a destroyable cache, this should have been placed in AppData\Local\Mozilla\Firefox\Profiles\4-Nightly\

The current location poses an annoyance to backup tools, as it will be now including alot of uncompressible megabytes by default in the backup archive.
