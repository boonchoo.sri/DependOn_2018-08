CSS Inspector does not toggle properly when there are multiple declarations of the same property
Load this HTML:
style="color: red; color: green">Inspect 

Inspect that element

In CSS inspector, disable "color: green"

Result: The text becomes red, but both "color: green" and "color: red" appear disabled in the inspector.

Expected: "color: red" should remain enabled.

This appeared in  previously the inspector only showed "color: green".

I guess it's bug 
