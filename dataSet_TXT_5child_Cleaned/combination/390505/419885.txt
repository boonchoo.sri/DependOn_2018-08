livemark update could fail due to an uncaught exception
Created attachment 
patch

originally in Bug  review for this patch has been already done by dietrich there, so this is r=dietrich, i'm bringing over his review:

apart the followup bugs i've filled, that should still be fixed, in _fireTimer
(the actual function that we fire every  to update) we have

for (var i=0; i < this._livemarks.length; ++i) {
this._updateLivemarkChildren(i, false);
}

_updateLivemarkChildren, if the livemark fails to load or the listener throws,
do

catch (ex) {
[omissis...]
LOG("exception: " + ex);
throw ex;
}

the exception go out and break the for cycle so subsequent feeds are not
updated, this is particularly bad if the failing livemark is the first in the
array.

Probably we don't need to pass out that exception, should be useful more for
debug purposes, so i'd simply change the return value to a bool, in case some
other code nees to see if the livemark has been updated.

should block since the original report was a blocker
