Restore "Copy Link Location" on mailto: link context menu
Created attachment 
patch

There used to be both "Copy Email Address" and "Copy Link Location" on the context menu for mailto links. Bug  removed the "Copy Link Location" item because it was redundant when you have both "Copy Email Address" and "Properties" (for anything else you might want to do). Bug  removed the properties menu. So now you can only copy the email address, even if there are other things specified in the link (subject, etc), see bug  comment  The only other way to copy this information would be to manually type it out, which is hard since the mouse must be over the link to see it; or view the source of the page.
