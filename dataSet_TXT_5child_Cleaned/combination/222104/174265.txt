Wrong favicons in bookmarks
I have quite a few bookmarks in the bookmark toolbar, and most of those sites
have favicons.
I noticed that from a fresh install, after loading the icons for the first  or
bookmarks, the icons start to repeat through other bookmarks that also have
favicons.

For example, if I have mozilla.org's bookmark next to slashdot.org's bookmark,
the mozilla icon will appear in both bookmarks.
If I then visit slashdot.org, it's icon will then replace both icons.
