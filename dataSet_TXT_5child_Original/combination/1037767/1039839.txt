Use Task.jsm more inside GMPInstallManager
In order to expedite landing Bug #1009816, I'm moving using Task.jsm to this bug.

See https://bugzilla.mozilla.org/show_bug.cgi?id=1009816#c63 for more information.

This development may happen on FF34 and will likely be uplifted to FF33.