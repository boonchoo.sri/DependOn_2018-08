Find a more conservative way for Simulator configurations to handle addon removal.
With bug 1090949, Simulators will become configurable. However, if a Firefox OS simulator addon is uninstalled or disabled, the corresponding Simulator configuration is simply deleted.

It's probably a better UX to keep that configuration somehow, especially when configurations will become persistent (bug 1146519) and start containing more and more valuable user-defined parameters.