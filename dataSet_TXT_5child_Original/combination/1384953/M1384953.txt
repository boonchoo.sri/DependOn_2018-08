Library icon animation is covered behind active tab when saving to Pocket or adding a bookmark
Created attachment 8890878
pocket animation covered by tab.mov

When saving to Pocket, the Pocket icon animation is being covered by an active tab. Please see video for reference.