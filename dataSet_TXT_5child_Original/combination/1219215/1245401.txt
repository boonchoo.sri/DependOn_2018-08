Can no longer prevent window dragging dynamically from an add-on
User can drag the window by clicking (and holding the mouse) on the title bar or on empty area of the toolbars. Dragging is also possible with modifiers - shift, ctrl, meta, alt.

This however prevents the use of mouse click options on empty area of TabsToolbar for example.

Mouse left click with modifiers shouldn't start window dragging, same as middle-click right-click and double-click.

Before bug 1219215, it was possible to call event.stopPropagation when "MozMouseHittest" was triggered with modifiers