Titles of tiles in new tab page lack adequate contrast when using High Contrast mode
Created attachment 8436861
Screenshot

The title of tiles in new tab page lack adequate contrast when using High Contrast mode. See attached screenshot.

It's possible this may be fixed by fixing bug 697836.