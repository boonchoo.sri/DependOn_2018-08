Add API to unload/hibernate tab
+++ This bug was initially created as a clone of Bug #1123261 +++

Did not want to clutter the previous bug further, thus opening a new one for a slightly different goal.

(In reply to Tim Taubert [:ttaubert] from Bug 1123261 comment #3)
> This seems perfect for an add-on and very unlikely to become core
> functionality. Closing a tab and retrieving its state is easy, implementing
> a good UI to expose restorable tabs is probably a tad harder.

There are extensions like UnloadTab (https://addons.mozilla.org/de/firefox/addon/unloadtab/?src=search), however they are marked as experimental, use private/unstable APIs (many no longer work due to changed APIs), or use some obscure mechanism to trigger the behaviour, which breaks other addons. 

Having an easy to use, public and maintained API to unload a tab would enable addon authors to explore both manual and automatic options to hibernating tabs without breaking other stuff in Fx or Addons. The API surface can be trivial: 

some.object.hibernateTab(aTabObjectOrId);
some.object.restoreTab(aTabObjectOrId); 

AFAICS the latter functionality should already exist somewhere. The former should essentially do the same that session restore does for all tabs / windows: Retrieve and store the state of the tab and unload it (transition it to the same state it is in when I enable "restore on demand", restart Firefox, and have not navigated to the tab yet). Any UI / interaction should be implemented by an addon. 

Maybe this is something that can be implemented in the Addon SDK (ni? :efaust)? This API should also be used as the building blocks for Bug 675539.