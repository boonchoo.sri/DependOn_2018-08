Localizations for updater hotfix
For the updater hotfix, we want to provide localizations. Here are the strings from the UX bug 994882:

Three rotating doorhanger messages:
"Free upgrade: a new, faster Firefox is already downloaded and ready for you to use!"

"Your version of Firefox is out of date, which can put your computer and information at risk. A new, secure Firefox is ready for you to use for free!"

"Your version of Firefox is out of date, which makes it run slower. But don't worry; a new, faster Firefox is already downloaded and ready for you to use!"

Two buttons.

"Not now"

"Start using it right now!" <-- I'm worried this might be too long, but we'll see

Any remaining UI will be on SUMO and translated separately.

Here are the top locales with outdated Firefox users, ordered (ADI data from dataviz, I'm not sure why it's missing the full locale):
en
de
es
fr
ru
pt
pl
zh
it
ja
tr
hu
ar
cs
nl
vi
id
el
fi
sv
th

Axel or Chofmann can you help me figure out how to get these strings from the right communities for the hotfix?