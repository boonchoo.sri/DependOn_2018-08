Index full text of pages as they are bookmarked
To be able to be searched quickly, the text of bookmarked pages needs to be added to a full-text indexing system.