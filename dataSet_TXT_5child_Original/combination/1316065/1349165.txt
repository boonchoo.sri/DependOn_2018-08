DevTools: complete documentation gaps for how to add telemetry to panels
There's some documentation in https://dxr.mozilla.org/mozilla-central/source/devtools/client/shared/telemetry.js but it is missing crucial parts such as the need to request a data review from the data team. This bug aims to fill in these gaps by documenting this on the docs folder.

We can then point people to this file each time telemetry is required for a new functionality.