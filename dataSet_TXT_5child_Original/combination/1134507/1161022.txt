The UI elements from the Reader View promo panel show alignment and position related discrepancies
Created attachment 8600883
promo panel display comparison across platforms

Reproducible on:
Nightly 40.0a1 (2015-05-03)

Affected platforms:
Windows 7 (x64), Ubuntu 14.04 (x64), Mac OS X 10.9.5

Steps to reproduce:
1. Launch Firefox with a clean profile.
2. Open a Reader View compatible page, e.g. http://www.bbc.com/news/world-asia-32574769
3. Check how content is displayed and positioned in the promo panel, e.g. the close button, the text, etc.

Expected result:
The Reader View promo panel is displayed similarly across the mentioned platforms.

Actual result:
3.a. Windows 7 (x64) - http://i.imgur.com/qLu9lNi.png - the close button has too much right padding and there's overall too much space at the top of the panel.
3.b. Ubuntu 14.04 (x64) - http://i.imgur.com/QuTC4FD.png - overall, the content from the panel should be centered better vertically.
3.c. Mac OS X 10.9.5 - http://i.imgur.com/ziqvDLc.png - the panel seems wider, with the close [x] button place differently in comparison with Windows and Linux.

Additional notes:
* I've attached a screenshot depicting how the promo panel is displayed across all three operating systems.