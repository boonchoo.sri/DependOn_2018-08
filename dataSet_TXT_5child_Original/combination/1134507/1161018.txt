The Reader View promo panel can no longer be dismissed after opening a new window
Created attachment 8600877
screenshot from Ubuntu 14.04 (x64)

Reproducible on:
Nightly 40.0a1 (2015-05-03)

Affected platforms:
Windows 7 (x64), Ubuntu 14.04 (x64), Mac OS X 10.9.5

Steps to reproduce:
1. Launch Firefox with a clean profile.
2. Open a Reader View compatible page, e.g. http://www.bbc.com/news/world-asia-32574769
3. Click the menu button (≡) and then "New Window".
4. Go back to the first window and open a new tab.

Expected result:
Opening a new window while the Reader View promo panel is displayed, dismisses it.

Actual result:
The Reader View promo panel can no longer be dismissed by: opening a new tab, a new window, a new private window after step 3.

Additional notes:
* The Reader View promo panel is also positioned randomly around the browser window after opening a new tab, see the screenshot.