Update Sidebar styles including Lion and Yosemite+ specific styles
Created attachment 554209
Lion Sidebar Style Design Spec

Update the sidebar styles in Lion to include:

- Background color gradient for Active/Inactive window
- Selected item color gradients for Active/Inactive window
- Lighter separator color
- Matching search field
- Inverted disclosure triangle icons and item icons