Setting the default browser through UITour should either no-op on pre-Win8 or should show the startup default-browser dialog
This bug is filed out of concern for silently setting the default browser through UITour APIs.

Setting the default browser on non-Windows 10 is done silently in the background, and even though it may hurt conversion rates, it would be nice to make sure there is something on the product side (outside of a webpage) that says what is about to happen.