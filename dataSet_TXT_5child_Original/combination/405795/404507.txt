Can't assign bookmark keywords from "Add Bookmark" dialog
User-Agent:       Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9b1) Gecko/2007110903 Firefox/3.0b1
Build Identifier: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9b1) Gecko/2007110903 Firefox/3.0b1

Currently the only way to assign bookmark keywords is to add a bookmark (CNTL-D), go into the bookmark manager, find the bookmark you JUST ADDED, click on 'Properties', then click the 'More' button, and finally enter your keyword.

You should be able to assign keywords directly from the quick add dialog.  It is a very small dialog already, so I can't imagine size is an issue.

I really like all the work that has been done on bookmarks in this release (Firefox 3, Beta 1) but why, oh why are bookmark keywords so shunned!  They're an awesome feature, but hardly anybody knows about them because you don't even see them unless you venture into bookmark manager.

Reproducible: Always

Steps to Reproduce:
1. Go to a website
2. Add a bookmark (CNTRL-D)
3. There is no way to assign a keyword from the dialog
Actual Results:  
Added a bookmark without any keyword

Expected Results:  
I expect the keyword field to be on the dialog

Proof of fans of this feature: http://lifehacker.com/software/bookmarks/hack-attack-firefox-and-the-art-of-keyword-bookmarking-196779.php

Also, I believe keyword autocomplete (https://bugzilla.mozilla.org/show_bug.cgi?id=212605) should be a very nice addition to fixing this bug.