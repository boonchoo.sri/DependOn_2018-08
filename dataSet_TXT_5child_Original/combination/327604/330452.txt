about:blank transfer state inconsistency
about:blank is treaded as a non-page in many places (e.g. it isn't displayed in the address bar). Because of this, it probably shouldn't have a transfer state ("Done") in the status bar either. However, it mostly does.

Reproducible: Always

Steps to Reproduce:
1. Set your homepage to about:blank ("Use Blank Page")
2. Open a new window
3. Load your homepage

Actual Results:
After step 2 the status bar is empty; after step 3 it displays "Done".

Expected Results:
There should be no difference between steps 2 and 3 (both load about:blank in the current tab).