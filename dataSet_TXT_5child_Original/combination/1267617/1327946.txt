Identity block is unaccessible: can't be focused by keyboard (a11y)
>>>   My Info:   Win7_64, Nightly 52, 32bit, ID 20161028030204 (2016-10-28)
STR_1:
1. Open attachment 8675621
2. Click in urlbar
3. Press Shift+Tab
4. Press Shift+Tab

AR:  Identity block doesn't become focused
ER:  Identity block should befome focused in Step 3 or Step 4

This is regression from bug 1267617. Regression range:
> https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=cc1d6ac8152de5394ca090ff24aa89c590ef87a4&tochange=3705eb1dc2cce4582cc3f95af52aeec700801fae@ Johann Hofmann [:johannh]:
It seems that this is a regresion caused by your change. Please have a look.