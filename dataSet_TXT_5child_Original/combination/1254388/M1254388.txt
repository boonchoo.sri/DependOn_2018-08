Apply touch simulation from selected device
Bug 1241714 adds a selected device.  We need to apply the touch simulation setting when it's selected.