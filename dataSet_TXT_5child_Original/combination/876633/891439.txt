Standardize the sheduleSearch/performSearch methods
There's an pattern occurring over and over again in which
1. I want to do something
2. But I'm depending on events that fire quickly and repeatedly
3. And I only want to do it once after things have settled down

This is mostly proeminent with search operations, but it's also been, naturally, occurring in the network monitor as well. Fortunately, there's a nice way to DRY all of this up.

This work continues the endeavor in bug 886848. Most of the code paths are already exercised by existing tests, but I'll make sure everything is accounted for in bug 876277, which I plan to work on next.