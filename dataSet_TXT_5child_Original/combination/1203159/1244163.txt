DevTools inaccessible to XUL apps using "firefox -app"
In bug 912121 and bug 1203159, we moved DevTools files in several ways.

By the end of bug 1203159, all DevTools files (client and server) end up inside the "dist/browser" directory, meaning they are specific to the browser app that Firefox runs.

The reason that both client and server ended up together is we wanted to have a single directory structure to map "resource://devtools" to.

However, this presents (at least) 2 problems for XUL apps using "firefox -app" to run something other than the browser:

1. You can no longer start a DevTools server from your app, since the files are hidden inside browser
2. The add-on manager fails because it tries to Cu.isModuleLoaded a DevTools file it can't access