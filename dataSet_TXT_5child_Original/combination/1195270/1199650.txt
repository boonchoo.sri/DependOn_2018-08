English copy review of the "Forgotten" and "Kept" headers of about:privatebrowsing in Firefox 42
In Firefox 43, we already revised the "Forgotten" and "Kept" headers of about:privatebrowsing to say "Not Saved" and "Saved" instead, in bug 1199168. This is the result of a detailed English copy review.

For the Firefox 42 launch, we would like to revise the "Forgotten" and "Kept" headers for English as well, and say "Not Saved" and "Saved". There is not a substantial difference in meaning, in fact some locales have already translated these words with the equivalent of "Not Saved" and "Saved".

Given that we accept that some locales may have translated this already as something more similar to "Forgotten" and "Kept", we would like to change only the English words on the Developer Edition channel, but keep the entity name in order not to break string freeze, similarly to what we would do with a spelling error. This is because we don't want to create a substantial amount of additional work for localizers.

We're also removing the lead sentence ("This window won't remember any history") from the XHTML file on the Developer Edition channel in any case (but obviously we won't remove the associated entity).