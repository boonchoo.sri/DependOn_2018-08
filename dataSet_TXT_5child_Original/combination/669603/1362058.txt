Further limit the amount of sessionStorage data we store and serialize
We have a pref called browser.sessionstore.dom_storage_limit that currently defaults to 10M. For localStorage however the default is 5M, so that doesn't seem to make a lot of sense. And this limit doesn't apply to partial sessionStorage updates at all, which probably completely bypasses the limit.

We should set a small limit, let's say 2kB, and shouldn't store/serialize DOMSessionStorage data beyond that. This will help with Google search pages spamming the file.

I can't imagine there being a lot of value for huge sessionStorage entries being persisted to disk. If a game stores textures they'll simply fetch them again, and search pages can preload next time too.

This should severely reduce I/O and make sessionstore.js a lot smaller. Also we will spend less time serializing hundreds of kilobytes of data.