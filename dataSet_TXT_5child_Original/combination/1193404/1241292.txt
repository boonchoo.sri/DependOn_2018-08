Revisit "Your login could be compromised" string for Insecure Password Warning
When you visit an HTTP page that has a password field, Nightly (and soon dev edition) will show a degraded UI (lock with strikethrough).  When you click on that to open the Control Center, the message says:
"Your login could be compromised".

Matt proposes that we change that string (either in general or just for dev edition) to something that is aimed more at developers.  When we are ready to move the feature to the release channel, we can switch to a different string for general users.  A couple ideas:
"The login page could be compromised"
"This login page could be compromised" (although this might be a general purpose page that happens to also have a login form)
"Login pages should be served over HTTP"
"Passwords/(Credentials) should not be collected over HTTP"
"Passwords/(Credentials) should be collected over HTTPS"

It depends on who our audience is.  We could even have two separate strings - one for dev edition and one for everything else.

Matej, what do you think?