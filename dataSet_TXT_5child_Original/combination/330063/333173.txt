Rise in number on allocs on balsa by 15%
The number of allocations on balsa went from a 206K to 239K last night between 4 and 6pm. The checkins in range are

http://tinderbox.mozilla.org/bonsai/cvsquery.cgi?treeid=default&module=PhoenixTinderbox&branch=HEAD&branchtype=match&dir=&file=&filetype=match&who=&whotype=match&sortby=Date&hours=2&date=explicit&mindate=1144362540&maxdate=1144372319&cvsroot=%2Fcvsroot

possibly except for mrbkap that checked in before the 206K run.

The increase can easily be seen on this graph:

http://build-graphs.mozilla.org/graph/query.cgi?testname=trace_malloc_allocs&units=count&tbox=balsa&autoscale=1&days=7&avg=1&showpoint=2006:04:06:17:58:38,244827

(you may have to adjust the number of days displayed since the graphtool is crappy).