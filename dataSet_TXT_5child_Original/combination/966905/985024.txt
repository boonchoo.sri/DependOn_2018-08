Show all bookmarks should not scroll with the bookmarks menu contents
The issue here is that users think we removed:
- the Library
- the backups
- the possibility to export html
- undo / redo
cause now Show all bookmarks is at the bottom of the Bookmarks panel, and on commonly long menus, it's out of the view. These users are unlikely to know about an exotic arrowscrollbox.

We need to change the places popup implementation to have static content out of the scrollable part, and likely stop using arrowscrollbox in favor of a normal box with a scrollbar.