DevTools Themes: Update network panel table headers to match new theme
Based on this mockup: https://people.mozilla.org/~shorlander/mockups/devTools/ux-refresh-2013/DarkTheme-Network@2x.png.

Let's focus on the headers here to free up some vertical space and match the design - there are some design elements in common with the sidebar tabs in Bug 929127.