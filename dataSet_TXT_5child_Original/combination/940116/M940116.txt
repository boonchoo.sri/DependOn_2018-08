Australis application menu buttons have inconsistent tooltips
The buttons in the Australis application menu have these tooltips:

Cut: "Cut"
Copy: "Copy"
Paste: "Paste"
-: "Zoom out"
100%: "Reset zoom level"
+: "Zoom in"
New Window: "Open a new window"
New Private Window: "Open a new Private Browsing window (⌘⇧P)"
Save Page: "Save this page (⌘s)"
Print: "Print this page"
History: "History... (⌘⇧h)"
Full Screen: "Display the window in full screen"
Find: "Find in this page (⌘f)"
Preferences: "Preferences..."
Add-ons: "Add-ons Manager (⌘⇧A)"
Help: -
Customize: -
Quit: -

There are a few inconsistencies in the tooltip text:

* Some shortcuts are shown with lowercase letters, and some with uppercase.
  They should all be uppercase.
* Some shortcuts are shown and others are not.  All of these buttons have
  shortcuts (apart from History and Help, which slide menus over, and Customize)
  so I think they should be shown.
* Most of the tooltips are imperative phrases, like "Do this thing".  The ones
  for History, Preferences and Add-ons aren't, but should be.  Although with
  History I'm not sure, due to its menu sliding behaviour.
* The use of ellipses is not consistent with the menu bar.  For example,
  the menu item is "Save this page..." while here it is "Save this page".  A
  modal window is opened, so I think "..." should be used.  Same with "Print this
  page" (although note that in the menu bar it is "Print..." and not "Print this
  page...".  I don't think History should have an ellipsis, as it doesn't open
  a modal window.