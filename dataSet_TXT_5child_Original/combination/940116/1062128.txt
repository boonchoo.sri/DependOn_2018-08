Open new tab button (and potentially various other buttons) has (have) no accessible name
Created attachment 8483316
1.png

User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0
Build ID: 20140716183446

Steps to reproduce:

win7

Tried to find the MSAA Name for Open new Tab button.


Actual results:

The name is empty.
Screen attached.


Expected results:

Name should be not empty.
Like it was in previous versions.