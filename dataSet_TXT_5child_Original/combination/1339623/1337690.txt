Use an icon for MDN Links
Created attachment 8834794
mdn-httpheaders.png

The Network panel introduced links to MDN for HTTP headers in bug 1320233. These links are rendered in 'Headers' side panel (within a side bar displayed on the right side).

The label for every link is: "[Learn More]"  (no quotes)

See the attached screenshot

The label takes quite a lot of space, which is especially valuable in the side bar and we might want to replace it by an icon. An icon can also make the UI more attractive.

Two notes:
1) The icon should be carefully designed since icons tend to be cryptic. I am thinking about a question mark, some existing MDN icons or combination of both.
2) There should be a tooltip for the icon saying something like "Learn more about this header on MDN"
3) MDN link is also displayed for JS errors in the Console panel. These should be replaced too so, the UI is consistent.
4) There is also bug 1323454 that should introduce MDN link for HTTP status code. It isn't landed yet, but should also use an icon if we agree on it (again to have consistent UI)

Honza