ruleview mistakenly reads valid rules as invalid and vice versa if editing was canceled via Escape key
>>>   My Info:   Win7_64, Nightly 49, 32bit, ID 20160526082509
STR_1:
1. Open url   data:text/html,<body style="background:transparent"><style>body{height:22px}
2. Open devtools -> inspector -> rules
3. Click on text "22px" in ruleview
4. Press Delete
5. Press Escape

AR:  Yellow rectangle appears near the rule
ER:  No yellow rectangles


STR_2:
1. Open url   data:text/html,<body style="background:transparent"><style>body{height:22pzx}
2. Open devtools -> inspector -> rules
3. Click on text "22pzx" in ruleview
4. Select letter "z" in "22pzx", press Delete
5. Press Escape

AR:  No yellow triangle
ER:  Yellow triangle should appear near the rule


This is regression from bug 1069829. Regression range:
> https://hg.mozilla.org/integration/fx-team/pushloghtml?fromchange=8b4e5ab50377a26e6610f21a0f8233467de19e9b&tochange=eb721f4d509f8826886fea9716ce06903bd3af0d@ Patrick Brosset <:pbro> (PTO until Jan. 3rd):
It seems that this is a regresion caused by your change. Please have a look.