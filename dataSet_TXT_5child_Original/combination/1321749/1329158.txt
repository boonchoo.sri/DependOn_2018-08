Optimize maintaining scroll position in RequestListContent
Profiling the React/Redux implemenation of Netmonitor UI with Cleopatra tells me that a lot of time is spent in RequestListContent.componentDidUpdate setting the node.scrollTop property. This scrolls the list to the bottom, and it's an expensive operation that triggers reflow.

My testing shows that it's performed twice as often than necessary. In approx half of the cases, the request list length didn't change, so updating the scroll position is not needed.