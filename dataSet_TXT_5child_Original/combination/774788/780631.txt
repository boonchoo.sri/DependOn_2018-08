Don't change the selected script while filtering if the file part from the search expression hasn't changed
If there are two very similarly named files, for example:

foo/bar/baz.js
baz.js

...then when trying a filter like baz.js#token, for each letter entered in the #token, the selected script switches to `foo/bar/todos.js` if the previously selected script was `todos.js`.

Thus, the script always switches to the first found mach. This annoyed me today.