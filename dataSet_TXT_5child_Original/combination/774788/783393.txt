Breakpoints not getting caught on reload
STR:

1. Open http://htmlpad.org/debug-startup-test/
2. click through the alerts
3. Open the Debugger
4. Set a breakpoint on line 5.
5. Press Reload

Expected Results: Page should reload and the breakpoint should hit.

Actual: Breakpoint is not hit and the alerts are presented.

Affects Nightly, Aurora (16) and Beta (15).