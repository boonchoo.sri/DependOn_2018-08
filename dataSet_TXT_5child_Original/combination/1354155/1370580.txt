Fix layout issues in Library Panel subviews
When selecting a subview in the Library Panel, there are sizing and scrolling behavior issues.
It'd be best to fix them all here.