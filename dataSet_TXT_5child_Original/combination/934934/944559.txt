[Session Restore] Just say no to sessionstore.js spam
Here's another idea: add (pref-controlled) limits after which we simply decide that we should not store anything meaningful about a site.

e.g.
- if DOM storage + postdata + formdata exceeds 1Mb; or
- if the tab has more than 100 frames;
- etc.

In this case, just give up about that tab and store only a set of URLs.