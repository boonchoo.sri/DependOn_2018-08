Focused Firefox windows (and another applications windows) become invisible when notifications from site identity block appear
User Agent 	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0

I have a problem with Firefox Beta 53. It also happens in Nightly 55. Doesn't happen in Beta 52, ESR 45.
Sometimes when creating / switching to another windows, it stays invisible, if site identity block appears in Firefox window.
Here's a ways to reproduce the bug:

1. Open https://permission.site/ in the last tab, click button "Location" on the page
2. Type anything in location bar
3. Press Ctrl+N (or Alt+Tab to switch to another application)

Result: target window is focused. Old window isn't focused, but it appears ABOVE the new window, making it invisible.
Expected: target window should be visible.

Probably related bugs: Bug 1349453, Bug 1349436.