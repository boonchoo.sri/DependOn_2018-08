Urlbar loses focus when typing after clicking on the icon of a permission doorhanger
No idea how I discovered this edge case:

STR:

1. Open http://permission.site/
2. Click on a button to get a permission notification
3. Click on the doorhanger anchor icon
4. Type something in the urlbar

I can only type a single character before it loses focus.