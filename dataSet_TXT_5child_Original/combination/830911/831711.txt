[meta] Papercuts to the Toolbox
We brainstormed a bunch of the fixes that we need to make to the toolbox and our tools in general here: https://etherpad.mozilla.org/devtools-fixes

This meta-bug tracks what we're actually fixing for Firefox 22.