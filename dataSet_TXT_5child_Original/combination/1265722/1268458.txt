Decouple fronts from highlighter actors.
The inspector actor depends on the highlighter actors, so the latter have to be decoupled before the former.