Official Ubuntu package of Firefox doesn't enforce add on signature
Reported here:
https://linuxfr.org/news/firefox-49-en-chansons#comment-1674604
in French

The user say that he/she can install unsigned addons using the Ubuntu packages.
Instead, this should not be possible. Ubuntu should use the solution implemented in bug 1255590 instead.