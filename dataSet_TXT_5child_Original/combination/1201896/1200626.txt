[UX] Provide final assets and spec for permissions notification design
Deliverables should include:
-single notification design
-multiple notification design
-notification behavior spec (click targets, when tab is closed, etc)
-notification designs for: 
--Web notifications
--Set cookies
--Load images (?)
--Pop-up windows
--Install Add-ons  
--Plugin for ex. Adobe Flash
--Push notifications
--Full screen mode
--Location
--Camera/mic
--Microphone
--Hide Mouse pointer
--Store data on your computer