The 'Translate' button's border looks blurry on Mac
Created attachment 8444435
Screenshot

The border of the 'Translate' button isn't satisfying. I remember Sevaan said on IRC it wasn't looking as designed, but I don't remember if/where we have a clear specification of how it should look. Sevaan, do you have the specification of how that border should be?