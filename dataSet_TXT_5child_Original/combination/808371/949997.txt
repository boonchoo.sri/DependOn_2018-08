Empty values cannot be edited later in the Manifest Editor
Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0
Mozilla/5.0 (X11; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0
Reproduced on latest Aurora (20131212004008).

Steps to reproduce:
1. Open App Manager
2. Add a packaged app 
3. On the Manifest Editor area, select the "+" button to add a new value for a property - a field to enter it's name is open
4. Click outside the text field or select anything outside Firefox window
5. Return to the value to complete it

Actual results:
If a value is not supplied the first time, it cannot be edited later. ":" remains on the screen and it can only be deleted.

Expected results:
The empty fields can be edited later.