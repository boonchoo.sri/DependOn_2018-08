VariablesView `Editable#_onCleanup` is not called when it should be
Regression after bug 808371.

STR:
1. Open debugger on http://htmlpad.org/debugger/
2. Add a watch expression (ex: "window")
3. Double click the expression in the variables view
4. Press RETURN

The value hides.
https://bugzilla.mozilla.org/show_bug.cgi?id=808371#c46