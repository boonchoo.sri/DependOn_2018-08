[app manager] toolbox in a new tab
The toolbox should be opened in a new tab. That means loading toolbox.xul in a tabbrowser. We'd need to get rid of the `iframe.onload` calls.

Orion won't work though.