Setup build configuration for Firefox Screenshots in Firefox
Pageshot is preparing to be part of Firefox as a system add-on.

This bug is for the patches that will hook it up to part of the build system.

We aren't ready to land this yet, we're using this as a placeholder for the time being to store the necessary patches.