Bookmarks outside the three default folders aren't restored
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9b3) Gecko/2008020514 Firefox/3.0b3
Build Identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9b3) Gecko/2008020514 Firefox/3.0b3

Bookmarks that are stored outside the "Bookmarks Toolbar", "Bookmarks Menu" and "Unfiled Bookmarks" aren't backuped, leading to dataloss (example: http://forums.mozillazine.org/viewtopic.php?t=627986 )


Reproducible: Always

Steps to Reproduce:
1.Create a bookmark called "XXXXXXXXXXXXXXX"
2.Move it, via the Library, at the top level (i.e. outside the toolbar, the menu, the unfiled bookmarks)
3.Do "Import and Backup" -> Backup
Actual Results:  
No "XXXXXXXXXXXXXXX" bookmark in the generated file

Expected Results:  
Either the bookmarks in the generated file, either the inability to put a bookmark there.

This may lead to dataloss if another problem happen. It already happened to some in beta2 -> beta3.

It is similar, though not identical (and thus not a dup) of bug 405936. A common fix may be possible.