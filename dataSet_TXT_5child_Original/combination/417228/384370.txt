use JSON as the on disk, lossless format for our bookmark backup
use JSON as the on disk, lossless format for our bookmark backup

As we all know, using bookmarks.html is not going to cut it for a lossless format of our places data.  

(Specifically, I'm thinking about bookmarks data.)

What about JSON as our on disk format?   We write out bookmarks.json, and on schema change, we can re-import that format.  (XML could be our format as well.)

Sayre (who is always ahead of the curve), is using JSON for sync:  See https://bugzilla.mozilla.org/show_bug.cgi?id=379517

And for JavaScript 1.8, it sounds like toJSONString, ParseJSONString might be coming, see https://bugzilla.mozilla.org/show_bug.cgi?id=340987

More thoughts:

1) In Firefox 2, there was a pref to specify where to read/write the bookmarks.html file on disk.  I think we'd need a new pref for where to read / write the .json format?

2) There's a RFE bug logged importing bookmarks from a URL (see https://bugzilla.mozilla.org/show_bug.cgi?id=382933).  

But imagine:  "Import / Export Bookmarks from a Web Service", using JSONRequest() (see https://bugzilla.mozilla.org/show_bug.cgi?id=360666) or an XMLHttpRequest.