UITour won't highlight pocket correctly anymore now that it's moved to the page action menu
https://dxr.mozilla.org/mozilla-central/rev/52285ea5e54c73d3ed824544cef2ee3f195f05e6/browser/components/uitour/UITour.jsm#189-193

We should make this return the url bar button if present, and the page action item otherwise.