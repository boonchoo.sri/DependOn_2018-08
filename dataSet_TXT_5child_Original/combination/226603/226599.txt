Need Window icons for Microsoft Windows
Mozilla and Netscape 7 have window icons for things like bookmarks, etc. We need
them for Firebird for the following windows:

Browser
Bookmarks
Downloads
"Security UI" (generic icon for all Security windows)
Info windows (something with an "i" in it probably)
JavaScript console
Help
DOM Inspector
Venkman

some of these (DOMI, Venkman) can simply be pillaged from seamonkey. The others
will have to be done specifically for Firebird.