Attach combined Stop/Go/Refresh button to the Location Bar
Created attachment 425748
[Mockup] Combo Stop/Go/Reload

For the new Firefox theme/UI we need a combined Stop/Go/Refresh button that is attached to the end of the Location Bar.
			
* If a page can be refreshed it appears as a glyph on the location bar background
* If it can't be refreshed the glyph is greyed out
* The refresh state turns blue on hover
* If you enter the location bar and start typing it will turn green with a go arrow
* Turns into a red "x" stop button when the page load can be stopped
* Slight gap between when a page is finished loading and when you can refresh again (like the behavior currently in the 3.7 nighties)

It should be removable via the Customization dialog. Ideally it would separate itself visually from the location bar in this mode.

See attachment for mockup.