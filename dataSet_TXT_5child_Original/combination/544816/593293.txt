pixel pushing and polishing for the new combined location bar
time to nudge those darn little dots into their correct locations for the final release.

a couple things that I'm already aware of:

1. from bug 544816 comment #60:
https://bug544816.bugzilla.mozilla.org/attachment.cgi?id=471762

2. at least on OS X, we'll need to add width: 30px or something like that to adjust the width of the combined stop/go/refresh button.

shorlander, anything else you see in need of some visual polish?