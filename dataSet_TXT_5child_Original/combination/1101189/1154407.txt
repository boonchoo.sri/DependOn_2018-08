Ghostery's options or tutorial do not go well with e10s
Running the latest nightly on Ubuntu.

STR:
* Install the Ghostery addon
* A wizard page should open after install

Expected results:
* I can see the wizard

Actual results:
* I cannot see the wizard

NOTE: This only happens for e10s

URL that opens up:
resource://firefox-at-ghostery-dot-com/ghostery/data/walkthrough.html

Ghostery addon:
https://addons.mozilla.org/en-US/firefox/addon/ghostery/?src=search