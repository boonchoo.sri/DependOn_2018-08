Auto-upgrade doesn't bring the new start menu tile on Windows 10
It seems to me auto-upgraded version doesn't automatically bring the new start menu tile implemented in bug 1232679 (and that's why I didn't realize that we have that until I saw the email about intern presentation of ktbee today).

Steps to reproduce:
1. Install an old version of Firefox Developer Edition. You can download Firefox 49 Developer Edition from [1] (if you have a newer one, you probably need to uninstall it first)
2. Pin Firefox Developer Edition to the start menu
3. Open the "About Firefox" dialog, and let it upgrade and restart
4. Open start menu to check the tile

Expected result:
The tile updates to the new one

Actual result:
The tile is still in its old appearance. It doesn't change even if you unpin and re-pin to the start menu. Given that I only realize this change today, but Firefox 50 Developer Edition has been released for a while, I believe restart the system doesn't make any difference either.

However, if I reinstall the latest Firefox Developer Edition via the installer (on top of the upgraded version), the tile does change.

FWIW, Chrome's auto-upgrade does update the tile, and I've noticed that many times. [2]


[1] https://ftp.mozilla.org/pub/firefox/nightly/2016/08/2016-08-01-00-40-02-mozilla-aurora/
[2] https://twitter.com/upsuper/status/666759939705040897