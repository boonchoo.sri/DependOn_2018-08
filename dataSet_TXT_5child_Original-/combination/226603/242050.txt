Need Window icons for Linux/Unix
This is the Linux/Unix version of Bug 226599.  However for Linux/Unix, there
needs to be the 48x48 version of each file, as well as the 16x16 version, such as:

JSConsoleWindow.xpm
JSConsoleWindow16.xpm

Also, that is the correct case of the JS Console for Firefox.  It does not match
the case of the JS Console on Seamonkey (jsconsoleWindow.xpm)

---------- In Bug 226599 Ben Goodger Said ----------
Mozilla and Netscape 7 have window icons for things like bookmarks, etc. We need
them for Firebird for the following windows:

Browser
Bookmarks
Downloads
"Security UI" (generic icon for all Security windows)
Info windows (something with an "i" in it probably)
JavaScript console
Help
DOM Inspector
Venkman

Some of these (DOMI, Venkman) can simply be pillaged from seamonkey. The others
will have to be done specifically for Firebird.