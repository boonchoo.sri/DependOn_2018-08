Warning message is not displayed when opening multiple tabs through Library.
User-Agent:       Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9b5pre) Gecko/2008032005 Minefield/3.0b5pre
Build Identifier: Build identifier: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9b5pre) Gecko/2008032005 Minefield/3.0b5pre

Preconditions: 1)user profile should have a lot of favorites. 
               2)'Warn me when opening multiple tabs' option should be checked. 




 




Reproducible: Always

Steps to Reproduce:
Steps to reproduce:
1. Start FF.
2. Select "Bookmarks->Organize Bookmarks" option from main menu.
-> 'Library' window is displayed.
3. Highlight 'Bookmarks Menu' in the left section of the window.
4. And select all favorites displayed in right section (by holding Ctrl; or pressing Ctrl+A)
5. Right click on the highlighted favorites and select "Open All in Tabs"
option.
Actual Results:  
All favorites are opened in separated tabs without any warning
message.

Expected Results:  
warning message should be displayed since 'Warn me when
opening multiple tabs' option is checked.