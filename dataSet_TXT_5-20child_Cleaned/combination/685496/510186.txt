Change "Warn me when web sites try to redirect or reload the page" checkbox label for accessibility.blockautorefresh to non-misleading text: it does not apply to HTTP Location header (301, 



The label for the checkbox to control accessibility.blockautorefresh in Preferences -> Advanced -> General, Accessibility is "Warn me when web sites try to redirect or reload the page":

#: blockAutoRefresh.label
#: blockAutoRefresh.accesskey
msgid "Warn me when we&b sites try to redirect or reload the page"

This is not exactly what this preference does. The preference seems properly documented on 
This preference only warns when a page redirects to another one via an HTML META element. It takes no effect when being redirected by an actual HTTP header such as  and 

So the bug seems to be in the wording of the label. The behavior seems correct, although the documentation page does discuss HTTP Refresh header.




No real step, but you can verify that accessibility.blockautorefresh does not block HTTP  by activating the option and going to 

Quiet redirection to 


Prompt for confirmation before redirecting
