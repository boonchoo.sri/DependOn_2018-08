[a11y] Autocomplete devtools suggestions selected by keyboard are not read
[Note]:
- This issue is happening under Windows with NVDA application.

[Affected versions]:
-  (2016-08-10

[Affected platforms]:
- Win  

[Steps to reproduce]:
Open the Developer Tools on a website.
On Console tab and on Inspector tab inside "Search HTML" field, start typing an element for which suggestions will be shown (eg "do")
Select one of the suggestions by pressing:
a. Tab key
b.  arrow keys followed by Enter
c.  arrow keys followed by Tab

[Expected result]:
- The selected items are read by NVDA.

[Actual result]:
- NVDA doesn't say what the selected item is.

[Regression range]:
- mozilla-central: 
- mozilla-inboud: 
Regressed by bug 

[Additional notes]:
- The issue doesn't reproduce when the items are selected using mouse right click.
