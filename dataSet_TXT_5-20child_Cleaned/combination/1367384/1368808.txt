Respect Windows  Dark  Light color settings
Windows  current Nightly

After bug  the Firefox titlebar looks wrong and alien compared to every other Windows application. All other apps use light grey or white, and Firefox is dark 

After some fiddling in Windows, I found Settings->Color->More Options->Choose Default App Mode

This is currently set to "Light". I think this is the default - I don't remember changing it.

When I set this to "Dark", Windows' own windows and other software such as Edge uses a dark titlebar, close but not exactly the same as Firefox. It looks like this setting is what the Firefox design was intended to match.

But when it is set to "Light", Firefox is, incorrectly, Dark.
