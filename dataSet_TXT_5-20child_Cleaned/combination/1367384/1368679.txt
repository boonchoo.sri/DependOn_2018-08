Menu bar and window control button hover feedback lacks contrast on dark titlebar
Created attachment 
bug.png

Almost I can not recognize the hover effect.

Steps To Reproduce:
Enable Menubar
Mouse hover over menu or keypress alt key

Actual Results:
Almost I can not recognize active menu.


More lighter background color.


PS. I think hover active color of caption buttons are also less contrast
