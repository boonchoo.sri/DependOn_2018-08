Figure out a touch-friendly UX for  fullscreen mode
This is a spinoff from  When a user is in fullscreen mode, they can move the mouse to the top of the screen to temporarily make the URL  visible. Moving the mouse back down makes it hidden again. This works fine if you have a mouse, but on touch-enabled Windows tablets for example there might not be a mouse.

Instead, we should provide some touch-equivalent for doing this, and for exiting fullscreen mode entirely. Right now the user can do a tap or (once bug  lands) a swipe at the top edge of the screen to show the toolbar, but dismissing it again (the way you can by moving the mouse down) is not obvious.

Also, I've only tested this behaviour on a Surface Pro and it may not work uniformly across all Windows touch devices, particularly ones with narrower or missing touch-sensitive bezels.

We should spend some time figuring out a good UX for this and make the necessary changes.
