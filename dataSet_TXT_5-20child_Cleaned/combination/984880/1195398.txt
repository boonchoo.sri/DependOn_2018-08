rule-view property editor should reject invalid values
See bug  comment 

With naive as-authored editing, it is possible to get the style sheet
into a bad state if one enters invalid property values in the rule view.
The rule view property editor should reject invalid edits and notify
the user.
