Keyframe syntax is showing up in rule view as part of a rule
Created attachment 
keyframe-ruleview.png

Open 
Inspect the #creature_orange-roughy1 element
Navigate rule view to gentlyHovering

{
☑  { -webkit-transform: translate(-50%, 
☑ transform: translate(-50%, 
}

I'm not expecting to see the '100% {' part on the second line (see screenshot).

This appears to be coming from CSS like:

@keyframes gentlyHovering {
{
-webkit-transform: translate(-50%, 
transform: translate(-50%, 
} 
}
}
