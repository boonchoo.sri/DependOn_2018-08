Consolidate typography and sizing of panel subview items
Created attachment 
Explanation

We currently use different font sizes on the menu panel, the bookmarks panel, submenus of the bookmarks panel, the history panel and other panel subviews.

All panels should have the same font size
All panels should have the same spacing between list items
The main view of the menu panel is the exception, because it follows a different paradigm (icon grid)

Shorlanders mockup shows the intended font size,  and styling for Windows. Specs for Mac and XP are on the way, but I believe they will be either identical or very close, at least as far as typography is concerned.

