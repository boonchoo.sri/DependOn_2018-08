Move findbar to the top
Move the find toolbar to be displayed on the top of the page instead of the bottom, which is its current position.

Also, the find toolbar should move to the top of the view-source page(s), for the sake of consistency.
