Correct line isn't jumped to when clicking the line for a pretty printed source
After I pretty print a source, I'd like to clicking on the line number to jump to the correct line number in the debugger.

STR:

Open 
Start profiling
Stop profiling
Click on "JavaScript" tab
Click on one of the jquery-2.1.3.min.js lines (takes you to debugger panel)
Stop blackboxing the source
Pretty print source

At this point the cursor should move to the correct line. At the least, if I flip back to the performance tool and click on the same line number again it should open to the correct place.
