Firefox sync has two images for same purpose
From bug 
There are now two images inside FF representing almost the same thing.
sync-illustration in about:preferences and graphic_sync_intro.png in about:accounts.

I would propose to select one, and use the same for both purposes for consistency.

Whilst bug  is about moving away from "about:accounts", I expect the "about:accounts" URL will probably stay, and will need to be more in line with about:preferences#sync. Will Firefox Accounts be more than Firefox Sync, or will it be one and the same? This is all confusing for end-users...
