properly message user when FxA sign in cannot be loaded from the Web (e.g., offline, captive portal, etc.)
Desktop Firefox loads firefox accounts sign in form via https. If it's unreachable (offline or service down) a page or message should inform the user.
