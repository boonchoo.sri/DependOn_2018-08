firefox main button COMPLETELY gone, menu bar disabled and tabs on top on.
User Agent:  (Windows NT  WOW64; rv:23.0)  
Build ID: 

Steps to reproduce:

opened UX  after updating to latest version.


Actual results:

the  button was GONE. completely GONE. i can't even find an about:config entry, and I can't restore it with stylish, because it IS NOT THERE IN THE  XUL PAGE! it s not display none, it is GONE. I have to RESORT to enabling the menu bar, taking up precious screen estate, and being harder to navigate. MAJOR usability downgrade.


Expected results:

the  button should have been there, and I should not have to resort to enabling the menu bar.
