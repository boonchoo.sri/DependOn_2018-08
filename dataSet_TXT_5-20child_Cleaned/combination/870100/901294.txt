Unexpected downloading will be started by after every restart of the browser


(Windows NT  WOW64; rv:25.0)   ID:20130723004349


Steps To Reproduce:
Create New Profile and Start Nightly with the profile
Open  and wait until completion of the drawing of the page

Exit Nightly(Alt >File > Exit) and Restart Nightly
Wait  sec
--- observe download toolbar button
Repeat step  if you want

Actual Results:
Unanticipated downloading will be started


Unexpected downloading should not be started

Regression window(m-i)
Good:

(Windows NT  WOW64; rv:25.0)   ID:20130723002346
Bad:

(Windows NT  WOW64; rv:25.0)   ID:20130723004349
Pushlog:


Regressed by: Bug 
