Disable background thumbnails on Beta
Created attachment 
preprocess out the front-end caller

The background thumbnail service isn't ready for prime time, mainly because we're still tracking down e10s-related crashes it causes (bug  bug  and also because it's just rough around the edges (see dependency trees of bug  and bug  We'd like to disable it once it makes its way to Beta.

This patch uses the text preprocessor to preprocess out the only front-end caller of the bg service so far, in sites.js, in the release, beta, and esr channels. Since this doesn't disable the service, add-ons and Firefox hackers can call still call it if they know about it, and I think that's OK. I looked around and there seem to be many release channels: esr, release, beta, aurora, nightly, nightly-elm, nightly-profiling, nightly-oak, default. So checking for the first three seemed like the easiest thing to do. Once the service is ready, we can revert this patch and let that ride the trains.
