Use BackgroundPageThumbs to capture user top sites that we don't currently capture
We should determine when we want to use BackgroundPageThumbs and then do it. There's the larger question, When do we want to capture thumbnails? And a specific question with that larger one, When do we want to use BackgroundPageThumbs vs. PageThumbs?
