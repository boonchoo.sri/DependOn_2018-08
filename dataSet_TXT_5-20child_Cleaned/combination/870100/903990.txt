Add the possibility to use BackgroundPageThumbs for all new tab thumbnails
Created attachment 
Google thumbnail showing private information

Currently, private information entered by the user can be leaked through thumbnails on the new tab page. For example, the attached screenshot is an image of my Google thumbnail, which was created after I already started entering a search term. In the past, I circumvented this by opening these pages another time, waiting a few seconds and closing them. This is quite annoying. Now there is a service which loads the pages in the background, there should at least be an option to use it as a default for all thumbnails.
