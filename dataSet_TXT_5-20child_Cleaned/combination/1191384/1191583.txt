Add telemetry probes for touch usage
Over in bug  we're adding probes for Windows  Tablet Mode usage. I think it would also be useful to have separate, simple probes for (1) does the user's display support touch and (2) has the user actually touched their display.

We don't really know just how prevalent touch hardware is, or how much users are making use of it. It also might help provide an effective upper-bound on _potential_ Tablet Mode usage.
