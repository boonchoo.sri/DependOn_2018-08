Don't use menuPanel-small.svg for #add-share-provider
The icon currently used is for the zoom controls in the application menu. It's not supposed to be used on some other random element in a completely different context.

Please use your own icon (can be a copy of menuPanel-small.png if that's what you want).

I don't know who maintains SocialAPI these days ... mixedpuppy, can you maybe take this since it seems you added this code?
