Eval textbox is revealed to the user if Page Style is set to "No Style"
Created attachment 
Patch

A hidden textbox with (presumably) scripts to evaluate is hidden on the page. But it is hidden with a CSS "display: none" rule. Therefore, if I turn off CSS styles I can see and freely edit the contents of the textbox. Uh oh.

I don't think there's any difference functionally between a HTML input and a XUL textbox.
