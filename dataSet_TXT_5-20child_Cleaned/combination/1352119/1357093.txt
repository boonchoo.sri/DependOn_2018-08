Find out why the GPU-accelerated tab throbbers caused Talos regressions
In bug  an SVG image was landed for the new loading  connecting throbbers in tabs. We used GPU-accelerated rotation transforms to ensure that these throbbers animated at  as much as possible.

When it landed, one of the things we noticed was a Talos regression - that was filed as bug  Specifically, the following Talos changes were noticed:

Regressions:

kraken summary osx-10-10 opt  -> 
tsvgx summary osx-10-10 opt  -> 
damp summary linux64 opt  -> 
tp5o summary osx-10-10 opt  -> 
damp summary linux64 pgo  -> 
tsvgr_opacity summary osx-10-10 opt  -> 
tp5o responsiveness linux64 opt  -> 
tp5o responsiveness linux64 pgo  -> 
tsvgx summary linux64 opt  -> 
tp5o summary linux64 opt  -> 
tp5o summary linux64 pgo  -> 
tsvgx summary linux64 pgo  -> 

Improvements:

tart summary osx-10-10 opt  -> 
tart summary linux64 opt e10s  -> 
tart summary linux64 opt  -> 
tart summary linux64 pgo e10s  -> 
tart summary linux64 pgo  -> 


Similarly, higher CPU usage was noticed with the new throbbers. Bug  was filed for that, and a patch was landed that took a chunk of the CPU usage regression away, but not all.

Bug  was backed out because of the above reasons, meaning that we're now using the animated PNGs again, which means that these animations can jank when the main thread gets blocked. 

Part of the rationale for backing out the new throbbers is because Photon is going to replace the throbbers with a new design anyways (bug  The animations in bug  (and in Photon in general) are all going to be GPU accelerated, so we should take this opportunity to at least understand (and hopefully fix) the reason why the throbbers caused the regression. We should do that soon, so that we can be prepared when the new animations land.
