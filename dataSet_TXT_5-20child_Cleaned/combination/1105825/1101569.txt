Add an "all items" menu to the sidebar in cases of overflow
STR:

- Open the devtools
- Switch to either the inspector or the netmonitor
- Resize the sidebar panel to be as small as possible

==> Not all sidebar tabs are visible, and there is no way to access the hidden items.
