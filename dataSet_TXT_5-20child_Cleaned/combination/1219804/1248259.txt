Bookmarks menu flickers when you open a subfolder
[Tracking Requested - why for this release]: Visual glitch



Steps To Reproduce:
Enable MenuBar
Click "Bookmarks" in MenuBar
Click(or mouse hover) subfolders i.e, "Bookmarks Toolbar", "Recent Tags", "Mozilla Firefox" etc... and repeat Step3


Actual Results:
Pull down flickers


Should not flicker


Regression window:


Regressed by:
Dão Gottwald — Bug  - Show last  recent bookmarks in the bookmarks menu. r=mak
