Remove the about:customizing preloading hack
As part of our effort to make the transition to customize smoother, we preload about:customizing[1] so that the cost of instantiating a docshell  browser doesn't jankify the transition.

We preload about:customizing when the user opens the menu panel.

That's a pretty bodacious hack, and means that we've got a docshell  browser just sitting in the background for the most part doing nothing.

We should try to remove this hack to help conserve memory, and to generally simplify things.

[1]: See bug 
