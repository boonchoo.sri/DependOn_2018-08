Implement core reconciliation logic for formautofill engine
We are working on a sync engine for formautofill, which requires  logic that's not supported by Sync today.

This bug is to implement simple reconciliation logic which roughly matches the logic required by formautofill, and to tweak sync so that this new model is supported. The bug for the *actual* implementation of the reconciliation logic is bug  - this bug is to implement a very rough approximation and better define the semantics ready for 
