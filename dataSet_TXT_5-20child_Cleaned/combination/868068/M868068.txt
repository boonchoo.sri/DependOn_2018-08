[meta] Australis - Investigate broken tests
The UX branch has had a bunch of Australis-related landings on it, and we don't show test results for them.

Jamun gets UX merged in periodically, and we *do* have test results there - and the results aren't great. Lots of orange.

We need to fix those tests if this all has a hope of landing on mozilla-central. This is a metabug to track that work.
