[meta] Re-work customize mode, the overflow panel and the hamburger panel
TL;DR:

- the overflow menu is visible in customize mode.
- the hamburger menu gets revamped, and isn't.
- customize mode gets a visual update

The  and  piece (and their integration into customize mode) are intimately linked. Some of the restyle can happen somewhat independently.
