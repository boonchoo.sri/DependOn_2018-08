When a sidebar is opened, the addressbar suggestion list is cropped from the left
Created attachment 


(X11; Linux x86_64; rv:55.0)   ID:20170422100218 CSet: 

STR:
Have a Nightly installed.
Open a sidebar, i.e. with bookmarks (Ctrl+B).
Start typing into the addressbar.

Result:
The suggestions list will open cropped from the left by the width of the opened sidebar. Note that the content of the list is not adjusted accordingly.

What should happen:
The suggestion list should be cropped from the side, where the sidebar is opened, and the content should be adjusted to fit in the list.
