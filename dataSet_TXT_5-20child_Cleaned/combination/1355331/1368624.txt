Allow drag&drop to move sidebar between the right and left side of the browser
There is only a context menu entry available yet to move the sidebar from the left to the right side (and vise versa) of the browser. It would be neat if drag and drop would also be possible.
