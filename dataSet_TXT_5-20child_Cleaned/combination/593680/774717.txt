Make tab closing async
Created attachment 
Screen recording

When closing tabs that have content allocating a lot of memory there is a large delay between clicking the close button and the tab being closed and removed from the tab strip. We should remove it from the tab strip asynchronously while content is still being destroyed.

I can reproduce this with long running Facebook, Netflix and Vimeo (with long HD videos) tabs. Chrome doesn't seem to be affected at all by content when closing tabs, and Safari has a much smaller but still noticeable delay.
