Opening new tab causes unnecessary tab overflow widget if New Tab button isn't placed right after tabs
On my setup (8 app tabs), I get the following behavior shown in the attached movie, where the tab overflow scroll button shows up for an instant before the tab is rendered, which causes the animation to not be as smooth as it should be, since it animates twice.

Slow down the video if you can't see it the first time (I show it twice).
