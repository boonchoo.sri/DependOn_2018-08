Tabcrash icon resize when changing Firefox width
Created attachment 
Screencast showing the issue

Reproducible in builds:
- Latest Nightly

Reproducible on OSs:
- Windows  
- Ubuntu  
- Mac OS X 

STR:
Open Firefox.
Load about:tabcrashed.
Resize Firefox width.

Expected results: The icon does not change size and it disappears after a while.

Actual results: The icon increases size after a while then it disappears.

Notes:
This is a regression

m-c:
Last good nightly: 
First bad Nightly: 

Last good revision: d7e156a7a0a6
First bad revision: c2359a6a6958



m-i:
Last good revision: d0c5e3607389
First bad revision: c2359a6a6958

Pushlog:



Probably caused by: bug 

Reproduces on all platforms.
