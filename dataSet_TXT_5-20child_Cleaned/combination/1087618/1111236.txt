Remove the border around the #categories of in-content pages
Bug  adds a border around richlistboxes. The categories in in-content pages shouldn't have this border.
