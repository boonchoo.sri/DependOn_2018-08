"New bookmark" popup disappears if I reopen it after creating bookmark - round 
>>> My Info: Win7_64, Nightly   ID  (2016-07-14)
STR_1:
Open  in a new tab
Move mouse pointer exactly where "Done" button is located when "New bookmark" popup is open
Press Ctrl+D
Press Enter or click "Done" (you can even move mouse within "Done" button, but not outside)
Press Ctrl+D
[you must perform Steps  in less than  seconds]
Click in the field "Name"

AR: After Step  (or after Step  if you perform it) "new bookmark" popup disappears
ER: Widget should react according to the last interaction, i.e. bookmarks panel shouldn't hide
[this cool expectation can be applied to the cases in Note (1) and probably more]

Use case:
I decided to bookmark a page, then I realized that I want to edit bookmark (Step 

This bug is regression from bug  Regression range:
> 

Notes:
Many widgets in the Mozilla's browser works like described above. If user keeps constantly moving
mouse pointer above fullscreen notification - it still hides. If user hovers "Push Notification"
in the  - the notification is hardly visible (bug 
