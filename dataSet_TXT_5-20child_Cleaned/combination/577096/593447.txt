App tabs notify even if title is set but not changed
Created attachment 
testcase that sets the title to the same thing every  seconds

I see the notification of a title change when I use Yahoo Mail as an app Tab. But the title hasn't changed. (I still have zero new messages.)

To me it seems like Yahoo Mail always sets the title when it checks the server for new messages. And even if the new title is the same as the old title the notification gets triggered.

I'm attaching a testcase to prove that.

Load it
make it an App Tab
switch to another tab.

Expected:
No notification since the title is not changing. It is being set to the same string.

Actual:
The App tab notifies that the title changed.

I heard other things like Google Reader also does the same thing.
