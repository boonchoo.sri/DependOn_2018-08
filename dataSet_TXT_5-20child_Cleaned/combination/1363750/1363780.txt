[meta] Photon - shutdown should not feel slow
Users should not have to wait while shutting down Firefox.

This means that we want
- all windows to be hidden immediately, (this overlaps with the 'close windows immediately' goal of bug 
- the profile lock to be gone as soon as possible.

To a lesser extent, we want the process to exit soon, but that's probably not user-visible, unless the user is frustrated enough by one of the  above points taking too much time.
