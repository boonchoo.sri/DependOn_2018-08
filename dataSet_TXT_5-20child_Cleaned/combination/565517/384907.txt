"Quit dialog" setting suppresses "Closing multiple tabs" warning on last window closing
When there is just one window open and the quit dialog is configured to not show, the usual warning about multiple tabs does not show either.
