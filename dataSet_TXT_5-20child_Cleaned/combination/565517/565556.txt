Change Save to Download
(Note: this is filed as part of the “Paper Cut” bugs — we assume that there may be multiple existing bugs on this. Please make them block this bug, and we will de-dupe if they are indeed exactly the same. Thanks!)

To reproduce:
Start up Firefox
open up a new page
go to "File->Save Page As ....

Recommendation:
We (the UX team) recommend changing all instances of "Save" to "Download". Such as "Save image as" -> "Download image as". The term "download" is now very much part of the internet language.
