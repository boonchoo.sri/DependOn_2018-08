Devtools toolbox should display the actual target url when detached
User Agent:  (Windows NT  rv:11.0)  
Build ID: 

Steps to reproduce:

Open www.google.co.uk, then style editor.
Open www.google.com, then style editor.

Both websites appear as "google" in the style editor.



Expected results:

The real url of the website should appear as there might be differences in the url name. The user should be able to see in which site he's working.
