Customized toolbar in older beta releases puts Firefox button on top right
I've been using the betas for a while and when I enabled the Firefox button recently, it ended up being on the top right of the UI. Which is not the default, as a fresh profile makes it appear on the top left, and an annoyance because the button is not moveable.

I could reproduce the issue with the following scenario:
- Start an older beta (tried 
- Reorder the buttons on the right of the tabbar
- Upgrade to 
- Enable Firefox button (disable menu bar)
-> Firefox button is on top right.

I think it happened on my old profile because I moved the panorama button a while ago.
