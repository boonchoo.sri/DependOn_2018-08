Implement the Firefox button on Linux
It seems that the entire look and feel of Firefox is being changed under Windows for Firefox  but Linux will be left with the Firefox  look.

The reason for this seems to be that there seems to be some unnecessary tie-in in developers minds between implementing the Firefox button and enabling drawing in the titlebar.

It seems to me that implementing making the menubar be hide-able and having the Firefox button at the left of the tabbar as Firefox Beta3 is going to work under  should be easy to implement under Linux to give people the option to use the old UI or chose a more Firefox  Windows-like UI by choosing to hide the menubar under Linux.
