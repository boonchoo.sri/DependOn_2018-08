Measure the interactions with the "show history" button menu items
When clicking on a history result from the "show history" triangle menu, we need to record the number of seconds since last visit, the total number of visits and the position in the menu list.
