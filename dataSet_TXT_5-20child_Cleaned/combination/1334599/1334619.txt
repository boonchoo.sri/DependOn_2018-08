Add a more elaborate probe for selecting a bookmark result
When clicking on a bookmark result we want to measure time in seconds since creation, seconds since the last visit and the total number of visits.
