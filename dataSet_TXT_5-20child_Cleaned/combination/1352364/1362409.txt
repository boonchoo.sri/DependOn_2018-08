The back button arrow is too opaque in inactive windows if the back button is disabled
[Tracking Requested - why for this release]: visual regression in the primary UI

STR:
Open a new tab.
See that the back button is translucent (bug  and has a gray arrow.
Focus a different window to make the Firefox window inactive.

Now the button is completely opaque and has a black arrow.

This does not happen if the back button is enabled. So in inactive windows, the back button now looks more enabled when it's disabled than when it's enabled.
