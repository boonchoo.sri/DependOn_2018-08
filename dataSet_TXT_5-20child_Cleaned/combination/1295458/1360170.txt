Change Search Settings button is not  when selected using the  keys
(Windows NT  rv:55.0)  
Build ID: 

[Affected versions]:
- Nightly 

[Affected platforms]:
- All

[Steps to reproduce]:
Launch Firefox
Write something in the Search Bar
Press Up or Down keys to navigate in the Search panel

[Expected result]:
- You should start cycling through all the elements of the Search Panel (search suggestions, one-off buttons and Change Search Settings button) and all the elements should have the same color when selected (blue on Windows and Mac OS X and orange on Ubuntu).

[Actual result]:
- The Change Search settings button remains gray when selected. This is pretty confusing since the color of the buttons on mouse hover is also gray.
Please see the screencast for more details.

[Additional notes]:
- This issue is not specific only to the search bar, is also encountered in the about:home or new:tab pages.
