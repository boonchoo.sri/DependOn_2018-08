Rework key and mouse handling for the one-off search buttons
I talked with Stephen, and we thought it would be a good idea to treat the one-offs as a single unit for keyboard navigation -- to treat them like another row in the list basically (kind of).

Say the textbox is focused. As you press  you move the selection to the first list item and then down the list items, and then when you get to the last list item and hit  again, you select the first one-off. From there, you can press  to select another one-off. When you press  again, the selection wraps around to the top of the list.  work similarly but in reverse.

So we don't break people's Tab habits, but you can still Tab down to the one-offs.

would still work as they work now.
