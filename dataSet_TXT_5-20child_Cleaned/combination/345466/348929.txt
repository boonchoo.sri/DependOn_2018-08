Tab strip right scroll button and All Tabs menu draw bevels on hover
After the landing of bug  the two buttons on the right of the tab strip both draw bevels on hover on Linux, which looks bizarre. The left scroll button does not do this.

Weirder, the buttons apparently aren't the same height, as the All Tabs menu's bevel extends down  pixel lower than the scroll button's bevel.

Screenshots to follow.
