Close buttons on inactive tabs too vague
When I want to focus an inactive tab there's a big chance that I inadvertently click on the close button. And when I want to close backgrounds tabs, it is not very clear (not as clear as before) where exactly I should click.
I think buttons should be clear as possible.
