Provide a way to open a link into private browsing mode
Along with creating a private browsing mode, I think there should be a way to open a link into private browsing mode. That way a user won't have to copy the link, go into private browsing mode and then paste the link.

Unless I'm not understanding how private browsing mode is going to work. Is it going to work like enter private browsing mode deletes all current cache but current tabs are still display (would this cause  to be reloaded?) and then any subsequent browsing is not cached?
