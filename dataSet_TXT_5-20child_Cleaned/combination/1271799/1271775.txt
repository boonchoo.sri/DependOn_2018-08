Create an initial implementation of auto-migration of the most-useful browser data on initial startup of a clean (non-reset) profile behind a pref
This should avoid showing the modal dialog  the user having to make any choices. Because all the migration dialog really does is call into component-ized migrator implementations per browser, assuming we have a decision for the things that need to be decided, this should just be a matter of calling the same component-ized migrator implementations automatically.

The initial implementation should live behind a pref so we can develop it while also working on things like the "undo" functionality orthogonally. We can turn it on in favour of the current implementation when we think it's "ready".

We'll need decisions from bug  and bug  in order to implement this.
