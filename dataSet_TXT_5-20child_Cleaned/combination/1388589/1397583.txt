With compact UI density, urlbar animations for Pocket and bookmark star are in the wrong place (offset to the right of the normal icon)
When the UI density is compact, the bookmark star and Pocket animations are shifted to the right, like they were in bug  It doesn't look like the related CSS handles :root[uidensity] specially, for margins and such, so maybe it should?

I noticed this while working on bug 
