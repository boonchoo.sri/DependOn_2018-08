[a11y] Box model inspector is not accessible
The box model tab is currently not keyboard accessible and lacks some description. We need to think of the way to let the screen reader user perceive the box model.
