Investigate removal of #includes from browser.js
An attempt to remove the #includes pre-processing from browser.js.

I sent to tryserver a patch that moves all includes into script tags to global-scripts.inc. Let's see if it breaks anything and what talos looks like.

Perhaps some of these could go to browser.xul instead of global-scripts.inc. This could be even a win for Mac because that would make macBrowserOverlay.xul smaller.


