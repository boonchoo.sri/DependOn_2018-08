[New Tab Page] New Tab Page is shown in Private Browsing Mode (with Preloading active)
With "browser.newtab.preload" set to true, the New Tab Page is shown, if a new tab is opened in private browsing mode.

Steps to reproduce:
set browser.newtab.preload to true
restart Firefox
activate Private Browsing Mode
Open a new Tab

Expected:
New "Private Browsing" Information page should be displayed

Actual Result:
New Tab Page is shown
