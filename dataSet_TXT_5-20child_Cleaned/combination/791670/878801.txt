BrowserNewTabPreloader needs to query tabbrowser bounds without flushing pending layout changes
Silly me introduced another uninterruptible reflow while fixing bug  The BrowserNewTabPreloader must not query window.gBrowser.boxObject when opening new tabs or else this will again cause us to flush pending layout changes.
