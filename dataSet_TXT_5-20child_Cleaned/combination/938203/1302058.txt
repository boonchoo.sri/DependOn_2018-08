Pretty Print puts spaces into arrow function => operators
User Agent:  (Windows NT  WOW64; rv:48.0)  
Build ID: 

Steps to reproduce:

Entered code with a => operator into the scratchpad.
Pressed Pretty Print

This happens with all the code I have containing arrow functions, no just the trivial example shown in the expected results below.


Actual results:

((i) = > {
alert(i)
}) (1)


Expected results:

((i) => {
alert(i)
}) (1)
