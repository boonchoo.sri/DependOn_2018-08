Pretty printing ES6 template strings
User Agent:  (Windows NT  rv:37.0)  
Build ID: 

Steps to reproduce:

Try to Pretty print the code containing ES6 template string in Scratchpad:

function test(x, y) {
return `x: ${x}, y: ${y}`
}
test(1, 'foo');


Actual results:


Exception: Unexpected character '`' (11:9)







