[Search experiment] Define experiment parameters
To publish a telemetry experiment, we need to define some configuration parameters. The most important are:

- start date
- end date
- max running time
- target locale: en-US
- target channel: beta
- version: probably  or  or both if the experiment window overlaps an uplift
- sampling size: a sample that will give us roughly  users per cell

Detailed explanation of all these fields, and other optional ones: 
