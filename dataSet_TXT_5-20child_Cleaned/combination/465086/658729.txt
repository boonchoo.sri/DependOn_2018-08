Bug  breaks with suggested CSS tab  width work around for bug 



Bug  which landed in Firefox  removed browser.tabs.tabMinWidth and browser.tabs.tabMaxWidth in favor of CSS  width style rules. As part of this bug, it was advised to use the custom tab width addon  by Dao Gottwald. Other addons like Classic Compact Options  followed Dao's suggested model for restoring the ability to customize the min & max widths of tabs (a feature CCO has had since FF3.0). Now Bug  has landed in FF5a2 to stop tabs from resizing when tabs are closed until the cursor leaves the tab bar. The problem is, the "fix" in bug  stops working if Dao's solution is applied to restore functionality lost because of bug 

Given that bug  and the suggested solution to restore functionality lost in bug  landed first in FF4. The "fix" in bug  needs to be reworked to accommodate Dao's  width CSS solution.




Install the Custom Tab Width add-on 
customize min and max tab widths
Open a bunch of tabs.
Start closing tabs


Tab widths jump around


Tab widths stay the same.
