cannot disable close tab animations easily due to new !important
User Agent:  (Windows NT  WOW64; rv:40.0)  
Build ID: 

Steps to reproduce:

The close tab animation introduced by #465086 has added important for some reason:

tab.style.setProperty("max-width", tabWidth, "important");

This stops me from being able to revert to the animation free behavior using a userstyle.

To resolve this I would probably need to write an addon something along the lines of hooking _lockTabSizing to kill the style change, which isn't really convenient (I'm not very experienced with addon dev). But this really should be a styling issue.

Can the important be gotten rid of here?
