No upper border on active favicon or folder when it is under the active tab while bookmarks toolbar is left of url
Created attachment 
fehler aktiv.png

User Agent:  (Windows NT  WOW64; rv:29.0)   
Build ID: 

Steps to reproduce:

Moved my bookmarks toolbar left to the url.
Moved the mouse arrow on a favicon or folder in the bookmark toolbar
I also use a Light Theme



Actual results:

When the folder or favicon is under the activ Tab, the border is not complete, the upper side is missing.



Expected results:

The border should be all around.
