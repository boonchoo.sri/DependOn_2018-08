Use Australis button styling for non-bookmarks toolbar items
User Agent:  (Windows NT  Win64; x64; rv:51.0)  
Build ID: 

Steps to reproduce:

Install latest nightly.

Place an add-ons icon on the bookmarks toolbar.


Actual results:

Patch  changed the bookmarks icon hover frame to Australis style from light blue to gray. However, non-bookmark icons like those from add-ons still have a light blue hover frame. Also the Chevron(Bookmarks Toolbar overflowed) is light blue.


Expected results:

All icons on bookmarks toolbar should adhere to the Australis style.
