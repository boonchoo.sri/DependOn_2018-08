Self-support v0 (heartbeat only): client tracking
Because some of the larger projects in self-support are at schedule risk, in the short term we're going to focus on the smallest possible set of client changes to support a limited form of heartbeat, with the intention of getting those changes into Firefox  (currently on Aurora).

This bug tracks the client implementation work required for this.
