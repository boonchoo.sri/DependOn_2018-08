When the zoom level is not  existing preloaded about:newtab is loaded at  first and then zoomed in
Bug  made it so that a newly created preloaded about:newtab gets the right zoom. However, existing preloaded about:newtab aren't updated.

+++ This bug was initially created as a clone of Bug #1383875 +++

Without activity stream, the page is opened directly at the right zoom level. With activity stream, you can sometimes notice the elements of the page being rendered small and then big.
