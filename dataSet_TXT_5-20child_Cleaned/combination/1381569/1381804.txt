-  Explicit Memory  Heap Unclassified  Images  JS  Resident Memory (linux64, osx-10-10, windows10-64, windows7-32) regression on push  (Tue Jul  
We have detected an awsy regression from push:



As author of one of the patches included in that push, we need your help to address this regression.

Regressions:

Heap Unclassified summary osx-10-10 opt  -> 
Heap Unclassified summary linux64 opt  -> 
Explicit Memory summary osx-10-10 opt  -> 
Explicit Memory summary linux64 opt  -> 
Heap Unclassified summary windows7-32 opt  -> 
Explicit Memory summary windows7-32 opt  -> 
Resident Memory summary windows7-32 opt  -> 
Images summary windows7-32 opt  -> 
Heap Unclassified summary windows10-64 opt  -> 
Explicit Memory summary windows10-64 opt  -> 
JS summary osx-10-10 opt  -> 
JS summary linux64 opt  -> 
Resident Memory summary linux64 opt  -> 
JS summary windows7-32 opt  -> 
JS summary windows10-64 opt  -> 
Resident Memory summary windows10-64 opt  -> 
Resident Memory summary osx-10-10 opt  -> 


You can find links to graphs and comparison views for each of the above tests at: 

On the page above you can see an alert for each affected platform as well as a link to a graph showing the history of scores for this test. There is also a link to a treeherder page showing the jobs in a pushlog format.

To learn more about the regressing test(s), please see: 
