Cloud Storage API
Implement Cloud Storage JSM module that facilitates
- Cloud storage service discovery on user desktop
- let user opt-in and set a preferred local storage service.
