Refresh the Privacy preference pane
There are a few bugs that touch the privacy preference pane, so I figure it is worth filing a tracking bug to aggregate them all. Here is a mockup of the proposed UI:



Bugs related to the prefpane:

bug  - Privacy pref for "Always on" Private Browsing Mode
bug  - Add privacy-section prefs to control awesomebar behaviour

This bug can also cover the remaining general UI changes (adding the drop down to the top of the preference pane, regrouping items into a history groupbox, etc.)
