[a11y] Make CodeMirror more accessible
Accessibility support in CodeMirror is not great. I've been researching what changes would be required to add screen reader support to CodeMirror. I have documented some of my findings here:  I found a way to make selection movement work by updating the contents and selection of the focused textarea that CodeMirror is using behind the scenes.

I'm open to feedback about the implementation. I'm particularly interested in feedback from someone who has accessibility experience and and who could comment on if it seems suitable (see the live demo on the project page). I outline more details on the project page, and added a screencast with a quick demo.

This implementation is not ideal, since it relies on a timeout to help NVDA catch changes. So we need to find a better way to do this.

Also, see  for more information.
