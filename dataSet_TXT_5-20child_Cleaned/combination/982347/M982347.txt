Implement new visual style for network error pages
Update existing network error pages over to new proposed visual style defined in bug  These would be all errors currently using the netError.xhtml template. No behaviours or interactions should be change (i.e. the removal of "try again" or adding the search bar should not be in this bug).

This will involve partially using the changes in the patches submitted to bug 
