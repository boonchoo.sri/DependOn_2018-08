Implement a storage inspector
We might want to cover:
- IndexedDB
- Cookies
- LocalStorage
- DataStores
- AppCache

Minimum required for V1 will be:
- IndexedDB
- LocalStorage
- DataStores

V1 can be read-only.
