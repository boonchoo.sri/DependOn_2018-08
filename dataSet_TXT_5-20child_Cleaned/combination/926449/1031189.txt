Toolbar for Storage Inspector
This toolbar should contain:
- searchbox
- page counter to view next  entries (this has to be thought through first wrt whether we want pagination at all)
- reload now button for indexedDB (until it gets auto refresh that is)

and in future
- delete button to delete selected entr(y|ies)
- create new button add a  item in the selected host.
