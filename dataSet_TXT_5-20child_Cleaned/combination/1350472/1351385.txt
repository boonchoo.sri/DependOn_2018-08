Lazily load JSON viewer
At startup in the content process, we run the JSON view converter-observer (in all release branches). This causes us to import the whole devtools loading infrastructure, which creates  different compartments, even if the user never uses the viewer. With  content processes, that's about  of memory.

Fixing this has two parts:

The factory registration code in converter-child.js needs to be moved into converter-observer.js. This will then load -child.js if an instance is actually created.

The code for converter-sniffer.js has to be moved entirely into -observer. The sniffer appears to be run regularly, so we can't avoid loading it.
