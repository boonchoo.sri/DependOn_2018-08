Create an about:debugging page to list all debuggable targets.
This page should act as a comprehensive catalog of all the targets that the developer tools can connect to:
- Tabs  Apps
- Add-ons  Extensions
- Workers (shared workers, service workers...)

In addition to local targets, it should also display remote targets from:
- Other browsers (Chrome, Safari...)
- Other devices (over USB  Wi-Fi, running Firefox OS  Android  iOS)
- Firefox OS Simulators

... with appropriate controls for device discovery, connection and management (e.g. configuration, settings, preferences...).
