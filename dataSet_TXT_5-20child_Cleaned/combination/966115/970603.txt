Analyze error recovery on about:neterror with "try again" button
Look at all the possible error situations in about:neterror and see how the "try again" button aids the users in solving the problem at hand.

Questions to answer:
- Does the try again button exist on the page?
- What will pressing "try again" do in terms of solving the problem?
- Does having this behavior make sense? (I.e. does the user actually recover from the problem?)
- Is there a better way of handling the error? (E.g. automatically fixing the problem; extra behavior that will diagnose or fix the problem)
