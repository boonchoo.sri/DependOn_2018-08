In-content Error Page Overhaul
Currently our network error pages leave much to be desired from a usability perspective.

Goals:
- Remove as many errors as possible and instead try to automatically fix the error.
- Help "normal" users understand how to recover from the errors.
- Clean up technical jargon but not eliminate it entirely. The technical details should still exist for  users.
- Attempt to leave a positive emotion with the user through "Whimsy".
- Update the visual style to be consistent with the new in-content themes we are going for.
