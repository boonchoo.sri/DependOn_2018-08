New theme landing caused a Ts regression on trunk
This was probably less-noticeable on branch since the theme went in in pieces, but when it went in in one big chunk on trunk, it's hit on Ts was fairly obvious.

The numbers are roughly a  increase on Linux, and a  increase on Windows.

Some numbers...
argo:  -> 
argo test:  -> 
gais test:  -> 
