Improve info related to requests caused by HTML
For requests caused by parsing HTML (e.g. <script> or <img> tags) the related HTML file (and maybe line and column numbers)  HTML node should be displayed.
Clicking it should switch to the Inspector panel and select the element there.

Sebastian
