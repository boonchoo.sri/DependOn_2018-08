Focus via Toolbox is lost when docking
Created attachment 
Screen recording

[Affected versions]:
- Firefox  beta 
- latest Aurora  e10s 
- latest Nightly  e10s 

[Affected platforms]:
- Windows  
- Mac OS X 
- Ubuntu  

[Steps to reproduce]:
Launch Firefox
Open Web console
Click to dock to  window

[Expected result]:
- The focus is maintained in the Web console.

[Actual result]:
- The focus is lost.

[Regression range]:
- Not reproducible with Firefox  will investigate further and update asap.

[Additional notes]:
- Note that this is applicable for the rest of the toolbox-hosted tools.
- Screen recording attached.
