The command-button-frames in toolbox-buttons does not have the open attribute
Created attachment 
checked-icon-state-is-not-enabled.png

User Agent:  (Windows NT  WOW64; rv:49.0)  
Build ID: 

Steps to reproduce:

Start latest Nightly
Go to any sites (e.g. 
Open DevTools
Click command-button-frames button (Select an iframe as the currently targeted document)
Confirm its icon color


Actual results:

command-button-frames icon does not switch to the opening state color (highlight-blue), because it does not have the open attribute.

Regression range:



Expected results:

command-button-frames should have the open attribute.
