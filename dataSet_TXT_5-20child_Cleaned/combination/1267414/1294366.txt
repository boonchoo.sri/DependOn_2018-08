Filter editor tooltip: filter select can only be opened once
STRs :

- (Windows only)
- open any page, open the devtools, go to inspector
- add rule "filter: unset" on any element
- open the filter editor tooltip (click on the filter button in the rule)
- expand the select "Select a Filter"
- (the select's list is displayed)
- collapse the select
- expand the select "Select a Filter"

ER: The select's list should be displayed again
AR: The list can no longer be displayed

Regression from bug  Since the migration of the filter editor tooltip to use HTML, the tooltip's HTML content is directly embedded in a XUL Panel (since the filter editor tooltip uses the HTMLTooltip xulPanelWrapper option).

A quick look at the XUL Panel limitations [1], it looks like HTML <select> elements are not working properly in panels. Our options for fixing this are either to :
-  encapsulate the editor tooltip in an iframe
-  stop using a XUL panel wrapper for this tooltip

More in favor of  since the XUL panel wrapper is just supposed to be a temporary measure, and I think the filter editor is the one that benefits the less from the increased space provided by the XUL panel wrapper.

[1] 
