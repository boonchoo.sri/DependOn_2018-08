on Vista use browsertabbar-toolbox for navigation toolbar
Created attachment 
mockup of toolbar styling for vista

In winstripe, we currently wrap all toolbars in a single box and style that box as a -moz-appearance: toolbox.

For Vista, we would like:

- menu bar to be -moz-appearance: toolbox
- a new box containing the other toolbars styled as -moz-appearance: browsertabbar-toolbox

See attached mockup.
