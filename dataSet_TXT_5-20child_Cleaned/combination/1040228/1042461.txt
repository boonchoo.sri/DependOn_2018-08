Update hotfix resumes when it shouldn't
It looks like the HTTP download resume code in the hotfix is busted.

This log shows us resuming after a  response among other weirdness. We're almost certainly a) saving when we shouldn't be b) not removing a partial file when we should be.

channel:onStartRequest()
Download is resumable.
Saving state file.
channel:onStopRequest(NS_OK)
Got HTTP 
Closing pipe due to failed channel.
copier:onStartRequest()
State save finished.
copier:onStopRequest(NS_BINDING_ABORTED)
Channel didn't complete successfully.
Download did not complete successfully.
Download failed. Attempt 
Retry timer fired. Attempting another download  update.
Starting download. Attempt 
Saving state file.
Resuming download at byte offset 
channel:onStartRequest()
Download is resumable.
Deferring save because another in progress.
channel:onStopRequest(NS_ERROR_NOT_RESUMABLE)
Closing pipe due to failed channel.
copier:onStartRequest()
copier:onStopRequest(NS_BINDING_ABORTED)
Channel didn't complete successfully.
Download did not complete successfully.
Download failed. Attempt 

After a restart:

Starting download. Attempt 
Saving state file.
Resuming download at byte offset 
State save finished.
channel:onStartRequest()
Entity ID changed!
Saving state file.
Download is resumable.
Deferring save because another in progress.
channel:onStopRequest(NS_ERROR_ENTITY_CHANGED)
Closing pipe due to failed channel.
copier:onStartRequest()
State save finished.
Saving state file from chain.
copier:onStopRequest(NS_BINDING_ABORTED)
Channel didn't complete successfully.
Download did not complete successfully.
Download failed. Attempt 
State save finished.
Retry timer fired. Attempting another download  update.
Starting download. Attempt 
Saving state file.
Resuming download at byte offset 
State save finished.
channel:onStartRequest()
Download is resumable.
Saving state file.
copier:onStartRequest()
State save finished.
Download progress: 
Download progress: 
Download progress: 
...
Download progress: 
channel:onStopRequest(NS_OK)
Got HTTP 
copier:onStopRequest(NS_OK)
Verifying download.
File hash mismatch!
Saving state file.
Download failure #1
Removing temp file due to failed verification.
Deferring save because another in progress.
Download failed. Attempt 
State save finished.
Saving state file from chain.
State save finished.
Retry timer fired. Attempting another download  update.
Starting download. Attempt 
Saving state file.
State save finished.
channel:onStartRequest()
Download is resumable.
Saving state file.
channel:onStopRequest(NS_OK)
Got HTTP 
copier:onStartRequest()
State save finished.
copier:onStopRequest(NS_OK)
Verifying download.
File size does not match:  != 
Saving state file.
Download failure #2
Removing temp file due to failed verification.
Deferring save because another in progress.
