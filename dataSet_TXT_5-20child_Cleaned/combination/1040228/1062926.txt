Store a field in forensics payload that identifies add-on version
We want to track which version of a hotfix a payload came from. Let's add it to the forensics payload.
