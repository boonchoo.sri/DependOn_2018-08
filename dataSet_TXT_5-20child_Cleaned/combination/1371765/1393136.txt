Download notification arrow is misaligned to the toolbar button
Since bug  when you start the first download in a session the arrow animation *sometimes* drops into a spot to the left of where the download button appears.

This was happening for me every time this morning with a new session, but when I went to file the bug I can no longer reproduce so beware the observer effect!
