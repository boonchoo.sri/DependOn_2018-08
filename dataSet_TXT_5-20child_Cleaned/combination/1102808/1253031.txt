Impossible to clear data cached by Service Workers through any exposed UI.
There is no way for a user to clear data cached by a Service Worker.

This causes the following problems:

- Private user data is at risk of disclosure
- Users can not reclaim disk space from greedy workers
- Developers can not reset the browser to a pristine state

Things that don't work:

- Forget button (Bug 
- Privacy -> Clear recent history (Bug 
- Privacy -> Clear everything when Firefox closes (Bug 
- Avanced -> Clear Cached Web Content (Bug 
- Advanced -> Clear Offline Web Content (Bug 
- DevTools -> "Disable Caches" (Bug 

STR:

Visit 
Try to find a way to clear the cached data.

You can check if the service worker is present or not by visiting about:serviceworkers.

You can check if cached data is still available by visiting  and running the following JavaScript in the console:

=> console.log(`Resource ${(x && x.ok) ? "WAS" : "WAS NOT"} found in cache:`, x)).catch(e => console.warn("Could not open cache:", e))
