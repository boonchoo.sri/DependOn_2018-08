Adjust toolbarbutton paddings and margins for small and big icon modes
Created attachment 
patch

We can make the icon size switch more useful by being more generous with the spacing in big icons mode and more compact in small icons mode.
