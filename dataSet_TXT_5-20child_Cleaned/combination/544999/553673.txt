Border and glow doesn't  to hovered state on toolbar button hover, only background



On the hover state on the toolbar buttons, instead of fading in, the border became instantly blue and the blue glow appeared instantly. Only the background faded.




Start minefield.
Hover the cursor on a toolbar button.

Only the background faded to blue. The border got instantly blue and the blue glow appeared instantly.


The whole button shall  to blue.

I use this stylish fix:

.toolbarbutton-menubutton-button:not([disabled="true"]):not(:active):hover,
toolbarbutton[type="menu-button"]:not([open="true"]):not(:active):hover > .toolbarbutton-menubutton-dropmarker:not([disabled="true"]),
.toolbarbutton-1:not([disabled="true"]):not([checked="true"]):not(:active):hover {
-moz-transition-property: background-color, border-color, -moz-box-shadow !important;
}
