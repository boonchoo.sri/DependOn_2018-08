Allow touch-based  of tabs by "touchstart, touchmove, touchend" gesture (in addition  opposed to the current double-tap-drag gesture)
Hardware: Surface Pro 
Windows version: Windows  —  "Creators Update"


On Nightly drag and drop appears to be broken.

This affects dragging tabs, Customization Mode, dragging tiles on New Tab and content HTML based drag and drop.

The release version of Firefox (53) works fine for me.
