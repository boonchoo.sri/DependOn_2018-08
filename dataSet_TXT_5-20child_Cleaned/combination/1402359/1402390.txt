CamelCase all React component files in \devtools\client\netmonitor\src\components\
We recently agreed to name our React component file names using the CamelCase convention.

However the files in \devtools\client\netmonitor\src\components\ do not follow this convention, so let's rename them.

This is an easy fix, but make sure you read the contribution docs before starting:  so that you can build Firefox locally and verify that things still work after your change.
After building, just running the built Firefox and opening the network panel should be enough of a test that things still work.
