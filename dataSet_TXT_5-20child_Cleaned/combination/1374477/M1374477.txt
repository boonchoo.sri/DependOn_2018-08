Photon API for adding, removing, and moving page actions
The functionality of both the screenshots and Pocket menu items live in extensions. The screenshots extension at least would like to call into browser code to add its item, as opposed to the browser adding the item and calling into the extension, and ultimately that's the design we want for extensions anyway. Also, the Photon spec calls for the ability to move items between the panel and the urlbar.

All of these things mean that we need to implement an API for adding and removing page actions and moving them between the panel and urlbar. AFAICT there are two use cases: actions that are simply a typical menu item like screenshots (or more generally either a menu item or a menu with  and subitems, like the Send to Device menu?), and actions that are a menu that expands into an iframe shown in a subview, like Pocket. So the API needs to support both cases.

The API I'm talking about is just a typical browser JSM. I'm thinking of CustomizableUI.jsm. I'm not talking about a WebExtension API. I'm planning on updating Pocket to call into it just like it calls into CustomizableUI. I'm less clear on screenshots right now.
