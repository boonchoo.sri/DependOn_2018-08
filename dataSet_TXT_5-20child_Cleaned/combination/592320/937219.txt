Rule Editor Adding Extra text when typing
User Agent:  (Windows NT  WOW64; rv:25.0)   
Build ID: 

Steps to reproduce:

Right click on an element and select Inspect.
Under the rule tab add a rule for the element. For example type display: inline;


Actual results:

The editor is sometimes selecting text from the auto complete and appending it to the text that has been typed. So when I was typing inline I actually got inlinee instead of inline.


Expected results:

This was working in Firefox  but broke in Firefox  Adding in rules should not cause extra text to get appended to what is being typed.
