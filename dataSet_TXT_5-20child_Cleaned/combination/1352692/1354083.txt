Add a pref to show a static hamburger menu when enabled
The pref name would be something along the lines of 'browser.photon.menusRefresh' --> 'true'.

When `true`, it'll show the new, static version of the menu panel when the hamburger button is clicked.

NB: we will be using the same pref for the new overflow menu, Library button, Action menu, sidebars refresh and the Customize mode refresh. This bug will _not_ implement the behavior for these UI areas.
