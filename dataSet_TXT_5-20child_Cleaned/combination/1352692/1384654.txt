The arrows in the overflow panel animation point right instead of left on RTL builds
Created attachment 
Demonstration

Environment:
Windows  x86
Nightly  RTL (2017-07-26)

STR:
On RTL Nightly, add an icon to the overflow panel
Observe its animation

At the end on the animation (right before it changes to the static arrows icon), the arrows point to the right instead to the left, as it would be expected on RTL builds.
Additionally, the static icon appears to be smaller than the one in the animation.

See demo attached.
