GCLI should have better "command not available" UX
Rather than just saying 'not found' or similar we could have some action the user could take to request a command.
The user could then provide us with notes as to what the command should do.

I wonder if we could use input.mozilla.org as a way to collect and collate this?
