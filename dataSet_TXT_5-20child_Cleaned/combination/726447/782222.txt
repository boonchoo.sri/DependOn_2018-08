Black or white square is displayed when downloading pdf files
(Windows NT  rv:17.0)  

On Windows, when downloading pdf files - a square is displayed in the proximity of the Download Indicator.



Steps to reproduce:
Launch Firefox and start downloading some pdf files

Expected results:
No squares (black or white) are displayed near the Download Indicator.

Actual results:
A white or a black square is displayed near the Download Indicator when the download is complete.
Please see the screen cast for more details: 
