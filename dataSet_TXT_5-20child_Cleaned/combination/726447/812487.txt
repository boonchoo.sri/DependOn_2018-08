Remove useless css styles for Download button
Given that the #downloads-indicator-progress-area is made invisible when counter, progress or paused is not set:
#downloads-indicator:not(:-moz-any([progress],
[counter],
[paused]))
#downloads-indicator-progress-area

{
visibility: hidden;
}

The following lines are therefor useless:
#downloads-indicator:not([counter]) > #downloads-indicator-anchor > #downloads-indicator-progress-area > #downloads-indicator-counter {
background: 
center no-repeat;
background-size: 
}

#downloads-indicator:not([counter])[attention] > #downloads-indicator-anchor > #downloads-indicator-progress-area > #downloads-indicator-counter {
background-image: 
}
As when counter is not set, the ...-progress-area is invisible, and therefor also the -counter is invisible.
So these rules are useless.
