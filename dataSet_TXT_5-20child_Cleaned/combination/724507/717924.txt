Move the HTML tree view in the Page Inspector above the inspector toolbar
Currently, the HTML tree view is below the page inspector toolbar. We want to move this above the (inspector) toolbar to better isolate this view from the page-centric tools.

We may want to file a separate bug to split it out of the registerTools mechanism used by the Inspector as these tools should be specifically for (nodecentric) tools that show up in the node sidebar.
