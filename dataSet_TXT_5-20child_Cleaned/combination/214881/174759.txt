Expand popup won't disappear
With a folder in the PTB do: right click ->Expand
The popup appears but won't disappear except if you click back on the folder
"button".

OTOH if you left click the folder, the expand popup appears but you can make it
vanish by any click anywhere outside the popup.

NT4
