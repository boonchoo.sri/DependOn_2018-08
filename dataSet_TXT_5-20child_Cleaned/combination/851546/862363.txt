Sync the killswitch upon (un)registering of a tool and respect it in Options Panel
In other words, if inspector is disabled (unregistered), the menu item should also go away and come back when it is registered again.
