Master password prompt is a modal dialog



Firefox has made good progress in making password saving work with a drop-down bar rather. Not just because the modal dialog was impractical. It was also annoying. The modal dialog that informed you that a server wasn't responding was also removed. Again, because it was annoying. Using these as a precedent I'm going to put forward the following as a bug rather than a feature request.

When asked for the master password a modal dialog box pops up and interrupts your surfing experience when you don't necessarily want to use it. For example, the following instances:

If you don't want to log in to a site but there is a log-in form in the corner of the front page.

If other people are using your computer and they want to log in to their account and not yours. (For me, one of the reasons that the master password exists is so you can let friends use your computers without worrying.)

Of course, the trade off is that using the master password would require an extra click but this is not a big deal since you do not need to do this many times. (As a side note, developers should go through every modal dialog in Firefox and ask themselves if it's absolutely necessary that they are modal and that they exist. Having said that, the only other ones I can think of that still are exist are security warnings and are fairly important.)

Hopefully, this will be easy to implement if there is a very standard way to call upon those slide-down bars. I don't know what they're called.




Save a password for a website.
Activate the master password feature.
Visit the site's log-in page, without the intention of using the saved password.

A useless modal dialog appears, that won't let you do anything until you click cancel or press escape.


An unobtrusive bar slides down at the top of the browser asking if you would like to enter the master password, with a button to click on to do so.
