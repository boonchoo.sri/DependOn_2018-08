HTTP authentication dialog should be tab-modal



The HTTP authentication dialog is currently window-modal. While it is shown, the user is unable to use any other tabs. Additionally, if the tab wishing to display the prompt is a background tab, it steals focus.

The desired behaviour is for the prompt to only block the tab it relates to, and not steal focus when it's shown.




Ctrl-click the specified URL to open it in a background tab.
Observe both the focus stealing and the modality.
