Chrome migrator should handle when databases are locked
Currently, if Chrome is open and I import history from the running profile then it will fail without telling me or importing any history. The failure is because the sqlite history database is locked. Below is the error reported in the error console:

Error: [Exception... "Component returned failure code:  (NS_ERROR_STORAGE_BUSY) [nsINavHistoryService.runInBatchMode]" nsresult: "0x80630001 (NS_ERROR_STORAGE_BUSY)" location: "JS frame ::  :: Chrome_migrateHistory :: line  data: no]
Source File: 
Line: 

We should probably tell users to close the source browser if we detect an item we want to import from is locked.
