[UX] Handle case where an insecure password field is present in a subframe of an HTTPS page
We should determine how to handle the case where an insecure password field is present in an HTTP subframe. We show the mixed active content loaded message now, and we might need to show the insecure password warning as an addition. We should also determine which icon to show.
