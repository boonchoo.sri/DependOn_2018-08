[Overlay] The Sync progress is checked even if the sign up process was not (properly) completed
[Affected versions]:
-  build1 (20170903140023)

[Affected platforms]:
- Windows  x64
- Ubuntu  x64
- Mac OS X 

[Steps to reproduce]:
Launch Firefox with a clean profile
Open a new tab and cleck the FoxHead icon to start the tour
Go to the Sync section from the navigation bar and click the Next button without filling the email address (or after typing an invalid email address)
- inspect the tour navigation bar

[Expected result]:
- The Sync progress isn't marked as completed (by a green check mark) until the sign up process is properly finished
- The Next button becomes active only when the email field is properly filled

[Actual result]:
- The Sync progress is marked as completed with the green check mark even if the email field is not filled or if an invalid email address is entered
- The Next button is active even if the email field isn't (properly) filled

[Regression range]:
- The issue is reproducible all the way back to  (2017-07-20), when bug  was fixed, so this issue is not a regression
