After Undo Close Tab, content area of newly-opened page isn't focused
(X11; U; Linux i686; en-US; rv:1.9a1)  

Steps to reproduce:
Open a new tab and close it.
Right click on a tab and choose Undo Close Tab.

Results:
Tab clicked on remains focused with dotted outline showing.

Expected results:
Newly-opened tab's content area needs to be focused.
