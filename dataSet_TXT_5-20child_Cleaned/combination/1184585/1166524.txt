Provide nice labels and descriptions for GC reasons in the marker's detail view
For example:

reason: ALLOC_TRIGGER

Could get L10N'd into something like this:

Reason: Allocation Trigger
Description: GC was triggered because there was too much allocation. Consider finding ways to avoid unnecessary allocations.

The tricky thing is keeping up-to-date and in sync with the actual GC reasons, while not losing data about old GC reasons, because we might be remote profiling an older 
