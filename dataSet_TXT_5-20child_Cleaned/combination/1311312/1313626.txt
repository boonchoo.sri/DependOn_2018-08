Clean up more issues found by eslint no-undef rule
Scanning through the current  errors raised when no-undef is enabled, there's a few obvious things that we can do:

- Add .eslintrc.js to some more test directories.
- Fix various minor errors raised by the no-undef rule. Only one of these would have been an actual issue (in translation.js) however that code is not enabled by default at the moment.
