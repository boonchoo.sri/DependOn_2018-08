Australis: Clean up downloads-button and downloads-indicator CSS, always use sprite sheet
Similar to bug  the downloads-button  downloads-indicator CSS can use some cleaning up. Slightly less of an issue because I think we mostly just have some CSS that can be simplified now that we don't have full  small icons mode anymore, not actual visual breakage, but still:







all have some outdated CSS for that. Plus, we should probably be using the toolbar sprite for the indicator icon like we are for the button icon. Marking P4 because of the latter - we probably don't show the right indicator icon for  situations.

(Mike, CC'ing you because you were working on the disappearing icon on Linux, so you might be able to pick this up relatively easily? Don't feel obliged! :-) )
