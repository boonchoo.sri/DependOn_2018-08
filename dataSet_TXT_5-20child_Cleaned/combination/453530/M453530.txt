Distributed  updating will cause frequent temp table syncs (and fsyncs), make livemarks dynamic containers
We are changing feeds updating to distribute them, what will happen when we will sync on bookmarks?
with many feeds updating could last for minutes, and we could be synching temp tables often.

Since feeds are continuously added and removed from moz_bookmarks table and probably are not so useful in offline mode we could find some way to hold them in a temp table,  children are not real bookmarks (they are not starred for example)
