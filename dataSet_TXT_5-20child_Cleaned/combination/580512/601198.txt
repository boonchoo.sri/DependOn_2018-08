Windows with pinned tabs being automatically restored at browser start up if crash recovery is disabled (also restores non-pinned tabs)
Steps to reproduce:

Set browser.sessionstore.resume_from_crash to false
Set browser.startup.page to  (blank tab)
Open any number of windows and tabs.
Pin one of the tabs.
Wait for sesionstore.js to be updated.
Exit Firefox.
Run Firefox.

Expected result:

One window with a blank tab. Previous session should be available for restore from History tool menu.

Actual results:

The active window at browser shutdown is automatically restored in it's entirety (all pinned and non-pinned tabs. For all other windows that were open at shutdown, only the pinned tabs are restored. Windows with that had no pinned tabs are opened with just a blank tab.


Cause:

As of bug  landing, saveState sets "sessionstore.resume_session_once" preference if "browser.sessionstore.resume_from_crash" is false and there are (or has ever been) pinned tabs. This triggers SessionStart to set the sessionType to "RESUME_SESSION" the next time Firefox is start.

Based on my understanding of bug  app tabs should only be restored on a crash and only the app tabs, not all the tabs. Neither is the case here though.

After shutdown, sessionstore.js actually contains all windows and tabs (pinned and non-pinned). The reason only the active window has all it's tabs restored and the other tabs only have their app tabs restored is because of bug 
