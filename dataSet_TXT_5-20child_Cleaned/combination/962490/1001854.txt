Search field length does not consistent after toggle "Hide the new tab page" and restart
Created attachment 
screenshot

Steps To Reproduce:
Start Firefox with new profile
Open New Tab (Click +)
--- observe Search field length in the new tab
Click "Hide the new tab page" button to hide the new tab page
Restart Browser
Open New Tab (Click +)
Click "Hide the new tab page" button to unhide the new tab page
--- observe Search field length in the new tab

Actual Results:
Search field length does not consistent.


Search field length should not change.
