Add Yandex and DuckDuckGo logos to about:newtab search
Created attachment 
Yandex logo for Russia.svg

User Agent:  (Windows NT  WOW64; rv:34.0)   
Build ID: 

Steps to reproduce:

Open about:newtab in latest Nightly
Set up Yandex as default search engine
Yandex logo isn't displayed


Actual results:

Yandex logo isn't displayed.

Yandex (Russian: Яндекс) is a Russian Internet company which operates the largest search engine in Russia with about  market share in that country.Yandex also has a very large presence in Ukraine and Kazakhstan, providing nearly a third of all search results in those markets and  of all search results in Belarus- (Wikipedia).


Expected results:

Need to add a logo to a about:newtab search
For Russia:

World and Turkey:

