Expose frecency in query result nodes
The frecency value for a URI should be exposed as a property of nsINavHistoryResultNode.
