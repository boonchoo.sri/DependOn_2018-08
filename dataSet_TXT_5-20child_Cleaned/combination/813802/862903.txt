Fix rough landing of bug  on UX branch
Looks like the UX branch got reset or something last night, and all of the patches (with the exception of the hacky, UX-only patches) re-landed.

That's all well and good, but it looks like this patch for bug 



didn't land properly, making things look pretty funky in Windows XP (menu doesn't go into titlebar, titlebar doesn't stretch behind tabstrip).
