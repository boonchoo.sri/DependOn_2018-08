Provide a notification when the newtab URL changes
Spun off from bug 

There should be a notification that add-ons can listen for to know when the new tab value is changed (the same way they could observer the pref before.

(In reply to Florian Quèze [:florian] [:flo] from comment #63)
> (In reply to Mike Kaply [:mkaply] from comment #61)
>
> I don't really understand the use case for your proposed notification (why
> would a user intentionally install  different add-ons both attempting to
> change the new tab page?), but if you think a notification is needed, I
> don't see a problem with adding one (in a new bug though).

There are a number of add-ons that offer new tab as one of multiple feature that could be turned on and off.

You need to provide a way for those add-ons that change the new tab page to coexist.

In addition, if Firefox provides a way to reset the new tab page in prefs, an add-on that is changing the new tab page needs to know when that happens so it doesn't take the new tab page the next time it starts up.

If you don't provide this, you're just going to have a bunch of add-ons that always take the new tab page at startup.
