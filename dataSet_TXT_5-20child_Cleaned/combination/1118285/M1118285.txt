The browser.newtab.url preference is abused and should be removed
Created attachment 
Screenshot of abused new tab page

The browser.newtab.url preference has no exposed UI, is not really supported, and is abused by search hijackers. We should remove it and encourage people using a non-default new tab page to install an add-on instead.
