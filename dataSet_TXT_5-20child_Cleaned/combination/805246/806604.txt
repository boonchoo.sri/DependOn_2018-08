large memory use by Facebook worker compartment
I'm seeing a  process, with fb as the top compartment AFAICT:

Main Process

Explicit Allocations
MB (100.0%) -- explicit
├────766.04 MB (61.69%) -- js-non-window
│ ├──418.98 MB (33.74%) -- compartments
│ │ ├──396.37 MB (31.92%) -- non-window-global
│ │ │ ├──208.46 MB (16.79%) --  [anonymous sandbox] (from: 
│ │ │ │ ├──162.75 MB (13.11%) -- gc-heap
│ │ │ │ │ ├───76.01 MB (06.12%) -- objects
│ │ │ │ │ │ ├──59.45 MB (04.79%) ── non-function
│ │ │ │ │ │ └──16.56 MB (01.33%) ── function
│ │ │ │ │ ├───52.73 MB (04.25%) ── unused-gc-things
│ │ │ │ │ ├───25.80 MB (02.08%) -- shapes
│ │ │ │ │ │ ├──14.99 MB (01.21%) ── dict
│ │ │ │ │ │ └──10.80 MB (00.87%) ++ (2 tiny)
│ │ │ │ │ └────8.21 MB (00.66%) ++ (4 tiny)
│ │ │ │ ├───22.32 MB (01.80%) -- shapes-extra
│ │ │ │ │ ├──17.68 MB (01.42%) ── dict-tables
│ │ │ │ │ └───4.64 MB (00.37%) ++ (3 tiny)
│ │ │ │ ├───18.03 MB (01.45%) -- objects
│ │ │ │ │ ├──14.67 MB (01.18%) ── slots
│ │ │ │ │ └───3.36 MB (00.27%) ++ (2 tiny)
│ │ │ │ └────5.36 MB (00.43%) ++ (7 tiny)
│ │ │ ├──117.82 MB (09.49%) ++ (663 tiny)
│ │ │ ├───39.43 MB (03.18%) -- compartment([System Principal], 
│ │ │ │ ├──22.03 MB (01.77%) -- gc-heap
│ │ │ │ │ ├──17.56 MB (01.41%) -- objects
│ │ │ │ │ │ ├──17.55 MB (01.41%) ── non-function
│ │ │ │ │ │ └───0.01 MB (00.00%) ── function
│ │ │ │ │ └───4.47 MB (00.36%) ++ (5 tiny)
│ │ │ │ └──17.40 MB (01.40%) ++ (8 tiny)
│ │ │ ├───18.21 MB (01.47%) ++ compartment([System Principal],  (from: 
│ │ │ └───12.45 MB (01.00%) ++ compartment([System Principal], [anonymous sandbox] (from: 
│ │ └───22.61 MB (01.82%) -- no-global
│ │ ├──22.59 MB (01.82%) -- compartment(atoms)
│ │ │ ├──13.55 MB (01.09%) -- gc-heap
│ │ │ │ ├──13.16 MB (01.06%) ── strings
│ │ │ │ └───0.39 MB (00.03%) ++ (2 tiny)
│ │ │ └───9.05 MB (00.73%) ++ (2 tiny)
│ │ └───0.02 MB (00.00%) ++ compartment([System Principal], about:blank)
│ ├──322.96 MB (26.01%) -- gc-heap
│ │ ├──311.34 MB (25.07%) ── decommitted-arenas
│ │ └───11.63 MB (00.94%) ++ (3 tiny)
│ └───24.09 MB (01.94%) ++ runtime
├────316.15 MB (25.46%) -- window-objects
│ ├───74.74 MB (06.02%) ++ (21 tiny)
│ ├───72.54 MB (05.84%) --  
│ │ ├──40.18 MB (03.24%) -- 
│ │ │ ├──38.49 MB (03.10%) -- js
│ │ │ │ ├──38.28 MB (03.08%) -- 
│ │ │ │ │ ├──20.50 MB (01.65%) ++ (9 tiny)
│ │ │ │ │ └──17.78 MB (01.43%) ++ gc-heap
│ │ │ │ └───0.21 MB (00.02%) ++  about:blank)
│ │ │ └───1.68 MB (00.14%) ++ (3 tiny)
│ │ ├──19.22 MB (01.55%) ++ (5 tiny)
│ │ └──13.14 MB (01.06%) ++ 
│ ├───50.59 MB (04.07%) --  
│ │ ├──41.75 MB (03.36%) -- 
│ │ │ ├──23.39 MB (01.88%) ++ gc-heap
│ │ │ └──18.36 MB (01.48%) ++ (9 tiny)
│ │ └───8.84 MB (00.71%) ++ (4 tiny)
│ ├───43.22 MB (03.48%) --  
│ │ ├──38.36 MB (03.09%) -- 
│ │ │ ├──31.29 MB (02.52%) -- js
│ │ │ │ ├──31.08 MB (02.50%) -- 
│ │ │ │ │ ├──17.12 MB (01.38%) ++ gc-heap
│ │ │ │ │ └──13.96 MB (01.12%) ++ (9 tiny)
│ │ │ │ └───0.21 MB (00.02%) ++  about:blank)
│ │ │ └───7.07 MB (00.57%) ++ (4 tiny)
│ │ └───4.86 MB (00.39%) ++ (2 tiny)
│ ├───24.43 MB (01.97%) --  
│ │ ├──16.66 MB (01.34%) ++ 
│ │ └───7.77 MB (00.63%) ++ (4 tiny)
│ ├───19.19 MB (01.55%) --  
│ │ ├──18.97 MB (01.53%) -- 
│ │ │ ├──13.89 MB (01.12%) ++ js
│ │ │ └───5.09 MB (00.41%) ++ (4 tiny)
│ │ └───0.22 MB (00.02%) ++ window(about:blank)
│ ├───16.58 MB (01.34%) -- 
│ │ ├──12.81 MB (01.03%) -- window([system])
│ │ │ ├──12.80 MB (01.03%) ++  Principal], about:blank)
│ │ │ └───0.01 MB (00.00%) ──  [10]
│ │ └───3.77 MB (00.30%) ++ 
│ └───14.87 MB (01.20%) ++  
├─────93.65 MB (07.54%) ── heap-unclassified
├─────37.43 MB (03.01%) ++ (14 tiny)
└─────28.48 MB (02.29%) -- storage
├──26.98 MB (02.17%) ++ sqlite
└───1.49 MB (00.12%) ++ prefixset

Other Measurements
(100.0%) -- js-compartments
├──784 (85.03%) ── system
└──138 (14.97%) ── user

MB (100.0%) -- js-main-runtime
├────662.97 MB (65.64%) -- compartments
│ ├──421.04 MB (41.69%) -- gc-heap
│ │ ├──167.17 MB (16.55%) -- objects
│ │ │ ├──123.24 MB (12.20%) ── non-function
│ │ │ └───43.93 MB (04.35%) ── function
│ │ ├──129.52 MB (12.82%) ── unused-gc-things
│ │ ├───63.09 MB (06.25%) -- shapes
│ │ │ ├──29.50 MB (02.92%) ── dict
│ │ │ ├──18.07 MB (01.79%) ── base
│ │ │ └──15.52 MB (01.54%) ── tree
│ │ ├───30.86 MB (03.06%) ── strings
│ │ ├───20.30 MB (02.01%) ── scripts
│ │ └───10.10 MB (01.00%) ++ (3 tiny)
│ ├───54.89 MB (05.43%) ── analysis-temporary
│ ├───50.82 MB (05.03%) -- shapes-extra
│ │ ├──28.93 MB (02.86%) ── dict-tables
│ │ ├──11.71 MB (01.16%) ── compartment-tables
│ │ └──10.17 MB (01.01%) ++ (2 tiny)
│ ├───41.64 MB (04.12%) ── script-data
│ ├───41.02 MB (04.06%) -- objects
│ │ ├──34.55 MB (03.42%) ── slots
│ │ └───6.47 MB (00.64%) ++ (2 tiny)
│ ├───25.94 MB (02.57%) -- string-chars
│ │ ├──16.31 MB (01.61%) ── non-huge
│ │ └───9.64 MB (00.95%) ++ huge
│ ├───15.77 MB (01.56%) ── cross-compartment-wrappers
│ └───11.85 MB (01.17%) ++ (6 tiny)
├────322.96 MB (31.98%) -- gc-heap
│ ├──311.34 MB (30.82%) ── decommitted-arenas
│ ├───11.63 MB (01.15%) ── chunk-admin
│ └────0.00 MB (00.00%) ++ (2 tiny)
└─────24.09 MB (02.39%) ── runtime

MB (100.0%) -- js-main-runtime-gc-heap-committed
├──303.15 MB (70.07%) -- used
│ ├──285.24 MB (65.93%) ── gc-things
│ ├───11.63 MB (02.69%) ── chunk-admin
│ └────6.28 MB (01.45%) ── arena-admin
└──129.52 MB (29.93%) -- unused
├──129.52 MB (29.93%) ── gc-things
└────0.00 MB (00.00%) ++ (2 tiny)

MB (100.0%) -- window-objects
├──26.50 MB (36.73%) -- layout
│ ├──13.68 MB (18.96%) ── style-sets
│ ├───5.58 MB (07.73%) ── pres-shell
│ ├───3.06 MB (04.24%) ── frames
│ ├───1.38 MB (01.91%) ── pres-contexts
│ ├───1.31 MB (01.82%) ── style-contexts
│ ├───0.98 MB (01.36%) ── rule-nodes
│ └───0.50 MB (00.70%) ++ (2 tiny)
├──26.09 MB (36.15%) ── style-sheets
├──19.38 MB (26.86%) -- dom
│ ├───6.95 MB (09.63%) ── orphan-nodes
│ ├───5.88 MB (08.14%) ── element-nodes
│ ├───5.08 MB (07.04%) ── text-nodes
│ ├───1.44 MB (01.99%) ── other
│ └───0.04 MB (00.06%) ++ (2 tiny)
└───0.19 MB (00.26%) ── property-tables

MB ── canvas-2d-pixel-bytes
MB ── explicit
MB ── gfx-surface-image
── ghost-windows
MB ── heap-allocated
MB ── heap-committed
MB ── heap-committed-unused
── heap-committed-unused-ratio
MB ── heap-dirty
MB ── heap-unused
MB ── images-content-used-uncompressed
MB ── js-gc-heap
── page-faults-hard
── page-faults-soft
MB ── resident
MB ── storage-sqlite
MB ── vsize
