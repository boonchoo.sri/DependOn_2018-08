Missing Pocket, home and downloads buttons if Hello is placed in the menu panel and then restored
Affected versions: latest Nightly  (from  e10s 
Affected platforms: Ubuntu   Mac OS X  and Windows  

Steps to reproduce:
Launch Firefox with a clean profile.
Click on Menu panel (Hamburger button).
Select Customize.
Drag Hello button to the Menu Panel.
Click 'Restore Defaults' button.

Expected results: All previous changes are properly restored.
Actual results: Pocket, home and downloads buttons are no longer displayed in the toolbar, Menu Panel nor Additional Tools and Features.

Additional notes:
Via about:config page, 'browser.uiCustomization.state' pref points that the buttons are still placed in the nav-bar:
'nav-bar': [
'urlbar-container',
'search-container',
'bookmarks-menu-button',
'pocket-button',
'downloads-button',
'home-button',
'loop-button'
],
Unaffected version:
* latest Aurora  (from  - not reproducible maybe because of the extra buttons (Web developer tools, WebIDE) placed in the toolbar?
*  beta  (Build ID: 
*  build  (Build ID: 
Regression range:
Last good revision: 
First bad revision: 
Pushlog: 

Regressed by  Shane Caraveo — Bug  move pocket to a system addon
