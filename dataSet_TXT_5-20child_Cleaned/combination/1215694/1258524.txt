Pocket  gone after  on Unix != Linux
Created attachment 
Nightly screenshot with  buttons

+++ This bug was initially created as a clone of Bug #1257947 +++

Similar to bug  chrome registration in Pocket extension needs to be platform- [1] rather than OS_TARGET -specific. JS code (via AppConstants.platform) already treats any GTK system as Linux. The following line works fine here with os=FreeBSD.

# 
% skin pocket   os=Linux

[1] 
