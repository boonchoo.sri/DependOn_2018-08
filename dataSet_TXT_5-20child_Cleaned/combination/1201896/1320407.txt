Add-on installation's "$ADDON has been successfully installed" popup has an unlabeled, unclickable blue button
+++ This bug was initially created as a clone of Bug #1320406 +++

STR:
In Nightly  load 
Click the page's "Add to Firefox" button.
Click the popup notification's "Install" button.

RESULT:
Firefox will show a "Pinboard Bookmark has been successfully installed." popup notification with a big blue button that is has no label and is not clickable.

You can discuss the popup by clicking the tiny X button, but it seems like this popup should have an OK button.
