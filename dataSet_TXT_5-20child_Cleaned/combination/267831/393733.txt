Allow resizing the search bar in Customize Toolbars mode
I think it should be possible to resize the search bar and address bar by just dragging while in Customize Toolbars mode. Reasons:

Users coming from Firefox  might not have the splitter on the toolbar. (Bug  added the splitter to the default set, used by new profiles, but did not make changes to existing profiles.)

Many users want the search bar to be a size other than the default but don't want to resize it often enough to justify having a splitter. For these users, resizing it takes a zillion steps (enter CT mode, put the splitter on the toolbar, exit CT mode, resize, enter CT mode again, remove the splitter, exit CT mode again).
