Debugger doesn't display anything in "variables" when I navigate to another page and start debugging
>>> My Info: Win7_64, Nightly   ID 
STR_1:
Open url  function F(){return 
Open devtools -> debugger
Set breakpoint at line 
Focus urlbar, replace "(F)" with "(F())", press Enter
Set breakpoint at line  Click on the page

AR: There's nothing in "variables" tab
ER: All variables should be displayed correctly

This is regression from bug  Regression range:
> 
