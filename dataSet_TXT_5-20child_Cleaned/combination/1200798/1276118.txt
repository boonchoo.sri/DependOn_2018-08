Source is unavailable if I switch from a broken script to already loaded script
>>> My Info: Win7_64, Nightly   ID 
STR:
Open  
Open debugger. Make sure there're at least  scripts. Otherwise close and reopen devtools
Make sure that script "26sdov5axQhN" is selected.
Switch to another script (which is broken) and back

AR: Source of script "26sdov5axQhN" is unavailable. It shows the same error as the broken script
ER: Source should be visible

This is regression from bug  Regression range:
> 
