The reload addon use new toolbox files only once
When using the reload addon, only the first modification made to toolbox.js (and most likely other UI files) are not considered. Either firefox file or the first modification are considered.

It is most likely an issue with gDevTools keeping reference to old 
