convert uses of "defer" to "new Promise"
Many places in devtools use "defer" or "promise.defer".
However, this method didn't make it into the final Promise spec,
and it would be better to use standard facilities here,
namely "new Promise".

See this thread:


In particular this message shows how to convert from defer to new Promise:



This is a good first bug, but as there are many places using defer, it would
be best to proceed by filing a new bug for some area (say, one directory); then
do the conversion there. When everything is converted, we could remove
and close this bug.

There was some speculation in the thread that maybe some of the conversions can
be done in an automated way. This would be worth looking into as well.
