Bookmarks have incorrect text colour on lightweight themes
Created attachment 
Black bookmark text with a black text shadow

This looks quite bad and affects anyone showing bookmark toolbar items (including if the items are in the nav-bar).

I believe this was regressed by bug  because it significantly increased the specificity of a selector by adding a :not(#bookmarks-menu-button)[1] which adds  to the specificity.

Since the inherited colour would be black it's getting set to #222, the easy fix (A) would be to remove the color property from that ruleset. The difference is very subtle IMO and this would reduce future regressions.

Other fixes:
B) Add !important to 
C) Add a new rule for the bookmark items with LWT to fix the colour which makes this CSS more complex.

I vote for option A to simplify our styles (followed by B).

[1] 
