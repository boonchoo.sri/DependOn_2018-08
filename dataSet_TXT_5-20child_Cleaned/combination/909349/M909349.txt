Australis, OS X: downloads and add-on buttons don't have a depressed state
When looking at bug  I noticed we don't have a blue-ish icon for the downloads button for when its panel is open (like for the menu button, the charset button, etc.). On regular nightly, we just have button outlines so you can *see* that the buttons are in a "pressed" state. However, as Australis got rid of those, there's really no visual indication of what's going on.

Now, this would be a question of just adding a blue icon to the sprite. Unfortunately, we do also already use blue to indicate that there are downloads. As Stephen so eloquently put it... we need more colors!

Note that Stephen also rightly pointed out to me that we'll likely have similar issues with add-ons, which don't necessarily ship extra icons for pressed state especially as Windows still has button outlines...

(P3 mostly because of the add-ons, but I could be convinced otherwise)
