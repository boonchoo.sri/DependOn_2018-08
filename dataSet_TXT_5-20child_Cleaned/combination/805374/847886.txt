fonts.xul should perform alias resolution using EncodingUtils::EncodingFromLabel when populating menu for intl.charset.default
When fonts.xul populates the menu for intl.charset.default, it should perform Encoding Standard-compliant alias resolution to account for legacy Western and Thai (at least) encoding names in existing profiles.

That is, the value of the intl.charset.default pref should be subject to canonicalization using EncodingUtils::EncodingForLabel before using it to populate the menu.
