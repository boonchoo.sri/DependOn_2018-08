Label truncation is not fully supported in Australis menu widgets
Created attachment 
Strings not truncated in the UI (French build)

This is badly impacting localized builds.

As you can see on the screenshot of the French build, "Modules complémentaires" (translation of "Add-ons") is much longer than the en-US string and make the widget wider, making it impossible to have a third widget in the row.

Second case, if you drag'n'drop the Bookmark toolbar into the menu, the label isn't truncated.

The weird part is that truncation is working well for "Téléchargements" (Downloads) -> we can see "Télécharge…"
