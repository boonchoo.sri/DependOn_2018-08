The event details are dismissed if clicking on the detailed code
[Affected versions]:
- Firefox  (2016-06-09)

[Affected platforms]:
- Windows  x64, Mac OS X  Ubuntu  x64

[Steps to reproduce]:
Launch Firefox.
Open the Inspector and click on an available 'ev' icon.
Click on one of the available detailed lines.
Click on the displayed code a few times.

[Expected result]:
- The pointer is positioned where the click is performed.

[Actual result]:
- The event details pop-up is dismissed.

[Regression range]:
- This is a regression since the bug is not reproducible on the previous builds.
- I didn't search for the regression range, since a patch is ready to be pushed.
