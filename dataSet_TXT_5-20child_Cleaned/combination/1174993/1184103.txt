[Control Center] Show grey lock icon when the connection is degraded
The connection degrades for weak ciphers  and whenever mixed display content is loaded. The lock in the URL bar becomes grey and so should the big lock in the control center.
