Include users with only webextensions in Release to receive Multi using the system add-on
Multi is first going out to users without add-ons in  (throttling up). After that is to all the users that qualify and looks good, we need to expand to Release users that have only WebExtensions. WebExtensions + Multi cohort was been in Beta 

Pros:
Expanding to that population will de-risk  by exposing any issues early in what will be the  release configuration. We only saw one issue in Beta (see below), there are no other known issues. It also gets Multi out to a broader audience, ideally improving their experience before 

Cons:
The Beta difference in stability between multi + no add-ons and multi + webextension cohorts was  more crash per  hours of use with WebExtensions. The small increase is outweighed by the benefits. The difference was a higher EnqueuePromiseReactionJob crash occurrence in WE. Still the Pros of expanding outweigh this Cons. The scale is misleading - but if needed is here stability between cohorts 

Mitigation:
we can watch that crash signature for a big change between before and after the new cohort is added and remove Webextensions from getting Multi if needed.

Note:
Please engage with Ben to validate in telemetry that the targeting is hitting the right add-ons. Along with QA, that has been the most successful way of finding issues in targeting before release.
