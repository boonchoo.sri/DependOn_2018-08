Listen for device name change notifications from open Firefox Accounts tabs
As part of the "devices view" in Firefox Accounts, a user is able to change a device's name from web content. FxA will attempt to notify the browser via a WebChannel message with the new device name. Firefox should update its internal state accordingly.
