Notify any open Firefox Accounts tabs of about:preferences#sync device name change
As part of the "devices view" of Firefox Accounts, any open FxA tabs should be notified if the user changes the device name from about:preferences#sync

See 
