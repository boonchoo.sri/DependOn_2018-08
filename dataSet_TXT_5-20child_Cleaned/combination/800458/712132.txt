Firefox hangs on quit
Created attachment 
Analysis of hanging Aurora process

When I use Firefox for some time and try to close it, it hangs, so I have to force close it. This is reproducible, so if you could tell me what you need to debug, I could try to provide that information. I've attached the output of the "analyze process" window that the Mac OS activity dialog provides. Not sure if that is of any help.
