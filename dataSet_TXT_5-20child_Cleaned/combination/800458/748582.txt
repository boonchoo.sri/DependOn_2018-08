Firefox hangs when shutting down OSX
User Agent:  (Macintosh; Intel Mac OS X  rv:12.0)  
Build ID: 

Steps to reproduce:

Logging out user  shutting down the system.


Actual results:

I get the following message "Logout has timed out because the application Firefox failed to quit."


Expected results:

Firefox quits.
