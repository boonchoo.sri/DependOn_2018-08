Understand what scripts in the browser front-end are run on timeouts and intervals
I was discussing this with ehsan today. Having looked at a number of profiles, we're concerned that there are timers and intervals being queued that might run at any time on the main thread. It's kind of the wild west. We have no idea how many of these things there are, we don't know their performance characteristics, and we don't know how often they run.

We should probably get a sense of this, at least to put our minds a bit at ease.
