should provide a keyboard-oriented UI for quickly tagging a page
Created attachment 
wip

see the extension in the bug URL. this patch provides the same UI: basically a centered panel that contains a text input box for adding tags to the currently visible page. it's using accel+shift+c (categorize) for the keyboard shortcut.
