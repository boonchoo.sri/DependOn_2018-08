Fix styling of type="menu" buttons in the menu panel
Created attachment 
australis noscript.png

User Agent:  (Windows NT  WOW64; rv:29.0)   
Build ID: 

Steps to reproduce:

Install https-everywhere, Stylish or NoScript on a clean profile
Add icon to panel


Actual results:

HTTPS Everywhere and Stylish will only allow one other icon on its row: They both knock the third to a new row. If placed on the menu together, the text for Stylish is cut off.

NoScript's icons will allow you to place  icons on a panel rather than the normal  Additionally, the icons are cut off.


Expected results:

Each icon should be spaced evenly throughout the panel

This didn't happen when Australis was originally pushed to Aurora users.
