Add experimental search capability to about:preferences
This should be a text input somewhere in about:preferences that allows a user to drill down to a preference that they're looking for.
