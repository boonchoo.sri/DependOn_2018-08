Preferences search results show results from Firefox Account as not logged in
(X11; Linux x86_64; rv:55.0)   ID:20170413100144 CSet: 

I have enabled the experimental search a gave it a try. What I have noticed is that in the results, Firefox Account is shown as if I am not logged in to Sync. If I open the Firefox Account section before the search, it works correctly.

Steps to reproduce:
Open about:preferences having browser.preferences.search enabled and being logged in to Firefox Account (Sync).
Type "history" in the search field.

What happens:
At the end of the results list, Firefox Account shows as not logged in.

What should happen:
The results should display correct Firefox Account section.

Note:
If you open the Firefox Account section and try to search again, it will work until you close and open about:preferences again.
