[meta] Preferences search highlight should match the spec
Currently, we default to highlighting the selected text with (at least with the default theme) a purple background.

I believe the spec showed this search feature with a yellow highlight. Unfortunately, I can't find the spec that actually defines the colour, so I'm going to needinfo Tina to see if she can link it here.
