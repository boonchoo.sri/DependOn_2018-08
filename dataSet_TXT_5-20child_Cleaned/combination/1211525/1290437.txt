Fix and land the console netlogging tests
For some reason, the patch with tests (attachment  in bug  was never finished and landed.

At this moment, the browser_net_cookies.js and browser_net_post.js tests fail. The reason for both failures is that the information in the UI is checked before it arrives from the server.

The workflow for every network request is this:
- when the request is initiated, the server sends a networkEvent with the little info that is available at that time
- as more info is available, server sends networkEventUpdate events with "updateType" field that says "responseHeaders" etc. Very little info is sent as part of this event. For example, the headers count and size, but not their values
- client sends a "getResponseHeaders" request (or "getAnythingElse") and receives the complete data (if available)

The browser_net_cookies test fails because the cookies are not present when check at [1] is performed and when they arrive, there is no code that would update the component state.

[1] 
