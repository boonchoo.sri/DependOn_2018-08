DAMP (Talos): Wait for requests to settle before closing toolbox
While investigating a Talos regression (bug  I noticed that the toolbox close portion does not wait for RDP requests to complete first. This means there may be many tasks in progress processing the reload that just took place, and those could make the close numbers much higher than they would be in real usage of a particular tool. It also leads various errors being logged when the toolbox closes with requests still in progress.

If we agree it's good to address this for more accurate close measurements, we can do so by adding a utility function to wait until all requests have settled.

Making this change will invalidate previous DAMP results.
