Responsive design mode triggers scrollbutton-down toolbarbutton as if another tab exists.
STR:
Open as many tabs as to create the right hand arrow for selecting more tabs
Open 
Open dev tools
Trigger the responsive design mode button
See the right arrow highlight orange(Ubuntu theme), then to a black arrow

Expected if on the right most tab the arrow should be a pale grey rather than black and shouldn't highlight.

Closing the RDM changes the arrow back to the grey color.
