Navigating to a non-remote URL from RDM breaks the tab
STR:

Open some page like mozilla.org
Open RDM
Enter about:addons in the location bar

ER:

Add-ons manager loads, can navigate to other pages like normal.

AR:

Add-ons manager does actually load, but the tab is then "broken" after that, like the location bar doesn't work anymore, etc.
