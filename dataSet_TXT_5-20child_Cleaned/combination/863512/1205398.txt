[e10s] can't properly drag-n-drop tabs on OSX because dndPanel gets underneath the mouse pointer
Created attachment 
patch

The panels should be transparent to the mouse, otherwise it's not possible to reorder tabs or drop them in another window, as the panel sits underneath the mouse pointer.

(Don't know if the panel being underneath the pointer is a bug or not, but in any way this fix probably makes sense by itself)
