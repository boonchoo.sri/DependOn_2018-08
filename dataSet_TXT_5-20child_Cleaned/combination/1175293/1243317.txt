browser_UITour_defaultBrowser.js isn't testing anything but would fail on OS X
WHile making this test work with e10s it began to fail because the test was never waiting for the result before. The problem is that one should either use taskift or the done argument but they shouldn't be mixed since taskify implies you're using a task and so the `done` method will be called automatically then the task is done. Since none of the Task functions yielded anything, the result of the async operation results weren't checked.

Jared, the tests fail on my OS X machine now but I'm not sure what the expected results are. Can you build on top of my patch which waits for the checks and fix it so it will pass on all platforms?

Thanks
