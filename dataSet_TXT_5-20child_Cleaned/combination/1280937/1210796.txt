Display keyframes' timing-functions in the animation-inspector
Bug  adds a keyframes and animated properties panel in the animation-inspector, when an animation is selected.

When a keyframe gets selected, we should allow users to  its timing-function. We could reuse the cubic-bezier editor tooltip for this.

See also bug  which is about showing the timing-function for the overall animation.
