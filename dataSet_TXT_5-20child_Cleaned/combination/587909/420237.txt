on Vista the hover effect in the location bar should match menuItem hover effect
The closest analog to the richlistbox in Windows Vista is an explorer window in "expanded tile" view, or rich dialog box. In either of those cases, the hover effect on items mimics the menuItem hover effect.

To do this for the locationbar richlistbox, use the following chrome:

.autocomplete-richlistitem {
-moz-appearance: menuitem ! important;
border:  ! important;
color: inherit ! important;
}
