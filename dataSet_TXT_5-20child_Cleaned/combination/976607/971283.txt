Preferences links to Terms of Service and Privacy Notice are too high and and too large
Much like my city’s mayor, the links to the Terms of Service and Privacy Notice in Preferences are too high and too large.

Current:


Proposed:


Please bump font size down, and align vertically with [?] icon.
