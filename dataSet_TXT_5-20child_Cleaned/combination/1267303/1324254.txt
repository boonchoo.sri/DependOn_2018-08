Inspecting an element selects the <body> element before the right element is selected inside the node view
When an element is inspected within the page, the <body> element is selected first before the actual element is selected. This causes a poor UX.
To reproduce the issue right-click this text and choose 'Inspect Element' from the context menu.

Sebastian
