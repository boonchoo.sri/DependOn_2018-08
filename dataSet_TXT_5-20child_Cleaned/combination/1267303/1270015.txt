Smarter suggestions for DOM properties
The auto-completion within the Web console's command line should be smarter with its suggestions.

E.g. when entering a 'd' the probability that the user wants to enter 'document' is much higher than for 'decodeURI', which is the current default suggestion.

It would improve UX a lot if the most common properties, functions, etc. would be selected by default similar to what was done for CSS properties in bug 

To get 'document.getElementById' you'd then only have to do this:

d<Tab>.<Tab>

instead of

do<Tab>.getElementB<Tab>

Firebug has this already, see 

Sebastian
