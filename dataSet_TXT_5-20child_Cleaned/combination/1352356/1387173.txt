Window controls misplaced after switching to touch density and back again
STR:

Go to customize
Popup the density options
Hover over touch then move off it again.

When in touch mode the window controls grow larger but they don't shrink again afterwards.

Tested on Windows 
