Create a search field widget
A general search field widget should be created, which can be reused by all the panels.

It's features would include:
- single content search
- multiple content search
- filtering instead of searching
-  options
- search suggestions
- showing number matches
- indication for no matches
- focus shortcut
- clearing the field
- go to line feature

This was first proposed at 

Sebastian
