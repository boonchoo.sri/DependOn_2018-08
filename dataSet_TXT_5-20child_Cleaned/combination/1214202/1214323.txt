Create a TreeTable widget
To handle both tree and table like features for the performance tool call  and heap views in memory tool. Should have all requirements of both TreeList and Table widget components.

Ideally we will be able to compose the properties of both the TreeList and Table components to do this.

Requirements:
*  rows
* Only render what's in view
* Reorderable columns
* Resizable columns
* Sortable columns
* Filter data via predicate
* Extensible for all above use cases
