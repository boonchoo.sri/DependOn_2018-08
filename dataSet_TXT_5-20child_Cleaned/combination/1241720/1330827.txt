"JSON", "Raw Data", and "Headers" buttons have incorrect hover styling
Created attachment 
showing (correct) hover styling used for buttons in developer tools (gray background, default cursor)

When you hover over the "JSON", "Raw Data", and "Headers" buttons, the following styling issues apply:
the cursor is changed to the text cursor, rather than the default cursor
the background color of non-selected buttons does not change to gray

to be consistent with the buttons in developer tools, the above  items should be addressed.

I am attaching a screen capture which shows how the hover state appears (correctly) in developer tools. I have not included a screen capture of how this looks in json viewer because for some reason when I take a screen capture it shows my cursor differently from how my cursor actually appears when hovering the json viewer buttons.
