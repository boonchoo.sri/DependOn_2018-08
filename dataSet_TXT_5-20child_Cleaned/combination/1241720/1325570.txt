The checkboxes from modal can be  using the right click
[Affected versions]:
- Firefox  (2016-12-22), Firefox  (2016-12-22)

[Affected platforms]:
- Ubuntu  Windows  Mac 

[Steps to reproduce]:
Prerequisites: Make sure that devtools.responsive.html.enabled pref is set to true.

Launch Firefox.
Enable RDM.
Open the list of devices and click on the 'Edit list...' button in order to open the modal.
Click on the check boxes using the right click.

[Expected result]:
- The context menu is displayed.

[Actual result]:
- The context menu is displayed and the checkbox is 

[Regression range]:
- This is not a regression. Here is the pushlog in order to see when the modal was implemented:  .
