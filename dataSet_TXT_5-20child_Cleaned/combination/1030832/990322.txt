[New Tab Page] Don't show multiple thumbnails from the same eTLD+1
I'm starting to see the better thumbnails on release builds - yay :) But anecdotally, I'm seeing user's thumbnails on about:newtab have many thumbnails from the same TLD. Eg, one specific user has:

*  thumbnails from their ISP (member login page, plus account usage page)
*  thumbnails from google (one with search results, the other the search page)
*  youtube thumbnails, each for different videos (and for some reason, one with a thumbnail of a "502 Server Error")
*  facebook login page.

This user has many other very commonly-used sites which aren't displayed.
I wonder if it might be better to try and restrict too many thumbnails from the same TLD to be displayed by default. I'm sure the devil is in the detail, and this might be judged INVALID, but figured it worth throwing out there.

CC and needinfo? boriss for a UX perspective.
