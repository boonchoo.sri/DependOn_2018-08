Enhanced Tiles (history tiles enhanced with  states)
Created attachment 
ui spec v1

We'll be replacing history thumbnail tiles with an enhanced directory tile. This initially means showing an unhovered image (e.g., picture, article clip) if available and a hovered image (marks like the current directory tiles).

dcrobot has put together an interactive mock (with slightly more functionality than described in the attached):


I'll file various broken down bugs for various pieces of the design:

- grid layout fills up available space (bug 
- update page layout: adjust  around tiles, grid, page
- update tile styling (unhover, hover, pinned): resize tiles to  center text, radius, shadow, 
- update search styling: fixed  column width
- add unhovered and hovered images for enhanced tiles
-  tile actions 
-  pieces
