Add gear button with doorhanger configuration of newtab page
From the prototype: 

There'll need to be a gear icon to customize the new tab page with presets:
- enhanced
- classic
- blank

Where these presets toggle various  customizations for the page:
- enhanced images
- search box
- tiles
-  area

enhanced = all
classic = search + tiles
blank = none
