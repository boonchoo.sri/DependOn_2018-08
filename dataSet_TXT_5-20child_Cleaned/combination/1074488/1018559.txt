Conditional break when function is on stack
Suppose you want a breakpoint to trigger only when another function is on the stack. I wrote a simple JSM to enable that capability, but I'm pretty sure there's no similar capability in the debugger. I'd really like that built-in if someone can come up with a good user interface.
