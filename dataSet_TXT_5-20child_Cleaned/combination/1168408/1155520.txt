Add telemetry for Pocket integration
Exact probes TBD as the Pocket implementation progresses.

Possibilities:

* Clicks to Save to Pocket entry points (toolbar button, context menu, RV button)
* RTT for Save to succeed
* Failure rates for Save
* How many times we offer the  FxA panel, and user reaction
* Overall panel views
