View Pocket List from Bookmarks toolbar icon opens Log In page in Private Browsing mode
Reproducible with Firefox  beta  latest Aurora  (2015-05-18) and latest Nightly  (2015-05-18) under all platforms.

Preconditions:
User is signed into Pocket
Steps to reproduce:
Launch Firefox and open a New Private Window
Open a website that's currently not saved to Pocket and click the Pocket button from the toolbar.
From the Bookmarks icon from toolbar, click "View Pocket List"

Expected results:
The user is redirected to Pocket's "My List" page in normal browsing window

Actual results:
The user is redirected to Pocket Log In page 

Note: When choosing the option "View Pocket List" from Bookmarks menu, the user is correctly redirected to Pocket List in normal window
