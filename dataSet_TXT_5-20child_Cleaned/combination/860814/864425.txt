Give toolbar-menubar, TabsToolbar and PersonalToolbar customization targets.
We want to make toolbar-menubar, TabsToolbar and PersonalToolbar to be customizable with the new Australis customization mode.

An immediate difficult that I see with this is that toolbar-menubar and TabsToolbar both have toolbar bindings for things like autohiding and dragging...and the way we're currently doing things (creating our own toolbar binding) means that we either need to write our own (hopefully simplified) copies of those extra bindings, OR we change tack and have our toolbar binding subclass the toolkit toolbar bindings.

Beyond these two, not sure there are other options.

Blair - is there a good reason to stick with our own bindings instead of subclassing toolkit's toolbar bindings?
