Reimplement access to context-specific language properties
The landing of bug  means that the only way to find out what language a particular selection of text is in was removed from Firefox. You used to be able to right-click on a selection of text (e.g. <p lang="en-US">This is American  and find out what language it was in (and from region).

Bug  was intended to expand upon this access by including all the new data allowed by BCP  more languages, more locations, scripts, etc. However, the removal of the Properties context menu meant that that expanded code had no place to live.

I recommend that this data be exposed somewhere. I honestly don't care where, as long as it's available (outside of the source code viewer). (This request is along the same lines as bug 
