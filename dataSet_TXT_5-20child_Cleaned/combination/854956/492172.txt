[meta] Store higher resolution favicons in places



I would like Firefox to store favicons with more than  pixels. The  version can still be kept in places.sqlite, but a higher, perhaps even full resolution version should be kept somewhere.

This is a good idea for several reasons:

Better Cloud - gives Mozilla Prism a potentially clearer default icon to display, as well as for desktop shortcuts (see  for parity with gchrome and safari).

Better Extensions - more recognizable representation of a website than a thumbnail in about:tab as well as for opera speed-dial type extensions.

Better Mobile  Convergence - useful for Fennec shorcuts, just as the iPhone has implemented a  apple-touch-icon.png for a higher quality representation of a website.

Better Polish - gives users an opportunity to have large toolbar icons without blurriness.

Ditto - opportunity to create a more beautiful bookmark properties dialog that includes an icon (just like  do on the desktop "properties" or "Get Info" dialogs).

Although most sites don't use large icons now, they will if Firefox puts it into the browser (same rationale for including Geolocation in  Some formats also support multiple resolutions in a single file (e.g. .ico).


