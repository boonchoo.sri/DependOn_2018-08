Use HiDPI ICO favicons throughout the UI
There are still many favicons throughout the UI that do not support HiDPI resolutions. We should append the #-moz-resolution fragment to this favicons URIs to get the right sized images, when available.
