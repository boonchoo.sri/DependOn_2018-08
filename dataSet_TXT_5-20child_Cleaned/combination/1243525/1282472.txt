Add camera effects to screenshot
The screenshot implementation in the RDM does camera effect simulation like flashing the screen and camera shutter sound. We should add these effects to the RDM to unify all the different screenshotting experiences.

Also, the shutter sound could be more subtle. And it should be the same for the RDM and other screenshot options.
