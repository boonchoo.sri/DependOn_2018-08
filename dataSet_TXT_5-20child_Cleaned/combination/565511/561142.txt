Startup issue: Rebuild profiles on major upgrades (or have manual option to do so)
(Note: this is filed as a meta bug as part of the “Paper Cut” bugs since we assume that there are multiple existing bugs related to this behavior. Please make them block this bug.)

Some quotes from the Reddit “paper cuts” thread to illustrate this issue:

“Firefox takes  minutes and  seconds. Chrome is  seconds.”
“That’s insane. Make sure you are on Firefox  & try a fresh profile.”
“Well, shit.  seconds.”

“Many users on FX  have been upgrading since Firefox  This means they've been using the same profile for years which I'm sure is impacting performance. In my experience, creating a new profile is like reformatting and reinstalling Windows. Everything is fresh and quick. How about making it easier to do a "profile refresh" keeping only saved passwords and bookmarks?”

"Any upgraded installation older than two years runs very slowly. The most notable problem is typing in the location bar. The first time you do this in a new session it will lockup the UI for about  seconds. Subsequently you will get small lockups of about  seconds for the remainder of the session. I've noted this on two separate machines. Clearing the profile fixes it."


Recommendation:
WE HAVE THE TECHNOLOGY, WE CAN REBUILD THEM

Possibly  bookmarks and passwords? Think: “Local Weave” that puts the existing profile in a safe location, syncs to disk, then recreates a new profile. I'd prefer this to be part of the major upgrade, similar to how iTunes does a “updating your Music Library” on first start after upgrading.

People have old and corrupted profiles, we need to help them start fresh.
