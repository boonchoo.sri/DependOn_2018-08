Startup issue: Don’t ask about updates all the time
(Note: this is filed as a meta bug as part of the “Paper Cut” bugs since we assume that there are multiple existing bugs related to this behavior. Please make them block this bug.)

This obviously affects a lot of different things, including the  project and updater code across the different platforms.

Recommendation:
- Silent updates for add-ons, with possibility to enable explicit upgrade notices if you want (being handled in the  project
- Firefox updates should be applied on shutdown, never on startup (ideally in the background while using the browser, but that might be harder)
