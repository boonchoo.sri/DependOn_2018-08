The Network Throttling selection and the drop down button are not vertically centered
[Note]:
- Logged based on Bug  Comment 

[Affected versions]:
- Firefox  (2016-10-24)

[Affected platforms]:
- Windows   Ubuntu  x86.

[Steps to reproduce]:
Launch Firefox.
Enable RDM and observe the Network Throttling section.

[Expected result]:
- The elements from the Network Throttling section are correctly positioned.

[Actual result]:
- On Ubuntu and Windows OSs, the elements from the Network Throttling section are not vertically centered.
- On Ubuntu OS, the size of the text is not the same with the size of the "Responsive Design Mode" string.
-Here is a screenshot with the issues described above: 

[Regression range]:
- This is not a regression.

[Additional notes]:
- This issues are not reproducible on Mac OS X.
