Experiment with simpler network throttling profiles
On IRC, :snorp brought up that the current network throttling profiles are very arbitrary (as we known on the RDM team all along).

He suggested experimenting with something more generic like:

* Slow
* Slower
* Tar Pit

so that it doesn't feel like the options are meant to match real connection technologies (since they won't anyway).

Defining "success" for an experiment with the labels could be tricky, but perhaps increased usage of throttling would suggest it helped...?
