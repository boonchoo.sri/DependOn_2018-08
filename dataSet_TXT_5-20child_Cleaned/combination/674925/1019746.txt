Tab Detaching: transitions when dropping to open a window
When the tab is dropped such that a new window should open, grow the window from the place where the drag feedback image is.
