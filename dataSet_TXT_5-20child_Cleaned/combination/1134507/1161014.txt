The Reader View promo panel is not adapting properly to window and screen size
Reproducible on:
Nightly  (2015-05-03)

Affected platforms:
Windows  (x64), Ubuntu  (x64), Mac OS X 

Steps to reproduce:
Launch Firefox with a clean profile.
Reduce browser window width and place the window close to the left edge of the display.
Open a Reader View compatible page, e.g. 
Resize the browser window to a much larger width.

Expected result:
The Reader View promo panel is displayed.
The Reader View promo panel adapts its UI to the screen and browser window size.
Modifying the browser window size does not affect the promo panel's UI.

Actual result:
- if there's not enough space at the left side of the screen AND the browser window is narrower, the promo panel is displayed as originating from outside the browser window.
- if the browser window width is significantly increased at once, the panel is temporarily shown as originating from a different spot.
