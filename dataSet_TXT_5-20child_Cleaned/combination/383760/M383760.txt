Add quit confirmation dialog
There should be a confirmation dialog on quit if the user has more than one webpage open. This also avoids having a confirmation dialog for every window that has tabs.
