Prefs UI for what to suggest in location bar isn't friendly to additional 
Currently, the UI for changing what should be suggested in the location bar results is a dropdown menu with the following items: History and Bookmarks, History, Bookmarks, Nothing.

Unfortunately, this makes adding additional items (and therefore combinations of items) very awkward.

For example, bug  adds tab matches to the location bar results. If this were to be added to the current prefs UI, the drop would have to include:

* History and Bookmarks and Tabs
* History and Bookmarks
* Bookmarks and Tabs
* History and Tabs
* History
* Bookmarks
* Tabs
* Nothing

This is not friendly, and is likely to result in someone's head exploding.

Rather than a dropdown menu listing every possible combination, a series of checkboxes could be used - a checkbox for each item, enabling or disabling that type of result.
