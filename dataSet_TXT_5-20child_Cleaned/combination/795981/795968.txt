Create the about:reader page
This bug is to set up the about handler for about:reader. The page should be a blank HTML page for now and should not have chrome privileges.

To do this, you'll need to add to  and  as well as add a new (for now) blank HTML page that will be loaded from 

You will need to set URI_SAFE_FOR_UNTRUSTED_CONTENT and ALLOW_SCRIPT for the page. See  and  for documentation on these two flags.
