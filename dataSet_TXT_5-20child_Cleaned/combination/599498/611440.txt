Smart abbreviation for URLs in the Web Console
Per a discussion in IRC we'd like to see "smart abbreviation" for URLs. The idea is that, for long URLs, we show the first part of the URL and the last component + first part of the query string. Some examples:

=> 
=> 
=> 

Ideally we'd like the user to be able to resize the window and see more and more of the URL appear.
