[Session Restore] e10s-style session restore
Reimplement a version of the Session Restore saveState() feature, e10s-style:
- all data collection should be handled by a content script, communicating with the (async) main loop by messages;
- all post-processing should be done only once data collection is complete;
- by the way, post-processing should be done on a worker thread.

Once this is done, we can think of extending the approach to async versions of the rest of Session Restore.
