Uncaught TypeError: Cannot read property 'complete' of undefined JavascriptField.onItemClick
User Agent:  (X11; Ubuntu; Linux i686; rv:10.0.2)  
Build ID: 

Steps to reproduce:

In the gcli demo app (version of gcli checked out from github master branch).
Commit hash = 

Start the gcli demo app.
Open in web browser.
Type {
Click in the list of suggested completions


Actual results:

Uncaught TypeError: Cannot read property 'complete' of undefined
JavascriptField.onItemClick

From  line 
No stacktrace available.



Expected results:

The selected item in the completion list should have been added as text to the input widget at the bottom of the page.
