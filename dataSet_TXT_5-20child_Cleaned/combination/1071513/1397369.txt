Adding a new Bookmark or Bookmark Folder in the Bookmark Manager and Bookmark Bar yields an empty alert box
Created attachment 
bug.png

User Agent:  (Windows NT  Win64; x64; rv:57.0)  
Build ID: 

Steps to reproduce:

Name Firefox
Version 
Build ID 
Update History
Update Channel nightly
User Agent  (Windows NT  Win64; x64; rv:57.0)  
OS Windows_NT 

Tested on fire
Open Firefox
Go to Library
Open Bookmark Manager
Right click bookmarks
Click "New Folder..." Item menu



Actual results:

Empty Dialog box


Expected results:

A dialog box to enter the name of the new folder.
