Setting Web Feeds in Application Preferences to "Add Live Bookmarks" then reselecting it appears to revert to "Preview in Nightly"
STR

Open Preferences
Select Applications
Next to "Web Feed" select "Add Live Bookmarks to Nightly"

-> It is correctly shown as selected.

Click on any other item
Click back on "Web Feed"

Expected Results

-> "Add Live Bookmarks to Nightly" remains displayed

Actual Results

-> "Preview in Nightly" is displayed
-> Console message: "ReferenceError: internalMenuItem is not defined"

This is a regression from bug  that I've found when working on enabling the no-undef rule.
