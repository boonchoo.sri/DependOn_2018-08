[e10s] Find bar doesn't search properly after detaching a tab to a new window
The bar should appear immediately, instead it is hidden and finding jumps to the wrong first result.
