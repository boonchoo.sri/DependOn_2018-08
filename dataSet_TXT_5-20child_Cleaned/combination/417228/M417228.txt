Bookmarks outside the three default folders aren't restored



Bookmarks that are stored outside the "Bookmarks Toolbar", "Bookmarks Menu" and "Unfiled Bookmarks" aren't backuped, leading to dataloss (example:  )





a bookmark called "XXXXXXXXXXXXXXX"
it, via the Library, at the top level (i.e. outside the toolbar, the menu, the unfiled bookmarks)
"Import and Backup" -> Backup

No "XXXXXXXXXXXXXXX" bookmark in the generated file


Either the bookmarks in the generated file, either the inability to put a bookmark there.

This may lead to dataloss if another problem happen. It already happened to some in beta2 -> beta3.

It is similar, though not identical (and thus not a dup) of bug  A common fix may be possible.
