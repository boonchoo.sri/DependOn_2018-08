Drag and drop in Bookmarks Sidebar allows bookmarks to be placed in top level ("All Bookmarks") folder
If a user opens the bookmarks sidebar, they can drag and drop a bookmark into the top level folder. This places the bookmark at the same level as the "Bookmarks Toolbar", "Bookmarks Menu", and "Unfiled Bookmarks" folders. Users should not be able to do this.

Note: In the Bookmarks Organizer, this drag and drop action will fail. It only works in sidebar.

Found in  (Macintosh; U; Intel Mac OS X  en-US; rv:1.9b2pre)  
