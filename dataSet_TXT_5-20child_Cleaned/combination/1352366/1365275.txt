Location and search bars become transparent on hover with a lightweight theme installed
I've only verified this on Win10 at this point, not sure if it's across all OSes or not.

STR:
* Install a LWT from AMO
* Hover the location bar

Expected results:
A similar box shadow effect as you get with the regular themes.

Actual results:
Location bar goes transparent and the LWT bleeds through, which can make text very difficult to read depending on the specific theme installed.
