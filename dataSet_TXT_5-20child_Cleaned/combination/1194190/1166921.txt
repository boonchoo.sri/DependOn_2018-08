Support storage inspector stores-cleared event
The localStorage inspector seems to update when keys are added to localStorage, but not when they're deleted. It would be great to have this to accurately reflect what's in localStorage.
