Multiple cookies with the same name not shown
The list of cookies appears to show only one cookie for each name, ignoring differences in path, domain, etc., thus a developer could think they only have one cookie when they might have several.
