Switch-to-tab does not work on always private browsing mode
User Agent:  (Windows NT  WOW64; rv:20.0)  
Build ID: 

Steps to reproduce:

- Check "Always use private browsing mode".
- Restart FireFox.
- Open any URL.
- Open other tab and input the same URL.



Actual results:

- "Switch-to-tab" does not work, that is, duplicate the same URL.
