Need Document Icons for Firefox
We need document icons for Firebird's assocations for MacOS X and Windows. Linux
can ride along with Windows.

We need several sizes for the different platforms, see bug  for a matrix.

Here are the icons required:

-  document
Windows:
this must have the *standard Windows document look* with a smaller Firebird
motif overlaid.
MacOS X
this must have the *standard OS X document look* with a smaller Firebird
motif overlaid.
- XUL document
Ditto above, but perhaps a different motif to indicate XUL.
- XPI
Icon style appropriate to  to indicate a package containing
content.

When I say "standard <platform> document look" I mean that the document
component of icon should look as if it had shipped with the OS - the same style
as others. This is so that we seem to integrate with the OS cleanly rather than
being a foreign intruder.

Mac icons should be high detail, high quality and perhaps have an aqua look.
