No shortcut key available for Attributes option via the Inspector context menu
[Affected versions]:
- latest Nightly  (e10s 

[Affected platforms]:
- Windows  
- Ubuntu  

[Steps to reproduce]:
Launch Firefox
Open the Inspector.
Right click on any node from the Markup View

[Expected result]: Keyboard shortcuts are displayed for every option available in the context menu.

[Actual result]: No shortcut available for Attributes option.

[Regression range]:
- Last good revision: 
- First bad revision: 
- Pushlog: 
- I guess this regression was triggered by bug  Brian, what's your input on this matter? Thanks in advance!

[Additional notes]:
- Screenshot → 
- On Mac OS X  none of the shortcuts are displayed, but all of them are working as expected when selected via keyboard; this behavior is present on both latest Aurora  and firefox  beta. Is this expected at all?
