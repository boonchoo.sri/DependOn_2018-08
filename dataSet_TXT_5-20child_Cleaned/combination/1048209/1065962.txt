update an already visible WebRTC doorhanger when a new gUM call occurs
Currently if a WebRTC doorhanger requesting permission for a website to use the camera is shown and the website requests permission to use the microphone, the microphone request won't be shown until the user answers the first request.

Instead, we should update the doorhanger to show the microphone request in addition to the camera request.

This also applies to requesting  sharing.
