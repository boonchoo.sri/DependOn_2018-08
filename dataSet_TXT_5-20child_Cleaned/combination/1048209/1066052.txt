show a global sharing doorhanger attached to the global WebRTC sharing indicator
This should look mostly like the merged doorhanger attached to the url bar that will be implemented in bug  but will contain all the devices shared with any tab.

In attachment  see images  and 
The global "Stop sharing all devices" button will be implemented in a separate bug.

There are platform dependencies on having a list of all the streams currently shared and the associated hostnames.
