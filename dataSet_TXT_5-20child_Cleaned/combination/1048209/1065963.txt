show the list of shared devices in the WebRTC sharing doorhanger
Currently we show only that a 'camera' or 'microphone' or 'window' is being shared; we should show the name of the device that the user gave permission to use.

This requires some platform changes, as currently we only have the mediaCaptureWindowState method of the media manager service which returns a set of booleans.
