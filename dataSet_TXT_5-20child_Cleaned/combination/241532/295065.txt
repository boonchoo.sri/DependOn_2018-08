Add install switch to specify which shortcuts to create.
(Windows; U; Windows NT  en-US; rv:1.8b2) 


To make scripted installations easier, there needs to exist an installer switch
that would allow the user to keep the portion of the installer that asks about
creating shortcuts from appearing as well as prevent their creation at all. I
was able to (for some freaky reason) get this effect with (I believe) the -ispf
switch, but that is apparently not the correct behavior for that switch per bug
Adding this enhancement would make it easier for people who have
multiple versions of FF installed for testing purposes and don't want to worry
about it creating unused and ultimately annoying shortcuts all over the place.
