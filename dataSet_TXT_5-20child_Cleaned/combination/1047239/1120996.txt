[UX] Reconsider add-on installation flow
With add-on signing becoming more rigorous, we can make the flow for approved add-ons easier. At the same time, we need UI for when an add-on gets blocked.
