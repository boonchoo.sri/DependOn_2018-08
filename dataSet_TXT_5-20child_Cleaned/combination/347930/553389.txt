No tabbar toolbar option in View -> Toolbars
There is no tabbar toolbar option in View -> Toolbars. If this toolbar is supposed to be customizable like a regular toolbar then an option should be added to the view -> toolbars menu.
