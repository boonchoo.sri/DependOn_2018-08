[RTL][Synced Tabs] The synced tabs sidebar has wrong direction in RTL locales
User Agent:  (Windows NT  WOW64; rv:47.0)  
Build ID: 

Steps to reproduce:

Start latest Nightly in RTL locales with synced environment
View the Synced Tabs sidebar


Actual results:

The Synced Tabs sidebar is displayed as LTR


Expected results:

The Synced Tabs sidebar should be displayed as RTL in RTL locales
