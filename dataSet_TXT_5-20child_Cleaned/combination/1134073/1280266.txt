Netmonitor JS stack traces shouldn't include chrome frames
Requests JS stack traces in Netmonitor sometimes show  stack frames. This is confusing for users - they should only see the JS code from the page content.

The  frames should be shown only in browser toolbox.

Two examples of this behavior:

When the page is reloaded, the main document request shows stack trace from the browser calling into webNav.reload at 

When issuing a request from the console (i.e., type "fetch('x')"), the request stack trace contains frames like:

WCA_evalWithDebugger 
