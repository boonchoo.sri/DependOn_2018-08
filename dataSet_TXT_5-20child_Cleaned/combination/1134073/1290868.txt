"Cause" popup should trigger on whole table cell not just "JS" bubble (like "File" cell for image popup)
Created attachment 
JS_vs_whole_column.png

This is a spinoff of the  part of 

>>  In the netmonitor, when hovering image names (the whole "File" column,
>> not just the supermini image icon) then the popup is show. I think that the
>> "Cause" column should behave the same. Not only show the popup when the "JS"
>> icon is hovered but also when the "cause name" (img, script, stylesheet) is
>> hovered.

> Yes, I'd also like to show the popups when hovering on the whole table cell.
> But our current tooltip code doesn't support triggering a popup over some
> element and then anchoring its arrow to another element. Was discussed some
> time ago in Bug  (see item #3 in comment 


See screenshot.
