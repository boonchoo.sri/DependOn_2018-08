URL autocomplete breaks keyword bookmarks
This is an edge case of the new URL autocomplete feature (see bug  c0).

Steps to reproduce:
bookmark a page, e.g. facebook.com, and assign it a keywork, say "fb"
visit fbackup.com or any other url with domain name starting with "fb" to add it to your browsing history
type "fb" in the urlbar and hit ENTER

Expected results:
fb is recognized as a keyword, Firefox opens the associated bookmark, facebook.com

Actual results:
Firefox autocompletes the URL with fbackup.com and gets you there as you hit ENTER.


I think that keywords should have priority over URLs, since they are assigned by the user.
