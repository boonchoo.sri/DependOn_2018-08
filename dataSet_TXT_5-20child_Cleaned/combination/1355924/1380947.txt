button should always look like "ready"
similar to bug  but this bug is not about distracting.

The stop button should be available as soon as the load happens.
however, with the animation, the stop button doesn't look like ready until the animation ends,
especially, the "x" icon (that means "stop") is not displayed until the animation ends.

for example, if you click "reload", clicking the button again behaves as "stop", but the appearance doesn't match until the animation ends.
the same thing happens for "stop" => "reload" case.

It means that the appearance doesn't explain the feature, and I think it's too bad.

IMO, there shouldn't be any kind of transition animation between them.
