New toolbar icons in tango style



This bug is actually part of bug  (Tango Style theme for better Linux UI integration).

As a first part of the tango theme, here we have the new images containing the icons needed for small and large toolbars.

We will use the same image for normal, active and hover, so the bitmaps only contain normal and disabled states. Note that most toolbar icons are taken from GTK now, so this new image map only includes icons for downloads, bookmarks, history, tab-new and window-new.

The tar also contains a CSS to override and test the new icons. No complete patch, as I am currently unable to build Firefox from source.


