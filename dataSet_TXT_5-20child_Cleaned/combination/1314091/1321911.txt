Hovering over light or dark compact themes only ever previews the dark theme
Created attachment 
theme-hover.gif

STR:

grab the try build from bug 
go to Hamburger menu => customize
click on the theme selector button to bring up the menu
hover over either the light or dark compact themes

ER: hovering over the light theme should previews the light theme

AR: hovering over the light theme actually previews the dark theme

I also noticed that if the light theme is currently applied, hovering over either light or dark previews only the light theme.

See attached .gif
