Restore borders to the toolbars for Vista Classic



Bug  removed toolbar borders from the Vista theme. While this looks fine in the default Vista theme, it does not look very good in Classic, especially since the Classic toolbox lacks a bottom border to separate the toolbars from the content.





