Restore access keys to new Context Menu
We've added icon navigation to our context menus in Firefox, but in doing so have removed some of the keyboard access keys.

Let's restore:

B - Back
F - Forward
- Refresh
M - Bookmark
