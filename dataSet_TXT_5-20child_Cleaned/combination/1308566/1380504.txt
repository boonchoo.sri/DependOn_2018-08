Object inspector does not show accessor properties
Open the web console and enter this code:

({a1:1, a2:2, a3:3, a4:4, a5:5, a6:6, a7:7, a8:8, a9:9, a10:10, get foo(){return 

The getter does not fit in the preview. Try expanding the object: it does not appear. Same for setters.
