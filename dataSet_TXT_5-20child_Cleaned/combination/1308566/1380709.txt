Object inspector does not show  entries.
Open the new console frontend
Enter new Map([[0,0],[1,1],[2,2],[3,3],[4,4],[5,5],[6,6],[7,7],[8,8],[9,9],[10,10]])
Not all the entries fit in the preview, so expand the object

Result: object inspector does not show the Map entries.
