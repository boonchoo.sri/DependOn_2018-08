E10S: Every time all tabs crash but I don't have the cause or origin (process isolation not work)
Created attachment 
Captura de tela de  

User Agent:  (X11; Ubuntu; Linux x86_64; rv:43.0)  
Build ID: 

Steps to reproduce:

Every time all tabs crash but I don't have the cause or origin.

"Bad news first: This tab has crashed
Now for the good news: You can just close this tab, restore it or restore all your crashed tabs."

I need tab  isolation!


Actual results:

All tabs crash, but not have reason.



Expected results:

Tab isolation and ways to trace origin of problem.
