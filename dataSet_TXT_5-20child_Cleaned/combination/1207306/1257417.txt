[e10s] Pinned tabs aren't loaded when the content process crashes
STR:

Content process crashes
I click to report the crash and restore the tab

Results:

Pinned tabs are unloaded.

Expected results:

Pinned tabs should reload when the content process is restored.
