Large memory usage after leaving the console open
Created attachment 
memory-report.json.gz

My Firefox has been very slow, using almost  of memory and  of CPU for the last couple hours. I've noticed that I had accidentally left in the background a browser console window.

about:memory shows:

MB (100.0%) -- explicit
├──2,315.89 MB (47.87%) ── heap-unclassified
├────824.87 MB (17.05%) -- heap-overhead
...
├────781.28 MB (16.15%) -- js-non-window
│ ├──679.33 MB (14.04%) -- zones
│ │ ├──616.00 MB (12.73%) -- zone(0x11a550000)
│ │ │ ├──266.00 MB (05.50%) ── unused-gc-things
│ │ │ ├──201.19 MB (04.16%) -- compartment([System Principal], Addon-SDK (from: 
│ │ │ │ ├──174.06 MB (03.60%) -- classes
│ │ │ │ │ ├───90.40 MB (01.87%) -- 
...
├────639.51 MB (13.22%) -- window-objects
│ ├──316.15 MB (06.54%) -- top(none)
│ │ ├──305.65 MB (06.32%) -- detached
│ │ │ ├──270.80 MB (05.60%) -- 
│ │ │ │ ├──232.83 MB (04.81%) -- dom
│ │ │ │ │ ├──232.69 MB (04.81%) ── orphan-nodes

Anonymized report attached.

I'm not exactly sure of what happened, but I think I left the console open for at least  days. Closing it doesn't seem to free memory. I have gmail open in an app tab, and I suspect it causes a lot of output to go continuously to the console.
