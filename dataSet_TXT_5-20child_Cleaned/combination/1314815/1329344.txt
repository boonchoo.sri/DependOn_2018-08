Developer console increasing memory consumption on each reload
Created attachment 
memory_reports.tar

User Agent:  (X11; Linux x86_64; rv:51.0)  
Build ID: 

Steps to reproduce:

Refreshing the page with developer console open results in high memory consumption over time. Attached are the memory reports before and after  consequtive reloads (ctrl+shift+r) of the same page with the developer console open.


Actual results:

Running out of memory over time results in system becoming unresponsive under huge system load.


Expected results:

Memory consumption should not increase that much on each reload.
