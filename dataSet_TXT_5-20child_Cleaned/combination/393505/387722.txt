streamline search UI in the bookmarks sidebar
see the bug URL for a mockup.

* becomes visible as user starts typing
* options for titles, tags, text, notes (, uri?)
* need to figure out what the default selected are
* need to play with it, see if we should use checkboxes as in the mockup, multiselect or scrolly list of checkboxes?
* if there are results, shows a "save" button

alex, can you explain what you intended the "plus" button in the mockup to do?
