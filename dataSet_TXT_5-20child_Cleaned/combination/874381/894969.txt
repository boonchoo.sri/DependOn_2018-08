[SessionStore] Cache generated JSON
Bug  introduces a mechanism that lets us cache tab data to avoid re-collecting data. The logical next step is to also cache pre-serialized data to avoid having to re-serialize all the time.
