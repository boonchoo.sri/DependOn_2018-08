Copying javascript: URL from location bar replaces spaces with %20
Steps to reproduce:
Type (or paste) into the address bar:
javascript:alert(1 + 
Cmd+A
Cmd+C

Result: what's copied to the clipboard has %20 in place of spaces.

This is annoying because when I start writing a bookmarklet, I often type a bit into the address bar and copy it for a "backup" just before executing it. If it includes %20 instead of spaces, it's much harder to read and edit.

Possible fixes:
A) Leave javascript: and data: URLs alone when copying.
B) Leave URLs being typed (as opposed to URLs of pages being displayed) alone when copying.
