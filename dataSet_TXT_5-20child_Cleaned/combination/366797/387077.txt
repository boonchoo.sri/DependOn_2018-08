Rename some class names of the new Location bar
Created attachment 
patch

The current class names (textbox-presentation-*) are neither very clear nor generic enough, given that the formatted URL stuff could become a separate binding for use outside of a textbox. I'd like to address this before others start adopting their themes.

The patch also fixes a bug in the stylesheets (.textbox-presentation-slash doesn't exist anymore).
