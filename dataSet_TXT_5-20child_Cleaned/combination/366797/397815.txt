strange behavior when entering URL with escaped percent character %25
(Feel free to resummarize, I'm not clear on exactly what the problem is.)

When I enter  into the URL bar, I'm redirected to a Google search page that has "é" in the search field (so "é" is what's submitted to the server). When I enter  I get the same result, and the URL bar changes to display  I would expect the second URL to result in a Google search page with "%E9" in the search box.
