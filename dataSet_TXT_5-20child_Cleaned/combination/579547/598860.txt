[About dialog] Collapse trademark area if it is not present (unbranded builds)



From Bug  Comment 

Then the trademark area of the dialog should be collapsed, either by making the
"content" longer of making the whole dialog shorter. The big, empty space at
the bottom of the dialog doesn't look good, and it gives the false impression
that something should be there.


