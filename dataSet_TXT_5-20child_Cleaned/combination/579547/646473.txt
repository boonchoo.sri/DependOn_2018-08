"End User Rights" reads like a protest sign (should be End-User Rights)



I realize that "End User Rights" is supposed to indicate the rights for the end user, but not everyone knows what an "end user" is, and it sounds like Firefox is a proponent of ending rights for users.

Kind of an "Eats, Shoots, Leaves" scenario.

How about changing the link text to "About Your Rights"? That's what displays when you click on it, anyway.


