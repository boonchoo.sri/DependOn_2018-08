favicon.ico image still used even if the image is sent with  status



My website has no favicon.ico, so I was surprised to find that Firefox would
display one that seemed to match my site. I realized that it was a shrunk down
version of my  image. My webserver generates and sends images instead of
standard HTML error pages if the browser's accept header tells the webserver
that it prefers an image. I added that feature precisely because I noticed that
gecko was smart and would send more appropriate accept headers for inline
images, and I thought that my webserver should be just as smart and satisfy its
request. Yes, it does send these images with  status codes.

To see what the  images look like at full size, see the examples on this page:

Or just link to a non-existent file on my domain in an img element.




Load 
Look at the tab image


The  image appears.


It should act as if no favicon.ico were found.
