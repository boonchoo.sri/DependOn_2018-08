Text on icons should be aligned onto a straight line



If the user selects "Text and Icons" from the "Show"-section in the customize
dialogue, he'll get a text underneath the icons in the toolbars. This text isn't
aligned onto a straight line. Instead, some icons are misaligned - at least with
the standard theme.




Open the customize dialogue.
Select Show: Text and Icons and select "Use Small Icons".
Click 'Done'.


The Toolbar produces the icons with some text misaligned.


The Toolbar should produce the icons with rightly aligned text.
