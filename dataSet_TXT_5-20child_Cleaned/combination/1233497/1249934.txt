[e10s] "Unsafe CPOW usage forbidden" trying to use "cookie list"
Created attachment 
Stack accompanying "unsafe CPOW usage forbidden" message

Using the latest  nightly (20160220030407), even in a fresh profile, I'm unable to use any of the "cookie" commands in the commandline toolbar. Using any of them results in a  reporting "unsafe CPOW usage forbidden" with no other apparent effect. I've attached the stack reported in the browser console.

After disabling e10s, the cookie commands work as expected.
