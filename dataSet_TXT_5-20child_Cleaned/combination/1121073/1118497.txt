Rename "parameters" tab to "properties" in web audio editor
"Parameters" implies that these are AudioParams, but lists both AudioParams, as well as unique properties to that node type (doesn't display channel and context information, but does display "type" on an oscillator, for example, which is not an AudioParam).

"Properties" would correctly identify both properties and AudioParams, which are stored as properties on the nodes.
