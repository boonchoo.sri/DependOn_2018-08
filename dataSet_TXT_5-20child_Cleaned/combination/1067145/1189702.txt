CSS animation re-run when a link is clicked
Created attachment 
off-canvas-navigation-1.html

User Agent:  (Windows NT  Win64; x64; rv:42.0)  
Build ID: 

Steps to reproduce:

I have created a web page (attached) with an off-canvas navigation panel.

To reproduce:

Open the attached HTML file in Firefox or Nightly on Windows  Resize the browser window to less than  wide. Click the hamburger icon in the top-right corner. The off-canvas panel will reveal.

Next, click any of the navigation links inside the off-canvas panel ("One", "Two", "Three", etc). Notice how, on click, the fadeIn animation on the overlay (which covers the content area) is repeated. It appears to flicker whenever an item in the menu is clicked.

No DOM or classes change dynamically when the menu links are pressed. And the overlay element (which has the fadeIn CSS animation) is not related in any way to click events within the off-canvas panel itself.

This flickering effect does not occur in the latest desktop editions of Chrome, Internet Explorer, Safari or Opera on Windows  I have not tested mobile browsers or Firefox OS, or other operating systems. I have tested with a clean installation of both Firefox and Nightly. The reported behaviour seems to be non-standard.

Feel free to email me directly if you need further information: 
