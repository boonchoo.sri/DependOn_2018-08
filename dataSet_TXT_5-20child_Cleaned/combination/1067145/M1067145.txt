[e10s] Get responsive design mode working in e10s
User Agent:  (X11; Linux i686; rv:35.0)  
Build ID: 

Steps to reproduce:

Entered responsive mode (Ctrl + shift + M)
Pressed the close button


Actual results:

Responsive design did not close


Expected results:

Should switch back to normal mode
