DOM panel rendering is slow
The DOM panel uses React to render its content (a tree). In case of huge amount of items to render (typically e.g. for the |window| object) it take a lot of time. We should utilize a virtual tree-view in this case and render only visible item.

Honza
