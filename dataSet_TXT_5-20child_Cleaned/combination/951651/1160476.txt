‘Bookmark this link’ context menu option doesn’t work
User Agent:  (Macintosh; Intel Mac OS X  rv:40.0)  
Build ID: 

Steps to reproduce:

Right clicked on a link, chose ‘Bookmark This Link’


Actual results:

Context menu disappeared, nothing else. This error appears in the browser console:

A promise chain failed to handle a rejection. Did you forget to '.catch', or did you forget to 'return'?
See 

Date: Fri May    GMT+0100 (BST)
Full Message: ReferenceError: linkURI is not defined
Full Stack: 







Expected results:

A bookmark to that link would appear in the Bookmarks  Recently Bookmarked menu.
