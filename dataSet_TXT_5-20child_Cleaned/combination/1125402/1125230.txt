[UX] Create specs for V1 of control center
Iterating on the work coming out of bug  we need to define all the states we think would be useful for a V1 of control center.

Objectives of this bug:
- Enumerate and mock up the states for V1
- Explore enough of the scenarios beyond V1 to make sure we're not blocking future development with the short-term work.
