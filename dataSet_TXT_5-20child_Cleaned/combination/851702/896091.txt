open a tab for users upgrading to Firefox  if we've reset their javascript prefs
Bug  added code to reset the "Enable javascript" preference to its default value on upgrade to Firefox  To avoid upsetting users who've done that intentionally, we'd like to add code that will open a tab on upgrade pointing to an explanatory page describing the reasoning and some alternatives (e.g. NoScript).

(We considered other options, like a notification bar or some such, but past string freeze that's not feasible.)
