When viewing Add-ons Manager in full screen, captions buttons are not visible
The Add-ons Manager now hides the browser toolbars. However, this means that the caption buttons (Min, Max, Close) disappear too; in this case, there is no way to leave full-screen mode without pressing F11 (which some users will not know) or switching tabs (a non-obvious way of leaving full-screen mode).

STR:
Open Add-ons Manager
Press F11
* No caption buttons visible
