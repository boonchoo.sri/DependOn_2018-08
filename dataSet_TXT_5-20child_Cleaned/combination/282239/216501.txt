Ability to pick and choose file associations when setting as default browser



Firebird, when set as default browser, steals file associations for .jpg, .gif,
.png, and also registers itself as default editor for .html

Mozilla gives option to de-associate image file associations, but firebird
currently does not. There are a lot of newbies on mozillazine complaining about
this. An example:






Download firebird
Set as default browser



All image filetypes are defaulted to open in firebird, and .htm files are
defaulted to be edited in firebird, with no option to revert it.



Firebird should ask before associating image filetypes, and not register itself
as a .html editor
