When choosing an alternative location via "Browse...", the Installer "defaults" to ns_temp subfolder



When installing either Firefox or Thunderbird, using the Installer build, if you
do not choose the default install location of c:\program files\Mozilla Firefox,
it defaults to the ns_temp subfolder, and you have to navigate to the folder you
want to install to.



Run installer program
When asked if you want to install to default directory, choose Browse
browse window opens up defaulting to c:\Documents and Settings\root\Local
Settings\Temp\ns_temp subfolder


I had to navigate to the folder location I wanted to install to.


It should open at the c:\program files folder, rather than some hidden
subfolder. That makes it a lot easier to find the folder I want.

This happens with the Installer version of both Firefox and Thunderbird, from

