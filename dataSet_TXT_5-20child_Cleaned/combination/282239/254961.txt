Installer usability problems - custom install - select folder to install - use entered entire path c:\xyz\firefox. dialog closes but doesnt change folder.



Noticed new user having usability problems with installer.
User (medium - high technical proficiency) started install.
user wanted to specify installation folder.
guided user to select custom.
User got a screen "firefox will be installed to <default dir> [browse]".
User wanted to edit the default dir. couldnt, since its not text field.
clicked browse.

Proceeded to enter entire path "c:\program files\misc\firefox" into Folder: textbox.
clicked OK. dialog closed, but selected folder unchanged.
clicked browse again. reenetered folder path. clicked [Make new folder] repeatedly.

tried combinations of these  repeatedly. gave up.


ps:
also noticed this minor problem:
when setup started, user clicked next and asked why it didnt give option of
choosing installation folder.
Told him to hit back to  option screen.
User said description of custom "you may choose individual options to be
installed" didnt mention selcting custom folder.

ps2:
"make new folder" problem also reported in below bugs.
in addition to moving focus as suggested, shud it provide a way of deleting the
new new folder created by mistake??



start install; select custom.
click browse
enter "c:\program files\misc\firefox" into folder textbox.
click OK



dialog closed. folder displayed has not changed!


folder shud have changed to user inputted one.

or even better: typical custom installation setup dialog's shud be used. i can
post some screenshots if needed.
this wud mean, at a minimum, displaying folder in editable textfield.

(same behavior in nightly)
