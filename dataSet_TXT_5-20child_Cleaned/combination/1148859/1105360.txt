Only enhance tiles that are explicitly enhanceable
Currently directory tiles with rollover images also enhance history tiles. Some partners want rollover directory tiles but not enhanced tiles, so we'll need to check a flag and only enhance then.
