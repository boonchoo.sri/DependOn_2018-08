multiprovider:  patch
Created attachment 
patch

This updates the Social module to use a pref for selecting the current provider. pref names are based on the provider origin to make matching easier. This also prevents Social.init from being called more than once.

A new notification is introduced, social:provider-set which is used to notify when the current provider changes. This is also sent during init when a provider is set.
