implement 'Never Share' persistent permissions for screen sharing
It makes no sense to have an "Always share" action for screensharing, because the user needs to select each time which application will be shared, but it would make sense for the user to have a way to never be bothered again by a specific website showing the screensharing prompt.
