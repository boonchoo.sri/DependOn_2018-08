address bar is not focused after new tab command in fullscreen



Missing feature from Firefox 




Toggle Full Screen (F11)
Open a new Tab (Ctrl + T)
Type a url and press enter

Nothing Happens.


Opened the url.

On Firefox  (tested on Win32 and Ubuntu Linux) when you open a new tab in fullscreen the address bar is automatically focused, just like when not in fullscreen, even if it was "invisible". On Firefox  the focus only works when not in fullscreen.

So, for example, if I want to open delicious in a new tab (I use keywords on bookmarks) I will type:
On Firefox  (Ctrl + t) delicious (enter)
On Firefox  (Full Screen): (Ctrl + t) delicious (enter)
On Firefox  (Ctrl + t) delicious (enter)
On Firefox  (FS): (Ctrl + t) (Alt + d) delicious (enter)

I had always put the address on the side of the Alt Menu, on Firefox  saves a lot of screen space.
