Modal dialogue stripped and presented in new Window after session restore
Created attachment 
Modal Before

FFx Version:
(Macintosh; U; Intel Mac OS X; en-US; rv:1.9a8pre) 


In Reference to Bug  and  After quitting the browser and
restoring the session (to break modal dialogue hang), the previously hung
modal dialogue is stripped off the original Yahoo Mail page and is
presented in a new browser window See Attached image "New Close Window".
This seems odd.

See Modal Before and Model After images
