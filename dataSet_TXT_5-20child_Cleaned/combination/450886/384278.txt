minimized windows aren't restored maximized although they were maximized before minimizing



When exiting and restarting Firefox, some of its windows are not restored to the original maximized state.




Start FF, open  windows and some tab in them
make sure all  are maximized
menu File  Exit (and confirm closing multiple tabs, if asked)
start FF again

windows restored maximized, one not (it is about half screen wide)


all  windows restored as maximized

Some tabs open a password dialog* at start, maybe that interferes ?

* - master password dialog (two times, known bug); HTTP login dialog
