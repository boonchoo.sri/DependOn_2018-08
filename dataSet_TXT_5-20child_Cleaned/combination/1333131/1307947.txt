Improve network event update stub generation
Originally posted by:linclark

see 

Network event update stubs are hard to generate. We have a good start in place in #283, but there are a couple of flaws still.

It's hard to come up with a unique name for update stubs. Currently we do a pretty good job, but there are still places where we use the same key for multiple stubs.
It's hard to determine how many packets we should listen for. Currently it's hardcoded to  but that number could easily change on the backend. Perhaps we should listen for a set amount of time in the stub generator instead.
Because the object being sent from the client is mutated in the client when new packets come in, it's likely that the stubs for certain messages are inaccurate... the object will have been mutated before we write it. We could potentially solve this by changing the client. In the client, we could break out the code that formats the packet in  Then it could be reused in the test. Eventually, I think we might want to rearrange the client so that it doesn't handle this at all, but that will impact net monitor so will require more planning.
