Change the hover behavior in summary section with downloading file
(In reply to :Paolo Amadini from comment #23)
> Comment on attachment 
> Bug  - Change the information details of the summary section in
> Downloads Panel.
>
> (In reply to Sean Lee [:seanlee][:weilonge] from comment #22)
> > The patch includes the following changes:
> > * The hover behavior in Summary Section when [showingsummary] is true.
>
> I haven't looked specifically at the styles now, but I think this part will
> be straightforward. You can even consider moving it to its own small bug to
> land sooner.
>

Based on the above discussion (bug  comment  this bug focuses on the hover behavior in summary section
