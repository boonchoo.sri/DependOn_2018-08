"Restore All Tabs" can fail when there are pre-existing tabs


(Windows NT  WOW64; rv:48.0)   ID:20160315030230

"Restore All Tabs" broken when there are "New Tab"(s) in the tab strip.
There is one tab failing in restoring.




Open  tabs with valid urls
Open "New Tab" , then [URL1] [URL2] [New Tab] now
Close all Tabs except [New Tab]
Alt > History > Recently Closed Tabs > Restore All Tabs

Actual Results:
"Restore All Tabs" fails.
There is one tab failing in restoring.


All Tabs should be restored.


Regression Window:


Regressed by: Bug 
