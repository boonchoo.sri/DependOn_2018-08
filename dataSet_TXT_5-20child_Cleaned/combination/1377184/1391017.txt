"Bookmarks Toolbar Items" icon is too small, when in Customization mode
Created attachment 
Small Bookmarks Toolbar Items icon

STR:
Open Nightly
Enter Customization mode, and drag'n'drop the Bookmarks Toolbar Items somewhere on the toolbar

ER:
The "Bookmarks Toolbar Items" icon should appear with a normal size.

AR:
It doesn't. A very small icon is visible.

See attached.

Tell me if pinpointing the regression range is needed.
