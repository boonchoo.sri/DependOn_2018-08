Support creating  importing  exporting configured Simulators in WebIDE.
With bug  Simulators become configurable in WebIDE.

That feature would be even more useful if one can:
- Configure a new Simulator from scratch,
- Export and share Simulators for other to import.
