Entries kept in cache when using Download Helper add-on with youtube
(X11; Linux i686; rv:20.0)  

Install Video Download Helper: 
Clear cache.
Start Private Browsing
Visit youtube and download a video using download helper
Exit private Browsing

Actual: private browsing data preserved (2 entries - one for the downloaded item)
Expected: no data preserved
