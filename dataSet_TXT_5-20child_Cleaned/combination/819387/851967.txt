Easy YouTube Video Downloader addon stopped working after dummy Private Browsing service removed
The "Easy YouTube Video Downloader" addon stopped working after bug  removed the dummy Private Browsing service. Is this a bug in the addon?

STR:
Install addon: 
Load any YouTube video page
Look for a "Download" button.

RESULT:
As of Nightly  the "Download" button is no longer displayed. I bisected mozilla-inbound changesets for that day and identified bug  as the regression point.
