about:debugging should display an error to a user if a reload fails
STR, once bug  lands:

Load an add-on with a valid manifest via "Load Temporary Add-on".
Edit the manifest for the add-on to make it incompatible with the current Firefox version (via strict_min_version  strict_max_version).
Reload the add-on from about:debugging.

Expected result:
An error message is displayed informing the user that the add-on could not be reloaded, including the reason.

Actual result:
The add-on is not reloaded, and the user is not informed.
