Should hide Firefox's workers unless show-platform-data is on
We should hide these workers unless someone is specifically debugging Firefox, and can add a button to show internal workers that will toggle devtools.show-platform-data (I don't think it'll gain much from separating out these prefs)
