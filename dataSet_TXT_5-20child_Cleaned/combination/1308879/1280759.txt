about:debugging service worker start button always creates worker thread in parent process
In e10s mode the about:debugging page can create a worker thread in the parent process if a user presses "start" on a service worker while there is no child process.

STR:

Open fresh browser instance
Open about:memory
Open about:debugging#workers in new tab
Click start on a service worker entry
Measure in about:memory
Observe there is only a main process and the service worker thread is running there.

I think this might be expected due to the way we are working around the ServiceWorkerManager in e10s mode, but I couldn't find it filed as a bug. I just wanted to file this as a known problem.
