Markers should be collapsed under their parent when fully encompassed
When a marker is fully eclipsed by another marker, it should be collapsed under it. Chrome does this similarly. Whenever a marker's start and end time is after and before another marker respectively, it should be nested within it.
