Responsive mode causes URL preview bar to cover Developer Tools pane
Created attachment 
screenshot of URL preview on top of Developer Tools

Launch Firefox and open a site in Responsive Mode
Open the Developer Tools at the bottom of the window
Resize the Responsive Mode frame so that its bottom part is covered by the Developer Tools
Hover over any link
Notice the URL preview shows up on top of the Developer Tools pane
