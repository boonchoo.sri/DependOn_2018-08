Synchronize inspection of viewports
Once we have multiple viewports, we can synchronize inspecting and tweaking them all at the same from a single toolbox.

Since the toolbox today only understands connecting to one thing, the first step is to broadcast the commands from a toolbox to all viewports, but only process replies from one viewport.

While this sounds horribly broken, it does allow several key tools like inspector and style editor to affect all viewports without any other changes.

We'd likely disable tools that don't work in this special mode.

Current UX thinking is that this would be enabled by default, but there will be some way to disable it as well. Disabling this would allow you to get back to a full toolbox for a single viewport.

After this step, we can improve more tools to get them working in this mode.
