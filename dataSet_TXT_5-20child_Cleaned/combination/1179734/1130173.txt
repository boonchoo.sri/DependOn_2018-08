Preview the current page in multiple window sizes at the same time
The devtools responsivedesign view is handy, but it would be also handy to have several of them at the same time, set at different sizes, and most importantly, synced together, so that any change in the "master" window would also be made on the "child" windows.

This would make it very useful when, for example, tweaking the CSS with the devtools. If you're working on a responsive site, you'd immediately see the effect of a CSS change across all your design breakpoints, without having to constantly resize the window.

Here's an edited version of a discussion that happened on IRC#developers about this:

< pbrosset> long shot: if I wanted to have the current page  across several viewports at the same time, so I
could work on my responsive design without having to constently resize, how would I go about doing it? I mean,
is there something at the platform level we could do to make such a thing possible?
< seth> pbrosset: aren't there tools that will watch the filesystem and automatically refresh the page when you change something?
< pbrosset> seth: yes there are, but I think it'd be cool to have several synced 'views' of the same page in the same browser so
that if I can tweak it with the devtools and see the changes
< dholbert> pbrosset, I don't think that's possible, at least not with a single DOM
< dholbert> pbrosset, it used to be sort-of possible (that's how we did printing at one time -- we created a second distinct
"presentation" of the same document, for printing)
< pbrosset> so, copying the DOM over to other windows would be the way to go
< dholbert> pbrosset, yup, you'd need to do something like that
< dholbert> pbrosset, there might exist some tools on the web that sort of let you do this, with together.js or something similar
< dholbert> pbrosset, though... probably only if you edit it via the web tool's UI, not via your local devtools
< pbrosset> I'm thinking about something that just copies the whole state of the DOM+style over to another window as soon as any
of them change
< pbrosset> the other windows would probably be non-interactive though, just outputs
< dholbert> pbrosset, yup, seems handy
<@smaug> pbrosset: so for print preview (on non-OSX and printing) we clone the document and relevant stuff, and open a new tab
and show the result there
<@smaug> pbrosset: so for pp we create a static clone. So no scripting running, no animated gifs, no  animations
<@smaug> you probably want something where everything runs normally
