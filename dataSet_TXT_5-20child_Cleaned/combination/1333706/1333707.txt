Grid stuck on page when using back button
Grid stuck on page when using back button (actually there are two grids, one stuck and one active even though grid tool is not active from Rules)

- Visit 
- Open Inspector and click on Main
- From Rules activate the CSS Grid tool
- On navigation bar visit another website
- Hit back button after the above page is loaded
- Open Inspector and check for the CSS Grid tool

Screencast showing the issue:

