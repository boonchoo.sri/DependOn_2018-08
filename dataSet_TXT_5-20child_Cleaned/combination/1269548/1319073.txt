Custom setting (browser.tabs.closeWindowWithLastTab) not synced
Created attachment 
PrefSync.png

User Agent:  (Windows NT  Win64; x64; rv:50.0)  
Build ID: 

Steps to reproduce:

I have tree custom settings that I want to sync between different firefox installations (I use a lot of virtual machines to test). Therefore I have added two three custom settings:
services.sync.prefs.sync.browser.search.openintab=false
services.sync.prefs.sync.browser.tabs.closeWindowWithLastTab=false
services.sync.prefs.sync.browser.tabs.loadDivertedInBackground=true



Actual results:

When I create a new virtual box and install firefox and sign in with the profile two settings are synced, openintab and loadDivertedInBackground, but the closeWindowWithLastTab isn't synced and its value is "true".


Expected results:

The custom setting closeWindowWithLastTab should be correctly synced and its value should be "false" and not "true" (default value)
