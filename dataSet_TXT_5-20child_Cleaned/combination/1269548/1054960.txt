Fix sync to handle the current search engine being stored outside of preferences
Bug  moved the storage of the current search engine outside of preferences to make hijacking search a bit more difficult. Without changes to sync, the current search engine won't be sync'ed anymore.
