The light themes are not synced
Reproducible on Firefox  (2016-01-11) and Firefox  under Mac OS x  Ubuntu  x86 and Windows  x64

Steps to reproduce:
Start two separate profiles (A & B) on the same machine.
Log in on both profiles with the same user.
On profile A, go to about:addons and install a light theme (e.g. 
Perform a Sync on profile A.
Perform a Sync on profile B.
On profile B, disable the light theme from about:addons -> Appearance.
Perform a Sync on profile B.
Perform a Sync on profile A.

Expected:
The profiles are synced and the light theme is disabled from profile A.

Actual:
The light theme is not disabled from profile A.

Additional notes:
Intermittent - After refreshing the profile A (F5), the light theme appears disabled in about:addons -> Appearance, while the theme is still enabled.
