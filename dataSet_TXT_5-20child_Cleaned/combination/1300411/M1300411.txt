user reports excessive load spinning when switching tabs with e10s in  release
After we turned e10s on for our full target population I saw someone with a complaint:



They seem to be getting the load spinner every time they switch tabs for  to  seconds. This seems excessive.

Brad, do you have anyone on your team who could investigate this?

His about:support is below. They have Avast installed, but those plugins seem disabled.

Informazioni di base
--------------------

Nome: Firefox
Versione: 
ID build: 
Canale di aggiornamento: release
User agent:  (Windows NT  WOW64; rv:48.0)  
SO: Windows_NT  x86
Finestre multiprocesso:  (Attivato per impostazione predefinita)
Modalità provvisoria: false

Segnalazioni di arresto anomalo degli ultimi  giorni
-----------------------------------------------------

Tutte le segnalazioni

Estensioni
----------

Nome: Firefox Hello Beta
Versione: 
Attiva: true
ID: 

Nome: Multi-process staged rollout
Versione: 
Attiva: true
ID: 

Nome: Pocket
Versione: 
Attiva: true
ID: 

Nome: Avast Online Security
Versione: 
Attiva: false
ID: 

Nome: Avast SafePrice
Versione: 
Attiva: false
ID: 

Grafica
-------

Caratteristiche
Composizione: Direct3D 
asincroni (APZ): input rotella attivo
Rendering WebGL: Google Inc. -- ANGLE (AMD Radeon HD  Direct3D11 vs_5_0 ps_5_0)
Decodifica hardware H264: No; D3D11 blacklisted with DLL atidxx32.dll (8.17.10.625);  blacklisted with DLL atiumd64.dll (9.14.10.1128)
Direct2D: true
DirectWrite: true (10.0.10586.494)
GPU #1
Attivo: Sì
Descrizione: AMD Radeon HD 
ID produttore: 
ID dispositivo: 
Versione driver: 
Data aggiornamento driver: 
Driver: aticfx64 aticfx64 aticfx64 amdxc64 aticfx32 aticfx32 aticfx32 amdxc32 atiumd64 atidxx64 atidxx64 atiumdag atidxx32 atidxx32 atiumdva atiumd6a atitmm64
ID sottosistema: 
RAM: 

Diagnostica
AzureCanvasAccelerated: 
AzureCanvasBackend: direct2d 
AzureContentBackend: direct2d 
AzureFallbackCanvasBackend: cairo


Preferenze importanti modificate
--------------------------------

accessibility.typeaheadfind.flashBar: 
browser.cache.disk.capacity: 
browser.cache.disk.filesystem_reported: 
browser.cache.disk.hashstats_reported: 
browser.cache.disk.smart_size.first_run: false
browser.cache.disk.smart_size.use_old_max: false
browser.cache.frecency_experiment: 
browser.cache.memory.capacity: 
browser.display.show_image_placeholders: true
browser.download.folderList: 
browser.download.importedFromSqlite: true
browser.fixup.domainwhitelist.artepress: true
browser.fixup.domainwhitelist.bitly: true
browser.places.smartBookmarksVersion: 
browser.sessionstore.upgradeBackup.latestBuildID: 
browser.startup.homepage: 
browser.startup.homepage_override.buildID: 
browser.startup.homepage_override.mstone: 
browser.tabs.remote.autostart.2: true
browser.urlbar.autocomplete.enabled: true
browser.urlbar.userMadeSearchSuggestionsChoice: true
dom.apps.reset-permissions: true
dom.max_script_run_time: 
dom.mozApps.used: true
extensions.lastAppVersion: 
font.internaluseonly.changed: true
gfx.crash-guard.d3d11layers.appVersion: 
gfx.crash-guard.d3d11layers.deviceID: 
gfx.crash-guard.d3d11layers.driverVersion: 
gfx.crash-guard.d3d11layers.feature-d2d: true
gfx.crash-guard.d3d11layers.feature-d3d11: true
gfx.crash-guard.glcontext.gfx.driver-init.direct3d11-angle: true
gfx.crash-guard.glcontext.gfx.driver-init.webgl-angle: true
gfx.crash-guard.glcontext.gfx.driver-init.webgl-angle-force-d3d11: false
gfx.crash-guard.glcontext.gfx.driver-init.webgl-angle-force-warp: false
gfx.crash-guard.glcontext.gfx.driver-init.webgl-angle-try-d3d11: true
gfx.crash-guard.status.d3d11layers: 
gfx.crash-guard.status.d3d9video: 
gfx.crash-guard.status.glcontext: 
gfx.direct3d.last_used_feature_level_idx: 
gfx.driver-init.appVersion: 
gfx.driver-init.deviceID: 
gfx.driver-init.driverVersion: 
gfx.driver-init.feature-d2d: true
gfx.driver-init.feature-d3d11: true
gfx.driver-init.status: 
media.benchmark.vp9.fps: 
media.benchmark.vp9.versioncheck: 
media.gmp-eme-adobe.abi: x86-msvc-x64
media.gmp-eme-adobe.lastUpdate: 
media.gmp-eme-adobe.version: 
media.gmp-gmpopenh264.abi: x86-msvc-x64
media.gmp-gmpopenh264.lastUpdate: 
media.gmp-gmpopenh264.version: 
media.gmp-manager.buildID: 
media.gmp-manager.lastCheck: 
media.gmp-widevinecdm.abi: x86-msvc-x64
media.gmp-widevinecdm.lastUpdate: 
media.gmp-widevinecdm.version: 
media.gmp.storage.version.observed: 
media.hardware-video-decoding.failed: false
media.webrtc.debug.aec_log_dir: C:\Users\FRANCE~1\AppData\Local\Temp
media.webrtc.debug.log_file: C:\Users\FRANCE~1\AppData\Local\Temp\WebRTC.log
media.youtube-ua.override.to: 
network.auth.allow-subresource-auth: 
network.cookie.prefsMigrated: true
network.http.max-connections: 
network.http.max-connections-per-server: 
network.http.max-persistent-connections-per-proxy: 
network.http.max-persistent-connections-per-server: 
network.http.pipelining.maxrequests: 
network.http.request.max-start-delay: 
network.predictor.cleaned-up: true
places.database.lastMaintenance: 
places.history.expiration.transient_current_max_pages: 
plugin.disable_full_page_plugin_for_types: 
plugin.expose_full_path: true
plugin.importedState: true
plugin.state.npctrl: 
print.printer_HP_Deskjet_2050_J510_series.print_bgcolor: true
print.printer_HP_Deskjet_2050_J510_series.print_bgimages: true
print.printer_HP_Deskjet_2050_J510_series.print_duplex: 
print.printer_HP_Deskjet_2050_J510_series.print_edge_bottom: 
print.printer_HP_Deskjet_2050_J510_series.print_edge_left: 
print.printer_HP_Deskjet_2050_J510_series.print_edge_right: 
print.printer_HP_Deskjet_2050_J510_series.print_edge_top: 
print.printer_HP_Deskjet_2050_J510_series.print_evenpages: true
print.printer_HP_Deskjet_2050_J510_series.print_footercenter:
print.printer_HP_Deskjet_2050_J510_series.print_footerleft:
print.printer_HP_Deskjet_2050_J510_series.print_footerright:
print.printer_HP_Deskjet_2050_J510_series.print_headercenter:
print.printer_HP_Deskjet_2050_J510_series.print_headerleft:
print.printer_HP_Deskjet_2050_J510_series.print_headerright:
print.printer_HP_Deskjet_2050_J510_series.print_in_color: true
print.printer_HP_Deskjet_2050_J510_series.print_margin_bottom: 
print.printer_HP_Deskjet_2050_J510_series.print_margin_left: 
print.printer_HP_Deskjet_2050_J510_series.print_margin_right: 
print.printer_HP_Deskjet_2050_J510_series.print_margin_top: 
print.printer_HP_Deskjet_2050_J510_series.print_oddpages: true
print.printer_HP_Deskjet_2050_J510_series.print_orientation: 
print.printer_HP_Deskjet_2050_J510_series.print_page_delay: 
print.printer_HP_Deskjet_2050_J510_series.print_paper_data: 
print.printer_HP_Deskjet_2050_J510_series.print_paper_height: 
print.printer_HP_Deskjet_2050_J510_series.print_paper_name:
print.printer_HP_Deskjet_2050_J510_series.print_paper_size_type: 
print.printer_HP_Deskjet_2050_J510_series.print_paper_size_unit: 
print.printer_HP_Deskjet_2050_J510_series.print_paper_width: 
print.printer_HP_Deskjet_2050_J510_series.print_resolution: 
print.printer_HP_Deskjet_2050_J510_series.print_reversed: false
print.printer_HP_Deskjet_2050_J510_series.print_scaling: 
print.printer_HP_Deskjet_2050_J510_series.print_shrink_to_fit: false
print.printer_HP_Deskjet_2050_J510_series.print_to_file: false
print.printer_HP_Deskjet_2050_J510_series.print_unwriteable_margin_bottom: 
print.printer_HP_Deskjet_2050_J510_series.print_unwriteable_margin_left: 
print.printer_HP_Deskjet_2050_J510_series.print_unwriteable_margin_right: 
print.printer_HP_Deskjet_2050_J510_series.print_unwriteable_margin_top: 
print.printer_PDFCreator.print_bgcolor: true
print.printer_PDFCreator.print_bgimages: true
print.printer_PDFCreator.print_colorspace:
print.printer_PDFCreator.print_command:
print.printer_PDFCreator.print_downloadfonts: false
print.printer_PDFCreator.print_duplex: 
print.printer_PDFCreator.print_edge_bottom: 
print.printer_PDFCreator.print_edge_left: 
print.printer_PDFCreator.print_edge_right: 
print.printer_PDFCreator.print_edge_top: 
print.printer_PDFCreator.print_evenpages: true
print.printer_PDFCreator.print_footercenter:
print.printer_PDFCreator.print_footerleft:
print.printer_PDFCreator.print_footerright:
print.printer_PDFCreator.print_headercenter: &U
print.printer_PDFCreator.print_headerleft:
print.printer_PDFCreator.print_headerright:
print.printer_PDFCreator.print_in_color: true
print.printer_PDFCreator.print_margin_bottom: 
print.printer_PDFCreator.print_margin_left: 
print.printer_PDFCreator.print_margin_right: 
print.printer_PDFCreator.print_margin_top: 
print.printer_PDFCreator.print_oddpages: true
print.printer_PDFCreator.print_orientation: 
print.printer_PDFCreator.print_page_delay: 
print.printer_PDFCreator.print_paper_data: 
print.printer_PDFCreator.print_paper_height: -1,00
print.printer_PDFCreator.print_paper_name:
print.printer_PDFCreator.print_paper_size_type: 
print.printer_PDFCreator.print_paper_size_unit: 
print.printer_PDFCreator.print_paper_width: -1,00
print.printer_PDFCreator.print_plex_name:
print.printer_PDFCreator.print_resolution: 
print.printer_PDFCreator.print_resolution_name:
print.printer_PDFCreator.print_reversed: false
print.printer_PDFCreator.print_scaling: 
print.printer_PDFCreator.print_shrink_to_fit: false
print.printer_PDFCreator.print_to_file: false
print.printer_PDFCreator.print_unwriteable_margin_bottom: 
print.printer_PDFCreator.print_unwriteable_margin_left: 
print.printer_PDFCreator.print_unwriteable_margin_right: 
print.printer_PDFCreator.print_unwriteable_margin_top: 
privacy.cpd.offlineApps: true
privacy.cpd.siteSettings: true
privacy.sanitize.migrateClearSavedPwdsOnExit: true
privacy.sanitize.migrateFx3Prefs: true
privacy.sanitize.timeSpan: 
security.disable_button.openDeviceManager: false
services.sync.declinedEngines:
storage.vacuum.last.index: 
storage.vacuum.last.places.sqlite: 
ui.osk.debug.keyboardDisplayReason: IKPOS: Touch screen not found.

Preferenze in user.js
---------------------

Nella cartella del profilo è presente un file user.js con preferenze che non sono state impostate da Firefox.

Preferenze importanti bloccate
------------------------------

JavaScript
----------

GC incrementale: true

Accessibilità
-------------

Attivato: false
Impedisci accessibilità: 

Versioni librerie
-----------------

NSPR
Versione minima prevista: 
Versione in uso: 

NSS
Versione minima prevista:  Basic ECC
Versione in uso:  Basic ECC

NSSSMIME
Versione minima prevista:  Basic ECC
Versione in uso:  Basic ECC

NSSSSL
Versione minima prevista:  Basic ECC
Versione in uso:  Basic ECC

NSSUTIL
Versione minima prevista: 
Versione in uso: 

Caratteristiche sperimentali
----------------------------
