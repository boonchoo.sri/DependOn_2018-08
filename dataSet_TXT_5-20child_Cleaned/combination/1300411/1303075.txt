Do longitudinal analysis on FX_TAB_SWITCH_SPINNER_VISIBLE_LONG_MS probe to see how many of our users see really bad tab switch spinners
Bug  landed FX_TAB_SWITCH_SPINNER_VISIBLE_LONG_MS, and bug  made the probe opt out.

Once we have enough data, we should do a longitudinal analysis to get a sense of how many of our users are seeing really long tab switch spinners.
