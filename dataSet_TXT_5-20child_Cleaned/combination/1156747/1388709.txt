Console `originAttributes` attributes slow things down and is unused
Based on bug  comment  it looks like the call to Cu.cloneInto()
has significant impact on console.log performances.

Clone originAttributes to prevent "TypeError: can't access  object"
exceptions when cached console messages are 
by the devtools webconsole actor.
aEvent.originAttributes = Cu.cloneInto(aEvent.originAttributes, {});

Commenting this line introduces a  win in console.log performances under heavy usage (see bug  comment 

I'm not an active contributor for the console, but I looked into its codebase, and I wasn't able to find any usage of this `originAttributes` object:


The only one reference coming from webconsole code is this one:

delete result.originAttributes;

Where we immediately delete it as soon as we receive the console-api-log-event message...
It has been introduced in bug  as well as the cloneInto, in order to prevent  wrapper exceptions.

If that is really unused, it looks like we should remove it completely.
