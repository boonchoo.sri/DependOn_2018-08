browser permanently hangs when logging tons of stuff
STR:

Load 
Open a scratchpad
Paste attachment into the scratchpad
Run the scratchpad
Click the big "play" button

ER:

Stuff logs in the console. Failing that, when there are too many messages, some are displayed and the browser is still responsive. Seems like at minimum we could just put some loop inside a DevToolsUtils.yieldingEach[0] or queue up messages to be rendered so that the browser is still responsive.

[0]: 

AR:

Permanent beach ball. Need to force-quit firefox.
