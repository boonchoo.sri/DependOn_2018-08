[e10s] ]Add shim for addSystemEventListener
As unearthed in  , we honor addEventListener() with a shim, but not the equivalent addSystemEventListener().

Seems reasonable for the shims to be consistent and I need a good first shim bug, so let's role with this one.
