Add more delay to some browser chrome tests that often timeout
The about:home redesign in bug  somehow ended up making a bunch of known oranges and timeouts more reproducible (like hitting at each run or such).
Using that fact I added some more breathing to some of these tests, allowing them to pass, most of those were on the timeout edge or hitting timing issues.
This patch helped moving from permaorange to green.
