Visual refresh for searchbar
This bug comprises several pieces, which will require some small number of separate patches. Roughly speaking:

Move search-engine selection drop-down back to the engine icon
Update search-engine button to contain space outside the icon, to hold glows
Implement hover state for buttons based on textbox hover
Implement glowing engine button in the presence of an autodetected engine
Change search button style
