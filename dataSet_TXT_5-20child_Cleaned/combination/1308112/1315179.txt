[Perf][Gsheet]  ms) slower than Chrome when opening a sheet then switch to a tab with image and  raw data
# Test Case
STR
Open the browser
Open the specified Gsheet document 
Switch to the next tab

* This test is tested with proxy network environment

# Hardware
OS: Windows 
CPU: i7-3770 
Memory:  Ram
Hard Drive:  SATA HDD
Graphics: GK107 [GeForce GT  GF108 [GeForce GT 

# Browsers
Firefox version: 
Chrome version: 

# Result
Browser | Run time (median value)
Firefox |  ms
Chrome |  ms
