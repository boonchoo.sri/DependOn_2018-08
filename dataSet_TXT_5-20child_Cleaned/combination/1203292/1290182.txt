The hovered 'x' icons on the control center panel look out of place on Mac
Created attachment 
Screenshot of current implementation

When hovered, the 'x' icons added in bug  are in a gray circle. This reminds me of touch UIs but doesn't match anything we already have on Mac.

It also seems strange to me that the X icons are bolder than the icons on tabs.
The current implementation matches the mockup at 
but the x icons on  are lighter.
