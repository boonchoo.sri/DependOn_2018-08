Loading data: URLs from bookmarks shouldn't inherit principal
In the address bar, enter


Result: shows Bugzilla cookie

Expected: show empty alert

(Figuring out what to do for *javascript:* URLs is controversial and covered in other bugs.)

(Changing the behavior of data: *links in web pages* is controversial and covered in other bugs.)
