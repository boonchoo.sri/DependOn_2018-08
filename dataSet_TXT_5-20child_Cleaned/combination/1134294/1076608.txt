WebIDE doesn't update its buffers after a file has been modified externally.
User Agent:  (X11; Linux x86_64; rv:35.0)  
Build ID: 

Steps to reproduce:

Open up a new project in WebIDE.
Open up a file from the project in Vim.
Modify and write the file.


Actual results:

WebIDE Shows the file as it existed when the project was created.


Expected results:

Notify me that files have been modified externally.
Prompt to either keep files as they exist in memory, or reload the changed files from disk.
