Add context menus to Awesome Bar items
Created attachment 
Mockup showing context menus in Awesome Bar

Context menus make it easier for our advanced users to interact with the results in the Awesome Bar. By right-clicking on a result a user can:

- Copy Link Address: Copies URL to Clipboard
- Share Link: Sends URL to Social API
- Add to Bookmarks: Adds History item to Bookmarks
- Add Tags: Once added to Bookmarks, this option opens up the Bookmark panel with the Tags field selected
- Remove Bookmark: Deletes bookmark
- Remove from History: Deletes item from history
- Open in Another Tab: Opens URL in new background tab
