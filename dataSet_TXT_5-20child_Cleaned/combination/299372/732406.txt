Download filename wrong when using "Save Link As" and another file download is in progress
User Agent:  (Windows NT  rv:13.0a1)  
Build ID: 

Steps to reproduce:

STR:
Go to a site that uses HTML GET for example 
Right click on the first attachment and select "Save Link As"
While the above download is in progress, repeat step  for the second attachment.



Actual results:

The filename is shown as "attachment.cgi"



Expected results:

The filename is shown as "attachment.cgi" instead of the actual filename.
Work with other browsers
