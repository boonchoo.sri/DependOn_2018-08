"Content-disposition: attachment;filename=a.exe" can be used as part of tricking user into running a downloaded executable



The keyboard shortcut ALT + "click on a link" allows to automaticly save a link
target to your download directory (which is default your desktop). By modifying
header data you can alter the download of the URL
(shown in the status bar when hovering above
the link) into "foo.exe" instead of "foo.txt". Since windows hides known file
extensions by default and an exe file can have any icon, average users probably
don't see the difference an could be tricked into executing a file.




Open 
Alt-Click on the demo link




Even with visible file extensions an attacker could try .hta files to trick the
user (.hta = Hyptertext Applications. Associated with Internet Explorer and able
to run arbitrary code.). Most users will assume it's just another variant of the
extension.

A realistic attack scenario would be some kind of MP3 download website. Users
will probably mass download those files using ALT+click, since using a right
click context menu for each file is annoying and left clicking will probably
play the file in an associated media  like quicktime. If you mix an
EXE file under the MP3s users will probably don't recognize.

Another problem is that the MSN toolbar that adds some tabbed browsing to
Internet Explorer uses the ALT + "click on a link" shortcut to open links in a
new tab. When switching between the browsers you easily mix that up and can
accidently download executable files.

What about adding a whitelist for save to download files  and
raise a download dialog for all other filetypes? Maybe add a config switch to
disable and restore the current behavior for "brave power users" that accept the
risk this feature has.
