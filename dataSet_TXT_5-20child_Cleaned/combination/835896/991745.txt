Change "Search HTML" placeholder text to indicate the search is by CSS Selectors only
As noted in  the text for the placeholder for the search box in the markup view should be changed, since it isn't searching the HTML.

Maybe: "Search with CSS Selectors"?
