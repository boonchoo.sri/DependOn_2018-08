"Search HTML" suggestions appear to be broken when I enter a selector
Created attachment 
screenshot  - 'Search HTML' suggestions appear to be broken when I enter a selector.png

>>> My Info: Win7_64, Nightly   ID  new profile <<<
STR:
Open this "data:" url:
>  b="cd"><a b="ce"><a b="cf">
Open devtools -> Inspector
Paste a[b*='c' in the field "Search HTML" (you'll see suggestion "a[" )
Press Enter key

Result: My carefully typed input was replaced with "a"
Expectations: The first <a> element should be selected
Pushlog:
> 

Leaving unconfirmed, because, who knows, maybe there's already plan to fix that.
