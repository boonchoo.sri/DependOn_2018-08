Shake while dragging tab to detach and hide other tabs
In windows  click and shaking the titlebar of a window causes all other windows to be hidden. I'm not sure if we can control hiding other windows or if there's an API to use, but it could make it easier to detach tabs. Just drag and shake and release to detach.
