Bookmark properties dialog needs tagging UI
(Windows; U; Windows NT  en-US; rv:1.9b3pre)   ID:2008010705

When you want to modify the bookmark properties within the bookmarks sidebar you are not able to  tags because no UI exists yet. Fields (like the details deck inside the Library) should be added to be able to modify the tags for that bookmark.
