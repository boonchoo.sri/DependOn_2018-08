checkSizing() and onPageFirstSized() cause uninterruptible reflows
By accessing |document.documentElement.clientWidth| and calling |.getBoundingClientRect()| for all hidden elements (+  about:newtab causes uninterruptible reflows when the page is first shown.

Code that collects telemetry data should not have any performance impact.
There might be a  way to determine how many children of #newtab-grid are visible?
