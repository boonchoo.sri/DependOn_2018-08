External links from within app tabs should always open in new tabs instead of replacing the app tab's page
External links from within app tabs should always open in new tabs instead of replacing the app tab's page. It's not quite clear what makes a link "external". We could compare the domain, but this seems like it would miss cases.
