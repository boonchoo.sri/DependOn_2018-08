Make the mixed content doorhanger more generic for all blocked content types
Currently, the blocked content doorhanger is specific to just mixed content blocking. We want to add more types of blocked content, so this doorhanger should be made more generic to allow showing of information and controls for other types of content blocking.

General structure should be a heading section describing that content was blocked, followed by one or more other sections for each type of blocked content.

This should be able to follow how the WebRTC notification doorhangers work.

Mockup: attachment 
