Determine how we want to handle HiDPI theme implementation for Windows
HighDPI (aka high pixels-per-inch) displays are becoming increasingly common. Perhaps most well-known are Apple's "Retina" displays, at ~220dpi (vs. ~96dpi for normal displays). HiDPI displays are becoming increasingly common on PC tablet, notebooks, and even external monitors. This leads to three interrelated issues:

How should we  HiDPI in the Firefox theme?
What  of icons are needed?
What should the asset workflow (UX designer --> in tree --> packaging -->
user's system) look like?

Our implementation of the OS X HiDPI theme (deps under meta bug  largely relies on using CSS media queries to set  for Retina displays. A typical example looks like:

#someButton {
list-style-image: 
}

@media (min-resolution:  {
#someButton {
list-style-image: 
}
}

This works, but isn't really ideal.

* It's verbose and repetitive.
* It's prone to mistakes. Especially when the  rules are separated (too
easy to forget to update one) or -moz-image-region is involved. We've had
a number of bugs where hidpi was implemented incorrectly as a result, from
simply neglecting to provide a hidpi image to having  misshapen UI.

The HTML srcset feature (bug   addresses these problems -- but only on <img> elements, which we rarely use in Firefox UI.

The biggest problem, however, is that this solution doesn't really scale (teehee) to what's needed on Windows. The situation with Apple's Retina hardware is simple -- things are either sized  (normal) or  (Retina). But Windows supports many additional scaling factors. Windows  supports scaling factors of   and  (as well as an easily accessible "custom" setting for anything between  and  Windows  adds an explicit  option. The guidelines for Windows Store apps  recommend   and  (And then there are the with text-only scaling, which isn't relevant here.)

It's not really practical to extend the current  scheme to use bitmaps for so many different scaling factors. So it seems likely that we'll need to use a vector format (SVG) for image assets, so that the user isn't seeing a bitmap  for their particular DPI setting (which will often look blurry). We _could_ make a tradeoff decision to only support a couple of DPI settings and scale for things in between, but that seems like just kicking the can down the road.

Note that even with SVG we will still need to (sometimes) provide multiple image assets! When an image is displayed on screen with a small number of physical pixels (16x16 favicons being the canonical example), hand-tweaked bitmaps are usually required to look acceptable.

Finally, there's the asset workflow. Currently, all stages are the same -- for a particular icon, an UX designer provides the necessary sizes and flavors and those all go into the tree and are are all shipped in the respective platform builds. Depending on how we solve the other problems here, this may or may not need to change. For example, we could switch to pure SVG and basically do the same thing. Or we could have UX provide SVG assets, and convert them to bitmaps at some point (for checkin, for packaging, or at run-time on the user's system).
