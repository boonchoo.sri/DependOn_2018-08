add parameter to openPreferences to specify which pane to open
With Mozilla Firebird you can't specify which panel to show when opening the
preferences.

The openPreferences() function should have some parameter where you could
specify the panel to show.
