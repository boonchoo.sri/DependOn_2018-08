on about:newtab page, the bookmark star button does nothing.
User Agent:  (Windows NT  WOW64; rv:23.0)  
Build ID: 

Steps to reproduce:


Create fresh profile
go to about:newtab
Hover the mouse pointer over the bookmark star button .
Bug -- > There is no visual indication that this page can be bookmarked (like other pages)

Press the new bookmark star button, keeping the mouse pointer * on the star button (and not the 'down arrow') *





Actual results:

Nothing happens


Expected results:

Either the about:newtab should be bookmarked
OR
the bookmarks should open.
