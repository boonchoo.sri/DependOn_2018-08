Bookmark icon not obvious when an alternative blue Firefox theme is set
Created attachment 
screenshot

Switch to the Sky Blue theme:



Now the bookmarks' icon which landed in bug  is no longer obvious, whether the page is bookmarked or not.

See screenshot.
