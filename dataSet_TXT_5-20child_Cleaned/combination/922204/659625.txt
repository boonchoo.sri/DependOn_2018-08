Implement console.clear to clear the console output
Firebug has a console.clear() function that clears the console output, same as our clear() method. It would be nice to implement that in our web console as well.
