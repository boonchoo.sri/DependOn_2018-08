Add console.dirxml


> Prints an  Element representation of the specified object if possible
> or the JavaScript Object view if it is not.
>
> var list = document.querySelector("#myList");
> console.dirxml(list);
>
>  The above is equivalent to ...
>  console.log("%o", list);
