Show a trace of how a css variable got set in the Inspector
Given markup like:

<style>
:root {
--foo: red;
}

div {
--foo: blue;
}

span {
color: foo;
}



It'd be great to see:

(1) what is the current assignment of foo when inspecting the span
(2) how did it get to be that way

Just brainstorming how this info could be displayed:
could be displayed in the rule view somewhere next to the value, or in a tooltip upon hovering the value
could be displayed in the computed view where we already have this sort of 'trace' feature. We would probably want to show all of the variables in scope in the computed view if we did this, and maybe link to the particular one from the rule view.
