Fix uncaught promise rejections in DevTools framework
When uncaught promise rejections throw errors, the following occur in framework:

TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection at  ->  - TypeError: this.doc is undefined
TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection at  ->  - TypeError: this.doc is undefined
TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection - unknownError
TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection - unknownError
TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection - unknownError
TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection - unknownError


The keybinding test is tracked in bug 

TEST-UNEXPECTED-FAIL |  | A promise chain failed to handle a rejection at  ->  - TypeError: this.doc is undefined
