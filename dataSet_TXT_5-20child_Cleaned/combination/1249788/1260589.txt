Add the ThreadSafeDevToolsUtils.flatten utility
The `flatten` function takes an array of arrays and flattens them to a single
array, removing one level of nesting. It does not recursively flatten multiple
levels of nesting.
