Opening a new tab goes next to current tab instead of the end of the tab list (behavior change)



In FF  when opening a link in a new tab, the new tab would come at the end of all open tabs. Now it appears to go to the immediate right of the tab you're opening the link from. This behavior is extremely annoying, it would be nice for this to at least be configurable.




Open up multiple pages in tabs
Google some term in the first tab
Open up links from those results, see the new tab be immediately to the right, instead of at the end of the list

New tab is immediately to the right of the current tab


New tab is at the end of all current tabs
