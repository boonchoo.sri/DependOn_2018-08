When restoring a session, "about:home" is displayed in the selected tab before the tab is restored
User Agent:  (Windows NT  WOW64; rv:31.0)   
Build ID: 

Steps to reproduce:

Set Firefox to "Restore windows and tabs from last time"
Set "Don't restore tabs until I click on them"
Restore a session, and click an inactive tab to restore it.


Actual results:

Firefox briefly displays a different page (usually about:home, sometimes the session restore screen or something else), then briefly displays a blank tab with the title "new tab", then restores the page from the session.


Expected results:

Restore the page immediately to the tab, do not call up a different page first, and do not call up a "new tab" page before displaying the page.
