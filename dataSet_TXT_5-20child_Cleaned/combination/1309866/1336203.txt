vertical scroll bar disappear when showing request details
Steps to reproduce:
open Nightly
open Network Monitor
open  in tab
confirm vertical scrollbar is shown for requests list
open request details

Actual result:
vertical scrollbar for requests list disappear (or perhaps covered by request details?)

Expected result:
vertical scrollbar for requests list is shown next to request details

regression range:


apparently from bug 
