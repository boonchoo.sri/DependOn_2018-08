Move Netmonitor context menu code to a separate module
The context menu code in requests-menu-view.js should be moved to a separate module (request-list-context-menu.js?).

This involves the _openMenu method and all the handlers (copyUrl etc.)

This will make the requests-menu-view.js file simpler and the changes in bug  easier to understand and review.
