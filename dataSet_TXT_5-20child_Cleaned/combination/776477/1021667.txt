Give Activity Stream URI_SAFE_FOR_UNTRUSTED_CONTENT
After moving to HTML and making things e10s-friendly, I believe we could hit the switches in aboutRedirector. At a minimum, we should be able to add URI_SAFE_FOR_UNTRUSTED_CONTENT, I think?
