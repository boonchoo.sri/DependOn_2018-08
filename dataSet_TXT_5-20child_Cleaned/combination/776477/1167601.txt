Convert newTab.xul to newTab.xhtml
newTab.xul cannot be rendered properly in the content process. We should be using HTML there.

This will involve moving the panels out of content, and relying on Enn's work on bug 
