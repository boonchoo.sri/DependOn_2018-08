Unprivileged about:newtab: Convert about:newtab to use HTML instead of XUL
The first step to make the new tab page unprivileged would be to convert it to use HTML instead of XUL. Tricky things here include:

- RTL support. We'll need to include a dir="&locale.dir;" attribute on <body> at a minimum, but then we'll need to do more work as well to make sure that the CSS works well.
- Flexy things - this should be fixable with the new flexbox support we have
- Not breaking tests - we have a lot of tests for the new tab page, and keeping them working might be tricky.
