Need visible indicator while a site has  access while Firefox window is open
+++ This bug was initially created as a clone of Bug #729522 +++

Original mockup: attachment  Tabs aren't a great place for this, as there's not enough space in pinned tabs and normal tabs can be scrolled off-screen.
