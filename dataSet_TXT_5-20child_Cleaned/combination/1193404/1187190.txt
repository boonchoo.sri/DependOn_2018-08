Importing passwords from other browsers should handle password changes for existing logins
Password-only changes from Chrome to Firefox need to be propagated when the user does the migration.
