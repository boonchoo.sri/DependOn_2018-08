Cleanup the debugger tests
msucan recently pointed out to me that the debugger tests are horror. I sincerely believe that's a gross understatement. Everything is WET (write-everything-twice), there are a million executeSoon's and private methods and properties are accessed everywhere.

Let's make them pretty and a joy to write!
