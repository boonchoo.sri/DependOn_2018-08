Reloading off-site OBJECT inserts offsite favicon in Firefox tab



Given a page that uses the OBJECT tag to load a page from another domain into
it, if you reload the inserted OBJECT page, the Firebird tab has its icon with
the favicon of the inserted OBJECT page.




Visit  in a new tab.
Right-click on the "Web Standards Meetup Stats" box, choose This Frame ->
Reload Frame


The icon in the Firebird tag was replaced with the favicon from the domain
linked in the box via <OBJECT> (meetup.com).


The favicon (or lack of any designated favicon) should have been maintained in
the tab.

This functions correctly in Mozilla build 

Minimal testcase to follow in case I drop the OBJECT from my page one day.
