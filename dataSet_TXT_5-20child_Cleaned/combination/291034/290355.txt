Focus events wrong in manage bookmarks and tree views in general
Open manage bookmarks (alt+b m)
First problem: accessible focus goes to window instead of tree item that's focused.
Second problem: shift+tab to text field, accessible focus goes back to folders tree
