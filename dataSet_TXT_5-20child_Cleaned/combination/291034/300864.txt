Regression: page load errors are no longer accessible
In the past page load errors brought up a modal dialog, which was accessible
with a screen reader.

After the checkin for bug  these became "XUL error pages". There is
notification sent to a screen reader that these error pages are ready to read,
because the typical web progress listener STATE_STOP event does not occur for them.
