Scrolling the mouse wheel over the tabs does not switch tabs



Tabs should be switched while scrolling over them.
This is normal behavior of GTK+ application, for example Nautilus.

This is probably related to bug #281192.

If this bug is considered as valid (and I hope so), it should block 


