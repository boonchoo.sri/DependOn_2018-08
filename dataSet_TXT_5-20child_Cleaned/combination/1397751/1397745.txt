DAMP doesn't report significant  because it only care at summary results
And it is *really* bad and makes the whole thing unfortunately useless.

Let's look at bug  as example.

Thanks to additional fixes to DAMP from bug bug  I get this DAMP results:


linux64  ±  <  ±    (low)

=> No it reports no win at all

But it clearly hides a significant one, which deeply impact inspector usefullness.
And it is unfortunate as you can really see it when looking at subtests:



simple.inspector.open.DAMP  ±  >  ±  -5.03%  (high)

I looked at all bugs from bug  and I only see reports about damp summary regression, so please correct me if we also have alerts on subtests.


Now. I know it isn't easy story. I imagine we don't track each subtest for various reasons. But I would like to hear all of them to see how to move forward from here.
