DevTools Profiler does not work on the Ignite foxfooding phone from WebIDE.
Doing a performance profile from DevTools view on the Ignite foxfooding phone, the profile starts ok, and the IDE shows it to be running ok, but clicking on the profile button to stop profiling, nothing happens, and the profile button stays in the pressed down state, and the UI shows as if profiling is still going on.
