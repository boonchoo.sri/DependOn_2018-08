Performance profiling a page on FF for Android shows bogus performance data
Created attachment 
bogus_performance_data_webide.png

STR:

Navigate Android Firefox to 
Open WebIDE on desktop and attach to phone (tested via USB).
Start running a performance profile in Performance tab of the developer tools.

Observed: The display shows bogus data ("avg -25.01 fps"). The numbers get larger and larger to negative to something like -800fps, and then they wrap around to +1000fps.
