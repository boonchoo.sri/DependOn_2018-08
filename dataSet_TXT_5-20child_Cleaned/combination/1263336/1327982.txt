Ctrl+F in inspector doesn't work if I use russian keyboard layout
>>> My Info: Win7_64, Nightly   ID 
STR_1:
Switch keyboard layout to russian
Open inspector on this page, click on <body> element in markup
Press Ctrl+F

AR: Findbar opens
ER: Search field "Search HTML" should be focused in inspector

Marked as blocking for bug  for now, because this mistake can be made in each tool.
This is regression from bug  Regression range:
> 
