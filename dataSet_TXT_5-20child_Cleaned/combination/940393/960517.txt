Adjust Australis's browser theme for Windows  and up
+++ This bug was initially created as a clone of Bug #859751 +++

While Australis looks terrific on Windows  and Vista, it looks totally out of place on Windows  It just doesn't fit in almost every way:
Color scheme - gray and white instead of blue.
Buttons and text fields - flat instead of bulky.
Hover effects - they're mixed because some are hard coded and some are not.

IMPORTANT: this bug is about the browser theme, NOT the toolkit theme work.
