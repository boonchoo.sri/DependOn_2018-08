Tooltips don't work in the sidebar



When there is an HTML "title" attribute on an element of a page loaded in the
sidebar, the tooltip does not appear for the attribute.



This is just one example, as it's the main sidebar I use which would have
tooltips on it.
Go to  .
Click on the link "SiteBar" in "Mozillazine topic describes how to use
SiteBar sidebar extension" which will install the sitebar extension.
Click on the link "SiteBar" and hover over some of the buttons up the top to
confirm that the tooltips are there.
Open the SiteBar sidebar and hover over the same buttons.


Nothing happens while hovering over the buttons.


Tooltips should have been displayed, the same as if the page were loaded in the
main frame.
