location bar flickers when backspacing a long search string
See 

When the location bar popup has only the heuristic result, and the user backspaces, for a while we may have  results, and that causes the popup to close (flicker).

Now that we have the heuristic result (And shortly one-off buttons by default) it could make sense to set minresultsforpopup="0" so that it won't close.
Though, there is one special case that is the dropdown button, it's the only one that may not have an heuristic result. With one-offs enabled it may not matter that much, but without them it would look bad to open an empty popup in case there's no typed history to show, so we may want to dinamically remove and set the attribute (or the relative autocomplete binding property).
Or we could set the property only when oneoffs are enabled.
