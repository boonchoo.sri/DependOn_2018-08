[meta] Photon - Interacting with the AwesomeBar should be fast and smooth
We need the user to get immediate feedback when they're interacting with the AwesomeBar. Immediate is  or less.

We need to ensure that keyboard events are prioritized so that typing feels smooth.

We need to avoid flicker and jank when the AwesomeBar populates.
