Drop  from URL preview if already on unencrypted connection
The URL-on-hover in the location bar should strive to be as  as possible, so we want to drop unnecessary text to improve this. As a bonus, there will also be more space for the URLs, and less truncation.

Proposed behavior:

* Drop  from the preview if both pages are on the same domain
* Show the protocol if it's different from the one you're currently on (e.g: show https if you're on http, show http if you are on https, show ftp if you're on http)
