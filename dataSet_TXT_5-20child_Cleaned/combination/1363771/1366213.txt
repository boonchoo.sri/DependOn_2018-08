Make the observer is called way less often in SessionCookies.jsm
This is a follow-up bug for bug  comment 

"add a new observer notification in nsICookieService, called 'session-cookie-changed', that only fires when a session cookie was   updated. This'll make sure that the observer is called way less often in SessionCookies.jsm."

As bug  comment  this might ideally save  observer calling, and can also save all isSession() check in SessionCookies.jsm.
