The remote debugger "Connecting..." string and animation should be larger
STR:
Enable remote debugging.
Go to Tools -> Developer Tools and click "Connect".
Click on the "Connect" button.

The "Connecting..." string and animation displayed at this point are much too small considering all the empty space and the much larger fonts around it.

Notes:
Reproducible on the latest Fx21 beta, Fx22 aurora, and Fx23 nightly builds.
The string and animation are larger on Firefox  but they could be enlarged even more than there.
This is most probably a regression from bug 
