gBrowser's contentDocument and contentWindow properties are not available for remote tabs
A quick DXR shows gBrowser.contentDocument being used in a number of places. The contentDocument property maps to this.mCurrentBrowser.contentDocument, which is undefined in the e10s case.

Same goes for contentWindow.

We should probably add  and change all in-tree usage of  to use  instead (much like we did in bug 

We should then probably mark  as deprecated.
