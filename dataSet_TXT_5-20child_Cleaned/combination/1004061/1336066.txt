'No screen' shows preview after clicking once on the device permission in the location bar
[Affected versions]:
- 

[Affected platforms]:
- all

[Steps to reproduce]:
Open 
Click the "Screen" button
From the dropdown select screen to preview
Click once the "screen share" permission in the location bar

[Expected result]:
- No change

[Actual result]:
- 'No screen' shows preview of the active screen

[Regression range]:
- tbd

[Additional notes]:
- Screencast: 
