Wire up the Session-data Service
convert the service used in the CrashRestore extension for inclusion in core:

- add inline docs
- remove CrashRestore-specific jargon
- add props to original author(s)
- write unit tests
