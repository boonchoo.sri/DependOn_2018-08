Nightly hangs and consumes  CPU when I try to scroll specific stylesheet in Style editor
Created attachment 
stylesheet  - Nightly hangs and consumes  CPU.txt

>>> My Info: Win7_64, Nightly   ID 
STR:
Set mouse option "when I rotate mouse wheel" in your OS to "scroll by  page"
Open attached "stylesheet  select all text there, copy it to clipboard
Open this "data:" url for "clear" testing
> 
Open devtools -> Style Editor, paste the text copied in Step  to the existing stylesheet
Scroll the stylesheet to the very top, then to the bottom
(repeat Step  if you haven't reproduced the bug)

Result:
Nightly becomes unresponsible (freezes) and nightly.exe consumes  CPU.

Expectations:
No high CPU usage. Nightly shouldn't become unresponsible.

Regression window:
> 
I think it was caused by bug  though I'm leaving keyword "regressionwindow-wanted"

Notes:
Step  isn't necessary. I can reproduce it with that mouse option set to "Scroll by  lines"
and stylesheet with only  lines with  "a{}"s in each line, if Style editor is resized
so that only ~10 lines were visible.
I just provided the most reliable STR.
Please request a screencast if you failed to reproduce this bug.
