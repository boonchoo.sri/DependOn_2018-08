css source editor's getInfoAt method sometimes fail
In bug  the 'getInfoAt' method was added to the source editor. It's not used yet anywhere in the devtools and my plan is that start using it for bug 
The goal of this bug is to highlighter nodes that match a given selector when the pointer hovers over selectors in the style-editor panel.

I've found that this function sometimes fail. Take these STR for instance:

- Open the style-editor on this page
- Open the scratchpad
- Switch it to "browser" environment
- Enter and execute the following code:

let editor = [...gDevTools._toolboxes][0][1].getPanel("styleeditor").UI.editors[0].sourceEditor;
editor.getInfoAt({line:  ch: 

The following exception is fired:

Exception: str is undefined
-> 
-> 
-> 
-> 

-> 
-> 
-> 
-> 
-> 
-> 

And the following log is outputted to the console:

console.log: Parse error at index  processing codepoint  in state comment.

Note that the {line:  ch:  position triggers the error because the stylesheet text content is the following:


body {font-family: sans-serif;color: ....

I don't know if the css parser state machine is able to detect comments, but in any case, I think the getInfoAt function should never fail and should at least return null when no info is found.
