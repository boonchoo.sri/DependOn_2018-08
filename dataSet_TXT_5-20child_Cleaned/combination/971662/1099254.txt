[rule view] selector highlight should respect scoping of the stylesheet
Example: 

Given this markup:

<div>
<style scoped>
h1 { color: FireBrick; }
p { color: SaddleBrown; }

<h1>Scoped 
<p>Scoped 


<p>Unscoped 

When selecting the scoped p tag and hovering the 'p' selector within the scoped style, only the proper node (that the style is actually being applied to) should be highlighted.
