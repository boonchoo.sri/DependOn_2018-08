Split up the "Older than  days" history folder



Bug  made me think of filing this bug. There may be several usability issues with the "Older than  days" folder:
it looks weird to have one folder per day, then one folder for  days.
expanding it takes a long time (bug 
it contains thousands of entries so it's nearly useless to scroll down in the hope to find  page you visited x months ago.

It'd be cool if history folders were dynamically created on the basis of browser.history_expire_days, or if a, say,  timeframe was used:
+  week ago
+ ...
+  month ago
+ ...
+  months ago







