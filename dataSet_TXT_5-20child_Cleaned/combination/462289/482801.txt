Middle-clicking the selected tab should not focus the tab bar
Follow-up to bug 

Middle-clicking the selected tab still focuses the tab bar on mousedown, before closing the tab on mouseup. The tab bar should not be focused.

Steps to reproduce:
Click (and don't release) the current tab on the tab bar with the *middle* mouse button, don't release the button yet.

What happens: the tab bar is focused
What should've happened: nothing yet

(you can release the middle button now, and the tab closes)
