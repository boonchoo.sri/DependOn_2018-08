Left-clicking then right-clicking the selected tab should not produce a focus ring on the tab.
As the summary says, STR:

left-click on the selected tab, then immediately,
right-click on the selected tab



Get a context menu and no focus ring on the tab.

Actual Results:

Focus ring appears on the tab and continues moving from tab to tab through mouse interactions thereafter. The focus ring goes away only when focus is transferred elsewhere (eg, to content).
