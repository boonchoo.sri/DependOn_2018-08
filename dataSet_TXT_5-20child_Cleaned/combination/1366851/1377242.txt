-  cart  tart (linux64) regression on push ba616afa8125a467295c08418d0dd9e1d69d303b (Wed Jun  
Talos has detected a Firefox performance regression from push:



As author of one of the patches included in that push, we need your help to address this regression.

Regressions:

cart summary linux64 opt e10s  -> 
cart summary linux64 pgo e10s  -> 
tart summary linux64 opt e10s  -> 


You can find links to graphs and comparison views for each of the above tests at: 

On the page above you can see an alert for each affected platform as well as a link to a graph showing the history of scores for this test. There is also a link to a treeherder page showing the Talos jobs in a pushlog format.

To learn more about the regressing test(s), please see: 

For information on reproducing and debugging the regression, either on try or locally, see: 

*** Please let us know your plans within  business days, or the offending patch(es) will be backed out! ***

Our wiki page outlines the common responses and expectations: 
