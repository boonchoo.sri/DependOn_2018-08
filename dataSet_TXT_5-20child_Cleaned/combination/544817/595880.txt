Make Bookmarks Widget(Bookmarks Menu Button) to be fully customizable(be able to place it anywhere without state of Menu  Toolbar).

ID:20100913041656

If Bookmarks toolbar is shown, The Bookmarks Menu Button *only* allow on the
toolbar. Otherwise, The Bookmarks Menu Button disappers,

If Menu Bar is shown and the Bookmarks toolbar is hidden , The Bookmarks Menu
Button *only* allow on the Menu Bar,

Problem is that the Bookmarks Menu Button is not fully customizable.
