drag and drop of bookmark from location bar to folder in bookmark toolbar broken using gtk2



Using a gtk2 build (compiled with --enable-default-toolkit=gtk2), it is not
possible to drag and drop a bookmark from the location bar to a folder in the
bookmark toolbar. The folder expands, but the little page icon representing the
bookmark stays stuck on the page and does not follow the mouse pointer. Clicking
on the page icon (that is stuck right by the destination folder) makes it
disappear and the bookmark is added to the folder.

This problem does not occur with builds that do not use the gtk2 toolkit.




Drag and drop a bookmark from the location bar to a folder in the bookmark
toolbar




The bookmark icons stays on the page, detached from the mouse pointer.


The bookmark icon should have stayed attached to the mouse pointer, in order to
choose where in the folder (or in subfolders) the bookmark should be inserted.

gtk2 toolkit
fresh install with no extension and default theme
