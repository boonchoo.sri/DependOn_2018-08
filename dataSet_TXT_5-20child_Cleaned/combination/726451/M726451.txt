Handle target files that are removed from disk
This bug tracks work to be done on top of the Downloads Panel in bug 

* When the target file is removed from disk, clicking on a download item does
nothing. The issue is that we can't detect whether the file is still there
on hover, for performance reasons. Thus, detect missing files on click,
with an asynchronous background process when the panel opens, and
assume the file is available until otherwise detected.
