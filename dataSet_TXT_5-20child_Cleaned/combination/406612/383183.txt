Include an identity indicator in primary chrome
Created attachment 
Initial prototype addon

In all the conversation about EV certificates, the need has become apparent for firefox to have some way of presenting website identity information to the user. While the padlock has traditionally been co-opted to do identity signalling as well as encryption, it's become a confused metaphor that tries to talk about security and identity simultaneously.

While bug  speaks specifically about the Microsoft approach to EV certificates, the resulting UI should not be couched in terms of EV exclusively. In principle (though I know of no other relevant identity technologies at the moment) this indicator should not be tied to EV as a technology, but to strongly verified identity in general.

The current discussion around such things points to a passport-inspector icon which, on click, presents an identity summary including site owner information (extracted from the subject information of a cert, for example), verifier information (extracted from a cert's issuer information, for example) and an affordance to get more information (launching the security tab of Page Info).

This has been implemented in the "Larry" extension, attached, and available during development from the supplied url.

For background, please see:





This also addresses Firefox PRD line item SPI-001b
