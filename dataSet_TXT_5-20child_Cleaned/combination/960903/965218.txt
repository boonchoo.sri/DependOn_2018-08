After restarting Firefox, about:newtab pages are restored as empty
STR:
- Set Firefox to "Show windows and tabs from last time".
- Open a new tab with the '+' button.
- Close Firefox.
- Open Firefox.

Expected:
- The newtab page is displayed.

Actual:
- An empty page appears instead of the newtab page.

This happens both if the newtab page is focused, and also if it's an unloaded background tab (shows empty after clicking that tab).
