App Tab: Unable to re-shuffle order when multiple normal tabs are open
I am unable to shift around the order of the application tab. It only allows dragging back to the main tab bar, but not within the application tab order itself

Repro:
install minefield:  (Macintosh; Intel Mac OS X  en-US; rv:2.0b2pre)  
add  app tabs
drag and shift app tabs with the app tab screen
Verify cannot be reshuffled

Expected:
- shuffle app tabs within the application tab bar
