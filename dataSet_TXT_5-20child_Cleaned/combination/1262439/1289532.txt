Cannot access the new eyedropper directly from the Style Editor
User Agent:  (Windows NT  WOW64; rv:47.0)  
Build ID: 

Steps to reproduce:

Start Nightly
Go to any sites (e.g. about:home)
Open DevTools > Style Editor
Use eyedropper in the style editor


Actual results:

Cannot access the new eyedropper directly from the Style Editor

Regression range:



Expected results:

The eyedropper should be placed on a public access area. (e.g. on the toolbox toolbar)
