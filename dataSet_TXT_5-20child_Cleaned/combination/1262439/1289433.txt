Arrow keys stopped working with new color picker
User Agent:  (Windows NT  Win64; x64; rv:50.0)  
Build ID: 

Steps to reproduce:

I opened the color picker and tried to move it using my arrow keys.


Actual results:

The color picker didn't move.


Expected results:

The color picker should move in the direction of the arrow key.
