When selecting an empty tab, autofocus on the location bar
This is a follow-up bug from bug  since the problem described there wasn't actually fixed. :)

To reproduce:
open a new tab
cause it to loose focus by clicking in the content area
start typing, or switch to a different tab and switch back
keyboard input is ignored

Recommendations:
* if you start typing on a blank tab when it isn't focused, focus the URL field
* if the search field or URL field is focused, retain this focus when switching back from a different tab (also related: bug  a subset of this problem)
* if you switch back to a blank, unfocused tab from another tab, make sure the URL field is focused

It should never be possible to get a blank tab without focus — the easy way might be to just accept keyboard input and focus when it happens, or build logic to make sure it doesn't happen — I'll leave that decision up to you guys, they are equivalent as far as the UX goes.
