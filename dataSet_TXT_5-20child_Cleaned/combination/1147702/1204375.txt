Locationbar has greater height than the searchbar on HiDPI
Created attachment 
screenshot - Locationbar has greater height than the searchbar on HiDPI.png

STR: (Win7_64, Nightly   ID  new profile, safe mode)
Set layout.css.devPixelsPerPx ->  OR
set layout.css.devPixelsPerPx -> -1.0 (default) and set font size =  on your OS

Result: Locationbar (23.8px) has greater height than the searchbar (23.6px)
Expectations: They should have the same width -  just like when I set layout.css.devPixelsPerPx ->  (100%)

Note: You can see on attached screenshot that location bar has visually greater height (by 
Here are screenshots of browser toolbox saying that location bar is  height and searchbar is  height
> locationbar 
> searchbar 
Selectors: location bar (#urlbar-wrapper) searchbar (#searchbar)
