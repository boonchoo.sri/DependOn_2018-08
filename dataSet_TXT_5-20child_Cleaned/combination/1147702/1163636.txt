High-DPI Downloads button gets Low-DPI once pressed
User Agent:  (Windows NT  Win64; x64; rv:40.0)  
Build ID: 

Steps to reproduce:

Firefox  finally adds some High-DPI toolbar icons (e.g. used at  system-level scale) for main GUI (Australis menu is still entirely Low-DPI though).

But the Downloads button is buggy:

by default, the Downloads button is High-DPI, but once  it immediately gets blurry Low-DPI as in previous Firefox versions.

Tested with the latest nightly build 

High-DPI icons themselves have been added in the build 


Actual results:

High-DPI Downloads button gets Low-DPI once 


Expected results:

High-DPI Downloads button should be permanently High-DPI regardless of whether is has been 
