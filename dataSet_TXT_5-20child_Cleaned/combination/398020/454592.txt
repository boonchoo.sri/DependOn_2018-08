innerWidth cannot be set under a certain minimum for certain themes



I have a Flash application that fills the entire browser window. Some javascript reduces in a certain event the browser window to  pixels wide (using window.innerWidth =  With the default theme, this works perfectly.

However, with the Noia  Extreme theme, there is a problem. The window resizes correctly, but the innerWidth is not correctly set. The innerWidth seems to be set to a value that corresponds to the minimum chrome size, in my case  As a consequence, the Flash app is sized to  but only part is shown, without browser scrollbars.

This problem does not occur if run in a window that was popped up with parameters "toolbar=No,location=No,status=No,resizable=Yes,fullscreen=Yes", probably because there the chrome is narrower.

It seems that the theme is able to impose a minimum width for its chrome but the browser only applies it half-heartedly: the window.innerWidth property is set to this minimum, but the window can be smaller.

A solution for this problem would be to fix the calculation of innerWidth.





Create an HTML file with the following contents:
<button 
Install the default theme.
Open this HTML file in Firefox and reduce the window width to about 
Push the Width button
Install Noia  extreme theme
Repeat steps  and 

With the default theme, the alert will show the correct innerWidth of about  pixels.
With the Noia  Extreme, the alert will show an innerWidth of about  while the window is really  and there are no scrollbars


In both cases the innerWidth should be about 

This bug breaks positioning and sizing of browser windows and internal content.
