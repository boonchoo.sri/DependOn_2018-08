Ruleview displays non-functional rules when style attribute is 
>>> My Info: Win7_64, Nightly   ID 
STR_1:
Open url  style="width:   scroll;">
Open inspector -> ruleview, inspect <textarea>
Disable CSS rule "overflow: scroll;" in ruleview
Resize the textarea via resizer
Enable CSS rule "overflow: scroll;" in ruleview

AR: The rule looks applied, but it's not
ER: Should be applied


STR_2:
Open url [1]
Open devtools -> inspector -> ruleview
Delete [style] attribute of body element in markup
Click on checkbox at the left side of rule "color: red;" in ruleview to enable it
Switch to another element in markup and back

> [1]  style="color:red"><b>hello_world

AR:
Step  - the rule in ruleview is displayed as disabled
Step  - the rule is displayed as enabled, but it is not applied (text on the page is black)
Step  - no rules displayed in ruleview
ER:
If ruleview displays rule as enabled, it should be applied on the page, and vice versa (4 options)
Therefore, either X or Y. X is better
X) Step  - rule "color: red;" should be applied ([style] attribute should be created again)
Y) Step  - no rules should be displayed in ruleview


This is regression from bug  Regression range:
>  Patrick Brosset <:pbro> (PTO until Jan. 
It seems that this is a regresion caused by your change. Please have a look.
