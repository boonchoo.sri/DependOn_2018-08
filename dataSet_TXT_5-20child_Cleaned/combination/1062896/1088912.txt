[UX] Investigate ways to improve default browser prompts for different user flows
Our current default browser prompts have a number of issues:
* In some instances it conflicts with our new user tour
* Generally, a new user has to choose to make Firefox their default browser before having an opportunity to use it.
* May not be shown at all to a returning user (someone who has previously uninstalled Firefox)
* We use an intrusive modal on startup and if a user disables this we have no other mechanism to ever ask them again.

At the same time, there is a correlation between having Firefox set as the default browser and usage. So we'd like to have everyone who wants Firefox as their default to be able to easily make it their default.

This document  maps out the first run flows and default prompts. More info here 

We should investigate ways to improve the way we prompt users to make Firefox default in these different contexts.
