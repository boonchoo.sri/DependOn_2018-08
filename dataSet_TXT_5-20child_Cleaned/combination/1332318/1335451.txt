Improve the look of tiles for which we don't have a favicon
In bug  we styled tiles with an initial and a color. This implementation used a lowercase initial and a range of mostly pale greens and blues for background colors. We want to update that styling to use an uppercase initial and wider range of bolder background colors.
