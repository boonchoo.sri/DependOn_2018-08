moving mouse from chevron to folders on the bookmarks toolbar does not auto-open popups


(Windows NT  WOW64; rv:18.0)   ID:20120930030610

I think Bug  came back.

Steps to reproduce:
Create enough number of Bookmark folder on the bookmarks toolbar.
And make sure chevron popup exists
Click the chevron to open popup
mouse hover a folder on the bookmarks toolbar

Actual results:
The folder does not auto-open.

Expected results:
The folder should auto-open popup.

Regression window:
Good:

(Windows; U; Windows NT  WOW64; en-US; rv:1.9.3a5pre)   ID:20100505053644
Bad;

(Windows; U; Windows NT  WOW64; en-US; rv:1.9.3a5pre)   ID:20100505071655
Pushlog:

