[Session Restore] Don't collect state right after startup when restoring the initial session


Right after restoring the initial state for the first window loaded after startup, we call .saveState() only to set the session state from STOPPED to RUNNING to count startup crashes correctly.

It would be great to just change the state without writing the whole file but we cannot do that. What we can do is at least not collect the browser state at a point where probably nothing has even finished restoring, yet.
