Style Inspector CSS autocomplete breaks manual entry in inplace editor
Created attachment 
autocomplete.png

The style inspector's autocomplete can butcher one's attempt to manually enter a CSS value. Let's say I wish to add a 'background-clip' property to an element... I click on some of the whitespace in the style inspector panel, type 'background-clip', and hit enter. From this point if I type (quite rapidly) 'content', it produces 'content-boxt-boxent-box'. I have also seen 'content-boxnt-boxnt-box', 'content-box-box-box-box-box', and 'content-boxnt-box'. I can also produce similar behavior for 'display' when I type 'inline'. The common factor seems to be a '-' in the list of potential property values.
