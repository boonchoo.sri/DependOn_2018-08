the context menu of places bookmarks with live titles is missing the "reload" option
In Fx2, microsummarized bookmarks have a context menu option for reloading the title, labeled "Reload Live Title". This needs to be added to the Places context menu for those bookmarks.
