JSON module's toString is too slow for objects with large strings
(Macintosh; U; Intel Mac OS X  en-US; rv:1.9.1b2pre)   (try server build)



With the try server builds for Private Browsing you can recognize a drastically slow down for entering Private Browsing mode when a huge sessionstore.js is located inside the profile. My one is around  in size. It will take around  seconds to enter Private Browsing mode. I'm able to see that on OS X and Windows.

Steps:
Create a new profile and activate Session Restore
Somehow try to get a huge sessionstore.js
Probably restart Firefox
Try to enter Private Browsing mode (save the session)

With the last step you have to wait around  until you can continue your work.

Simon, is there any simple way to reduce the size of my sessionstore.js? I would like to add it as a testcase but have to cleanup the file first. Or are you able to generate one?
