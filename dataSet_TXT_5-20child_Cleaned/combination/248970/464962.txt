Zoom-Level reset to default when switching tabs while in Private Browsing Mode



When in the new private browsing mode setting the zoom level to anything other than default will be lost after switch to a second tab then back to the zoomed page.




to Private Browsing Mode
tab#1
the zoom level for Tab#1
tab #2
back to Tab#1 -> zoom level will be set to default a few milliseconds later

Zoom level is reset


Site zoom level maintained whilst in the same private browsing session.
