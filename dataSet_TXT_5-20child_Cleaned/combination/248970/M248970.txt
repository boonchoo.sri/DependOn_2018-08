Private Browsing mode (global toggle for  everything)



In the next version of OS X, Safari will have a "Private Browsing" mode which,
basically, disables writing to cache, URL history, etc.
I think this is a good idea, and it's small and useful enough that it should go
into the browser, not an extension.



