Assertion running  in debug builds
TEST-UNEXPECTED-FAIL |  | test failed, see log

>>>>>>>
*** test pending
pldhash: for the table at address  the given entrySize of  definitely favors chaining over double hashing.
*** test pending
*** test finished
###!!! ASSERTION: consider quit stopper out of bounds: 'mConsiderQuitStopper >  file  line 
nsAppStartup::ExitLastWindowClosingSurvivalArea() 
nsAppStartup::AttemptingQuit(int) 
nsAppStartup::Quit(unsigned int) 
NS_InvokeByIndex_P 
XPCWrappedNative::CallMethod(XPCCallContext&, XPCWrappedNative::CallMode) 
XPC_WN_CallMethod(JSContext*, JSObject*, unsigned int, long*, long*) 
js_Invoke 
js_Interpret 
js_Execute 
JS_ExecuteScript 
ProcessFile(JSContext*, JSObject*, char const*, __sFILE*, int) 
Process(JSContext*, JSObject*, char const*, int) 
ProcessArgs(JSContext*, JSObject*, char**, int) 
main 
_start 
start 

<<<<<<<

The offending line is the |appStartup.quit(Ci.nsIAppStartup.eForceQuit)| line, not surprisingly. I don't know, but I suspect you cannot legally call this method from xpcshell. bsmedberg?
