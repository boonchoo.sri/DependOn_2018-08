Remove ability to set Firefox as the default browser from the stub installer and installer
Created attachment 
opt-out-checkbox.png

We should remove the default browser opt-out checkbox in the stub installer for Windows XP, Vista and &  This opt-out checkbox doesn't exist in the stub installer for Windows  or Windows  In Bug  we learned that not having the checkbox reduces the instances of people setting Firefox as their default browser, but it doesn't affect how much they use it which is what we're really concerned about. In addition, in user testing this checkbox was sometimes seen as "a gotcha moment" or like we were "sneaking it in." That is definitely not the kind of impression we want to make on people.
