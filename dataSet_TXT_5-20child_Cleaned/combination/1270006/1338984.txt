The "attention" state of the Downloads Indicator does not have enough contrast for the "arrow" style progress
By comment #74 of bug  the contrast of the icon on OS X  is insufficient. We either need to ask someone from UX to update the toolbar icon in Toolbar.png and  or use an SVG filter to brighten it.
