Places search box does not have an accessible name



The Places search box does not expose an accessible name to assistive technologies. By contrast, the main web search box has an accessible name of "Search".




Open Inspect32.exe
Open Places
Set focus to Places search box



Inspect32 shows accessible name = ""


Inspect32 shows a useful accessible name, such as "Search bookmarks and history" (if that option is selected) or "Search <collection name>" if "Current collection only" is selected.
