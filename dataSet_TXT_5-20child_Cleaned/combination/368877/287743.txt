Firefox "back" and "forward" toolbar submenus not keyboard accessible
Next to the back and forward icons on the toolbar are small dropdown buttons
that provide a list of  sites in the history, in either the back or fwd direction.

One option would be to add "Back to" and "Forward to" items to the Go menu, each
with the  submenus right there.

Or these functions could be combined into one thing in the Go menu, as follows:
Back >
Google News Alt+Left
Slashdot
ESPN
CNN

Forward >
Google Local Alt+Right
Ars Technica
