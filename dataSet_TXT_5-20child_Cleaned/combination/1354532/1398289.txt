Library > Downloads Panel sub-label font-size is too small
Created attachment 
downloads-panel-font-size.png

See the attached image for more detail. We should be following the same pattern as in the Downloads Menu. Current font-size is .7em and looks like it should be .9em.
