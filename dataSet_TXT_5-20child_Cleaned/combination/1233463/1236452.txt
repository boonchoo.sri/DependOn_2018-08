DOMHelpers.onceDOMReady
DOMHelpers.onceDOMReady never resolves if the target document is already loaded.
Ideally, the caller wouldn't have to check that before calling onceDOMReady,
but we need additional parameter (target URL) to check that.

I hit that issue while working on bug 
