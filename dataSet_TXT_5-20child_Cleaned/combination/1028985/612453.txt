Provide search suggestions on Firefox Start Page (about:home)
Created attachment 
prototype patch

Step to reproduce:
Type something in the search box on about:home

Actual result:
No suggestions.

Expected result:
Suggestions appear.

This is just one step in enhancing the search experience on about:home to bring it closer to the rich interface provided by Google's home page.

=====

The experimental prototype patch uses the HTML5 datalist element to provide autocomplete suggestions via the Google Suggest API over SSL.

Unfortunately, the list of suggestions flickers when using this API, which should not happen. See our in-toolbar search box for a great non-flickery experience.
