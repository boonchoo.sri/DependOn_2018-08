Caret browsing doesn't work well in XUL documents (such as about:preferences) and should be disabled there
Reproducible on the latest Beta (BuildID:20140407135746)
(Windows NT  WOW64; rv:29.0)  
Reproducible on the latest Aurora (BuildID:20140407004002)
(Windows NT  WOW64; rv:30.0)  
Reproducible on the latest Nightly (BuildID: 
(Windows NT  WOW64; rv:31.0)  

Steps to reproduce:
Open about:preferences.
Go to Advanced tab, on General and select the first option from Accesibility (“Always use the cursor keys to navigate within pages”)

Actual results: The following intermitent issue can be seen: a prompter ("|") appears in random places (on Advanced tab) in the left side of buttons, radio buttons and checkboxes.

Expected results: No issue occurs.

Notes:  is reproducible also on Ubuntu x86.
issue is not a regression.
