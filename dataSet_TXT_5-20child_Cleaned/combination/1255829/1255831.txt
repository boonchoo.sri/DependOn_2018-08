[FrameComponent] Style unlinkable sources
We can have unlinkable sources listed like "(unknown)", "self-hosted" and the like which should be rendered differently, as we can't link to them.

In the memory tree, we also display a function name which is unstyled, resulting in the function name and source name being styled the same, which could be confusing: 

In web console, with bug  unlinkable sources are just dark blue, looking strange, as they are unlinkable. This should probably be our normal text color at the very least in this case.
