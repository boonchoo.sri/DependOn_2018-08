Editor doe not reopen if you create a new style and then change the orientation.
STR:

Open the style editor on any tab.
Create a new blank style (or may be 
Change the orientation of Style Editor from Horizontal to Vertical (by decreasing its width)
Select any other style sheet (that was not created by you)
Close the Style Editor.
Try to re open it for that tab.

Result

Style Editor does not reopen for that tab.

Expected:

It should reopen.



I get all these errors :

Timestamp:   PM
Error: An error occurred updating the cmd_undo command: [Exception... "Component returned failure code:  (NS_ERROR_INVALID_POINTER) [nsIController.isCommandEnabled]" nsresult: "0x80004003 (NS_ERROR_INVALID_POINTER)" location: "JS frame ::  :: goUpdateCommand :: line  data: no]
Source File: 
Line: 

for cmd_(cut|paste|undo|redo|switchTextDirection)
