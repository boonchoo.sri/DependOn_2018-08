Implement compact and touch theme modes for the navigation toolbar
The idea is that besides the default mode, there would be a compact mode with reduced spacing around UI elements (much like current compact themes) and a touch mode with extra spacing.
