Compact themes should stop messing with the back and forward buttons
Since the compact (and touch) modes will be decoupled from "compact" themes, these themes should stop adjusting the back and forward buttons to be compact.
