"Device" tab is VERY misleading
I have been missing on all the functionalities available under the "Device" tab for months, because I understood that the "Device" tab was used for inspecting things in whatever *physical* device I had connected to the computer.

It took someone in the Gaia team to point out that the "Device" tab allowed accessing things in the current "device" we were connected to, such as the System app.

Can I suggest renaming "Device" to "Debug", "Inspect" or something similar? :-)
