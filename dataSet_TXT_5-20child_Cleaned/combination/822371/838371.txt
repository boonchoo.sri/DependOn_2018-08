The triangle warning icon is displayed instead of the globe icon when mixed active content is loaded even when mixed active content blocking is disabled
+++ This bug was initially created as a clone of Bug #822371 +++
+++ This bug was initially created as a clone of Bug #782654 +++

STR:
Visit  when security.mixed_content.block_active_content=false

Expected results:
Globe icon.

Actual results:
Yellow warning icon.

We need to keep the current behavior (globe) because we need the mixed content blocker to have  user-visible effect when the feature is disabled, for risk mitigation. The behavior is confusing because people do not get why the only occasionally shows up on pages with mixed content. (The distinction between mixed active content and mixed content is not obvious to people.)
