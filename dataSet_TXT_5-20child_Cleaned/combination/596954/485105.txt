improve tab-to-window experience
The current tab-to-window experience is a little clunky; what I see is:

- I click on a tab and start to drag down
* A very translucent version of the page appears on my drag cursor, with the tab still being present in the source window
- I drop the tab
* The window I dragged from flashes, because we removed the tab, and so some other tab comes to the forefront
- And finally the window with the new tab appears

I think the two starred items can be improved significantly:

- When we drag the tab, the drag image should be much more solid near the top, perhaps even completely opaque (but still fading out towards the edges). I would even draw the tab image and put the cursor right on the tab as it moves around, as opposed to anchored to the top left of the image

- When we start the drag, we should hide the tab in the source window and switch to whatever tab we would've switched to otherwise. We can do a quick shrinking animation or something of the tab itself. That way when the user drops, there's no flashing because the content switched, just the new window appears. If the drag gets canceled, we can just unhide the tab.

Cc'ing Alex, since this sounds like a polish-hard bug.
