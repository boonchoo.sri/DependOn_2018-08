Detaching a tab does not open the new window at the drop location (dragging tab to an empty second monitor opens window on the wrong monitor)
If a tab is detached and dropped at a given point, the new window that is opened will be located near the window that contained the tab but not where the tab was dropped.

This is most visible if you have two monitors. If you drag a tab to the other monitor, the window will open on the wrong one. Safari does the right thing in this case.
