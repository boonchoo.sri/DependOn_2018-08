New Tab button should be right of last tab
The new 'new tab' button is placed very out of range of discoverability for users new to tabbing and far away for the sake of convenience.

In the study done on the close tab position (  ) it was hypothesised from data results that the far right position of the close tab button make discovability very low for new users. Surely the same idea can be applied to the new tab button?

Also bug  related
