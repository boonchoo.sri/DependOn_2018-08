[User Story] Based on category of information - present info  query results separated by type
As a user, when I elect to retrieve said categorized information about the term(s) I have selected, I will be presented with different types of information  query results that are separated by type (e.g. definition, local, mapping info, translation, etc.)  source (e.g. Wikipedia, TripAdvisor, Yahoo Finance, etc.), and are relevant to the term(s) I have selected (e.g. place names would bring up Wikipedia, Maps, and Local results. Brand names would bring up the corporate website, Finance info, and shopping results, Trending Topics would bring up News, Twitter, etc.).

Full Theme at: 
