Closing tab flips to next-next tab before reverting to next tab
I've seen this both on Windows and OS X, most recently with Firefox  When I have multiple tabs open, if I close a tab, the content window switches to a tab that's to the right of the "next" tab (the one to the right of the one I just closed) real quick, before reverting to the correct one.

So with tabs    when I close   briefly flashes, then  shows up.
