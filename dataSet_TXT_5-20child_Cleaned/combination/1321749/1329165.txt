Optimize Immutable.js usage in Netmonitor
Profiling the  implemenation of Netmonitor UI with Cleopatra tells me that a lot of time is spent inside Immutable.js:

- finding a record by id in Immutable.List. We do a O(n) sequential search. Switching to Immutable.Map will improve that to O(log n).

- property access in Immutable.Record is expensive - computing hashes, searching in a trie... We need to be more careful about that in code that is executed frequently - like comparators for sorting.
