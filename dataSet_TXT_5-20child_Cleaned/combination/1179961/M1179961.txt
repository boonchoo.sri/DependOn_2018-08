Use a lock with a strikethrough for HTTP pages that have Password Fields in the Control Center
HTTP websites have no business asking for passwords. The passwords can be intercepted by man-in-the-middle attackers and eavesdroppers. Given that password reuse is very prevalent, when an HTTP website asks for a password, it is compromising the users security on its site and any other site where the user shares the same password.

When we detect that an HTTP page has a password field, we should show a grey lock with a strike through instead of the grey globe. If the user clicks the grey globe we can add text to the Control Center.

This is somewhat related to bug  but not exactly. bug  is about adding a doorhanger. This bug changing the existing globe icon without popping anything up to the user automatically.

This also moves us a step further in the HTTP deprecation plan 
