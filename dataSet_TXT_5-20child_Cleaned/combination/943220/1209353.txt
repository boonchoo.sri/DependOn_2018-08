[meta] Worker debugger console do not work
ER: if I'm at a breakpoint in worker code, I should be able to use the console

AR: the console is dead, nothing happens. Scratchpad is also dead, eg running `console.log(this)` gets nothing.
