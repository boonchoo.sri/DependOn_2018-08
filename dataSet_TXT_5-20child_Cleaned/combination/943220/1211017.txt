When opening a toolbox targeting a worker  service worker, the title is `Debugger (null)`
Created attachment 
debugger-toolbox-title.png

STR:

Enable devtools.debugger.workers
Open 
Open debugger and click on one of the workers

A toolbox opens and the title is "Debugger (null)". It should probably have the source name parenthesized (or nothing at all!).

See screenshot
