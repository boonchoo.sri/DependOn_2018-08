Implement a Worker Debugger API
This is not currently a high enough priority, but here's what it'll take:

- We need a hook so that when a new Worker is started, we get to run some chrome JS code on that stack (to bootstrap). The Debugger object must be accessible there.

- We need a hook to interrupt an existing Worker and run some chrome code on its stack, again with the Debugger object accessible (to attach to a running Worker).

- The root actor needs code that uses the above features to make Workers debuggable.

- Additional remote debugging API needs to be designed for this. We need to support enumerating existing Workers, distinguishing between chrome and regular Workers, being notified when new Workers are created, inspecting their parent relationships, peeking at their message queues, being notified when messages are sent...
