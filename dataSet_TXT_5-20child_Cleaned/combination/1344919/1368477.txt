Disabled search suggestions are being enabled after update
(Windows NT  WOW64; rv:55.0)  
Build ID: 

[Affected versions]:
- Nightly 

[Affected platforms]:
- All

[Steps to reproduce]:
Install an older version of Nightly  where search suggestions are not enabled by default 
Launch the older version of Nightly 
Focus the URL Bar and start typing
In the search suggestions, opt-in notification bar select NO
Update to the latest Nightly 
Focus the URL bar and start typing
Navigate to about:preferences#general and check the status of the option "Show search suggestions in location bar results"

[Expected result]:
- In step  - the search suggestions should be disabled
- In step  - the "Show search suggestions in location bar results" box should be unchecked

[Actual result]:
- In step  - the search suggestions are enabled
- In step  - the "Show search suggestions in location bar results" box is checked
