Decide how to handle external libs post file migration
Some external libs won't be following the "require match source tree path" rule even after bug  including:

* glci
* acorn
* tern
* sourcemap

We should decide how to handle this, and makes changes if needed.
