identity ui: Non-PFS should be called as weak encryption (yellow triangle)
User Agent:  (X11; Linux x86_64; rv:55.0)  
Build ID: 

Steps to reproduce:





Actual results:

Plain RSA is shown as secure as the best encryption like TLS  or TLS1.2 + ECDHE-RSA-AES256-GCM-SHA384.


Expected results:

A plain RSA website should not have a green padlock without a yellow triangle. (weak encryption identity)
Please make a pref to warn about ancient encryption, which should get enabled in Nightly be default (for now).

What should get checked for this:
* cert has OCSP address, but not response is stapled + response is not available
* if TLS  or TLS  is used
* if the cipher (even in TLS  is
TLS_RSA_WITH_3DES_EDE_CBC_SHA
TLS_RSA_WITH_AES_256_CBC_SHA
TLS_RSA_WITH_AES_128_CBC_SHA
* show warning also if a subrequest breaches this

and later in the future when TLS  is avaiable in Nginx+Apache, those additional ciphers:
TLS_DHE_RSA_WITH_AES_256_CBC_SHA
TLS_DHE_RSA_WITH_AES_128_CBC_SHA
TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA
TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA
TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA
TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA
we may open a new bug for this far later.

This is in a line with bug  and bug  for bug 
