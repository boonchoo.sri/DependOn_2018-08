security.ssl.treat_unsafe_negotiation_as_broken is not indicated for subresources on a secure page
User Agent:  (X11; Linux x86_64; rv:55.0)  
Build ID: 

Steps to reproduce:

you set securiy.ssl.treat_unsafe_negotiation_as_broken to true.
(has a green padlock with yellow triangle as it should)
but if you go to the parent site, this error of a subrequest is not shown:  (green padlock without yellow triangle)



Expected results:

A website should have a green padblock with a yellow triangle if a subrequest would have one.

I want to suggest to enable this pref by default, but only in Nightly for now. The "enable for all" bug would be bug  It would be nice if bug  could get enabled by default in Nightly together with this on the long way to bug 
