Feedview: The date is incorrect
The shown date is incorrect in some case because the day part is done in UTC but
the hours and minutes are done using local time

I found this bug because a post had this GMT string:

Wed,  Jul   +0000

parsing this using js Date gives:

Thu Jul    GMT+0200

so the Feedview displays:

Wed,  Jul @ 


I guess this bug will be marked as invalid because the date time needs to be
localized anyway. But until the date time is is localized this is indeed a valid
bug.
