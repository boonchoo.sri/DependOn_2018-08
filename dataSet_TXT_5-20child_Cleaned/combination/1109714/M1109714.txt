Handle anonymous XUL  subscriber UI in e10s
We currently embed XUL inside the HTML document that's generated when viewing feeds in order to show the subscribe UI. With e10s this will no longer be possible as we are not going to support XUL popups from the content process.
