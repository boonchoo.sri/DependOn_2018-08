Add SHA-1 warnings to web console for end entities
SHA-1 is on the way out. While SHA-1 certificates remain valid in the short term, developers need to be aware if their certificate (or its chains) uses SHA-1.
