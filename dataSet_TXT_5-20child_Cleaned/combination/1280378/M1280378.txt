Write system addon to direct users to download a fresh copy of Firefox from www.mozilla.org
We want to deploy a system addon to users who are clearly out-of-date. These users are also known as "orphaned users". Currently, users are considered orphaned when they use a version of Firefox that is at least  versions behind the current version. We may choose to deploy the system addon to more users at a later point (for example to users who are  versions behind).

We met in London and agreed that the system addon should display a yellow bar across the top of web content in the browser, notifying the user that Firefox is out-of-date and it should direct the user to  to download a fresh copy. This yellow bar should not be dismissable.
