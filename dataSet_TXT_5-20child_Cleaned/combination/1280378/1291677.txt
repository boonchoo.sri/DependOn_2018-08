[Out Of Date Notification] Notification bar can't be selected with keyboard only
[Affected OS]:
-Mac

[Affected versions]:


[Prerequisites]:

Install Firefox version 
Outofdate notifications system add-on required configurations:
about:config set extensions.systemAddon.update.url to value 
about:config set extensions.logging.enabled to value "True"
about:config Add string app.update.url.override with value "www.softvision.ro"
Set update preferences -> about:preferences#advanced and set Firefox updates to "Automatically install updates (recommended: improved security)"
Force system addons check : Open  Console and run the snippet: "  AddonManagerPrivate.backgroundUpdateCheck(); "

In the   a similar line should be listed: "addons.productaddons INFO Downloading from  to 

Restart FF to install the OutOfDate System Addon - !!!! Do not open About:Support to check if updates are being downloaded --> use about:support


[Steps]:
Open FF44.*
Navigate through page using keyboard only, use "TAB" key, and select OutofDate notification bar.

[Expected Result]:
Out of Date notification bar can be selected and "Get Firefox" button can be activated if you push Enter.

[Actual Result]:
The Out Of Date notification bar can't be selected and "Get Firefox" button can be triggered.
