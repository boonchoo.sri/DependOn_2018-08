Analyze telemetry data from out of date system addon
The out of date system addon will be submitting telemetry data[1]. We should analyze this data to answer the following questions:
Do users continue to run Firefox  after having seen the notification bar?
Do users continue to run Firefox  after having clicked the button to download the latest version?
Do users get used to seeing the notification bar and don't click the button?

By correlating telemetry IDs with main ping telemetry, it would also be great to see how many people (percentage or total number of users) end up on the most recent version of Firefox.

[1] 
