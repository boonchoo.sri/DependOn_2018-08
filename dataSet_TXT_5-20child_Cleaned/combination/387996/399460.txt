if browser crashes or is killed before we write out prefs, you can get two Places folders on the toolbar
Created attachment 
screenshot



- Make a new profile with a branch build
- Run it with a trunk build (you'll see one Places folder)
- Paste a recent places.sqlite file in the new profile (or delete everything except places.sqlite)
- Start Minefield

Result: you'll see two functional Places folders
