queries that use maxResults can return incorrect results due to post query filtering
maxResults is integrated into the SQL query, and any post-SQL filtering can affect it's validity be reducing the size of the final result set.
