searching in bookmarks (without restricting the folder) ends up with duplicates due to the tag root
searching in bookmarks (without restricting the folder) ends up with duplicates due to the tag root

in bug #393464, marcia wrote:

when I tag the espn.go.com home page with "sports" and then
search for "sports" in organize bookmarks. This happens when I tagged other
sites as well - I always seem to get two entries that come up.

in bug #393464, michael writes:

This has been thus since tags hit the trunk. I saw it when I searched bookmarks
for  to see the infamous invisible "all bookmarks". You get one "no title"
entry per tag, plus the titled original. The untitleds show up in the bookmarks
table, as I recall, as children of the tags, one row per child.

I just assumed it was due to the lack of UI at the time, and they'd be hidden
from view once tags were functional, so I didn't file a bug.
