CSS Cleanup on side panels
Created attachment 
Firefox_WebIDE.png

Fix the following:

- padding, margins, text layout
- buttons (larger buttons are not styled consistently)

To enable for  locally:

Go to about:config and set |devtools.webide.enableLocalRuntime| to true and |devtools.webide.sidebars| to true

Reopen WebIDE
