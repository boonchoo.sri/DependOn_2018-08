Places UI: Selecting a folder in the bookmarks sidebar, menu or toolbar should navigate on that folder
Note: this is a meta tracking bug for UI mockups and discussion of the overall user experience.

When the user clicks on a folder in the bookmarks sidebar or bookmarks menu, we should navigate the content are to display the contents of that folder.

In the case of the bookmarks toolbar, we will need to make it clear on hover that folders can either be navigated on (by clicking directly on them), or expanded to show their contents in a menu (by clicking on the small triangle to the right). On hover there should be a clear line separating the two different sections of the split button control.
