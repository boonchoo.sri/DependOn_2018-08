Escape no longer stops animated GIFs
User Agent:  (X11; Ubuntu; Linux x86_64; rv:20.0)  

Steps to reproduce:

Load a page containing an animated GIF, or just the GIF itself. Here's a suitably amusing one: 
Press Escape on the keyboard.


Actual results:

Nothing.


Expected results:

All animated GIFs on the page should stop, the expected behaviour since circa Navigator 
