Web Console on chrome: URLs allows input even if devtools.chrome.enabled is false
Steps to reproduce:
Make sure devtools.chrome.enabled is false.
Open about:newtab (or about:config or any other chrome-privileged pages).
Press Ctrl+Shift+K to open Web Console on the page.

Actual result:
Web Console have an input field.

Expected result:
Web Console on chrome-privileged pages should have no input field unless devtools.chrome.enabled is true, just like Browser Console (bug 

Attackers can instruct users to type the secret command "Ctrl+T Ctrl+Shift+K blah-blah-blah" to pwn the browser using the self-XSS.
Looks like this attack scenario is already pointed out in bug  comment #23, but it was ignored somehow.

If this is by design, feel free to WONTFIX this. It is very good for me :)
