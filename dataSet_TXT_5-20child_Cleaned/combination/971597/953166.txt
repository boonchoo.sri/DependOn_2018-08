Devtools panel should show a note explaining what it is on first open
Sparked by this: 

I think Aral is right when he says that we could do better here. I don't know how frequently this happens, but it certainly does every now and again.

One downside is that it could be annoying when using throwaway profiles. Thus, it should be very easy to click away, or just hide itself after a short-ish timeout if the user doesn't interact with it. (Though I don't know what that interaction should look like.)
