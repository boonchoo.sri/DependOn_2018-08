add temporary  methods for testing convenience
The current SocialService doesn't support  providers, which makes it difficult to test certain things without directly creating SocialProvider objects. Let's fix that by adding  helpers (that don't actually persist changes beyond the current session, for the moment).
