SSL state should be visually separated from rest of URL bar
Created attachment 
proposal ss

SSL state should be separated from normal state.

User confuses a state of current url connection without SSL state separated from normal http state. Google Chrome & Opera separate state SSL from normal http.

I propose that the block of part favicon (lock icon) should be colorized when an url is https connection.
(Attachmented screen shot is old identity block with "browser.identity.ssl_domain_display;0")
