[new tab page] Tiles are sometimes arranged all in a single line (wrapping as appropriate, e.g. to two lines with  items and then  items), instead of  grid
Created attachment 
screenshot.png

I don't have STR, but this has happened twice in the past few days, so it's not a fluke.

Basically, occasionally my "new tab" page has the tiles in an awkward  layout:



...instead of...




This just happened to me when I quit & reopened Firefox, when a new tab page was restored. Subsequent "new tab" pages that I opened had the correct  layout.

Screenshot attached
