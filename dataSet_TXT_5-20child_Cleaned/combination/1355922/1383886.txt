Remove redundant element.closest() call in 
Right now we do:
> if (!libraryButton.closest("toolbar") ||
> libraryButton.closest("toolbar").id != "nav-bar") {

But this can just be replaced by:
> if (libraryButton.closest("#nav-bar")) {
