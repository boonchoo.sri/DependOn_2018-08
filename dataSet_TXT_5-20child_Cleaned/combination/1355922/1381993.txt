The  animation is placed in the wrong position when using a non-default font-size
+++ This bug was initially created as a clone of Bug #1381991 +++

STR:
Open the Browser Toolbox
Inspect the <window>
Set the font-size on the window to font-size:1.4em;
Load mozilla.org

Expected result:
The  animation should run, vertically centered and aligned with the other toolbar icons

Actual result:
The  animation is shifted down and not in line with the other toolbar icons.
