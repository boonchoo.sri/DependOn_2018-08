[10.7] Update Firefox’s Toolbar Button Style for OS X Lion
Created attachment 
Toolbar Button States in OS X Lion

OS X Lion introduces a new toolbar button style and a new button state for hovering over buttons in an inactive window. Firefox’s toolbar buttons should be updated to match this new style.
