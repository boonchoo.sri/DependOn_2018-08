[10.7] Update Firefox Theme for Mac OS X  Lion
Created attachment 
Mockup of OS X Theme Refresh for Lion

The release of OS X Lion is nearing and running Firefox on it works well, but it’s default theme hasn’t been updated yet to incorporate all the visual loveliness introduced in this OS update.

So this bug is for tracking all the theme changes that are necessary to feel native again. Above you’ll find a mockup, showing what I think is necessary to be done.
