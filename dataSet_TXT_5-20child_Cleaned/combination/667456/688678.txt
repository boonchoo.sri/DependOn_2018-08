[10.7] unfocused Firefox window needs dimmed text in tab titles and addressbar
The text in the tab title and the addressbar in an unfocused Firefox window on Lion is far too dark. IMO, it should be the same as the titlebar text or at least closer to that. Otherwise, on the new paler background with more muted buttons, the text sticks out like a bit of a sore thumb.

Safari grays out the tab title text but not the addressbar text, FWIW.
