Don't abort shutdown on plugins sanitization
We can't do much about plugins sanitization taking huge time in a short time.

So, alternatively, we can try to avoid waiting too much for it, in the end we don't get immediate benefits from knowing a plugin causes us to crash on shutdown, since we can't control their code.

We can either set a timeout after which we give up waiting for the plugin, or we could just not wait at all. In any case we should try to retain the plugin sanitization telemetry.
