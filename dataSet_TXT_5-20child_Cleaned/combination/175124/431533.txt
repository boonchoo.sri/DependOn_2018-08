Bookmarks menu "Open All in Tabs" should not replace the currently existing tab (unless it is blank), if browser.tabs.loadFolderAndReplace is set to false



"Open All in Tabs" in Bookmarks menu or context menu of Bookmarks Folder should not replace the currently existing tab (unless it is blank), if browser.tabs.loadFolderAndReplace is set to false.




browser.tabs.loadFolderAndReplace to false.
a new bookmark folder and make some Bookmark items in the folder.
some tabs.
"Open All in Tabs" in Bookmarks menu or context menu of Bookmarks Folder on the created bookmark folder.

Replace the currently existing tab.


Should not replace the currently existing tab.


It is no problem that middle clicking of Bookmarks folder.

It is no problem in Firefox2.0.0.14
