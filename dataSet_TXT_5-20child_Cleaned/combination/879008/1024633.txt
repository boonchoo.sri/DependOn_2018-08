Add a way of specifying a fixed width or height for canvas graphs
Currently, they flex to span across all the available space of the parent container. Sometimes, this may not be desirable.
