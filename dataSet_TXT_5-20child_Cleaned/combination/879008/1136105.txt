Having Greasemonkey enabled (even with no scripts) makes the Performance tool hang at "loading..." and the recorded profile is not displayed
Created attachment 
Video_2015-02-24_143609.wmv

User Agent:  (Windows NT  WOW64; rv:36.0)  
Build ID: 

Steps to reproduce:

In a clean Firefox  stable profile:
- install Greasemonkey addon v2.3 (without any userscripts) and restart FF
- launch the Performance tool (shift+F5 or via the hamburger button|Developer)
- press the Record button (either of the two available) to start recording a new profile
- open any page, eg 
- after the page has loaded press Record button again in order to stop the recording.


Actual results:

The Performance tool displays "Loading..."
(note that the text "Loading..." has a momentary display glitch then)
and hangs at "Loading...",
the recorded profile is not displayed,
and clicking again on the Record button doesn't fix the problem.
(see the attached video)

Disabling Greasemonkey and restarting Firefox fixes the problem.


Expected results:

Clicking on the Record button
should make the recording stop.
Then, the recorded profile should be opened
and the Record button should be available again.
