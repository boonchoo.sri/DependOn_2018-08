The framerate actor needs a way to cancel a recording without retrieving the accumulated data
Right now, stopRecording and getPendingTicks both return the accumulated refresh driver ticks. In bug  a method of cancelling a recording without having additional protocol transfer overhead is needed.
