Add nsIBrowserSearchService.selectEngine
See breakdown bug  Quoting 

> 
> * Selects a new search engine to use as the current engine.
> *
> * Behaviour is to require explicit confirmation from
> * the user through a browser-provided dialog. Add-ons must use
> * this API to modify search behaviours or  blocklisting
> *
> * @param engine
> * The engine to select.
> * @param addonID
> * the ID of the add-on requesting the change
> *
> * @throws NS_ERROR_INVALID_ARG if engine is unknown or addonID is invalid
> 
> void selectEngine(in nsISearchEngine engine,
> in AString addonID);
>
> * addonID should be used to get the name (at least) of the add-on triggering the
> prompt to include as the requester. Invalid
>
> * addonID + version should be recorded, and multiple prompt attempts blocked if
> the user chooses to keep current.
>
> * [optional] To reduce bad behaviour, we should attempt to identify the source
> of the API call and verify it's the same add-on as passed in. Bad behaviours
> should be  somehow.

nsIBrowserSearchService lives in netwerk and its implementation is in toolkit. To hook it up to the UI part in the front end, I think we can do what prompts do: have an nsISearchEnginePrompt, have an object in browser implement it, and then the search service implementation would call a method on it that shows the UI. The method would return the user's choice, I guess asyncly via a callback?

This bug only covers the  part. Another bug will cover the front-end part.

Regarding the optional third bullet point above, I wonder if it's possible to get the compartment or sandbox or principle of the calling add-on code, and then look up the add-on's metadata with it. It's weird that selectEngine takes an add-on ID to begin with. It's the honor system and pushes security out to add-on reviewers. But, given that this is the spec, and this point is optional, we should leave it to a follow-up.
