use same proxy for all protocols works incorrectly after  to  upgrade



Issue with the "Use same proxy for all protocols" check box in the connections
preferences. Even though box is set before upgrade, settings do not take for
all protocols. Found a workaround which the user should not have to perform.

WORKAROUND:

Unchecked "use same proxy for all protocols"

Filled in the same text for each individual protocol, pointing to correct
proxy server and port

Browser works fine now




USE CASE:

Upgraded from  to  Prior installation had "use same proxy for all
protocols" checked in the Preferences.

The box was checked and only the HTTP PROXY line was filled in.

Was able to connect to HTTP sites, however "Connection refused" errors when
attempting to connect to HTTPS sites.

The correct HTTPS proxy was set (but greyed out, due to "use same proxy..."
check box)

Further examined prefs.js file. The HTTPS, FTP, other protocols were set to
"localhost:8080" which did not correspond to what the Preferences indicated.


Used only the configured proxy server for the HTTP protocol, even though use the
same for all protocols was checked.


It should have used the same proxy server for all protocols. No user
intervention should have been necessary after the SW upgrade.

See workaround.

I set this as major because it may impact people upgrading Firefox.
