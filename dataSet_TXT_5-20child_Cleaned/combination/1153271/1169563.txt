Add a timeline scrubber to the list of animations to visualize the current time and set it
Bug  introduces a new display for the animation-panel, with animation timelines and a time scale header.

In this bug, we should add a vertical scrubber line that moves when animations progress and that can be moved  to set the current time of all the current animations.
