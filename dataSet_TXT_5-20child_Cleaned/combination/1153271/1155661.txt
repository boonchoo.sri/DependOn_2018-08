Add playback control buttons to the top toolbar in the animation inspector
The animation inspector will show more animations with bug  and specifically, animations that are often sequenced together.

In this context, it makes sense for the top toolbar (now only used for the global  button) to become the toolbar that can control all currently displayed animation widgets.

I can see  cases:

- there are currently no animations displayed inside the sidebar:

in this case, the top toolbar should contain the global  button, like today + a global playback rate select to set the playback rate for *all* animations on the page.

- there are animations displayed in the sidebar:

in that case, the controls in the top toolbar become contextual to the animations currently displayed and allow:  playback rate selection, rewind, fast-forward.

See the mockup in bug  comment 
