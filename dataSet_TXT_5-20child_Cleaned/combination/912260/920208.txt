Except conversion, almost functions of IME are unusable in Scratchpad.


(Windows NT  WOW64; rv:27.0)   ID:20130924030202

Steps To reproduce:
Open ScratchPad
Type text using IME(IME2010, ATOK maybe)

Attempt to Segment Width Shrink(Shift Left Arrow)
Attempt to Segment Width Expand(Shift Right Arrow)
Attempt to Segment Focus Left(Left Arrow)
Attempt to Segment Focus Right(Right Arrow)

Actual Results:
These IME functions do not work


These IME functions should work properly
