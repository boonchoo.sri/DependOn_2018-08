[INSTALLER] Windows Installer
Feature tracking. Windows Installer FE requirements analysis, respecification,
implementation. Creation of generic installer that can be used by Firebird and
Thunderbird.
