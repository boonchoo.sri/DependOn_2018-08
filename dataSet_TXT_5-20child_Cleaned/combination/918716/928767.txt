Don't show inspector color swatches next to special values
User Agent:  (Windows NT  WOW64; rv:27.0)   
Build ID: 

Steps to reproduce:

Open the inspector.
Inspect something with background-color: 


Actual results:

There's a color swatch next to these values.


Expected results:

There shouldn't be a color swatch next to those values.
