Location Bar doesn't highlight domains with ports
Created attachment 
Example

User Agent:  (Windows NT  rv:7.0)  
Build ID: 

Steps to reproduce:

Enter a domain with a port such as 


Actual results:

All of the text in the location bar is black


Expected results:

The domain part and the port should be highlighted
