scroll position for restored inactive tabs is not remembered
User Agent:  (Windows NT  WOW64; rv:40.0)  
Build ID: 

Steps to reproduce:

Enable "Show my windows and tabs from last time" and "Don't load tabs until selected".
In one tab, load a site that's scrollable and has a form for text input; scroll whatever amount and input whatever text in the form.
In another tab, load any site. Make this tab active before the next step.
Close the browser, then open it.
Repeat step  then select the tab from step 



Actual results:

Scroll position and form data for tab in step  are not restored.


Expected results:

Scroll position and form data for tab in step  are restored.

The mozregression tool produced this:

