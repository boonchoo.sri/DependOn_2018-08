Closing a tab that closes a dependent tab breaks tabbrowser and leaves you with a useless + tabless window
User Agent:  (Windows NT  WOW64; rv:43.0)  
Build ID: 

Steps to reproduce:

Open a Google Spreadsheet
Open the Script Editor from that tab via the website's menu bar (opens a second tab)
Close the Spreadsheet tab


Actual results:

The second tab closes as well


Expected results:

The second tab should have stayed open as the preference (set to false, as it is by default) should prevent it from being closed
