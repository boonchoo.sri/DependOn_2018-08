add speculativeConnect() method to search engines
Created attachment 
searchSpeculativeConnectAPI

We use nsISpeculativeConnect to speed up search  in a couple of places (desktop searchbar and mobile search, the latter of which doesn't do the right thing if search and suggest URLs differ). I'd like to use it in about:home in bug  and eventually about:newtab (though that should be one shared bit of code). At this point, it seems saner to just make this a method on the engine object.

Thus, here's a patch that does this. If this is good, I'll replace the mobile caller with the API call as a part 
