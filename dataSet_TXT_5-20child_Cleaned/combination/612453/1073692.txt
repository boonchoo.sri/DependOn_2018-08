Search suggestions popup on  should show each result according to its text direction
Created attachment 
patch

dir is set on the table, which means that the first result in the popup determines the text direction of the whole popup. So if the popup contains both RTL and LTR text, one or the other will be shown incorrectly.

Instead dir should be set on each row.
