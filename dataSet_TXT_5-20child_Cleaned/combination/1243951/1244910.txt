JSON Viewer: font size and family styles need improving
Let's review the current font choices for the JSON viewer.

from bug  comment 

> ## Readability: problematic typographic choices
>
> By default the text is hard to read, since it’s displayed as  Courier on
> OSX or  Courier New on Windows. The combination of the small font-size
> and a font with a small x-height hurts readability big time.
>
> Let’s break it down:
>
> Size: the JSON viewer doesn’t follow user preferences for monospace
> font-size (hard  This is unlike the View Source view (uses the user’s
> preference,  by default), but similar to what the DevTools tend to do
> (11px by default, no user preference except for the global zoom).
>
> If the JSON viewer will use the same (small) font-size as the DevTools,
> perhaps it should use the DevTools zoom level too? So that if a user finds
>  too small in the DevTools and has zoomed it, this will be matched in
> the JSON viewer.
>
> Font family: currently the JSON viewer CSS uses "font-family:monospace;",
> but sadly   New is a no-go, and Firefox seems stuck with
> Courier  by default for monospace web content on Windows and OSX
> (those OSes are using more modern and more readable monospace fonts, mostly
> Menlo and Consolas I think, but I guess the point is to not "break" the
> visual style and spacing of sites relying on the small  New).
>
> So, if the JSON viewer is not going to follow the user’s preferences for
> monospace font-family *and* font-size, it should imitate the DevTools better
> and try to pick a more readable font. (The Inspector uses a
> --monospace-font-family custom property, I guess with one stylesheet per
> platform. On OSX it's set to Menlo.)
>
> Finally, some of the text is sans-serif, which is alright for headings in
> the Headers tab, but perhaps not so great for keys in the main JSON view. In
> any case, the font-family used is: "Lucida Grande, Tahoma, sans-serif" (same
> on OSX and Windows). It would be nice if this text could use the main system
> font (or main UI font used by Firefox). Apparently using "font:message-box"
> gets you the system font, and it's the technique used all over the place in
> Firefox UI (DevTools, preferences).
>
> (I'm attaching a screenshot of the Headers tab with the OSX system font +
> Menlo for the values + small spacing tweaks + font-size bumped to just 
> I’d say it looks nicer.)
