Onboarding tour links remain highlighted after selection
Affected builds:
- Firefox  Aurora, build ID: 
- Firefox  Nightly, build ID: 
Affected platforms: Windows   Mac OS X  and Ubuntu  

STR:
Open Firefox using a clean profile;
Open a newtab page;
Click one of the links from the onboarding tour.

Results:
The links remain highlighted. Navigating between the tour tabs resolves this issue.

Screenshot: 
