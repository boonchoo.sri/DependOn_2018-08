Wrong styling for the onboarding tour footer links
Affected builds:
- Firefox  Aurora, build ID: 
- Firefox  Nightly, build ID: 
Affected platforms: Windows   Mac OS X  and Ubuntu  

STR:
Open Firefox using a clean profile;
Open a newtab page;
Visit the "More about new Tab" or "About your privacy" links from the page tour.

Results:
The styling is wrong after the links are visited, the links become black.

Screenshot: 
