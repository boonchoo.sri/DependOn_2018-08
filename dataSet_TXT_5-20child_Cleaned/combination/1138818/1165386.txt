Always show SPONSORED for any type of sponsored tiles and SUGGESTED for other suggested
(STR: Open a new tab in a fresh Firefox profile. Click through the newtab tour.)

The newtab tour (added in today's nightly, via bug  I believe) includes this language:
> Some of the sites you will see may be suggested by Mozilla
> and may be sponsored by a Mozilla partner. We'll always
> indicate which sites are sponsored.

This can be read to indicate that there are two classes of tiles -- "suggested by Mozilla", and then (within that) "sponsored" tiles.

Then, when I end the tour & see the actual new tab, it's quite confusing, because:
- *all* of the tiles seem to be "suggested by Mozilla" (they're all provided by Mozilla & are mostly Mozilla-related)
- only one tile is actually tagged as "Suggested". Given the tour, this seems odd.
- *Zero* tiles are tagged as "sponsored". But there is one that's clearly sponsored (and it's the one that's labeled as "suggested")

EXPECTED RESULTS: The sentence "We'll always indicate which sites are sponsored" needs to be clearer & maybe use the word "suggested" to make the connection more direct. (assuming the "suggested" label really means "sponsored")
