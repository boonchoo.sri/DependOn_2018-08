Toggle performance analysis view without reloading if analysis has already been done
When performing a performance analysis from the network panel, the page reloads. I would actually suspect the analysis already been done when clicking the "toggle performance analysis" icon on the lower right hand corner - similar to Bug  regarding the network request panel.

When clicking on the "Back" button within the performance analysis tool it simply switches to the network request panel as expected. But when clicking the button again, it performs another refresh. Is that a requirement? I suspected that I could toggle between both views without refreshing the page all the time.
