images should have preview tab
It seems that the preview tab is for HTML only? Images should have it too. Sometimes it's just easier to switch to the tab rather than try and perfectly hover the mouse.

Also, I thought I had an outdated build or something when the tab was missing.
