Secondary doorhanger options menu can't be opened with the keyboard on Ubuntu
Alt+Down is supposed to open the secondary options menu, but it doesn't since Alt dismisses the panel, at least on Ubuntu.
