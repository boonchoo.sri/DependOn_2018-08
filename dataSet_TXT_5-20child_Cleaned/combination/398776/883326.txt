swapping docshells does not swap popup notifications
Created attachment 
swap notifications prototype patch

If you have a tab that has dismissed notifications (ie. you didn't grant or deny the permission, just clicked away) then you drag that tab out to a new window, the notifications are lost.

I started working on this as a part of bug  and bug  (though this issue is generic) and got part way (see attached patch, notifications are swapped to new browser). What I realized is that some notifications modify their panel prior to calling PopupNotifications.show (e.g. webrtc, see prompt() in  To make this work, we'll need a way to callback to any prompts that do this kind of modification so the new window is setup properly.
