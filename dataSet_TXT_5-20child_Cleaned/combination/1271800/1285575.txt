Allow distinguishing imported passwords so we can remove only those passwords (so that we can make 'undo' of automatic import available for longer)
Right now the 'undo' functionality just wipes out all the passwords. To make this better, we need:

some way of tagging the autoimported passwords in the  manager
use this in the various browser importers
an API to remove all these logins based on their tag
