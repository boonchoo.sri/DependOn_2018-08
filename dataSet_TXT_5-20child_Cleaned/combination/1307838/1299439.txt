The perf tool is really slow or unresponsive when used from the browser toolbox
STR:
- Open a really simple page in a tab. In my case I only had one tab opened in my browser, on 
- Open the browser toolbox
- Switch to the Perf tool
- Start a recording
- Switch to the browser window and do something quick, like resizing the window a little bit
- Switch back and end the recording
- Wait!

I don't think it used to be this way, because I've used the perf tool in the browser toolbox a few times in the past, and I don't remember it being this unresponsive.

Now, it's basically unusable, it freezes for minutes (maybe forever, I haven't tried waiting mor than  minutes or so) and is unresponsive to user events.
It's obviously performing some really intensive tasks that prevents the whole process from doing anything else like:
- hovering over tabs in the browser toolbox doesn't even display them in dark grey like they normally do,
- trying to move the toolbox window doesn't work, or seconds later,
- trying to select an area of the timeline doesn't work.

In fact, after a few minutes, nothing works for me anymore at all and I have to kill the browser toolbox.

I tried using the perf tool in the *normal* toolbox too, and noticed that right after a recording was made, there's a bit of time when the tool is unresponsive. That's when it needs to load a lot of the data and draw the various graphs. But after a few seconds it works fine.
This initial processing that takes only a few seconds in a tab with a short recording is probably what takes minutes in the browser toolbox. Although I'm relatively certain that it didn't use to be this way.

Tested on the latest  on a desktop PC with Windows  And a rather powerful one too:
- Dell Precision T1700
- Intel Core i7 
-  RAM
