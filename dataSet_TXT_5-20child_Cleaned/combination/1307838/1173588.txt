can easily be blocked and fail
When calling  between long running JS, or back to back, or even with a small delay, it seems that `console.profile()` hits possibly at the same time as `console.profileEnd()`, which results in starting the profile AFTER the time range, meaning profileEnd will never be found, so the console profile recording will go on indefinitely. Can easily recreate this or use the sourcemaps[0] benchmark which will do it. This scenarios work in chrome's profiler.

Scenarios where this will fail:

console.profile();
console.profileEnd();


console.profile()
<long running js>
console.profileEnd()


console.profile()
<long running js or nothing at all>
setTimeout(() => console.profileEnd(), 


[0] 
