Fix waterfall-view display style and direction in RTL locales
Created attachment 
fix-waterfall-view-display-style-and-direction-in-rtl.png

User Agent:  (Windows NT  WOW64; rv:51.0)  
Build ID: 

Steps to reproduce:

Start Nightly in RTL locales (e.g. Arabic)
Go to 
Open DevTools > Performance
Start Recording and stop
Select "Waterfall" view
Open any twisties
Check waterfall-view


Actual results:

- .waterfall-marker-bullet and .waterfall-marker-name have wrong hanging indent
- .waterfall-header-ticks has wrong direction
- .waterfall-header-ticks does not have line alignment
- .waterfall-marker has wrong direction



Expected results:

- Fix .waterfall-marker-bullet and .waterfall-marker-name hanging indent
- Fix .waterfall-header-ticks direction to "ltr" and line alignment
- Fix .waterfall-header-ticks line alignment
- Fix .waterfall-marker has direction to "ltr"
