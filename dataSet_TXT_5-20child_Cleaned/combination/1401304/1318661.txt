The menu from dropdown buttons are not revealed while the Touch simulation tool is enabled
[Affected versions]:
- Firefox  (2016-11-16), Firefox  (2016-11-17)

[Affected platforms]:
- Windows  Mac OS X  Ubunutu  x86

[Steps to reproduce]:
Launch Firefox.
GO to a page that contains dropdown buttons. (Eg. 
Enable RDM.
Enable touch simulation.
Click on the dropdown buttons.

[Expected result]:
- The menu from the dropdown buttons is revealed.

[Actual result]:
- Nothing happens.

[Regression range]:
- This is not a regression.
- Firefox Nightly (2016-10-18) is the first build where the bug can be reproduce.

[Additional notes]:
- Note that the menu under the dropdown button is revealed if clicking multiple times on the button.
