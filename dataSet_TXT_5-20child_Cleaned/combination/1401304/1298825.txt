preventDefault doesn't work on simulated touch events
User Agent:  (X11; Fedora; Linux x86_64; rv:47.0)  
Build ID: 

Steps to reproduce:

Enabled responsive design mode
Enabled "simulate touch events"
Clicked an element that had this handler for both 'mousedown' and 'touchstart':

handler: function(e) {
e.preventDefault();
}


Actual results:

The handler fired twice, once for 'touchstart' and once for 'mousedown'.


Expected results:

The handler should just have fired once, for 'touchstart'.
