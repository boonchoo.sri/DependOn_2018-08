Favicon hard to see on the dark Blue Security button
The 'Dark Blue' on Larry is almost too dark and makes it difficult to see the Favicon.

Perhaps it could be lightened up a bit to improve visibility. Its been confirmed on IRC that its also difficult to see on XP.
