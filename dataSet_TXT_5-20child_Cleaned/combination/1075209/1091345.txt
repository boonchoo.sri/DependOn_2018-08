No tiles shown for "ja"
dynamis reports not seeing any tiles for a "ja" locale, but I'm guessing this was tested with a "ja-JP-mac" build.

Our published distributions only have  and  but not  nor 

I'm not sure if we want to allow for wildcardish locale matching in addition to STAR of geo. But at least for now, we'll just need to remember to duplicate any distributions for ja and ja-JP-mac.
