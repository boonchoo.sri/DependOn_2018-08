thumbnail service captures pages that have "Cache-Control: no-store" content
+++ This bug was initially created as a clone of Bug #822948 +++

The fix for bug  only looks for the Cache-Control: no-store directive for the main page. It doesn't consider content embedded on the page (images, iframed content, videos, etc.) that has the Cache-Control: no-store directive.

See also bug 
