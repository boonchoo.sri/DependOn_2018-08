dt-addon: support isInstalled in devtools-shim
In preparation for landing Bug  the DevToolsShim test should also be updated to work whether devtools are installed or not. This means using a stub for DevToolsShim isInstalled rather than relying on the real implementation which checks for the existence of the  path.
