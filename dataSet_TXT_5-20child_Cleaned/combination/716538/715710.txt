[New Tab Page] Black bars behind titles should be lowered in opacity from  to 
Created attachment 
Mockup: The change in opacity to thumbnail titles

In the current iteration of New Tab Page, the background of the thumbnails’ titles is black at  opacity. On colorful and dark thumbnails, this is not very visually distracting and blends nicely into the thumbnails. However, on mostly-white thumbnails, the  black bars appear create a stark horizontal stripe pattern. By reducing the opacity on those black bars from  to  the readability on the dark thumbnails isn’t very affected and the white thumbnails look better because there’s less of a stark contrast between the thumbnails and bars.
