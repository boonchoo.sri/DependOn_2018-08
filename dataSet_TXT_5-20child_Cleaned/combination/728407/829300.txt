Library window seems to cause some runtime leaks
I noticed some stuff from Library window in the CC graph.
No documents, but elements and js stuff.
Elements are from document 
and there are various DPV_* prefixed functions.

To analyze leaks one can create cc and gc logs

but it might be enough to just look at the cc graph in the browser.
Ugly, yet rather effective addon can be installed from Bug 
Restart after installation. Load about:cc and press 'Run cycle collector'.
You may need to run CC few times to get more stable graph.
Nowadays the base graph in FF which is just started and has only  tab is close
to  objects. After downloading something and opening and closing library window
I see some extra objects in the graph.
