List shortcuts in Menus (especially context menus)
(Note: this is filed as part of the “Paper Cut” bugs — we assume that there may be multiple existing bugs on this. Please make them block this bug, and we will de-dupe if they are indeed exactly the same. Thanks!)

To reproduce:
in Mac, go the the menu "Tools"
notice the shortcut for "Web Search" (Apple K)

Recommendation:
Implement more shortcuts in menus where the menu has a shortcut. This should especially happen in context menus.
