Vertical layout is not initialized correctly in newly opened devtools
Created attachment 
splitter_always_initialized_vertically.gif

Regressions from Bug 

STRs:

- open inspector
- resize the window so that inspector switches to vertical layout
- close devtools
- open devtools

ER: Inspector should use vertical layout
AR: Inspector is in horizontal layout, but switches to vertical layout as soon as the window is resized.
