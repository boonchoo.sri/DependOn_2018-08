Cannot log in to Facebook Messenger in Private Browsing mode
User Agent:  (Windows NT  rv:19.0)  
Build ID: 

Steps to reproduce:

Start Nightly on Private Browsing mood. Log on facebook.


Actual results:

Social API in not log in. It always shows Log in button. When I click the facebook page comes out. But the Social API messenger remains same.


Expected results:

It should log in, show me the  and friend list.
