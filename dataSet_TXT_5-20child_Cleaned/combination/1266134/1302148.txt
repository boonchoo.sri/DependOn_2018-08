Toolbox document leaks in many possible ways
I don't get why the toolbox is always reported as leaking during tests, but there is many places where we leak the toolbox documents from CommonJS Modules.
Modules outlive all documents. Module sandboxes are allocated once and stay alive until Firefox closes. So any reference from such module to any devtools document like toolbox or webconsole.xul, inspector.xul, ... should report a leak.

But it only happens when working on some random patches modifying slightly toolbox codepath. I was finally able to produce a little helper to highlight all these references and fix those for the leaks reported in bug 

Hopefully, fixing those will help us landing patches without having these misterious leaks. But I suspect I introduce another, higher level leak in bug  which introduce a toolbox leak...
