Invalid form popup should take into account rtl
Currently, invalid form popup points to the left of the element (maybe the right with RTL, I don't know). We should probably make it point to the middle to prevent potential issues with RTL and to prevent positions issues when the elements are completely restyled and have a padding like in this screenshot: 
