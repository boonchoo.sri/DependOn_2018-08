Context menu from spellchecker keeps growing
When testing the spellchecker from bug  I came across an issue with the context menu of textfields. It seems that old suggestions are not removed from the menu. After it has made suggestions they remain to be seen when right clicking on a totally different misspelling, or anywhere on the page.

A similar thing happens with the languages sub menu which has languages added to it each time the context menu is opened.

(Windows; U; Windows NT  en-US; rv:1.9a1)   ID:2005120522
