Devtools themes - Light theme issues
Created attachment 
Screenshot

Here's a list :
- sidemenu arrow is missing (I actually know why, it's because the file is named itemArrow-ltr.png while it should be itemArrow-ltr.svg, as it's an svg file)
- The addon command buttons are inverted
- The search icon is too light and nearly invisible
- Infobar is still dark

These are details and are less important :
- The tab active effect is horrible
- Some borders are too dark compared to the mockup
