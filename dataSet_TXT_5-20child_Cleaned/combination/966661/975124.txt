DevTools - Light Theme - Style Editor - seems like the 'jump to line' box uses the dark theme
User Agent:  (Macintosh; Intel Mac OS X  rv:27.0)   
Build ID: 

Steps to reproduce:

Navigate to some page that uses CSS
Open DevTools
Make sure the light theme is selected
Select Style Editor
In the document, right click and choose Jump to line...



Actual results:

The Goto line box seems to use the dark theme


Expected results:

The Goto line box should be match the light theme better.
