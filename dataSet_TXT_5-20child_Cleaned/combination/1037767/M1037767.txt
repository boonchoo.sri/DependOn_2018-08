Use straight nsIChannel instead of nsIIncrementalDownload for GMPInstallManager
rstrong mentioned in bug  that it would be better to use a straight channel from uri download instead of using incremental-downloader.

Since bug  is time critical, and it's currently working and well tested with incremental downloader, doing this work is being moved to this lower priority bug which won't be done for FF33.

If we end up obsoleting GMPInstallManager at some point, then this bug can be closed.

+++ This bug was initially created as a clone of Bug #1009816 +++

> Comment on attachment  [details] [diff] [review]
> Base implementation and tests for module to check, download, and install
> openh264 updates. rev1
>
> There has been some discussion of not using incremental download for app
> update and I think for this use case it likely makes sense to just download
> it.
