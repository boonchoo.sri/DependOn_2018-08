[Perf][Gslide]  ms) slower than Chrome when creating one slide for  pages slide on GSlide
# Test Case
STR
Open the browser
Open the specified Gslide document 
Wait for loading finished
Press "End" Key to the end page
Press "Enter" to create a new slide

# Hardware
OS: Windows 
CPU: i7-3770 
Memory:  Ram
Hard Drive:  SATA HDD
Graphics: GK107 [GeForce GT  GF108 [GeForce GT 

# Browsers
Firefox version: 
Chrome version: 

# Result
Browser | Run time (median value)
Firefox |  ms
Chrome |  ms
