[Perf][Gslide]  ms) slower than Chrome when reading a slide with table content on GSlide
+++ This bug was initially created as a clone of Bug #1315214 +++

# Test Case
STR
Open the browser
Open the specified Gslide document 
Wait for loading finished

# Hardware
OS: Windows 
CPU: i7-3770 
Memory:  Ram
Hard Drive:  SATA HDD
Graphics: GK107 [GeForce GT  GF108 [GeForce GT 

# Browsers
Firefox version: 
Chrome version: 

# Result
Browser | Run time (median value)
Firefox |  ms
Chrome |  ms
