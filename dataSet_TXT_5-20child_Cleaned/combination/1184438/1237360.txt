[userstory] Responsive Design Viewport
Base element of the responsive design view, a single viewport to handle the most common functions.

User Story:
* As a web developer I want to open my web site in a specified window size such that I can easily test my web site in sizes like mobile device screens and smaller desktop screens.

Acceptance Criteria:
* Viewport that resizes the current page being viewed
* Grippers to allow for custom vertical and horizontal sizing of the viewport
* Chooser for resizing the viewport to common device  screen sizes
* Landscape  Portrait button that rotates the viewport
* Touch events button to turn on simulation of mobile touch events
* Screenshot button for taking a screenshot of the current viewport
* Close button for existing the responsive design view
