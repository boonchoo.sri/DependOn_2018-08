[non-e10s] JSON view is unreliable on huge (~2Mb) JSON files
Created attachment 
screenshot  - [non-e10s] JSON view always returns error on huge JSON file.png

STR: (Win7_64, Nightly   ID  new profile)
Set pref devtools.jsonview.enabled -> true
Open this  JSON attachment from bug  in a new tab:
> 
Repeat this  times: click "reload" button or press Ctrl+R.

Result:
In non-e10s mode JSON viewer fails at Step  and fails  times in Step 
In e10s mode JSON viewer works OK at Step  and OK  in Step 

Expectations:
Non-e10s performance should be at least  (lol)

Note:
I tried to copy "Raw data" and compare it to original file .
JSON viewer appears to make mistakes like turning this fragment
> {"someInnocentName":"blah", "anotherName":1.234567}
into this one
> {"someInno:1.234567}
