Avoid showing the migration dialog if there are multiple profiles to import from and surface import post-startup
(In reply to Verdi [:verdi] from bug  comment #1)
> > - what profile to use. Both Chrome and  seem to support multiple user
> > profiles. I have no idea how to decide on which profile to use if there are
> > multiple. I suppose if we detect this situation we could go back to showing
> > the wizard, and we could auto-import if there's just the one? I doubt that
> > importing from all the profiles is a reasonable solution here, and the
> > "whole point" of this change is not to get in the user's  on startup, so
> > asking also seems wrong. The only other thing I can think of is trying to
> > work out which profile has the most data and basing it on that? Which seems
> > like a pretty rubbishy strategy for user profiles moreso than for which
> > browser to use (feels like multiple profiles has more user intent). I don't
> > see anything in the code that indicates which of many profiles would be the
> >  either. :-\
> >
>
> I think if there are multiple profiles we shouldn't try to guess or ask the
> user. We should skip importing and use a default data set (e.g. Alexa top
> sites). We can surface the "Import data from another browser" command on the
> new tab page to give people the option to manually figure out which profile
> they want to import from.

Because we don't have the new tab page state we need for this (still using about:home etc.) let's put this in a separate bug.
