The inspector sidebar is not displayed in portrait mode
Created attachment 
sidebar-disappear-in-portrait-mode.png

User Agent:  (Windows NT  WOW64; rv:50.0)  
Build ID: 

Steps to reproduce:

Start Nightly
Open DevTools > Inspector
Dock to side of Browser window
Resize devtools until portrait mode
Check sidebar and page toggle


Actual results:

The inspector sidebar is not displayed in portrait mode. And also the pane toggle does not work. You can expand sidebar manually by mouse dragging, but tab panel is blank.

Regression range:



Expected results:

The inspector sidebar should be displayed in portrait mode. And also the pane toggle works well.
