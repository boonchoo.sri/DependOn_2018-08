Don't overlap font-showall button and scrollbar in the Font Inspector
Created attachment 
do-not-overlap-font-showall-and-scrollbar.png

User Agent:  (Windows NT  WOW64; rv:50.0)  
Build ID: 

Steps to reproduce:

Start Nightly
Go to Getting Started 
Open DevTools > Inspector > Fonts
Click font-showall button


Actual results:

font-showall button and scrollbar are overlapped.

Regression range:



Expected results:

Don't overlap font-showall button and scrollbar.
