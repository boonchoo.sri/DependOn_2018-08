consolidate Firefox search preferences
In order to help address the problem that is search hijacking, and to make search engine selection more intuitive, I'd like to have location bar, context menu, and about:home searches use the same engine, and have that be controlled by a single pref (to be configured in the preferences dialog).

It's not clear exactly what the plan is yet, but this could mean:
- adding some prefs UI for "Default search engine" selection
- removing the possibility of override with keyword.URL (or perhaps just changing the pref name?)
- adding support for arbitrary engines to about:home
- fixing bug  to better support tracking multiple search origins
