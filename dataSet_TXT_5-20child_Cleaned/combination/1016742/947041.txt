Navbar top border color isn't consistent in fullscreen on OSX
Created attachment 
navbar_top_border.png

User Agent:  (Macintosh; Intel Mac OS X  rv:28.0)   
Build ID: 

Steps to reproduce:

I'm on  using latest nightly with australis theme, entered fullscreen


Actual results:

Top border of the navbar where the tabs attach changes color to a lighter grey than when the browser was windowed


Expected results:

The color should remain consistent. I actually think when in fullscreen the color blends into the tab border better. Note that the difference is pretty slight, but it's there. I've attached a picture showing the issue: on the left is in a normal window, the right is after it has been made fullscreen.

I haven't ever filed a bug before so please let me know if I need to include any other info. Thanks.
