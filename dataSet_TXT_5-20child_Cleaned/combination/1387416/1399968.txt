browser.search.widget.inNavBar isn't set correctly on startup
Bug  changed the browser.search.widget.inNavBar to default to false, and it also attempted to change it to be set correctly on start up according to its state in CustomizableUI.

However, looking into bug  comment  I've found that the pref is always set to false on startup, regardless of if the search box is in toolbar or not.
