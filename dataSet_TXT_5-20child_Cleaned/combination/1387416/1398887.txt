IME always turn off when Address Bar gets focus
After landing Bug 
When I want to search Japanese term, I have to use the Address bar.
However, When I move focus to Address bar, IME always turn off.
So I have to turn on IME every time.
It is very annoying for Japanese IME user.



Steps To Reproduce:
Make sure that MS-IME is default IME.
Launch Nightly
Type Japanese words in Address bar.
In this step I have to turn on IME.
--- maybe this is as expected if IME OFF
Focus input field on web page(incl. about:home or about:newtab)
--- IME is still ON as expected (if ime-mode is not specified)
Focus Address bar
--- IME is automatically turned off! --- This is a BUG


Actual Results;
IME always turned off when Address bar gets focus


Do not change the state of the IME
