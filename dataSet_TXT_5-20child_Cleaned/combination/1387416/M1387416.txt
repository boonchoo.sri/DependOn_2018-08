[photon] Move the search bar in the customization palette for new profiles
The Photon design spec calls for a unified location and search bar. We have to make similar changes as in bug  at least for the new profile case.

There are experiments underway to identify the right set of profiles to receive this experience, so the scope may change a bit soon.
