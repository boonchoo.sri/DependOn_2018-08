[Performance] Slow action when switching rapidly through tabs in Details panel
Created attachment 
screencast showing the issue.gif

[Affected versions]:
- latest Nightly 
- latest Aurora 

[Affected platforms]:
- Windows  x64
- Ubuntu  x64 LTS
- macOS 

[Steps to reproduce]:
Start Firefox.
Go to a website that has many requests e.g.:  or 
Open netmonitor. (Ctrl + Shift + Q) and select a request from the list.
Switch rapidly through Details panel tabs (Headers, Cookies, Params, Response etc. )

[Expected result]:
- Tabs are switched properly and no delay can be seen.

[Actual result]:
- When switching through tabs there is a bit of delay.

[Regression range]:
- This seems to be a regression in Fx  after bug  landed. I will follow-up with a regression range asap.

[Additional notes]:
- see the attached sceencast
