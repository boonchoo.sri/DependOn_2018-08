network monitor is extremely slow
User Agent:  (Macintosh; Intel Mac OS X  rv:55.0)  
Build ID: 

Steps to reproduce:

Open network monitor dev tool
visit or reload a page
interact with the network monitor area
- hover over various things
- open context menu
- change sort column


Actual results:

The network monitor was extremely slow to respond, and update. I'm guessing that your react code has duplicated a lot of DOM state management in js that the browser does for you in native code, you shouldn't do that. Duplicating state is poor practice and will lead to bugs in addition to slowness like this.


Expected results:

The network monitor should not be super slow.
