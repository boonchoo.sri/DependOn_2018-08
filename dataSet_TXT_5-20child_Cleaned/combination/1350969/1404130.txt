StatusBar updates too frequently  should cache l10n values

Search for "StatusBar"
Expand the first item in the Call Tree until seeing StatusBar.

You will see in the profile that StatusBar is slow because of its calls to L10N APIs getFormat* and getStr.


When you look at its implementation you can see many call to L10N.getStr for example.
At very least all L10N values should be cached. We call StatusBar() about once per request. We at least update the number of requests. But most of its content doesn't change...
