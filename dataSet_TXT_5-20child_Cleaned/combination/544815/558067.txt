F6 no longer focuses the location bar when tabs are on top
Pressing the F6 key no longer focuses the location bar when the tabbar is on top. Pressing the F6 now focuses the active tab and places a focus ring around its title.
