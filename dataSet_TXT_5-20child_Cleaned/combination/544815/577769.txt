Bookmarks toolbar should be above tabs with tabs on top
(X11; Linux i686; en-US; rv:2.0b2pre)  

With the new tabs on top option, the bookmarks toolbar is underneath the tabs and the URL bar. It shouldn't be. Instead, it should be below the menu bar (or whatever the equivalent on non-linux platforms) but above the tabs.

Reasoning:
From a conceptual standpoint, the bookmarks toolbar is not intimately tied to the page in the way the URL bar is, and should be associated by position more with the chrome than the page.
From a personal experience standpoint, even though I use it all the time I've been turning the toolbar off (and back on only when I need it) because it feels misplaced and in the way where it is now.
