-  tart  tresize  tsvgx (linux64, windows7-32, windows8-64) regression on push  (Fri Aug  
Talos has detected a Firefox performance regression from push  As author of one of the patches included in that push, we need your help to address this regression.

Summary of tests that regressed:

tresize windows8-64 opt:  ->  (15.83% worse)
tart summary linux64 opt e10s:  ->  (2.1% worse)
tart summary windows7-32 pgo e10s:  ->  (2.3% worse)
tsvgx summary windows8-64 opt:  ->  (4.09% worse)
tart summary windows8-64 opt:  ->  (3.53% worse)
cart summary windows8-64 opt:  ->  (3.94% worse)


You can find links to graphs and comparison views for each of the above tests at: 

On the page above you can see an alert for each affected platform as well as a link to a graph showing the history of scores for this test. There is also a link to a treeherder page showing the Talos jobs in a pushlog format.

To learn more about the regressing test(s), please see: 

For information on reproducing and debugging the regression, either on try or locally, see: 

*** Please let us know your plans within  business days, or the offending patch(es) will be backed out! ***

Our wiki page outlines the common responses and expectations: 
