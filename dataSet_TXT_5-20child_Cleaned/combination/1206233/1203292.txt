Replace permissions dropdown with 'x' icon in Permissions main view in Control Center
Based on the new UI, there will not be a dropdown next to non-default permissions anymore. Instead there will be an 'x' icon that reverts it back to default.

See the latest designs here  or the attached mockup for an example.
