[Regression] Clicking on audioVideoButton  sharing-icon don't work when typed on urlbar
I have a problem with Firefox Beta  It doesn't happen on Firefox ESR 
Sometimes when I click on permissions block floating at the top of the screen to open exact tab that has permission to use my  sometimes it opens popup with site permissions, and sometimes it doesn't.
It happens unpredictably, however, I noticed one specific scenario when it happens

Open  , allow to use the microphone
Select all text in location bar, type "."
Click on the right side of floating block with  permissions

Result: Firefox window blinks (looses focus, then gets focus)
Expected: Panel with site permissions should open

Floating block with  permissions is a small rectangle that appears at the top of the screen and contains of  halves: the left side displays Firefox logo, the right part displays white  icon on orange background
