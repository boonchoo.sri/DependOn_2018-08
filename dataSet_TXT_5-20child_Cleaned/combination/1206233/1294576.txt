[e10s] Red pulsing camera icon for subframe remains in address bar after main page refresh in Nightly
STR:
Open 
Observe grey camera icon in address bar and permission request.
Click "Share Selected Devices".
Observe pulsing red camera icon in address bar.
Refresh the page.

Expected result:
- Red pulsing camera icon disappears. Goto 

Actual result:
- Red pulsing camera icon remains. Goto  (i.e. both red and gray icons).

This misbehavior is a regression that appears to have arrived with the new pulsing look.
