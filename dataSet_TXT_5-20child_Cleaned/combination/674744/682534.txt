Implement conditional forward button for winstripe  large icons mode
Created attachment 
patch

Jared asked me to help him out with the conditional forward button affecting the horizontal position of other elements in the toolbar. Since I wasn't sure whether what I told him was sane, I implemented this myself. This also handles RTL, which AFAICT was broken with Jared's patch.
