Place default navbar items in the navbar to begin with
Created attachment 
Patch

A few of the default navbar widgets are currently located in the palette, which they then have to get moved to the navbar upon building the areas. We might be able to get better perf by putting them in the navbar to begin with.

I also had to fix a bug here where items that were in the overflow panel on window load weren't getting the location attributes. I don't necessarily like hardcoding "widget-overflow-list", so if you see a better way then please advise :)

Pushed to try, 
