Create a pref to  the Synced Tabs UI refresh
The pref services.sync.syncedTabsUIRefresh is set to true by default on Nightly and false for other releases.

When the pref is true:

> Synced Tabs toolbar menu item and sidebar

> - Toolbar item: Sync Now, Sidebar toggle
> - History menus: Expands Synced Tabs toolbar menu item, wherever it is
> - View›Sidebar menu: Synced Tabs

When the pref is false:

> has toolbar menu item and about:sync-tabs

> - Toolbar item: Sync Now; no Sidebar toggle
> - History menus: about:sync-tabs (Tabs From Other Devices)
> - View›Sidebar menu: No Synced Tabs
