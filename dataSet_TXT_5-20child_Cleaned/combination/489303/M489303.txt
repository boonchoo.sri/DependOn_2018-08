No resizer since the statusbar is gone



When the statusbar is disabled through the View menu, it becomes difficult to resize the window because of the small size of the window frame. This breaks Windows XP expected behavior.






When the statusbar is disabled and both scrollbars are active, a resizer gripper should appear in the otherwise wasted space of the "scrollcorner" element. When the statusbar is disabled and only one scrollbar is active then the resizer gripper should be inserted into the corner and the scrollbar should be nudged to make room for it. In Windows XP's file explorer, when the statusbar is disabled and both scrollbars are active, then a resizer does appear in the corner. Windows file explorer does not add the resizer if only one scrollbar is active, but Firefox should.
