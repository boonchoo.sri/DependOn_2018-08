messages are missing from the console log with the new front-end
For example, open the Web Console, and then load 

With devtools.webconsole.new-frontend-enabled = false, I see a bunch of CSS errors reported in the log, starting with lots of occurrences of:

Expected ‘none’, URL, or filter function but found ‘Alpha(’. Error in parsing value for ‘filter’. Declaration dropped.

etc.

However, with devtools.webconsole.new-frontend-enabled = true, which is now the default on Nightly, none of these messages appear. The console lists the GET requests as they're made, but none of the expected  show up.

This means that since bug  which preffed-on the new console front-end, Nightly has become un-useful for diagnosing site problems.
