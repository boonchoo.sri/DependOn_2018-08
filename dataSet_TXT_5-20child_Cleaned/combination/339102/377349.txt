Page info : Media Tab. The broken image indicator is often wrong
All the images that can't be found in the cache are grayed out in the tree view of the media tab. But images from the chrome scheme will never be in the cache. Also, if the cache is disabled, it doesn't work any more.

We need a better check to determine if an image is broken or not. For images (img tags) it would be better to check whether the represented dom element has imageloadingcontent but that wouldn't work for background images.
