Use separate files for AutoTimelineMarker and AutoGlobalTimelineMarker
At the moment they share the same header and implementation. In the future when we add additional auto RAII markers things will get messy.
