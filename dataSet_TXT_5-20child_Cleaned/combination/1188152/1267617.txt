Move notification anchors to the identity block
When a permission request is active, it should be anchored to the "i" icon and prevent the Control Center and other permission requests from being opened until the request has been dismissed.
