[meta] Asynchronous call stacks
Chrome  added support for linking the call stack of something async back up to the original call site that scheduled the async behavior (setTimeout, etc.).

Seems like a neat idea for us to consider too. A little more info is available on their post[1].

[1]: 
