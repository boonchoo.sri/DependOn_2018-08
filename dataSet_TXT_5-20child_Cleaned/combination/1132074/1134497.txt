Implement infopanels for in-product FTU Reading  tour
See UX mocks in bug  (currently "Reading List, First Time Use Experience" in the middle-right of attachment 

These are info panels to point out articles in the sidebar, the reading mode button, and sync. (The mock currently highlights Sync in the menu panel, but this may change to be a sync-promo blurb at the bottom of the sidebar).

We also probably want to combine the first infopanel with the "#3 Transition state" panel since they're so similar?
