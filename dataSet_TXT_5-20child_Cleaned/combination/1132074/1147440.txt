Improve the transition of the Add to Reading List URL Button
The Add to Reading List button in the URL bar appears jarring on hover. Let's smooth out the transition by adding in some minor easing.
