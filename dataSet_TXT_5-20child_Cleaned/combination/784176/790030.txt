Redirect about:home searches for "facebook.com" (etc) to facebook.com website, not Google search
Many users do not understand the difference between web search and the address bar.

If a user searches for "facebook.com" from about:home's search box, there is a VERY good chance they intended to visit  and not  For amusing evidence, please see:



Consider making about:home's searches that look like a URL or domain name like  load the facebook.com website, not Google search. Or, more conservatively, only redirect the Alexa Top  site names (for the user's locale).

This behavior might annoy technically-savvy users, but they are likely to change their home page or enter URLs in the Awesome Bar. Other users would be impressed that Firefox can now open Facebook without the intermediate Google step. :)
