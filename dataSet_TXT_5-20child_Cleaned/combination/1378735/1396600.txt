Prevent loading properties-db in parent process, or delay its load

loads

a  file

Today, we load this file in parent process for the frontend and also in the actors.
The copy loaded in the frontend shouldn't be loaded, ideally we would only use the one served from the actors, if possible.

While it is hard to untangle frontend code to always use the actor's version,
we can at least make some efforts to delay its load and prevent loading it at all in the parent process.

A profile without this patch:

in the parent process and  in the child process

Another profile with this patch:

in the child process

The important fact is that it disappeared from the parent process.
It is reported as slower with this patch, but I think it is mosly related to the lazy loading in the child process. It is no longer loaded during actors load, but later, and I imagine there is more things happenning in parallel and reports slower loading.
