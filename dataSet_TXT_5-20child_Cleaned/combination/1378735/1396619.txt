Lazy load inspector related actors
When opening the inspector for the first time, we load a lot of actors, which may not be all necessary.
Loading  (and all its dependencies) appears as the biggest chunk of JS computation in the child process:

