When temporary panels are destroyed, the ViewHide event may be dispatched on the wrong panelview
