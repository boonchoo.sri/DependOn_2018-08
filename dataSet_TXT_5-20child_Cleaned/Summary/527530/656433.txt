Disallow javascript: and data: URLs entered into the location bar from inheriting the principal of the currently-loaded page
