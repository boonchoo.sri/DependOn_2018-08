Perform initialization tasks from idle callbacks instead of random timeouts whenever possible
