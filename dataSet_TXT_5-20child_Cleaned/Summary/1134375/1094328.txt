[e10s] Detaching XUL tabs (about:newtab, about:preferences, about:addons) leaves a ghost tab in the old window, and opens a blank tab in the new window
