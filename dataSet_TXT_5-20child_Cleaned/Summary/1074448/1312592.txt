While paused in the middle of a JS event loop, the debugger allows other event loops to run. Can't trust that JavaScript execution is actually paused.
