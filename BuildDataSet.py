inputPath = 'dataSet-original-19062561/'
subPath = ['main/','child/']
outputPath1 = 'dataSet_XML_5child_Original/'
outputPath2 = 'dataSet_TXT_5child_Original/'
report = '_report/AmountOFDependon/'
setNumChild = 5

import glob
import os
from shutil import copy
import xml.etree.ElementTree as ET
import sys
sys.path.append('../BSLib/')
import createFolders as cFolder
import createFile as cFile
import xml2text as X2T

cFolder.createFolder2(report)

tmpMainPath = inputPath+subPath[1]
folderList = os.listdir(tmpMainPath)

of_report = open(report+'AmountOFDependon.csv','w')
of_report.write('Main,AmountChilds,Childs\n')

####------>>>>> List Main has == setNumChild
tmpMain = []
for folder in folderList:
    tmpPath_ = tmpMainPath+folder+'/'
    Child = glob.glob(tmpPath_+'*.xml')
    tmpCText = ''
    for text in Child:
        text_ = text.replace(tmpPath_,'')
        text_ = text_.replace('.xml','')
        if tmpCText == '':
            tmpCText = text_
        else:
            tmpCText = tmpCText + ' | '+ text_
    numChild = len(Child)
    tmpText = folder+','+str(numChild)+','+tmpCText+'\n'
    of_report.write(tmpText)
    if numChild == 5:
        tmpMain.append(folder)
of_report.close()

####------>>>>> Coppy File Main&Child in List
cFolder.createFolder2(outputPath1+subPath[0])

for mID in tmpMain:
    inputMPath = inputPath+subPath[0]+mID+'.xml'
    outputMPath = inputMPath.replace(inputPath,outputPath1)
    copy(inputMPath,outputMPath)
    
    tmpCPath = inputMPath.replace(subPath[0],subPath[1])
    tmpCPath = tmpCPath.replace('.xml','/')
    cPath = tmpCPath.replace(inputPath,outputPath1)#+subPath[1]+mID+'/'
    cFolder.createFolder2(cPath)
    for cFile in glob.glob(tmpCPath+'*.xml'):
        outputCPath = cFile.replace(inputPath,outputPath1)
        copy(cFile,outputCPath)
print ('>>>>> Complete!! <<<<<')

####------>>>>> Build TXT Dataset
mainPath = outputPath1+subPath[0]
childPath = outputPath1+subPath[1]
summaryPath = outputPath2+'Summary/'
combinationPath = outputPath2+'combination/'
X2T.xml2txt(mainPath,childPath,summaryPath,combinationPath)
print('>>>> Complete <<<<')