<bugID>1023488<Summary>Determine how we want to handle HiDPI theme implementation for Windows</Summary><Description>HighDPI (aka high pixels-per-inch) displays are becoming increasingly common. Perhaps most well-known are Apple's "Retina" displays, at ~220dpi (vs. ~96dpi for normal displays). HiDPI displays are becoming increasingly common on PC tablet, notebooks, and even external monitors. This leads to three interrelated issues:

  1) How should we specify/control HiDPI in the Firefox theme?
  2) What sizes/types of icons are needed?
  3) What should the asset workflow (UX designer --&gt; in tree --&gt; packaging --&gt;
     user's system) look like?

Our implementation of the OS X HiDPI theme (deps under meta bug 785330) largely relies on using CSS media queries to set style/images for Retina displays. A typical example looks like:

  #someButton {
    list-style-image: url(chrome://browser/skin/someButton.png);
  }

  @media (min-resolution: 2dppx) {
    #someButton {
      list-style-image: url(chrome://browser/skin/someButton@2x.png);
    }
  }

This works, but isn't really ideal.

* It's verbose and repetitive.
* It's prone to mistakes. Especially when the 1x/2x rules are separated (too
  easy to forget to update one) or -moz-image-region is involved. We've had
  a number of bugs where hidpi was implemented incorrectly as a result, from
  simply neglecting to provide a hidpi image to having giant/tiny misshapen UI.

The HTML srcset feature (bug 870021, http://www.w3.org/html/wg/drafts/srcset/w3c-srcset/) addresses these problems -- but only on &lt;img&gt; elements, which we rarely use in Firefox UI.

The biggest problem, however, is that this solution doesn't really scale (teehee) to what's needed on Windows. The situation with Apple's Retina hardware is simple -- things are either sized 100% (normal) or 200% (Retina). But Windows supports many additional scaling factors. Windows 7 supports scaling factors of 100%, 125%, and 150% (as well as an easily accessible "custom" setting for anything between 100% and 500%). Windows 8 adds an explicit 200% option. The guidelines for Windows Store apps (Metro/RT) recommend 100%, 140%, and 180%. (And then there are the with text-only scaling, which isn't relevant here.)

It's not really practical to extend the current OSX/Retina scheme to use bitmaps for so many different scaling factors. So it seems likely that we'll need to use a vector format (SVG) for image assets, so that the user isn't seeing a bitmap upscaled/downscaled for their particular DPI setting (which will often look blurry). We _could_ make a tradeoff decision to only support a couple of DPI settings and scale for things in between, but that seems like just kicking the can down the road.

Note that even with SVG we will still need to (sometimes) provide multiple image assets! When an image is displayed on screen with a small number of physical pixels (16x16 favicons being the canonical example), hand-tweaked bitmaps are usually required to look acceptable.

Finally, there's the asset workflow. Currently, all stages are the same -- for a particular icon, an UX designer provides the necessary sizes and flavors and those all go into the tree and are are all shipped in the respective platform builds. Depending on how we solve the other problems here, this may or may not need to change. For example, we could switch to pure SVG and basically do the same thing. Or we could have UX provide SVG assets, and convert them to bitmaps at some point (for checkin, for packaging, or at run-time on the user's system).</Description></bugID>